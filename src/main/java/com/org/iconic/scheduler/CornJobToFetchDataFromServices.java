package com.org.iconic.scheduler;

import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import com.org.iconic.dao.UnParsableDataDao;
import com.org.iconic.inputs.BseIndiaBlockCsv;
import com.org.iconic.inputs.BseIndiaBulkCsv;
import com.org.iconic.inputs.CSVHeader;
import com.org.iconic.inputs.NseIndiaBlockCsv;
import com.org.iconic.inputs.NseIndiaBulkCsv;
import com.org.iconic.logic.CsvReaderAsExcel;
import com.org.iconic.logic.InputTransactionsWriter;
import com.org.iconic.rest.client.model.RestClinetTransaction;

@Configuration
@EnableScheduling
@PropertySource("classpath:application.properties")
@Service("cronService")
public class CornJobToFetchDataFromServices {

	private final Logger LOGGER = Logger.getLogger(CornJobToFetchDataFromServices.class.getName());

	@Autowired
	CsvReaderAsExcel csvReaderAsExcel;

	@Autowired
	BseIndiaBulkCsv bseIndiaBulkCsv;
	
	@Autowired
	BseIndiaBlockCsv bseIndiaBlockCsv;
	
	@Autowired
	NseIndiaBulkCsv nseIndiaBulkCsv;
	
	@Autowired
	NseIndiaBlockCsv nseIndiaBlockCsv;
	
	@Autowired
	InputTransactionsWriter transactionsWriter;
	
	@Autowired
	UnParsableDataDao unParsableDataDao;

	@Value("${run_corn_job}")
	private boolean doRun;
	

	@Scheduled(cron = "${cron_expression}")
	public void schdulerJob() {
//		System.out.println("Started to get transaction");
//		LOGGER.log(Level.WARNING, "Started to get transaction");
//		if (doRun) {
//			LOGGER.log(Level.INFO, "Started to get transaction from BSE INDIA Bulk");
//			process(bseIndiaBulkCsv);
//			LOGGER.log(Level.INFO, "Completed to get transaction from BSE INDIA Bulk");
//			
//			LOGGER.log(Level.INFO, "Started to get transaction from BSE INDIA Block");
//			process(bseIndiaBlockCsv);
//			LOGGER.log(Level.INFO, "Completed to get transaction from BSE india Block");
//			
//			LOGGER.log(Level.INFO, "Started to get transaction from NSE INDIA Bulk");
//			process(nseIndiaBulkCsv);
//			LOGGER.log(Level.INFO, "Completed to get transaction from NSE INDIA Bulk");
//			
//			LOGGER.log(Level.INFO, "Started to get transaction from NSE INDIA Block");
//			process(nseIndiaBlockCsv);
//			LOGGER.log(Level.INFO, "Completed to get transaction from NSE INDIA Block");
//		}
//		LOGGER.log(Level.WARNING, "Completed to get transaction");
//		System.out.println("Started to end transaction");
	}

	private void process(CSVHeader csvHeader) {
		ArrayList<RestClinetTransaction> transactions = null;
		try {
			transactions = csvReaderAsExcel.readCsvFile(csvHeader);
		} catch (Exception e) {
			LOGGER.log(Level.SEVERE, "No transactions saved for " + csvHeader.getURL());
			return;
		}
		transactionsWriter.write(csvHeader.getServiceNumber(), transactions);
		
		//Write unparsable data if any
		if(!csvReaderAsExcel.getUnParsableList().isEmpty())
		{
			unParsableDataDao.save(csvReaderAsExcel.getUnParsableList());
		}
		
	}

}
