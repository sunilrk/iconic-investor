package com.org.iconic.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.org.iconic.entity.UnParsableData;

@Repository("unParsableDataDao")
public class UnParsableDataDaoImpl extends AbstractDao implements UnParsableDataDao {

	public void save(List<UnParsableData> dataList)
	{
		for (UnParsableData unParsableData : dataList) {
			save(unParsableData.buildEntity());
		}
	}
}
