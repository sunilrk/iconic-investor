package com.org.iconic.dao;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.springframework.stereotype.Repository;

import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.PreparedQuery;
import com.google.appengine.api.datastore.Query;
import com.google.appengine.api.datastore.Query.Filter;
import com.google.appengine.api.datastore.Query.FilterOperator;
import com.google.appengine.api.datastore.Query.FilterPredicate;
import com.org.iconic.entity.IconicInvestor;
import com.org.iconic.entity.enums.IconicInvestorEnum;

@Repository("iconicInvestorDao")
public class IconicInvestorDaoImpl extends AbstractDao implements IconicInvestorDao {

	private final Logger LOGGER = Logger.getLogger(IconicInvestorDaoImpl.class.getName());
	
	@Override
	public List<IconicInvestor> getAllInvestors() {
		List<IconicInvestor> investors = new ArrayList<>();
		Query query = new Query(IconicInvestor.TABLE_NAME);
		PreparedQuery results = getDataStoreService().prepare(query);

		for (Entity entity : results.asIterable()) {
			investors.add(new IconicInvestor(entity));
		}

		LOGGER.log(Level.INFO, "All investors count : " + investors.size());
		
		return investors;
	}

	@Override
	public List<IconicInvestor> getAllActiveInvestors() {
		List<IconicInvestor> investors = new ArrayList<>();

		Filter propertyFilter = new FilterPredicate(IconicInvestorEnum.ACTIVE.name(), FilterOperator.EQUAL, true);
		Query query = new Query(IconicInvestor.TABLE_NAME).setFilter(propertyFilter);
		PreparedQuery results = getDataStoreService().prepare(query);

		for (Entity entity : results.asIterable()) {
			investors.add(new IconicInvestor(entity));
		}

		return investors;

	}
}
