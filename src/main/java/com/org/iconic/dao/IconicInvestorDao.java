package com.org.iconic.dao;

import java.util.List;

import com.org.iconic.entity.IconicInvestor;

public interface IconicInvestorDao extends EntityDao {

	List<IconicInvestor> getAllInvestors();
	
	List<IconicInvestor> getAllActiveInvestors();

}
