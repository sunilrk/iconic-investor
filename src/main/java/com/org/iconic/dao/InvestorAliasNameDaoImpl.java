package com.org.iconic.dao;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.springframework.stereotype.Repository;

import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.PreparedQuery;
import com.google.appengine.api.datastore.Query;
import com.google.appengine.api.datastore.Query.CompositeFilterOperator;
import com.google.appengine.api.datastore.Query.Filter;
import com.google.appengine.api.datastore.Query.FilterOperator;
import com.google.appengine.api.datastore.Query.FilterPredicate;
import com.org.iconic.entity.InvestorAliasName;
import com.org.iconic.entity.enums.InvestorAliasNameEnum;

@Repository("investorAliasName")
public class InvestorAliasNameDaoImpl extends AbstractDao implements InvestorAliasNameDao {

	private final Logger LOGGER = Logger.getLogger(InvestorAliasNameDaoImpl.class.getName());

	@Override
	public List<InvestorAliasName> getAllAliasNames() {
		List<InvestorAliasName> investorsNames = new ArrayList<>();

		Query query = new Query(InvestorAliasName.TABLE_NAME);

		LOGGER.log(Level.INFO, "Query : " + query.toString());

		PreparedQuery results = getDataStoreService().prepare(query);

		for (Entity entity : results.asIterable()) {
			investorsNames.add(new InvestorAliasName(entity));
		}

		LOGGER.log(Level.INFO, "Result : " + investorsNames.size());

		return investorsNames;

	}

	@Override
	public InvestorAliasName saveIfNotFound(InvestorAliasName aliasName) {

		InvestorAliasName aliasNameFromDb = get(aliasName.getService(), aliasName.getNameFromService());

		if (aliasNameFromDb == null) {
			Key key = save(aliasName.buildEntity());
			if (key.isComplete()) {
				aliasName.setId(key.getId());
			}
			aliasNameFromDb = aliasName;
		}

		LOGGER.log(Level.INFO, "Result : " + aliasNameFromDb);

		return aliasNameFromDb;
	}

	@Override
	public List<InvestorAliasName> get(String name) {
		List<InvestorAliasName> investorsNames = new ArrayList<>();

		Filter nameFilter = new FilterPredicate(InvestorAliasNameEnum.NAME_FROM_SERVICE.name(), FilterOperator.EQUAL,
				name.toLowerCase().trim());
		Query query = new Query(InvestorAliasName.TABLE_NAME).setFilter(nameFilter);

		LOGGER.log(Level.INFO, "Query : " + query.toString());

		PreparedQuery results = getDataStoreService().prepare(query);

		for (Entity entity : results.asIterable()) {
			investorsNames.add(new InvestorAliasName(entity));
		}

		LOGGER.log(Level.INFO, "Result : " + investorsNames.size());

		return investorsNames;

	}

	@Override
	public List<InvestorAliasName> getAliasNamesList(Long investorId) {
		List<InvestorAliasName> investorsNames = new ArrayList<>();

		Filter investorIdFilter = new FilterPredicate(InvestorAliasNameEnum.ICONIC_INVESTOR_ID.name(),
				FilterOperator.EQUAL, investorId);
		Query query = new Query(InvestorAliasName.TABLE_NAME).setFilter(investorIdFilter);

		LOGGER.log(Level.INFO, "Query : " + query.toString());

		PreparedQuery results = getDataStoreService().prepare(query);

		for (Entity entity : results.asIterable()) {
			investorsNames.add(new InvestorAliasName(entity));
		}

		LOGGER.log(Level.INFO, "Result : " + investorsNames.size());

		return investorsNames;
	}

	@Override
	public InvestorAliasName get(String serviceType, String name) {

		InvestorAliasName returnObj = null;

		Filter nameFilter = new FilterPredicate(InvestorAliasNameEnum.NAME_FROM_SERVICE.name(), FilterOperator.EQUAL,
				name);
		Filter serviceFilter = new FilterPredicate(InvestorAliasNameEnum.REST_SERVICE.name(), FilterOperator.EQUAL,
				name);

		Filter combinedFilter = CompositeFilterOperator.and(nameFilter, serviceFilter);

		Query query = new Query(InvestorAliasName.TABLE_NAME).setFilter(combinedFilter);

		LOGGER.log(Level.INFO, "Query : " + query.toString());

		PreparedQuery results = getDataStoreService().prepare(query);
		
		Entity entity = results.asSingleEntity();

		if (entity != null) {
			returnObj = new InvestorAliasName(entity);
		}

		return returnObj;
	}
}
