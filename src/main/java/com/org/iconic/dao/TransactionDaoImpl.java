package com.org.iconic.dao;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.springframework.stereotype.Repository;

import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.FetchOptions;
import com.google.appengine.api.datastore.PreparedQuery;
import com.google.appengine.api.datastore.Query;
import com.google.appengine.api.datastore.Query.CompositeFilterOperator;
import com.google.appengine.api.datastore.Query.Filter;
import com.google.appengine.api.datastore.Query.FilterOperator;
import com.google.appengine.api.datastore.Query.FilterPredicate;
import com.org.iconic.entity.Transaction;
import com.org.iconic.entity.enums.TransactionEnum;
import com.org.iconic.rest.services.helper.RestInputForTransaction;

@Repository("transactionDao")
public class TransactionDaoImpl extends AbstractDao implements TransactionDao {

	private final Logger LOGGER = Logger.getLogger(TransactionDaoImpl.class.getName());

	@Override
	public List<Transaction> getTransaction(RestInputForTransaction input) {

		List<Transaction> transactions = new ArrayList<>();

		List<Filter> filters = new ArrayList<>();

		Filter dummy = new FilterPredicate(TransactionEnum.ICONIC_INVESTOR_ID.name(), FilterOperator.EQUAL,
				input.getInvestorId());
		filters.add(dummy);

		Filter investorIdFiler = new FilterPredicate(TransactionEnum.ICONIC_INVESTOR_ID.name(), FilterOperator.EQUAL,
				input.getInvestorId());
		filters.add(investorIdFiler);

		if (input.getFormatedFromDate() != null) {
			Filter fromDateFiler = new FilterPredicate(TransactionEnum.DATE.name(),
					FilterOperator.GREATER_THAN_OR_EQUAL, input.getFormatedFromDate());
			filters.add(fromDateFiler);
		}

		if (input.getFormatedToDate() != null) {
			Filter toDateFiler = new FilterPredicate(TransactionEnum.DATE.name(), FilterOperator.LESS_THAN_OR_EQUAL,
					input.getFormatedToDate());
			filters.add(toDateFiler);
		}

		if (input.getStockId() != null) {
			Filter stockFilter = new FilterPredicate(TransactionEnum.STOCK_ID.name(), FilterOperator.EQUAL,
					input.getStockId());
			filters.add(stockFilter);
		}

		Filter combinedFilter = CompositeFilterOperator.and(filters);

		Query query = new Query(Transaction.TABLE_NAME).setFilter(combinedFilter);

		query.addSort(TransactionEnum.DATE.name(), Query.SortDirection.DESCENDING);

		LOGGER.log(Level.INFO, "Query : " + query.toString());

		PreparedQuery results = getDataStoreService().prepare(query);

		// Check if limit of records provided or not
		if (input.getLastNTransaction() <= 0) {

			for (Entity entity : results.asIterable()) {
				transactions.add(new Transaction(entity));
			}

		} else {

			List<Entity> records = results.asList(FetchOptions.Builder.withLimit(input.getLastNTransaction()));

			for (Entity entity : records) {

				transactions.add(new Transaction(entity));
			}
		}

		LOGGER.log(Level.INFO, "Result : " + transactions.size());

		return transactions;

	}

	@Override
	public List<Transaction> getTransaction(Long investorAliasNameId, Date fromDate) {
		List<Transaction> transactions = new ArrayList<>();

		Filter investorIdFiler = new FilterPredicate(TransactionEnum.INVESTOR_ALIAS_NAME_ID.name(),
				FilterOperator.EQUAL, investorAliasNameId);
		Filter fromDateFiler = new FilterPredicate(TransactionEnum.DATE.name(), FilterOperator.GREATER_THAN_OR_EQUAL,
				fromDate);

		Filter combinedFilter = CompositeFilterOperator.and(investorIdFiler, fromDateFiler);

		Query query = new Query(Transaction.TABLE_NAME).setFilter(combinedFilter);

		LOGGER.log(Level.INFO, "Query : " + query.toString());

		PreparedQuery results = getDataStoreService().prepare(query);

		for (Entity entity : results.asIterable()) {
			transactions.add(new Transaction(entity));
		}

		LOGGER.log(Level.INFO, "Result : " + transactions.size());

		return transactions;
	}

	@Override
	public List<Transaction> getTransaction(Long investorId, Long stockId, int noOfTransactions) {
		List<Transaction> transactions = new ArrayList<>();

		Filter investorIdFiler = new FilterPredicate(TransactionEnum.ICONIC_INVESTOR_ID.name(), FilterOperator.EQUAL,
				investorId);

		Filter stockIdFiler = new FilterPredicate(TransactionEnum.STOCK_ID.name(), FilterOperator.EQUAL, stockId);

		Filter combinedFilter = CompositeFilterOperator.and(investorIdFiler, stockIdFiler);

		Query query = new Query(Transaction.TABLE_NAME).setFilter(combinedFilter);
		query.addSort(TransactionEnum.DATE.name(), Query.SortDirection.DESCENDING);

		LOGGER.log(Level.INFO, "Query : " + query.toString());

		PreparedQuery results = getDataStoreService().prepare(query);

		List<Entity> records = results.asList(FetchOptions.Builder.withLimit(noOfTransactions));

		for (Entity entity : records) {

			transactions.add(new Transaction(entity));
		}

		LOGGER.log(Level.INFO, "Result : " + transactions.size());

		return transactions;
	}

	@Override
	public List<Transaction> getTransaction(String sourceStockCode, boolean onlyNullStockIds) {
		List<Transaction> transactions = new ArrayList<>();

		Filter stockFiler = new FilterPredicate(TransactionEnum.SRC_STOCK_CODE.name(), FilterOperator.EQUAL,
				sourceStockCode);
		Query query = null;
		if (onlyNullStockIds) {
			Filter nullStockFiler = new FilterPredicate(TransactionEnum.SRC_STOCK_CODE.name(), FilterOperator.EQUAL,
					null);
			Filter combinedFilter = CompositeFilterOperator.and(stockFiler, nullStockFiler);
			query = new Query(Transaction.TABLE_NAME).setFilter(combinedFilter);

		} else {
			query = new Query(Transaction.TABLE_NAME).setFilter(stockFiler);
		}

		LOGGER.log(Level.INFO, "Query : " + query.toString());

		PreparedQuery results = getDataStoreService().prepare(query);

		for (Entity entity : results.asIterable()) {
			transactions.add(new Transaction(entity));
		}

		LOGGER.log(Level.INFO, "Result : " + transactions.size());

		return transactions;
	}

}
