package com.org.iconic.dao;

import java.util.Date;
import java.util.List;

import com.org.iconic.entity.Transaction;
import com.org.iconic.rest.services.helper.RestInputForTransaction;

public interface TransactionDao extends EntityDao {

	/**
	 * Returns list of transactions for given investor to toDate
	 * 
	 * @param RestInputForTransaction
	 * @return
	 */
	List<Transaction> getTransaction(RestInputForTransaction input);

	/**
	 * Returns list of transactions for given investor alias name id (not
	 * investor id) form given fromDate to till today
	 * 
	 * @param investorAliasNameId
	 * @param fromDate
	 * @return
	 */
	List<Transaction> getTransaction(Long investorAliasNameId, Date fromDate);

	/**
	 * 
	 * Get n number of transactions for given investor id, stock id
	 * 
	 * @param investorId
	 * @param stockId
	 * @param noOfTransactions
	 * @return
	 */
	List<Transaction> getTransaction(Long investorId, Long stockId, int noOfTransactions);

	/**
	 * Returns list of transactions for given source stock code
	 * 
	 * @param sourceStockCode
	 *            - stock code which comes from NSE or BSE
	 * @param onlyNullStockIds
	 *            true - get transaction that don't have stock id (stock id are
	 *            not if not found in stock master table) 
	 *            
	 *            false - get all
	 *            transactions
	 * @return
	 */
	List<Transaction> getTransaction(String sourceStockCode, boolean onlyNullStockIds);
}
