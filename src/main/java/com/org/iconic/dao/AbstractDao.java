package com.org.iconic.dao;

import java.util.Collection;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.Key;


public abstract class AbstractDao {

	private final Logger LOGGER = Logger.getLogger(AbstractDao.class.getName());
	
	private final static DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
	
	protected DatastoreService getDataStoreService() {
		return datastore;
	}

	/**
	 * Store a single entity to DataStore
	 * 
	 * @param entity
	 */
	public Key save(Entity entity) {
		Key key = getDataStoreService().put(entity);
		
		if (key == null || !key.isComplete())
		{
			LOGGER.log(Level.SEVERE, "Unable to save entity : " + entity.toString());
		}
		else
		{
			LOGGER.log(Level.INFO, "Entity Saved : " + key.getId());
		}
		
		return key;
	}
	
	/**
	 * Store a list of entities to DataStore
	 * 
	 * @param entities
	 */
	public List<Key> save(Collection<Entity> entities) {
		List<Key> keys = getDataStoreService().put(entities);
		
		boolean allSaved = true;
		
		for (Key key : keys) {
			if (!key.isComplete())
			{
				allSaved  = false;
			}
		}
		if (allSaved)
		{
			LOGGER.log(Level.INFO, "All Entities Saved  ");
			
		}
		else
		{
			LOGGER.log(Level.SEVERE, "Unable to save entity one or more entities ");
		}
		
		return keys;
	}
}
