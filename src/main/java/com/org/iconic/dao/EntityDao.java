package com.org.iconic.dao;

import java.util.Collection;
import java.util.List;

import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.Key;

public interface EntityDao {
	
	Key save(Entity entity);
	
	List<Key> save(Collection<Entity> entities);
}
