package com.org.iconic.dao;

import java.util.List;

import com.org.iconic.entity.UnParsableData;

public interface UnParsableDataDao extends EntityDao {
	void save(List<UnParsableData> dataList);
}
