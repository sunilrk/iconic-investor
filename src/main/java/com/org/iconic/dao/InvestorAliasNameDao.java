package com.org.iconic.dao;

import java.util.List;

import com.org.iconic.entity.InvestorAliasName;

public interface InvestorAliasNameDao extends EntityDao {

	/**
	 * Get All Aliasname form the table
	 * @return
	 */
	List<InvestorAliasName> getAllAliasNames();
	
	/**
	 * Get All Aliasname for an investor
	 * @return
	 */
	List<InvestorAliasName> getAliasNamesList(Long investorId);
	
	/**
	 * Save an Aliasname object to db if not found
	 * @param aliasName
	 * @return
	 */
	InvestorAliasName saveIfNotFound(InvestorAliasName aliasName);
	
	/**
	 * Get list of all Aliasname objects for given matching name 
	 * @param name
	 * @return
	 */
	List<InvestorAliasName> get(String name);

	/**
	 * Get a investor alias name for given sergice type and name
	 * @param serviceType
	 * @param name
	 * @return
	 */
	InvestorAliasName get(String serviceType, String name);
}
