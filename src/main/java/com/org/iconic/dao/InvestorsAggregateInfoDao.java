package com.org.iconic.dao;

import java.util.List;

import com.org.iconic.entity.MasterAggregate;
import com.org.iconic.entity.MonthAggregates;

public interface InvestorsAggregateInfoDao extends EntityDao {

	/**
	 * Get aggregation details for month wise from the table IC_STOCK_INVESTOR_STOCKS
	 * @param investorId
	 * @param monthYear
	 * @return
	 */
	List<MonthAggregates> getMonthAggregates(long investorId, String monthYear);
	
	/**
	 * Get aggregation details for month wise from the table IC_STOCK_INVESTOR_STOCKS
	 * @param investorId
	 * @param monthYear
	 * @param stockId
	 * @return
	 */
	List<MonthAggregates> getMonthAggregates(long investorId, String monthYear, long stockId);
	
	/**
	 * Get aggregation details from table IC_MASTER_STOCK_INVESTOR_AGGREGATES
	 * @param investorId
	 * @return
	 */
	List<MasterAggregate> getMasterAggregates(long investorId);
	
	/**
	 * Get aggregation details from table IC_MASTER_STOCK_INVESTOR_AGGREGATES
	 * @param investorId
	 * @param stockId
	 * @return
	 */
	List<MasterAggregate> getMasterAggregates(long investorId, long stockId);
}
