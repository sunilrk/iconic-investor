package com.org.iconic.dao;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.springframework.stereotype.Repository;

import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.PreparedQuery;
import com.google.appengine.api.datastore.Query;
import com.google.appengine.api.datastore.Query.CompositeFilterOperator;
import com.google.appengine.api.datastore.Query.Filter;
import com.google.appengine.api.datastore.Query.FilterOperator;
import com.google.appengine.api.datastore.Query.FilterPredicate;
import com.org.iconic.entity.MasterAggregate;
import com.org.iconic.entity.MonthAggregates;
import com.org.iconic.entity.enums.AggregateInfoEnum;
import com.org.iconic.utils.DefaultConstants;

@Repository("investorsAggregateInfoDaoImpl")
public class InvestorsAggregateInfoDaoImpl extends AbstractDao implements InvestorsAggregateInfoDao {

	private final Logger LOGGER = Logger.getLogger(InvestorsAggregateInfoDaoImpl.class.getName());

	@Override
	public List<MonthAggregates> getMonthAggregates(long investorId, String monthYear) {
		return getMonthAggregates(investorId, monthYear, DefaultConstants.DUMMY_ID);
	}

	@Override
	public List<MonthAggregates> getMonthAggregates(long investorId, String monthYear, long stockId) {

		List<MonthAggregates> aggInfos =  new ArrayList<>();

		Filter investorIdFiler = new FilterPredicate(AggregateInfoEnum.ICONIC_INVESTOR_ID.name(), FilterOperator.EQUAL,
				investorId);
		Filter monthYearFiler = new FilterPredicate(AggregateInfoEnum.MONTH_YEAR.name(), FilterOperator.EQUAL,
				monthYear);

		Filter combinedFilter = CompositeFilterOperator.and(investorIdFiler, monthYearFiler);

		if (stockId != DefaultConstants.DUMMY_ID.longValue()) {
			Filter stockFiler = new FilterPredicate(AggregateInfoEnum.STOCK_ID.name(), FilterOperator.EQUAL, stockId);
			combinedFilter = CompositeFilterOperator.and(investorIdFiler, monthYearFiler, stockFiler);
		}

		Query query = new Query(MonthAggregates.TABLE_NAME).setFilter(combinedFilter);

		LOGGER.log(Level.INFO, "Query : "+query.toString());
		
		PreparedQuery results = getDataStoreService().prepare(query);

		for (Entity entity : results.asIterable()) {

			aggInfos.add(new MonthAggregates(entity));
		}

		LOGGER.log(Level.INFO, "Found month aggreate  "+aggInfos.size());
		
		return aggInfos;
	}

	@Override
	public List<MasterAggregate> getMasterAggregates(long investorId) {
		return getMasterAggregates(investorId, DefaultConstants.DUMMY_ID);
	}

	@Override
	public List<MasterAggregate> getMasterAggregates(long investorId, long stockId) {
		List<MasterAggregate> aggInfos = new ArrayList<>();

		Query query = null;

		Filter investorIdFiler = new FilterPredicate(AggregateInfoEnum.ICONIC_INVESTOR_ID.name(), FilterOperator.EQUAL,
				investorId);

		if (stockId != DefaultConstants.DUMMY_ID.longValue()) {
			Filter stockFiler = new FilterPredicate(AggregateInfoEnum.STOCK_ID.name(), FilterOperator.EQUAL, stockId);
			Filter combinedFilter = CompositeFilterOperator.and(investorIdFiler, stockFiler);
			query = new Query(MasterAggregate.TABLE_NAME).setFilter(combinedFilter);

		} else {
			query = new Query(MasterAggregate.TABLE_NAME).setFilter(investorIdFiler);
		}

		LOGGER.log(Level.INFO, "Query : "+query.toString());
		
		System.out.println("Query : "+query.toString());

		PreparedQuery results = getDataStoreService().prepare(query);

		for (Entity entity : results.asIterable()) {

			aggInfos.add(new MasterAggregate(entity));
		}

		LOGGER.log(Level.INFO, "Found master aggreate  "+aggInfos.size());
		
		System.out.println("Found master aggreate  "+aggInfos.size());

		return aggInfos;
	}

}
