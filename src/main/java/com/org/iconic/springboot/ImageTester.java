/**
 * Copyright 2017 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.org.iconic.springboot;

import java.awt.image.BufferedImage;
import java.io.BufferedOutputStream;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.net.URL;
import java.util.Base64;

import javax.imageio.ImageIO;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.org.iconic.logic.GcpImageUploader;

@RestController
public class ImageTester {

	@Autowired
	GcpImageUploader createBlob;
	
	@RequestMapping(value = "/imagetester", method = RequestMethod.POST)
	public String investorProfile(@RequestBody ImageHelper imgHelper) {
		File image = decodeToImage(imgHelper.getImageData());
		//createBlob.createImageBlob("my_first_imag.png", image);
		return imgHelper.getImageData();
	}
	
	@RequestMapping(value="/upload", method=RequestMethod.POST)
    public @ResponseBody String handleFileUpload( 
            @RequestParam("imgfile") MultipartFile imgfile,
            @RequestParam("displayName") String displayName,
            @RequestParam("active") boolean active,
            @RequestParam("socialAccounts") String socialAccounts,
            @RequestParam("emailId") String emailId,
            @RequestParam("aboutInvestor") String aboutInvestor){
            String name = "test11";
        if (!imgfile.isEmpty()) {
            try {
            	createBlob.createImageBlob(displayName, imgfile.getInputStream());
                byte[] bytes = imgfile.getBytes();
                BufferedOutputStream stream = 
                        new BufferedOutputStream(new FileOutputStream(new File("D:/MyApplication/IconicInvestorCloud/uploaded.png")));
                stream.write(bytes);
                stream.close();
                return "You successfully uploaded " + displayName + " into " + name + "-uploaded !";
            } catch (Exception e) {
                return "You failed to upload " + displayName + " => " + e.getMessage();
            }
        } else {
            return "You failed to upload " + displayName + " because the file was empty.";
        }
    }
	
	private File decodeToImage(String imageString) {
		 
        BufferedImage image = null;
        String filePath = null;
        File outputfile = null;
        byte[] imageByte;
        try {
            imageByte =  Base64.getDecoder().decode(imageString);
            ByteArrayInputStream bis = new ByteArrayInputStream(imageByte);
            image = ImageIO.read(bis);
            bis.close();
            URL pathUrl = this.getClass().getClassLoader().getResource("images");
            filePath = pathUrl.getPath()+File.separator+"test.png";
            outputfile = new File(filePath);
            ImageIO.write(image, "png", outputfile);
            
        } catch (Exception e) {
            e.printStackTrace();
        }
        return outputfile;
    }
}
