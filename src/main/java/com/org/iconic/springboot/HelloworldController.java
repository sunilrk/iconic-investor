/**
 * Copyright 2017 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.org.iconic.springboot;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.org.iconic.entity.MonthAggregates;
import com.org.iconic.entity.IconicInvestor;
import com.org.iconic.inputs.BseIndiaBlockCsv;
import com.org.iconic.inputs.BseIndiaBulkCsv;
import com.org.iconic.inputs.NseIndiaBlockCsv;
import com.org.iconic.inputs.NseIndiaBulkCsv;
import com.org.iconic.logic.CsvReaderAsExcel;
import com.org.iconic.logic.GcpImageUploader;
import com.org.iconic.logic.InputTransactionsWriter;
import com.org.iconic.rest.client.model.RestClinetTransaction;
import com.org.iconic.scheduler.CornJobToFetchDataFromServices;
import com.org.iconic.service.IconicInvestorService;
import com.org.iconic.service.InvestorsAggregateService;
import com.org.iconic.service.StockService;

@RestController
public class HelloworldController {

	@Autowired
	IconicInvestorService icService;

	@Autowired
	InputTransactionsWriter grabTransactionScheduler;

	@Autowired
	InvestorsAggregateService investorsAggregateService;

	@Autowired
	CsvReaderAsExcel csvReaderAsExcel;

	@Autowired
	BseIndiaBulkCsv baseIndiaCsvHeaders;
	
	@Autowired
	BseIndiaBlockCsv baseIndiaBlock;
	
	@Autowired
	NseIndiaBulkCsv nseIndiaCsvHeaders;
	
	@Autowired
	NseIndiaBlockCsv nseIndiaBlock;
	
	@Autowired
	StockService stockService;
	
	@Autowired
	CornJobToFetchDataFromServices cornService;
	
	@Autowired
	GcpImageUploader createBlob;
	
	@GetMapping("/sayhello")
	public String hello() throws Exception {
		// saveIconicInvestor();
		// saveAggregateInfos();
		//ArrayList<RestClinetTransaction> transactions = csvReaderAsExcel.readCsvFile(baseIndiaCsvHeaders);
		//transactions = csvReaderAsExcel.readCsvFile(baseIndiaBlock);
		//transactions = csvReaderAsExcel.readCsvFile(nseIndiaCsvHeaders);
		//transactions = csvReaderAsExcel.readCsvFile(nseIndiaBlock);
		//grabTransactionScheduler.write(baseIndiaCsvHeaders.getServiceNumber(), transactions);
		//stockService.getStockId("PVR", true);
		cornService.schdulerJob();
		//createBlob.create("test_blob", "I am writing to storage buucket");
		return "Hello world - springboot-appengine-standard!";
	}

	private void saveIconicInvestor() {
		List<IconicInvestor> ics = new ArrayList<>();
		IconicInvestor ic = new IconicInvestor();
		ic.setId(1L);
		ic.setDisplayName("TestName1");
		ic.setAboutInvestor("bla bla bla 1");
		ic.setEmailId("test@test.com");
		ic.setLogoImageUrl("http://test/test.img");
		ic.setSocialAccounts("@test");
		ics.add(ic);
		ic = new IconicInvestor();

		ic.setId(2L);
		ic.setDisplayName("TestName2");
		ic.setAboutInvestor("bla bla bla 2");
		ic.setEmailId("test@test.com");
		ic.setLogoImageUrl("http://test/test.img");
		ic.setSocialAccounts("@test");
		ics.add(ic);
		ic = new IconicInvestor();

		ic.setId(3L);
		ic.setDisplayName("TestName3");
		ic.setAboutInvestor("bla bla bla");
		ic.setEmailId("test@test.com");
		ic.setLogoImageUrl("http://test/test.img");
		ic.setSocialAccounts("@test");
		ics.add(ic);
		ic = new IconicInvestor();

		ic.setId(4L);
		ic.setDisplayName("TestName4");
		ic.setAboutInvestor("bla bla bla");
		ic.setEmailId("test@test.com");
		ic.setLogoImageUrl("http://test/test.img");
		ic.setSocialAccounts("@test");
		ics.add(ic);
		ic = new IconicInvestor();

		ic.setId(5L);
		ic.setDisplayName("TestName5");
		ic.setAboutInvestor("bla bla bla");
		ic.setEmailId("test@test.com");
		ic.setLogoImageUrl("http://test/test.img");
		ic.setSocialAccounts("@test");
		ic.setActive(false);
		ics.add(ic);
		ic = new IconicInvestor();

		ic.setId(6L);
		ic.setDisplayName("TestName6");
		ic.setAboutInvestor("bla bla bla");
		ic.setEmailId("test@test.com");
		ic.setLogoImageUrl("http://test/test.img");
		ic.setSocialAccounts("@test");
		ics.add(ic);

		icService.saveIconicInvestors(ics);

		// grabTransactionScheduler.saveTransaction();
	}

	/*private void saveAggregateInfos() {
		MonthAggregates info = new MonthAggregates();
		info.setAggMonth("FEB_2018");
		info.setAggPrice(208098.98);
		info.setTotalUnits(787898L);
		info.setIconicInvestorId(4785074604081152L);
		info.setStockId(348L);

		investorsAggregateService.saveMonthAggregate(info);

		info = new MonthAggregates();
		info.setAggMonth("FEB_2018");
		info.setAggPrice(308098.98);
		info.setTotalUnits(587898L);
		info.setIconicInvestorId(4785074604081152L);
		info.setStockId(948L);

		investorsAggregateService.saveMonthAggregate(info);

		info = new MonthAggregates();
		info.setAggMonth("FEB_2018");
		info.setAggPrice(508098.98);
		info.setTotalUnits(77898L);
		info.setIconicInvestorId(4785074604081152L);
		info.setStockId(398L);

		investorsAggregateService.saveMonthAggregate(info);

		info = new MonthAggregates();
		info.setAggMonth("JAN_2018");
		info.setAggPrice(208098.98);
		info.setTotalUnits(787898L);
		info.setIconicInvestorId(4785074604081152L);
		info.setStockId(348L);

		investorsAggregateService.saveMonthAggregate(info);

		info = new MonthAggregates();
		info.setAggMonth("JAN_2018");
		info.setAggPrice(308098.98);
		info.setTotalUnits(587898L);
		info.setIconicInvestorId(4785074604081152L);
		info.setStockId(948L);

		investorsAggregateService.saveMonthAggregate(info);

		info = new MonthAggregates();
		info.setAggMonth("JAN_2018");
		info.setAggPrice(508098.98);
		info.setTotalUnits(77898L);
		info.setIconicInvestorId(4785074604081152L);
		info.setStockId(398L);

		investorsAggregateService.saveMonthAggregate(info);

		info = new MonthAggregates();
		info.setAggMonth("DEC_2017");
		info.setAggPrice(208098.98);
		info.setTotalUnits(787898L);
		info.setIconicInvestorId(4785074604081152L);
		info.setStockId(348L);

		investorsAggregateService.saveMonthAggregate(info);

		info = new MonthAggregates();
		info.setAggMonth("DEC_2017");
		info.setAggPrice(308098.98);
		info.setTotalUnits(587898L);
		info.setIconicInvestorId(4785074604081152L);
		info.setStockId(948L);

		investorsAggregateService.saveMonthAggregate(info);

		info = new MonthAggregates();
		info.setAggMonth("DEC_2017");
		info.setAggPrice(508098.98);
		info.setTotalUnits(77898L);
		info.setIconicInvestorId(4785074604081152L);
		info.setStockId(398L);

		investorsAggregateService.saveMonthAggregate(info);
	}*/
}
