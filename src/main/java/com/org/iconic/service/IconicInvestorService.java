package com.org.iconic.service;

import java.util.List;

import com.google.appengine.api.datastore.Key;
import com.org.iconic.entity.IconicInvestor;
import com.org.iconic.ui.model.UiIconicInvestor;

public interface IconicInvestorService {

	/**
	 * Returns all active iconic investors
	 * @return List<IconicInvestor> 
	 */
	List<IconicInvestor> getAllActiveInvestors();
	
	/**
	 * Returns all iconic investors including active and not active
	 * @return List<UiIconicInvestor> 
	 */
	List<UiIconicInvestor> getInvestorsList(boolean onlyActive);
	
	/**
	 * Save an IconicInvestor to Database
	 * @param entity
	 */
	Key saveIconicInvestor(IconicInvestor entity);
	
	/**
	 * Save list of IconicInvestor to Database
	 * @param List<IconicInvestor> 
	 */
	void saveIconicInvestors(List<IconicInvestor> entities);
	
	/**
	 * Returns Investor information for given id
	 * 
	 * @param investorId
	 * @return
	 */
	IconicInvestor getInvestorProfile(Long investorId);
	
	/**
	 * Returns Investor information for given id
	 * 
	 * @param investorId
	 * @return
	 */
	UiIconicInvestor getUiIconicInvestor(Long investorId);
	
	
	/**
	 * Returns Investor information by name
	 * 
	 * @param name
	 * @return
	 */
	IconicInvestor getInvestorProfileByName(String name);
	
	/**
	 * activate or deactivate profile for given id
	 * 
	 * @param investorId
	 * @return
	 */
	boolean changeProfileStatus(Long investorId, boolean status);
	
}
