package com.org.iconic.service;

import java.util.List;

import com.org.iconic.entity.Transaction;
import com.org.iconic.ui.model.UiAggregates;
import com.org.iconic.ui.model.UiMonthAggreates;

public interface InvestorsAggregateService {


	/**
	 * Returns List of Month wise aggregate details for given 
	 * @param investorId - investor id
	 * @param pastNoOfMonths - integer no.of past months
	 * @return
	 */
	List<? extends UiAggregates> getAggregatedDetails(Long investorId, int pastNoOfMonths, String profileView);
		
	/**
	 * It finds all matching transactions for the active investors 
	 * and aggregates it to month wise and total aggregates
	 * @param transactions
	 */
	void aggreateTransactions(List<Transaction> transactions);
	
	
}
