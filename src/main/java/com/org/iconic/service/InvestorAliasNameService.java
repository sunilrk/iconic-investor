package com.org.iconic.service;

import java.util.List;

import com.google.appengine.api.datastore.Key;
import com.org.iconic.entity.InvestorAliasName;


public interface InvestorAliasNameService {
	
	static final Long REST_SERVICE_TWO = 2L;
	
	InvestorAliasName findInvestorNameInService(String restSerivce, String name);
	Long saveAliasName(InvestorAliasName entity);
	List<InvestorAliasName> updateInvestorId(List<String> aliasNames, Long investorId, StringBuilder statusMsg);
	List<InvestorAliasName> getAliasNamesList(Long investorId);

	/**
	 * Get All Aliasname form the table
	 * @return
	 */
	List<InvestorAliasName> getAllAliasNames();
}
