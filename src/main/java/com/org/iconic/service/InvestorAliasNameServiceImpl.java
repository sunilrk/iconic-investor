package com.org.iconic.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;

import com.google.appengine.api.datastore.Key;
import com.org.iconic.dao.InvestorAliasNameDao;
import com.org.iconic.entity.InvestorAliasName;
import com.org.iconic.utils.DefaultConstants;

@Service("investorAliasNameService")
@PropertySource("classpath:application.properties")
public class InvestorAliasNameServiceImpl implements InvestorAliasNameService {

	private final Logger LOGGER = Logger.getLogger(InvestorAliasNameServiceImpl.class.getName());

	@Value("${enable_cache}")
	private boolean cacheEnabled;

	@Autowired
	private InvestorAliasNameDao dao;

	private HashMap<String, HashMap<String, InvestorAliasName>> serviceViseAliasNames = new HashMap<>();

	@PostConstruct
	public void init() {
		loadAllInvestorNameForSerivice();
	}

	private void loadAllInvestorNameForSerivice() {

		serviceViseAliasNames.clear();

		List<InvestorAliasName> aliasNamesList = dao.getAllAliasNames();

		if (aliasNamesList != null && !aliasNamesList.isEmpty()) {
			for (InvestorAliasName aliasName : aliasNamesList) {

				addToCache(aliasName);
			}
		}
	}

	private void addToCache(InvestorAliasName aliasName) {

		if (cacheEnabled) {

			HashMap<String, InvestorAliasName> mapForAService = serviceViseAliasNames.get(aliasName.getService());
			if (mapForAService == null) {
				mapForAService = new HashMap<String, InvestorAliasName>();
				serviceViseAliasNames.put(aliasName.getService(), mapForAService);
			}

			mapForAService.put(aliasName.getNameFromService(), aliasName);
		}
	}

	@Override
	public Long saveAliasName(InvestorAliasName entity) {
		entity = dao.saveIfNotFound(entity);
		if (entity != null) {
			addToCache(entity);
		}
		return entity.getId();
	}

	private InvestorAliasName getFromLocalCache(String service, String name) {
		if (cacheEnabled) {
			HashMap<String, InvestorAliasName> mapForAService = serviceViseAliasNames.get(service);
			if (mapForAService != null) {
				InvestorAliasName aliasName = mapForAService.get(DefaultConstants.replaceTabsAndMultpleSapces(name));
				return aliasName;
			}

		}
		return null;
	}

	private List<InvestorAliasName> getFromLocalCache(String name) {

		List<InvestorAliasName> aliasNames = new ArrayList<>();

		for (String serviceNumber : serviceViseAliasNames.keySet()) {
			InvestorAliasName obj = getFromLocalCache(serviceNumber, name);
			if (obj != null) {
				aliasNames.add(obj);
			}
		}

		return aliasNames;
	}

	@Override
	public InvestorAliasName findInvestorNameInService(String restSerivce, String name) {

		InvestorAliasName investorAliasName = getFromLocalCache(restSerivce, name);

		if (investorAliasName == null) {
			investorAliasName = new InvestorAliasName();
			investorAliasName.setNameFromService(name);
			investorAliasName.setService(restSerivce);
			saveAliasName(investorAliasName);
		}

		return investorAliasName;
	}

	@Override
	public List<InvestorAliasName> updateInvestorId(List<String> aliasNames, Long investorId, StringBuilder statusMsg) {

		List<InvestorAliasName> investorAliasNames = new ArrayList<>();
		for (String name : aliasNames) {

			name = DefaultConstants.replaceTabsAndMultpleSapces(name);

			LOGGER.log(Level.INFO, "Starting update id for name : " + name);

			List<InvestorAliasName> aliasNamesObjts = getFromLocalCache(name);

			if (aliasNamesObjts == null || aliasNamesObjts.isEmpty()) {
				aliasNamesObjts.addAll(dao.get(name));
			}

			if (aliasNamesObjts.isEmpty()) {
				String msg = "Skipped due to following Alias Name is not found " + name + "for investor id "
						+ investorId;
				LOGGER.warning(msg);
				statusMsg.append(msg + "<>");
			} else {
				for (InvestorAliasName investorAliasName : aliasNamesObjts) {

					investorAliasName.setIconicInvestorId(investorId);
					Key key = dao.save(investorAliasName.buildEntity());
					if (key == null || !key.isComplete()) {
						String msg = "Following Alias Name is not able to update " + name + "for investor id "
								+ investorId;
						LOGGER.log(Level.SEVERE, msg);
						statusMsg.append(msg + "<>");
					} else {
						investorAliasName.setId(key.getId());
						addToCache(investorAliasName);
					}
					investorAliasNames.add(investorAliasName);
				}
			}

			LOGGER.log(Level.INFO, "End update id for name : " + name);
		}

		return investorAliasNames;
	}

	@Override
	public List<InvestorAliasName> getAliasNamesList(Long investorId) {
		return dao.getAliasNamesList(investorId);
	}
	

	/**
	 * Get All Aliasname form the table
	 * @return
	 */
	public List<InvestorAliasName> getAllAliasNames()
	{
		return dao.getAllAliasNames();
	}
}
