package com.org.iconic.service;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.google.appengine.api.urlfetch.FetchOptions;
import com.google.appengine.api.urlfetch.HTTPHeader;
import com.google.appengine.api.urlfetch.HTTPMethod;
import com.google.appengine.api.urlfetch.HTTPRequest;
import com.google.appengine.api.urlfetch.HTTPResponse;
import com.google.appengine.api.urlfetch.URLFetchService;
import com.google.appengine.api.urlfetch.URLFetchServiceFactory;
import com.google.gson.Gson;
import com.org.iconic.rest.client.model.CurrentStockPrice;
import com.org.iconic.rest.client.model.CurrentStockPriceWrapper;
import com.org.iconic.rest.client.model.ScriptCodeDomain;
import com.org.iconic.utils.DefaultConstants;
import com.stockboo.app.configuration.Constants;
import com.stockboo.app.cronjobs.HighLow52;

@Service("stockService")
@PropertySource("classpath:application.properties")
public class StockServiceImpl implements StockService {

	private final Logger LOGGER = Logger.getLogger(StockServiceImpl.class.getName());

	@Value("${enable_cache}")
	private boolean cacheEnabled;

	@Autowired
	private RestTemplate restTemplate;

	@Value("${stock_stock_code}")
	private String stockStockCodeUrl;

	@Value("${stock_script_code_id}")
	private String stockScriptCodeIdUrl;
	
	@Value("${stock_current_price}")
	private String currentStockPrircePythonUrl;

	private List<ScriptCodeDomain> stocks = new ArrayList<>();

	private void addToCache(ScriptCodeDomain stockObj) {
		if (cacheEnabled) {
			stocks.add(stockObj);
		}
	}

	@Override
	public String getCompanyName(Long stockId) {

		String stockName = "";

		if (stockId != null && stockId.longValue() != DefaultConstants.DUMMY_ID) {
			ScriptCodeDomain stockObj = getStockDetails(stockId);

			if (stockObj == null) {
				stockObj = getStockIdFromHttpClient(stockScriptCodeIdUrl + "=" + stockId);

				if (stockObj != null) {
					addToCache(stockObj);
					stockName = stockObj.getCompanyName();
				} else {
					LOGGER.log(Level.SEVERE, "Stock Details not found for the ID " + stockId.longValue());

				}

			} else {
				stockName = stockObj.getCompanyName();
			}

		}
		return stockName;
	}

	@Override
	public Long getStockId(String code, boolean isBSECode) {
		Long stockId = null;
		stockId = isBSECode ? getStockIdforBse(code) : getStockIdforNse(code);

		if (stockId == null) {
//			ScriptCodeDomain stockObj = restTemplate.getForObject(stockStockCodeUrl + "=" + code,
//					ScriptCodeDomain.class);
			
			ScriptCodeDomain stockObj = getStockIdFromHttpClient(stockStockCodeUrl + "=" + code);

			if (stockObj != null) {
				addToCache(stockObj);
				stockId = stockObj.getScriptCodeId();
			} else {
				LOGGER.log(Level.SEVERE, "Stock Details not found for the stock code  " + code);
			}
		}

		return stockId;
	}

	private Long getStockIdforBse(String code) {
		if (!StringUtils.isEmpty(code)) {
			ScriptCodeDomain stockObj = stocks.stream().filter(p -> code.equalsIgnoreCase(p.getBseCode())).findFirst()
					.orElse(null);
			return stockObj == null ? null : stockObj.getScriptCodeId();
		}
		return null;
	}

	private Long getStockIdforNse(String code) {
		if (!StringUtils.isEmpty(code)) {
			ScriptCodeDomain stockObj = stocks.stream().filter(p -> code.equalsIgnoreCase(p.getNseCode())).findFirst()
					.orElse(null);
			return stockObj == null ? null : stockObj.getScriptCodeId();
		}
		return null;
	}

	private ScriptCodeDomain getStockDetails(Long id) {
		ScriptCodeDomain stockObj = stocks.stream().filter(p -> p.getScriptCodeId() == id).findFirst().orElse(null);
		return stockObj;
	}
	
	private ScriptCodeDomain getStockIdFromHttpClient(String url){
		URLFetchService fetcher = URLFetchServiceFactory.getURLFetchService();
		FetchOptions fetchOptions = FetchOptions.Builder.validateCertificate();
		
		HTTPResponse responseHigh = null;
		try {
			HTTPRequest requestForHigh = new HTTPRequest(new URL(url), HTTPMethod.GET, fetchOptions);
			requestForHigh.setHeader(new HTTPHeader(Constants.HEADER_USER_AGENT, Constants.HEADER_USER_AGENT_VALUE));
			
			responseHigh = fetcher.fetch(requestForHigh);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("High52 Response code "+responseHigh.getResponseCode());
		String jsonStringHigh = httpResponseToJsonString(responseHigh);
		Gson gson = new Gson();
		return gson.fromJson(jsonStringHigh, ScriptCodeDomain.class);

	}
	
	private String  httpResponseToJsonString(HTTPResponse response) {
		String urlFetchedHtml = null;
		if (response != null && null != response.getContent()) {
	            StringBuilder stringBuilder = new StringBuilder();
	            for (byte b : response.getContent()) {
	                stringBuilder.append((char) b);
	            }
	            urlFetchedHtml = stringBuilder.toString();
	            System.out.println("*************************"+urlFetchedHtml);}
		
		return urlFetchedHtml;
	}

	@Override
	public CurrentStockPriceWrapper getCurrentStockPrice(Long stockId) {
		ScriptCodeDomain stockObj = restTemplate.getForObject(stockScriptCodeIdUrl + "=" + stockId, ScriptCodeDomain.class);
		if (stockObj != null) {
			addToCache(stockObj);
			String stockName = (null == stockObj.getBseCode() || "" == stockObj.getBseCode()) ? stockObj.getNseCode() : stockObj.getBseCode();
			if(null != stockName){
				CurrentStockPrice curStockPrice = restTemplate.getForObject(currentStockPrircePythonUrl + "=" + stockName, CurrentStockPrice.class);
				return null != curStockPrice ? new CurrentStockPriceWrapper(curStockPrice, stockId, stockName, stockObj.getCompanyName()) : new CurrentStockPriceWrapper(stockId, stockName, stockObj.getCompanyName());
			}
			
		} else {
			LOGGER.log(Level.SEVERE, "Stock Details not found for the ID " + stockId.longValue());

		}
		return null;
	}
}
