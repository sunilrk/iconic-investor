package com.org.iconic.service;

import com.org.iconic.rest.client.model.TwitterUserDetails;

public interface TwitterService {

	/**
	 * Get twitter details from Python api
	 * 
	 * @param name
	 * @return
	 */
	TwitterUserDetails[] getTwitterDetails(String name);
	
}
