package com.org.iconic.service;

import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.NoSuchElementException;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.appengine.api.datastore.Key;
import com.org.iconic.dao.InvestorsAggregateInfoDaoImpl;
import com.org.iconic.entity.MonthAggregates;
import com.org.iconic.entity.AbstractAggregate;
import com.org.iconic.entity.IconicInvestor;
import com.org.iconic.entity.MasterAggregate;
import com.org.iconic.entity.Transaction;
import com.org.iconic.ui.model.UiMonthAggreates;
import com.org.iconic.ui.model.UiAggregateData;
import com.org.iconic.ui.model.UiAggregates;
import com.org.iconic.ui.model.UiMasterAggreates;
import com.org.iconic.utils.DefaultConstants;

@Service("investorsAggregateService")
public class InvestorsAggregateServiceImpl implements InvestorsAggregateService {

	private final Logger LOGGER = Logger.getLogger(InvestorsAggregateServiceImpl.class.getName());

	@Autowired
	private InvestorsAggregateInfoDaoImpl dao;

	@Autowired
	private IconicInvestorService investorService;

	@Autowired
	private StockService stockService;

	private final Format monthFormater = new SimpleDateFormat("MMM");
	private final Format yearFormater = new SimpleDateFormat("yyyy");

	private HashMap<Long, HashMap<String, Set<MonthAggregates>>> monthAggreMap = new HashMap<>();
	private HashMap<Long, Set<MasterAggregate>> masterAggreMap = new HashMap<>();

	@PostConstruct
	public void init() {
		List<IconicInvestor> investorList = investorService.getAllActiveInvestors();

		for (IconicInvestor iconicInvestor : investorList) {
			pouplateMonthAggregates(iconicInvestor.getId(), DefaultConstants.DEFAULT_AGGREGATED_NO_OF_MONTHS);
		}

	}

	@Override
	public List<? extends UiAggregates> getAggregatedDetails(Long investorId, int pastNoOfMonths, String profileView) {
		List<UiAggregates> aggregateInfos = new ArrayList<>();
		Set<? extends AbstractAggregate> aggInfos = null;

		if (profileView.equals(DefaultConstants.MONTH_PROFILE_VIEW)) {
			for (int i = 0; i < pastNoOfMonths; i++) {
				UiMonthAggreates maval = new UiMonthAggreates();
				String monthYear = getPastnMonthYear(i);
				maval.setMonthYear(monthYear);
				aggInfos = getMonthAggregates(investorId, monthYear);
				if (aggInfos != null) {
					maval.setAggreList(getUiAggregatedDetails(aggInfos));
					aggregateInfos.add(maval);
				}
			}
		} else {
			UiMasterAggreates maval = new UiMasterAggreates();
			aggInfos = getMasterAggregates(investorId);
			if (aggInfos != null) {
				maval.setAggreList(getUiAggregatedDetails(aggInfos));
				aggregateInfos.add(maval);
			}
		}
		return aggregateInfos;
	}

	private void pouplateMonthAggregates(Long investorId, int pastNoOfMonths) {

		for (int i = 0; i < pastNoOfMonths; i++) {
			String monthYear = getPastnMonthYear(i);
			getMonthAggregates(investorId, monthYear);
		}
	}

	private Set<MonthAggregates> getMonthAggregates(Long investorId, String monthYear) {

		HashMap<String, Set<MonthAggregates>> monthsInfoMap = monthAggreMap.get(investorId);
		if (monthsInfoMap == null) {
			monthsInfoMap = new HashMap<>();
			monthAggreMap.put(investorId, monthsInfoMap);
		}

		Set<MonthAggregates> aggeInfos = monthsInfoMap.get(monthYear);
		if (aggeInfos == null) {
			aggeInfos = new HashSet<MonthAggregates>(dao.getMonthAggregates(investorId, monthYear));
			if (aggeInfos != null && !aggeInfos.isEmpty()) {
				monthsInfoMap.put(monthYear, aggeInfos);
			}
		}
		return aggeInfos;
	}

	private String getPastnMonthYear(int n) {
		Calendar cal = Calendar.getInstance();
		int currentMonth = cal.get(Calendar.MONTH);
		cal.set(Calendar.MONTH, (currentMonth - n));

		Date date = cal.getTime();

		String month = monthFormater.format(date).toUpperCase();
		String year = yearFormater.format(date);

		return month + "_" + year;
	}

	private Key saveMonthAggregate(MonthAggregates aggregateInfo) {
		Key key = dao.save(aggregateInfo.buildEntity());
		aggregateInfo.setId(key.getId());
		addToCachedMap(aggregateInfo);
		return key;
	}

	private Key saveMasterAggregate(MasterAggregate aggregateInfo) {
		Key key = dao.save(aggregateInfo.buildEntity());
		aggregateInfo.setId(key.getId());
		addToMasterCachedMap(aggregateInfo);
		return key;
	}

	private void addToCachedMap(MonthAggregates aggregateInfo) {

		Long investorId = aggregateInfo.getIconicInvestorId();
		HashMap<String, Set<MonthAggregates>> monthsInfoMap = monthAggreMap.get(investorId);

		if (monthsInfoMap == null) {
			monthsInfoMap = new HashMap<>();
			monthAggreMap.put(investorId, monthsInfoMap);
		}

		String monthYear = aggregateInfo.getAggMonth();

		Set<MonthAggregates> aggeInfos = monthsInfoMap.get(monthYear);
		if (aggeInfos == null) {
			aggeInfos = new HashSet<MonthAggregates>();
			monthsInfoMap.put(monthYear, aggeInfos);
		}

		aggeInfos.add(aggregateInfo);
	}

	private void addToMasterCachedMap(MasterAggregate aggregateInfo) {

		Long investorId = aggregateInfo.getIconicInvestorId();
		Set<MasterAggregate> masterAggregates = masterAggreMap.get(investorId);

		if (masterAggregates == null) {
			masterAggregates = new HashSet<MasterAggregate>();
			masterAggreMap.put(investorId, masterAggregates);
		}

		masterAggregates.add(aggregateInfo);
	}

	private List<UiAggregateData> getUiAggregatedDetails(Set<? extends AbstractAggregate> aggInfos) {
		List<UiAggregateData> uiAggInfos = new ArrayList<>();
		for (AbstractAggregate aggInfoObj : aggInfos) {
			uiAggInfos.add(new UiAggregateData(aggInfoObj, stockService.getCompanyName(aggInfoObj.getStockId())));
		}

		return uiAggInfos;

	}

	/**
	 * Aggregation logic to aggregate day to day transactions and save to db
	 * 
	 * @param transactions
	 */
	public void aggreateTransactions(List<Transaction> transactions) {
		List<MonthAggregates> aggeInfos = new ArrayList<>();
		List<MasterAggregate> masterInfos = new ArrayList<>();

		Long investorId = null;

		LOGGER.log(Level.INFO, "Started Aggregating transactions");
		for (Transaction transaction : transactions) {
			investorId = transaction.getIconicInvestorId();
			if (investorId != null && investorId.longValue() != DefaultConstants.DUMMY_ID.longValue()
					&& transaction.getStockId() != null
					&& transaction.getStockId().longValue() != DefaultConstants.DUMMY_ID.longValue()) {
				String monthYear = getMonthYear(transaction.getTransactionDate());

				// update month aggregate
				aggeInfos.add(getMonthAggregateInfo(investorId, transaction, monthYear));

				// update master aggregate
				masterInfos.add(getMasterAggregateInfo(investorId, transaction));
			}
		}

		LOGGER.log(Level.INFO, "Completed Aggregating transactions");

		LOGGER.log(Level.INFO, "Started Saveing/updateding Month wise aggregates");

		for (MonthAggregates aggregateInfo : aggeInfos) {
			if (aggregateInfo.isModified()) {
				saveMonthAggregate(aggregateInfo);
				aggregateInfo.resetModified();
			}
		}
		LOGGER.log(Level.INFO, "Completed Saveing/updateding Month wise aggregates");

		LOGGER.log(Level.INFO, "Started Saveing/updateding Master aggregates");

		for (MasterAggregate aggregateInfo : masterInfos) {
			if (aggregateInfo.isModified()) {
				saveMasterAggregate(aggregateInfo);
				aggregateInfo.resetModified();
			}
		}

		LOGGER.log(Level.INFO, "Completed Saveing/updateding Master wise aggregates");

	}

	/*
	 * Creates or update Master aggregate information
	 */
	private MasterAggregate getMasterAggregateInfo(Long investorId, Transaction transaction) {
		MasterAggregate aggInfo = retriveMasterAggregates(investorId, transaction.getStockId());

		if (aggInfo == null) {
			aggInfo = new MasterAggregate();
			aggInfo.setAggPrice(transaction.getPrice());
			aggInfo.setTotalUnits(transaction.getUnits());
			aggInfo.setIconicInvestorId(investorId);
			aggInfo.setStockId(transaction.getStockId());
			Key key = saveMasterAggregate(aggInfo);
			aggInfo.setId(key.getId());
		} else {
			aggInfo.update(transaction.getPrice(), transaction.getUnits(), transaction.isThiBuyTransaction());
		}
		return aggInfo;
	}

	/*
	 * Creates or updates Month aggregate information
	 */
	private MonthAggregates getMonthAggregateInfo(Long investorId, Transaction transaction, String monthYear) {
		MonthAggregates aggInfo = retriveMonthAggregates(investorId, monthYear, transaction.getStockId());

		if (aggInfo == null) {
			aggInfo = new MonthAggregates();
			aggInfo.setAggMonth(monthYear);
			aggInfo.setAggPrice(transaction.getPrice());
			aggInfo.setTotalUnits(transaction.getUnits());
			aggInfo.setIconicInvestorId(investorId);
			aggInfo.setStockId(transaction.getStockId());
			Key key = saveMonthAggregate(aggInfo);
			aggInfo.setId(key.getId());
		} else {
			aggInfo.update(transaction.getPrice(), transaction.getUnits(), transaction.isThiBuyTransaction());
		}
		return aggInfo;
	}

	private String getMonthYear(Date date) {
		String month = monthFormater.format(date).toUpperCase();
		String year = yearFormater.format(date);

		return month + "_" + year;
	}

	/**
	 * To get Month wise aggregate object for given, first check in cached map,
	 * if not found get from Database and add to cache
	 * 
	 * @param investorId
	 * @param monthYear
	 * @param stockId
	 * @return
	 */
	private MonthAggregates retriveMonthAggregates(Long investorId, String monthYear, Long stockId) {

		HashMap<String, Set<MonthAggregates>> monthsInfoMap = monthAggreMap.get(investorId);
		MonthAggregates aggregateInfo = null;
		Set<MonthAggregates> aggeInfos = null;

		if (monthsInfoMap != null) {

			aggeInfos = monthsInfoMap.get(monthYear);
			if (aggeInfos != null) {
				try {
					aggregateInfo = aggeInfos.stream().filter(e -> e.getStockId().longValue() == stockId.longValue()).findFirst().get();
				} catch (NoSuchElementException ex) {
					aggregateInfo = null;
				}
			}
		}

		if (aggregateInfo == null) {
			List<MonthAggregates> aggeList = dao.getMonthAggregates(investorId, monthYear, stockId);
			if (aggeList != null && !aggeList.isEmpty()) {
				aggregateInfo = aggeList.get(0);
				addToCachedMap(aggregateInfo);
			}
		}

		return aggregateInfo;
	}

	/**
	 * To get Master aggregate object for given, first check in cached map, if
	 * not found get from Database and add to cache
	 * 
	 * @param investorId
	 * @param monthYear
	 * @param stockId
	 * @return
	 */
	private MasterAggregate retriveMasterAggregates(Long investorId, Long stockId) {

		Set<MasterAggregate> masterAggregates = masterAggreMap.get(investorId);
		MasterAggregate aggregateInfo = null;

		if (masterAggregates != null) {
			try {
				aggregateInfo = masterAggregates.stream().filter(e -> e.getStockId().longValue() == stockId.longValue()).findFirst().get();
			} catch (NoSuchElementException ex) {
				aggregateInfo = null;
			}
		}

		if (aggregateInfo == null) {
			List<MasterAggregate> masterList = dao.getMasterAggregates(investorId, stockId);
			if (masterList != null && !masterList.isEmpty()) {
				aggregateInfo = masterList.get(0);
				addToMasterCachedMap(aggregateInfo);
			}
		}

		return aggregateInfo;
	}

	
	private Set<MasterAggregate> getMasterAggregates(Long investorId) {

		Set<MasterAggregate> masterAggList = masterAggreMap.get(investorId);
		if (masterAggList == null) {
			masterAggList = new HashSet<MasterAggregate>(dao.getMasterAggregates(investorId));
			if (masterAggList != null && !masterAggList.isEmpty()) {
				masterAggreMap.put(investorId, masterAggList);
			}
		}
		return masterAggList;
	}

}
