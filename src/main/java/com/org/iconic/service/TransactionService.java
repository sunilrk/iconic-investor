package com.org.iconic.service;

import java.util.Date;
import java.util.List;

import com.org.iconic.entity.Transaction;
import com.org.iconic.rest.services.helper.RestInputForTransaction;
import com.org.iconic.ui.model.UiTransaction;


public interface TransactionService {
	
	/**
	 * Save list of Transaction 
	 * @param transactions
	 */
	boolean saveTransactions(List<Transaction> transactions);
	
	/**
	 * Get all transaction information for given investor id and between given date range
	 * @param RestInputForTransaction
	 * @return
	 */
	List<UiTransaction> getTransaction(RestInputForTransaction input);
	
	/**
	 * 
	 * Get Transactions for an given Alias name id and for given past no.of months
	 *  
	 * @param investorAliasNameId
	 * @param noOfMonths
	 * @return
	 */
	List<Transaction> getTransaction(Long investorAliasNameId, int noOfMonths);
	
	/**
	 * 
	 * Get n number of transactions for given investor id, stock id 
	 * 
	 * @param investorId
	 * @param stockId
	 * @param noOfTransactions
	 * @return
	 */
	List<UiTransaction> getTransaction(Long investorId, Long stockId, int noOfTransactions);
	
	/**
	 * Returns list of transactions for given source stock code
	 * 
	 * @param sourceStockCode
	 *            - stock code which comes from NSE or BSE
	 * @param onlyNullStockIds
	 *            true - get transaction that don't have stock id (stock id are
	 *            not if not found in stock master table) 
	 *            
	 *            false - get all
	 *            transactions
	 * @return
	 */
	List<Transaction> getTransaction(String sourceStockCode, boolean onlyNullStockIds);
}
