package com.org.iconic.service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.Key;
import com.org.iconic.dao.IconicInvestorDao;
import com.org.iconic.entity.IconicInvestor;
import com.org.iconic.ui.model.UiIconicInvestor;

@Service("iconicInvestorService")
public class IconicInvestorServiceImpl implements IconicInvestorService {

	private final Logger LOGGER = Logger.getLogger(IconicInvestorServiceImpl.class.getName());

	@Autowired
	IconicInvestorDao dao;

	Map<Long, IconicInvestor> allInvestors = new HashMap<>();
	Map<Long, IconicInvestor> activeInvestors = new HashMap<>();

	@PostConstruct
	public void init() {
		List<IconicInvestor> investorList = dao.getAllInvestors();

		for (IconicInvestor iconicInvestor : investorList) {
			allInvestors.put(iconicInvestor.getId(), iconicInvestor);

			if (iconicInvestor.isActive()) {
				activeInvestors.put(iconicInvestor.getId(), iconicInvestor);
			}
		}

		LOGGER.log(Level.INFO, "Active investors count : " + activeInvestors.size());
		System.out.println("Active investors count : " + activeInvestors.size());
	}

	@Override
	public List<IconicInvestor> getAllActiveInvestors() {
		List<IconicInvestor> list = new ArrayList<>();
		list.addAll(activeInvestors.values());
		return list;
	}

	@Override
	public Key saveIconicInvestor(IconicInvestor entity) {
		Key key = dao.save(entity.buildEntity());

		if (key != null && key.isComplete()) {
			entity.setId(key.getId());
			allInvestors.put(entity.getId(), entity);
			activeInvestors.put(entity.getId(), entity);
		}

		return key;
	}

	@Override
	public List<UiIconicInvestor> getInvestorsList(boolean onlyActive) {
		List<UiIconicInvestor> investors = new ArrayList<>();
		Collection<IconicInvestor> ics = null;
		UiIconicInvestor uiIc = null;
		
		if (onlyActive) {
			ics = activeInvestors.values();
		} else

		{
			ics = allInvestors.values();
		}
		
		for (IconicInvestor ic : ics) {
			uiIc = buildUiIconicInvestor(ic);
			investors.add(uiIc);
		}
		return investors;
	}

	private UiIconicInvestor buildUiIconicInvestor(IconicInvestor ic) {
		UiIconicInvestor uiIc;
		uiIc = new UiIconicInvestor();
		uiIc.setDisplayName(ic.getDisplayName());
		uiIc.setAbout(ic.getAboutInvestor());
		uiIc.setActive(ic.isActive());
		uiIc.setEmailId(ic.getEmailId());
		uiIc.setId(ic.getId());
		uiIc.setLogoImageUrl(ic.getLogoImageUrl());
		uiIc.setSocialAccounts(getSocialAccounts(ic.getSocialAccounts()));
		return uiIc;
	}
	
	private Map<String, String> getSocialAccounts(String socialAccounts)
	{
		Map<String, String> accounts = new HashMap<>();
		
		String[] indivAc = socialAccounts.split(";");
		String[] singleAc = null;
		for (int i = 0; i < indivAc.length; i++) {
			singleAc = indivAc[i].split("<>");
			if(singleAc.length == 2)
			{
				accounts.put(singleAc[0], singleAc[1]);
			}
		}		
		return accounts;
	}

	@Override
	public void saveIconicInvestors(List<IconicInvestor> investors) {
		int position = 0;

		List<Entity> entities = new ArrayList<>();
		for (IconicInvestor iconicInvestor : investors) {
			entities.add(iconicInvestor.buildEntity());
		}
		List<Key> keys = dao.save(entities);

		if (!keys.isEmpty()) {

			for (IconicInvestor iconicInvestor : investors) {
				iconicInvestor.setId(keys.get(position).getId());
				if (iconicInvestor.isActive())
					activeInvestors.put(iconicInvestor.getId(), iconicInvestor);
				allInvestors.put(iconicInvestor.getId(), iconicInvestor);
			}
		}
	}

	@Override
	public IconicInvestor getInvestorProfile(Long investorId) {
		IconicInvestor iconicInvestor = allInvestors.get(investorId);

		if (iconicInvestor == null)
			LOGGER.log(Level.WARNING, "Investor information not Found for id : " + investorId);

		return iconicInvestor;
	}

	@Override
	public boolean changeProfileStatus(Long investorId, boolean status) {

		IconicInvestor investor = allInvestors.get(investorId);

		// As we have checked for given investorId profile exists or not, so no
		// need to check here again.

		investor.setActive(status);
		Key key = saveIconicInvestor(investor);

		if (key != null && key.isComplete()) {
			if (status)
				activeInvestors.put(investor.getId(), investor);
			else
				activeInvestors.remove(investor.getId());
			return true;
		}

		return false;
	}

	@Override
	public IconicInvestor getInvestorProfileByName(String name) {

		IconicInvestor investor = null;

		try {
			investor = allInvestors.values().stream().filter(e -> e.getDisplayName().equalsIgnoreCase(name)).findFirst()
					.get();
		} catch (NoSuchElementException ex) {
			investor = null;
		}

		return investor;
	}

	@Override
	public UiIconicInvestor getUiIconicInvestor(Long investorId) {
		IconicInvestor investor = getInvestorProfile(investorId);
		if(investor != null)
		{
			return buildUiIconicInvestor(investor);
		}
		return null;
	}
}
