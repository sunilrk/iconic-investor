package com.org.iconic.service;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.Key;
import com.org.iconic.dao.TransactionDao;
import com.org.iconic.entity.Transaction;
import com.org.iconic.rest.services.helper.RestInputForTransaction;
import com.org.iconic.ui.model.UiTransaction;
import com.org.iconic.utils.DefaultConstants;

@Service("transactionService")
public class TransactionServiceImpl implements TransactionService {

	@Autowired
	private TransactionDao dao;

	@Autowired
	private StockService stockService;

	@Override
	public boolean saveTransactions(List<Transaction> transactions) {

		List<Entity> entities = new ArrayList<>();

		for (Transaction transaction : transactions) {
			entities.add(transaction.buildEntity());
		}

		List<Key> keys = dao.save(entities);

		return (keys.size() == transactions.size());
	}

	@Override
	public List<UiTransaction> getTransaction(RestInputForTransaction input) {

		List<UiTransaction> uiTransactions = new ArrayList<>();

		List<Transaction> transactions = dao.getTransaction(input);

		for (Transaction transaction : transactions) {
			if (isStockIdNotEmpty(transaction.getStockId())) {
				uiTransactions
						.add(new UiTransaction(transaction, stockService.getCompanyName(transaction.getStockId())));
			}
		}

		return uiTransactions;
	}

	@Override
	public List<Transaction> getTransaction(Long investorAliasNameId, int noOfMonths) {
		return dao.getTransaction(investorAliasNameId, getPastDate(noOfMonths));
	}

	
	private Date getPastDate(int noOfMonths) {
		noOfMonths = -1 * noOfMonths;
		Calendar cal = Calendar.getInstance();
		cal.setTime(new Date());
		cal.add(Calendar.MONTH, noOfMonths);
		return cal.getTime();
	}

	@Override
	public List<UiTransaction> getTransaction(Long investorId, Long stockId, int noOfTransactions) {

		List<UiTransaction> uiTransactions = new ArrayList<>();
		List<Transaction> transactions = dao.getTransaction(investorId, stockId, noOfTransactions);

		for (Transaction transaction : transactions) {
			if (isStockIdNotEmpty(transaction.getStockId())) {
				uiTransactions
						.add(new UiTransaction(transaction, stockService.getCompanyName(transaction.getStockId())));
			}
		}

		return uiTransactions;
	}

	private boolean isStockIdNotEmpty(Long id) {
		return (id != null && id.longValue() != DefaultConstants.DUMMY_ID.longValue());
	}

	@Override
	public List<Transaction> getTransaction(String sourceStockCode, boolean onlyNullStockIds) {
		return dao.getTransaction(sourceStockCode, onlyNullStockIds);
	}

}
