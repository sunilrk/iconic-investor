package com.org.iconic.service;

import com.org.iconic.rest.client.model.CurrentStockPrice;
import com.org.iconic.rest.client.model.CurrentStockPriceWrapper;

public interface StockService {

	/**
	 * Returns company for given stock id
	 * @param stockId
	 * @return
	 */
	String getCompanyName(Long stockId);
	
	/**
	 * Get stock id from stock master table for given name
	 * @param name
	 * @return
	 */
	Long getStockId(String name, boolean isBSECode);
	
	/**
	 * Get current stock id from Python server
	 * 
	 * @param stockId
	 * @return
	 */
	CurrentStockPriceWrapper getCurrentStockPrice(Long stockId);
	
}
