package com.org.iconic.service;

import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.org.iconic.rest.client.model.TwitterUserDetails;

@Service("twitterService")
@PropertySource("classpath:application.properties")
public class TwitterServiceImpl implements TwitterService {

	private final Logger LOGGER = Logger.getLogger(TwitterServiceImpl.class.getName());
	
	@Autowired
	private RestTemplate restTemplate;

	@Value("${twitter_details}")
	private String twitterDetailsPythonUrl;

	
	@Override
	public TwitterUserDetails[] getTwitterDetails(String name) {
		TwitterUserDetails[] twitterDetails =null;
		
		try {
			twitterDetails = restTemplate.getForObject(twitterDetailsPythonUrl + name + "/?count=30", TwitterUserDetails[].class);
		} catch (Exception e) {
			System.out.println(e);
		}
		
		System.out.println(twitterDetails);
		return twitterDetails;
	}

	
}
