package com.org.iconic.logic;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.org.iconic.entity.InvestorAliasName;
import com.org.iconic.entity.Transaction;
import com.org.iconic.service.InvestorAliasNameService;
import com.org.iconic.service.InvestorsAggregateService;
import com.org.iconic.service.TransactionService;
import com.org.iconic.utils.DefaultConstants;

/**
 * 
 * An utility class to update iconic investor id for given alias names and map
 * the transactions to investor and calculate aggregation logic
 * 
 */
@Component("transactionsMapper")
public class AliasNamesAndTransactionsMapper {

	private final Logger LOGGER = Logger.getLogger(AliasNamesAndTransactionsMapper.class.getName());

	@Autowired
	InvestorAliasNameService aliasNameService;

	@Autowired
	TransactionService transactionService;

	@Autowired
	InvestorsAggregateService aggregateService;

	public String process(Long investorId, List<String> aliasNames) {
		StringBuilder status = new StringBuilder();

		List<InvestorAliasName> investorAliasNames = aliasNameService.updateInvestorId(aliasNames, investorId, status);

		LOGGER.log(Level.INFO, "Toatal no.of aliasnames updated " + investorAliasNames.size());

		List<Transaction> transactions = getTransaction(investorAliasNames, status);
		aggregateService.aggreateTransactions(transactions);

		return status.toString();
	}

	private List<Transaction> getTransaction(List<InvestorAliasName> investorAliasNames, StringBuilder statusMsg) {
		List<Transaction> transactions = new ArrayList<>();

		for (InvestorAliasName aliasName : investorAliasNames) {
			List<Transaction> transactionsFromDao = transactionService.getTransaction(aliasName.getId(),
					DefaultConstants.DEFAULT_AGGREGATED_NO_OF_MONTHS);

			LOGGER.log(Level.INFO, "Toatal no.of transaction " + transactionsFromDao.size() + "for  aliasName "
					+ aliasName.getNameFromService() + "for id " + aliasName.getId());

			if (transactionsFromDao.isEmpty()) {
				statusMsg.append("No Transactions found for AliasName : " + aliasName.getNameFromService() + "for id "
						+ aliasName.getId() + "<>");
			} else {

				for (Transaction transaction : transactionsFromDao) {
					transaction.setIconicInvestorId(aliasName.getIconicInvestorId());
				}
				boolean saved = transactionService.saveTransactions(transactionsFromDao);
				if (!saved) {
					LOGGER.log(Level.SEVERE,
							"One more transaction are not updated for IconicInvestorId  "
									+ aliasName.getIconicInvestorId() + "for AliasName Id " + aliasName.getId()
									+ ". The related trasaction are not included in aggregation");
					statusMsg.append("Unable to updated id in Transactions  for AliasName : " + aliasName.getNameFromService() + "for id "
							+ aliasName.getId() + "<>");
				} else {
					transactions.addAll(transactionsFromDao);
				}
			}
		}

		return transactions;
	}
	
	public List<InvestorAliasName> getList(Long investorId)
	{
		return aliasNameService.getAliasNamesList(investorId);
	}
	
	public List<String> getAliasNames()
	{
		List<String> names = new ArrayList<>();
		List<InvestorAliasName> aliasNames = aliasNameService.getAllAliasNames();
		for (InvestorAliasName investorAliasName : aliasNames) {
			names.add(investorAliasName.getNameFromService());
		}
		
		return names;
	}
}
