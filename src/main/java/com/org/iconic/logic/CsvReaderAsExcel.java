package com.org.iconic.logic;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.apache.commons.io.input.BOMInputStream;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.springframework.stereotype.Component;

import com.org.iconic.entity.UnParsableData;
import com.org.iconic.inputs.CSVHeader;
import com.org.iconic.rest.client.model.RestClinetTransaction;

@Component("csvReaderAsExcel")
public class CsvReaderAsExcel {

	private static final Logger LOGGER = Logger.getLogger(CsvReaderAsExcel.class.getName());

	private List<UnParsableData> unParsableList = new ArrayList<>();

	private String toDay = "";

	public CsvReaderAsExcel() {

	}

	private void setToday(Date date) {
		toDay = DateFormatUtils.format(date, "yyyyMMdd");
	}

	public ArrayList<RestClinetTransaction> readCsvFile(CSVHeader csvHeader) throws Exception {
		
		System.out.println("");
		
		setToday(csvHeader.getDate());
		URL url = new URL(csvHeader.getURL());
		InputStream stream = null;
		HttpURLConnection connection = null;
		try {
			connection = (HttpURLConnection) url.openConnection();
			if (connection != null && connection.getResponseCode() == 200) {
				stream = connection.getInputStream();
			}
		} catch (IOException e) {
			LOGGER.log(Level.SEVERE, "Unable open connection for " + csvHeader.getURL(), e);
			if (connection != null)
				connection.disconnect();
			return null;
		}

		if (stream != null) {
			ArrayList<RestClinetTransaction> transactions = prepareTransactioins(csvHeader, stream, true);
			connection.disconnect();
			return transactions;
		}

		return null;
	}

	public ArrayList<RestClinetTransaction> prepareTransactioins(CSVHeader csvHeader, InputStream stream,
			boolean onlyForToday) throws Exception {
		ArrayList<RestClinetTransaction> transactions = new ArrayList<>();
		RestClinetTransaction transaction;
		Reader reader = new InputStreamReader(new BOMInputStream(stream), "UTF-8");
		CSVParser parser = new CSVParser(reader, CSVFormat.EXCEL.withHeader());
		unParsableList.clear();
		try {

			for (CSVRecord record : parser) {
				if (record.size() == csvHeader.getLineLength()) {
					Date transDate = convertToDate(record.get(csvHeader.getDateColumn()), csvHeader.getDateFormat());
					// Only add the add the transaction for today
					if (isThisTodayTransaction(transDate, onlyForToday)) {
						transaction = new RestClinetTransaction();
						transaction.setTransDate(transDate);
						transaction.setSecuriyCode(record.get(csvHeader.getSecurityColumn()));
						transaction.setCompany(record.get(csvHeader.getCompanyColumn()));
						transaction.setClientName(record.get(csvHeader.getClientNameColumn()));
						transaction.setTransType(record.get(csvHeader.getDealTypeColumn()));
						transaction.setQuantity(convertTolong(record.get(csvHeader.getQuantityColumn())));
						transaction.setPrice(convertToDouble(record.get(csvHeader.getPriceColumn())));
						transactions.add(transaction);
					}
				} else {
					LOGGER.log(Level.SEVERE, "Missing or un expected information in Transaction Details : " + record);
					unParsableList.add(new UnParsableData(csvHeader.getURL(), record.toString()));
				}
			}

		} catch (Exception e) {
			LOGGER.log(Level.SEVERE, "Unable to praseing data for URL " + csvHeader.getURL(), e);
			throw e;
		} finally {
			stream.close();
			parser.close();
			reader.close();
		}

		return transactions;
	}

	private double convertToDouble(String strValue) throws Exception {
		double value = 0.0;
		try {
			value = Double.parseDouble(strValue.replace(",", ""));
		} catch (NumberFormatException e) {
			LOGGER.log(Level.SEVERE, "Unable to convert string " + strValue + " to double");
			throw e;
		}

		return value;
	}

	private long convertTolong(String strValue) throws Exception {
		long value = 0;
		try {
			value = Long.parseLong(strValue.replace(",", ""));
		} catch (NumberFormatException e) {
			LOGGER.log(Level.SEVERE, "Unable to convert string " + strValue + " to long");
			throw e;
		}

		return value;
	}

	private Date convertToDate(String strDate, String format) throws Exception {
		Date date = null;

		try {
			date = DateUtils.parseDate(strDate, format);
		} catch (ParseException e) {
			LOGGER.log(Level.SEVERE, "Unable to convert string " + strDate + " to date");
			throw e;
		}

		return date;
	}

	private boolean isThisTodayTransaction(Date transDate, boolean onlyForToday) {
		if (onlyForToday) {
			String formatedDate = DateFormatUtils.format(transDate, "yyyyMMdd");
			return formatedDate.equals(toDay);
		}

		return true;
	}

	public List<UnParsableData> getUnParsableList() {
		return unParsableList;
	}

	// private boolean isThisTodayTransaction(Date transDate)
	// {
	// String formatedDate = DateFormatUtils.format(transDate, "yyyyMMdd");
	// return formatedDate.equals("20180402") ;
	// }
}
