package com.org.iconic.logic;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.org.iconic.entity.InvestorAliasName;
import com.org.iconic.entity.Transaction;
import com.org.iconic.rest.client.model.RestClinetTransaction;
import com.org.iconic.scheduler.CornJobToFetchDataFromServices;
import com.org.iconic.service.InvestorAliasNameService;
import com.org.iconic.service.InvestorsAggregateService;
import com.org.iconic.service.StockService;
import com.org.iconic.service.TransactionService;
import com.org.iconic.utils.DefaultConstants;

@Component("inputTransactionsWriter")
public class InputTransactionsWriter {

	private final Logger LOGGER = Logger.getLogger(InputTransactionsWriter.class.getName());

	@Autowired
	private InvestorAliasNameService investorAliasNameService;

	@Autowired
	private TransactionService transactionService;

	@Autowired
	private StockService stockService;

	@Autowired
	private InvestorsAggregateService aggreationService;

	public List<Transaction> write(String service, List<RestClinetTransaction> restTransactions) {
		if (restTransactions != null) {
			List<Transaction> transactions = new ArrayList<>();
			Transaction transaction;
			boolean isBse = (service == DefaultConstants.BSE_INDIA_SERVICE);

			LOGGER.log(Level.WARNING, "Started writing transaction tod db Size : " + restTransactions.size());

			for (RestClinetTransaction restTransaction : restTransactions) {
				transaction = new Transaction();

				InvestorAliasName iNameRest = investorAliasNameService.findInvestorNameInService(service,
						restTransaction.getClientName());

				transaction.setIconicInvestorId(iNameRest.getIconicInvestorId());
				transaction.setInvestorAliasNameId(iNameRest.getId());

				transaction.setTransactionDate(restTransaction.getTransDate());
				
				transaction.setSouceStockCode(restTransaction.getSecuriyCode());
				
				transaction.setStockId(stockService.getStockId(restTransaction.getSecuriyCode(), isBse));

				transaction.setPrice(restTransaction.getPrice());

				// Need to change type as constant of Transaction
				transaction.setTransactionType(restTransaction.getTransType());
				transaction.setUnits(restTransaction.getQuantity());

				transaction.setTransacationOrigin(service);
				transactions.add(transaction);
			}

			transactionService.saveTransactions(transactions);
			aggreationService.aggreateTransactions(transactions);

			LOGGER.log(Level.WARNING, "Finished writing transaction tod db");
			return transactions;

		} else {
			LOGGER.log(Level.WARNING, "No Data to write");
			return null;
		}
	}
}
