package com.org.iconic.logic;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URLConnection;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import com.google.auth.oauth2.GoogleCredentials;
import com.google.cloud.storage.Acl;
import com.google.cloud.storage.Acl.Role;
import com.google.cloud.storage.Acl.User;
import com.google.cloud.storage.Blob;
import com.google.cloud.storage.BlobId;
import com.google.cloud.storage.BlobInfo;
import com.google.cloud.storage.Storage;
import com.google.cloud.storage.StorageOptions;

@Component("gcpImageUploader")
@PropertySource("classpath:application.properties")
public class GcpImageUploader {

	private final Logger LOGGER = Logger.getLogger(GcpImageUploader.class.getName());
	
	@Value("${gcp_storage_bucket_name}")
	private String storageBukcet;
	
	public String uploadImageToGcp(String fileName, MultipartFile imgfile) {
		if (!imgfile.isEmpty()) {
			try {
				return createImageBlob(fileName, imgfile.getInputStream());
			} catch (IOException e) {
				LOGGER.log(Level.SEVERE, "Unable to convert image data to input stream ", e);
			}
		} else {
			LOGGER.log(Level.SEVERE, "No image information came from request");
		}

		return null;
	}

	/**
	 * Saves image on GCP Storage bucket, Gives Reader access for all user
	 * Returns path on the storage bucket.
	 * 
	 * @param fileName
	 * @param inputStream
	 * @return
	 */
	public String createImageBlob(String fileName, InputStream inputStream) {
		String pathOnGcp = null;
		ClassLoader classLoader = getClass().getClassLoader();
		File file = new File(classLoader.getResource("service_acc_auth.json").getFile());
		StorageOptions storageOptions = StorageOptions.getDefaultInstance();
		try {
			String mimeType = URLConnection.guessContentTypeFromStream(inputStream);
			String fileExn = mimeType.split("/")[1];
			fileName = fileName + "." + fileExn;
			storageOptions = StorageOptions.newBuilder().setProjectId(storageOptions.getProjectId())
					.setCredentials(GoogleCredentials.fromStream(new FileInputStream(file))).build();
			Storage storage = storageOptions.getService();
			BlobId blobId = BlobId.of(storageBukcet, fileName);
			BlobInfo blobInfo = BlobInfo.newBuilder(blobId).setContentType(mimeType).build();
			Blob blob = storage.create(blobInfo, inputStream);
			if (blob != null) {
				storage.createAcl(blobId, Acl.of(User.ofAllUsers(), Role.READER));
				https://storage.googleapis.com/iconic_investor_images/Ranjith.jpeg
				pathOnGcp = "https://storage.googleapis.com/"+storageBukcet+"/"+fileName;
			}
		} catch (IOException e) {
			LOGGER.log(Level.SEVERE, "Unable to upload image to gcp storage ", e);
		}

		return pathOnGcp;
	}
}