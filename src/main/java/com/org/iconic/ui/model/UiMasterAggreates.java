package com.org.iconic.ui.model;

import java.util.List;

public class UiMasterAggreates implements UiAggregates {

	private List<UiAggregateData> aggreList;

	public List<UiAggregateData> getAggreList() {
		return aggreList;
	}

	public void setAggreList(List<UiAggregateData> aggreList) {
		this.aggreList = aggreList;
	}

}
