package com.org.iconic.ui.model;

import com.org.iconic.entity.AbstractAggregate;
import com.org.iconic.utils.DefaultConstants;

public class UiAggregateData {

	private String stockName;
	private Long stockId;
	private Double aggPrice;
	private String monthAndYear;
	private Long aggUnits;
	private String updatedDate;

	public UiAggregateData(AbstractAggregate aggInfo, String stockName) {
		setStockName(stockName);
		aggPrice = aggInfo.getAggPrice();
		monthAndYear = aggInfo.getAggMonth();
		aggUnits = aggInfo.getTotalUnits();
		stockId = aggInfo.getStockId();
		updatedDate = DefaultConstants.getStringDate(aggInfo.getUpdatedDate());
	}

	public void setStockName(String stockName) {
		this.stockName = stockName;
	}

	public Double getAggPrice() {
		return aggPrice;
	}

	public void setAggPrice(Double aggPrice) {
		this.aggPrice = aggPrice;
	}

	public String getAggMonth() {
		return monthAndYear;
	}

	public void setAggMonth(String aggMonth) {
		this.monthAndYear = aggMonth;
	}

	public Long getAggUnits() {
		return aggUnits;
	}

	public void setAggUnits(Long units) {
		this.aggUnits = units;
	}

	public String getUpdatedDate() {
		return updatedDate;
	}

	public Long getStockId() {
		return stockId;
	}

	public void setStockId(Long stockId) {
		this.stockId = stockId;
	}

	public String getStockName() {
		return stockName;
	}

}
