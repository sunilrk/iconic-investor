package com.org.iconic.ui.model;

import com.org.iconic.entity.Transaction;
import com.org.iconic.utils.DefaultConstants;

public class UiTransaction {

	private Long iconicInvestorId;
	private Long stockId;
	private String stockName;
	private String transactionType;
	private long units;
	private double price;
	private String transactionDate;
	private String transactionOrigin;

	public UiTransaction(Transaction entity, String stockName) {
		iconicInvestorId = entity.getIconicInvestorId();
		this.stockName = stockName;
		transactionType = entity.getTransactionType();
		units = entity.getUnits();
		price = entity.getPrice();
		transactionDate = DefaultConstants.getStringDate(entity.getTransactionDate());
		transactionOrigin = entity.getTransacationOrigin();
		this.stockId = entity.getStockId();
	}

	public Long getIconicInvestorId() {
		return iconicInvestorId;
	}

	public void setIconicInvestorId(Long iconicInvestorId) {
		this.iconicInvestorId = iconicInvestorId;
	}

	public String getStockName() {
		return stockName;
	}

	public String getTransactionType() {
		return transactionType;
	}

	public void setTransactionType(String transactionType) {
		this.transactionType = transactionType;
	}

	public long getUnits() {
		return units;
	}

	public void setUnits(long units) {
		this.units = units;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public String getTransactionDate() {
		return transactionDate;
	}

	public void setTransactionDate(String transactionDate) {
		this.transactionDate = transactionDate;
	}

	public Long getStockId() {
		return stockId;
	}

	public void setStockId(Long stockId) {
		this.stockId = stockId;
	}

	public void setStockName(String stockName) {
		this.stockName = stockName;
	}

	public String getTransactionOrigin() {
		return transactionOrigin;
	}

	public void setTransactionOrigin(String transactionOrigin) {
		this.transactionOrigin = transactionOrigin;
	}

	
}
