package com.org.iconic.ui.model;

import java.util.Map;

import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;

@Entity
public class UiIconicInvestor {

	public static final int ACTIVE = 1;
	
	@Id
	private Long id;
	
	private String displayName;
	private boolean active;
	private String logImageUrl;
	private Map<String, String> socialAccounts;
	private String emailId; 
	private String about;
	

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDisplayName() {
		return displayName;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	public boolean isActive() {
		return active;
	}
	
	public void setActive(boolean active) {
		this.active = active;
	}
	
	
	
	public String getLogoImageUrl() {
		return logImageUrl;
	}

	public void setLogoImageUrl(String logImageUrl) {
		this.logImageUrl = logImageUrl;
	}

	public Map<String, String> getSocialAccounts() {
		return socialAccounts;
	}

	public void setSocialAccounts(Map<String, String> socialAccounts) {
		this.socialAccounts = socialAccounts;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public String getAbout() {
		return about;
	}

	public void setAbout(String about) {
		this.about = about;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id.hashCode();
		result = prime * result + displayName.hashCode();
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof UiIconicInvestor))
			return false;
		UiIconicInvestor other = (UiIconicInvestor) obj;
		if (id != other.id)
			return false;
		if (displayName == null) {
			if (other.displayName != null)
				return false;
		} else if (!displayName.equals(other.displayName))
			return false;
		return true;
	}

}
