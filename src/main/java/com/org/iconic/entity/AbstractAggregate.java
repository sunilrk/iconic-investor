package com.org.iconic.entity;

import java.util.Date;

import com.google.appengine.api.datastore.Entity;

public abstract class AbstractAggregate extends AbstractIconicEntity {

	public AbstractAggregate()
	{
		super(null);
	}
	
	public AbstractAggregate(Entity entity) {
		super(entity);
	}
	
	public abstract String getAggMonth();
	public abstract Long getIconicInvestorId();
	public abstract Long getStockId();
	public abstract Double getAggPrice();
	public abstract Long getTotalUnits();
	public abstract Date getUpdatedDate();
	
}
