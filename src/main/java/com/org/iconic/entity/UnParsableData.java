package com.org.iconic.entity;

import com.google.appengine.api.datastore.Entity;
import com.org.iconic.entity.enums.IconicInvestorEnum;
import com.org.iconic.entity.enums.UnParsableDataEnum;

public class UnParsableData extends AbstractIconicEntity {

	public static final String TABLE_NAME = "IC_UN_PARSABLE_DATA";

	private String data;
	private String sourceUrl;

	public UnParsableData(Entity entity) {
		super(entity);
		data = (String) entity.getProperty(UnParsableDataEnum.DATA.name());
		sourceUrl = (String) entity.getProperty(UnParsableDataEnum.SOURCE_URL.name());
	}
	
	public UnParsableData(String url, String data) {
		super(null);
		this.data =  data;
		this.sourceUrl = url;
	}

	public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}

	public String getSourceUrl() {
		return sourceUrl;
	}

	public void setSourceUrl(String sourceUrl) {
		this.sourceUrl = sourceUrl;
	}

	@Override
	public Entity buildEntity() {
		Entity entity = createEntity(TABLE_NAME);

		entity.setProperty(UnParsableDataEnum.DATA.name(), getData());
		entity.setProperty(UnParsableDataEnum.SOURCE_URL.name(), getSourceUrl());
		
		return entity;
	}

}
