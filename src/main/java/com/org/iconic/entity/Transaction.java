package com.org.iconic.entity;

import java.util.Date;

import com.google.appengine.api.datastore.Entity;
import com.org.iconic.entity.enums.TransactionEnum;

public class Transaction extends AbstractIconicEntity {

	public static final String TABLE_NAME = "IC_TRANSACTION";
	public static final String BUY_TRANSACTION = "BUY";
	public static final String SELL_TRANSACTION = "SELL";

	// Foreign key reference of id at IconicInvestor.java
	private Long iconicInvestorId;
	// Foreign key reference of id at Stock
	private Long stockId;
	// Foreign key reference of id at InvestorAliasName.java
	private Long investorAliasNameId;
	private String transactionType;
	private long units;
	private double price;
	private Date transactionDate;
	private String transacationOrigin;
	private String souceStockCode;

	public Transaction() {
		super(null);
	}

	public Transaction(Entity entity) {
		super(entity);
		iconicInvestorId = (Long) entity.getProperty(TransactionEnum.ICONIC_INVESTOR_ID.name());
		stockId = (Long) entity.getProperty(TransactionEnum.STOCK_ID.name());
		transactionType = (String) entity.getProperty(TransactionEnum.TYPE.name());
		units = (long) entity.getProperty(TransactionEnum.UNITS.name());
		price = (double) entity.getProperty(TransactionEnum.PRICE.name());
		transactionDate = (Date) entity.getProperty(TransactionEnum.DATE.name());
		investorAliasNameId = (Long) entity.getProperty(TransactionEnum.INVESTOR_ALIAS_NAME_ID.name());
		transacationOrigin = (String) entity.getProperty(TransactionEnum.ORIGIN.name());
		souceStockCode = (String) entity.getProperty(TransactionEnum.SRC_STOCK_CODE.name());
	}

	public Long getIconicInvestorId() {
		return iconicInvestorId;
	}

	public void setIconicInvestorId(Long iconicInvestorId) {
		this.iconicInvestorId = iconicInvestorId;
	}

	public Long getStockId() {
		return stockId;
	}

	public void setStockId(Long stockId) {
		this.stockId = stockId;
	}

	public Long getInvestorAliasNameId() {
		return investorAliasNameId;
	}

	public void setInvestorAliasNameId(Long investorAliasNameId) {
		this.investorAliasNameId = investorAliasNameId;
	}

	public String getTransactionType() {
		return transactionType;
	}

	public void setTransactionType(String transactionType) {
		if (transactionType.equalsIgnoreCase("S")) {
			transactionType = SELL_TRANSACTION;
		} else if (transactionType.equalsIgnoreCase("P")) {
			transactionType = BUY_TRANSACTION;
		}

		this.transactionType = transactionType;
	}

	public long getUnits() {
		return units;
	}

	public void setUnits(long units) {
		this.units = units;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public Date getTransactionDate() {
		return transactionDate;
	}

	public void setTransactionDate(Date transactionDate) {
		this.transactionDate = transactionDate;
	}

	public boolean isThiBuyTransaction() {
		return transactionType == BUY_TRANSACTION ? true : false;
	}
	
	public String getTransacationOrigin() {
		return transacationOrigin;
	}

	public void setTransacationOrigin(String transacationOrigin) {
		this.transacationOrigin = transacationOrigin;
	}
	
	public String getSouceStockCode() {
		return souceStockCode;
	}

	public void setSouceStockCode(String souceStockCode) {
		this.souceStockCode = souceStockCode;
	}

	@Override
	public Entity buildEntity() {
		Entity entity = createEntity(TABLE_NAME);
		// entity.setProperty(TransactionEnum.ID.name(), getId());
		entity.setProperty(TransactionEnum.STOCK_ID.name(), getStockId());
		entity.setProperty(TransactionEnum.ICONIC_INVESTOR_ID.name(), getIconicInvestorId());
		entity.setProperty(TransactionEnum.TYPE.name(), getTransactionType());
		entity.setProperty(TransactionEnum.UNITS.name(), getUnits());
		entity.setProperty(TransactionEnum.DATE.name(), getTransactionDate());
		entity.setProperty(TransactionEnum.INVESTOR_ALIAS_NAME_ID.name(), getInvestorAliasNameId());
		entity.setProperty(TransactionEnum.PRICE.name(), getPrice());
		entity.setProperty(TransactionEnum.ORIGIN.name(), getTransacationOrigin());
		entity.setProperty(TransactionEnum.SRC_STOCK_CODE.name(), getSouceStockCode());


		return entity;
	}

}
