package com.org.iconic.entity;

import com.google.appengine.api.datastore.Entity;
import com.org.iconic.entity.enums.InvestorAliasNameEnum;
import com.org.iconic.utils.DefaultConstants;

public class InvestorAliasName extends AbstractIconicEntity {

	public static final String TABLE_NAME = "IC_INVESTORS_ALIAS_NAME";
	
	private Long iconicInvestorId = DefaultConstants.DUMMY_ID;
	private String nameFromService;
	private String service;


	public InvestorAliasName() {
		super(null);
	}
	
	public InvestorAliasName(Entity entity) {
		super(entity);
		iconicInvestorId = (Long) entity.getProperty(InvestorAliasNameEnum.ICONIC_INVESTOR_ID.name());
		setNameFromService((String) entity.getProperty(InvestorAliasNameEnum.NAME_FROM_SERVICE.name()));
		service = (String) entity.getProperty(InvestorAliasNameEnum.REST_SERVICE.name());
	}
	
	public Long getIconicInvestorId() {
		return iconicInvestorId;
	}

	public void setIconicInvestorId(Long iconicInvestorId) {
		this.iconicInvestorId = iconicInvestorId;
	}

	public String getNameFromService() {
		return nameFromService;
	}

	public void setNameFromService(String nameFromService) {
		this.nameFromService = DefaultConstants.replaceTabsAndMultpleSapces(nameFromService);
	}

	public String getService() {
		return service;
	}

	public void setService(String service) {
		this.service = service;
	}

	@Override
	public Entity buildEntity() {
		Entity entity = createEntity(TABLE_NAME);
		entity.setProperty(InvestorAliasNameEnum.ICONIC_INVESTOR_ID.name(), getIconicInvestorId());
		entity.setProperty(InvestorAliasNameEnum.NAME_FROM_SERVICE.name(), getNameFromService());
		entity.setProperty(InvestorAliasNameEnum.REST_SERVICE.name(), getService());		
		return entity;
	}

	
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder("[");
		sb.append("iconicInvestorId  : ");
		sb.append(iconicInvestorId);
		sb.append(", nameFromService : ");
		sb.append(nameFromService);
		sb.append(", sourceService : ");
		sb.append(service);
		sb.append("]");
		
		return sb.toString();
	}
}
