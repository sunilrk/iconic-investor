package com.org.iconic.entity;

import org.springframework.util.StringUtils;

import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;

public abstract class AbstractIconicEntity implements IconicEntity {

	private Long id;

	public AbstractIconicEntity(Entity entity) {
		if (entity != null) {
			this.id = entity.getKey().getId();
		}
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getActualValue(String orgValue, String updateValue) {
		return (StringUtils.isEmpty(updateValue)) ? orgValue : updateValue;
	}

	public Long getActualValue(Long orgValue, Long updateValue) {
		return (updateValue == null) ? orgValue : updateValue;
	}

	public Double getActualValue(Double orgValue, Double updateValue) {
		return (updateValue == null) ? orgValue : updateValue;
	}

	public Entity createEntity(String tableName) {
		Entity entity = null;

		if (getId() != null) {
			Key key = KeyFactory.createKey(tableName, getId());
			entity = new Entity(key);
		} else {
			entity = new Entity(tableName);
		}

		return entity;
	}
}
