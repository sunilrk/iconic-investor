package com.org.iconic.entity;

import java.util.Date;
import java.util.Objects;

import com.google.appengine.api.datastore.Entity;
import com.org.iconic.entity.enums.AggregateInfoEnum;

/**
 * 
 * Entity Object for table STOCK_INVESTOR_STOCKS for month wise
 *
 */
public class MonthAggregates extends MasterAggregate {

	public static final String TABLE_NAME = "IC_STOCK_INVESTOR_STOCKS";

	private String monthAndYear;

	public MonthAggregates() {
		super();
		modified = true;
		updatedDate = new Date();
	}

	public MonthAggregates(Entity entity) {
		super(entity);
		monthAndYear = (String) entity.getProperty(AggregateInfoEnum.MONTH_YEAR.name());
	}

	public String getAggMonth() {
		return monthAndYear;
	}

	public void setAggMonth(String aggMonth) {
		this.monthAndYear = aggMonth;
	}

	@Override
	public Entity buildEntity() {

		Entity entity = createEntity(TABLE_NAME);
		buildEntity(entity);
		entity.setProperty(AggregateInfoEnum.MONTH_YEAR.name(), monthAndYear);

		return entity;
	}

	@Override
	public String toString() {

		StringBuilder sb = converToString();
		sb.append(", monthAndYear : ");
		sb.append(monthAndYear);
		sb.append("]");

		return sb.toString();
	}

	@Override
	public int hashCode() {
		return Objects.hash(super.hashCode(), this.monthAndYear);
	}

	@Override
	public boolean equals(Object obj) {
		boolean isEqual = super.equals(obj);
		if (isEqual && !this.monthAndYear.equalsIgnoreCase(((MonthAggregates) obj).getAggMonth())) {
			isEqual = false;
		}
		return isEqual;
	}

}
