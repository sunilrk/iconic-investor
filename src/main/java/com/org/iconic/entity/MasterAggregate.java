package com.org.iconic.entity;

import java.util.Date;
import java.util.Objects;

import com.google.appengine.api.datastore.Entity;
import com.org.iconic.entity.enums.AggregateInfoEnum;

/**
 * 
 * Entity Object for table STOCK_INVESTOR_STOCKS
 *
 */
public class MasterAggregate extends AbstractAggregate {

	public static final String TABLE_NAME = "IC_MASTER_STOCK_INVESTOR_AGGREGATES";

	private Long iconicInvestorId;
	private Long stockId;
	private Double aggPrice;
	private Long totalUnits;
	protected Date updatedDate;
	protected boolean modified;

	private Double totalPrice = 0.0;

	public MasterAggregate() {
		super();
		modified = true;
		updatedDate = new Date();
	}

	public MasterAggregate(Entity entity) {
		super(entity);
		iconicInvestorId = (Long) entity.getProperty(AggregateInfoEnum.ICONIC_INVESTOR_ID.name());
		stockId = (Long) entity.getProperty(AggregateInfoEnum.STOCK_ID.name());
		aggPrice = (Double) entity.getProperty(AggregateInfoEnum.AGGREATED_PRICE.name());
		totalUnits = (Long) entity.getProperty(AggregateInfoEnum.TOTAL_UNITS.name());
		updatedDate = (Date) entity.getProperty(AggregateInfoEnum.UPDATED_DATE.name());
	}

	public void update(Double price, Long units, boolean isBuy) {

		if (isBuy) {
			totalPrice = (aggPrice * totalUnits) + (price * units);
			totalUnits += units;
		} else {
			totalPrice = (aggPrice * totalUnits) - (price * units);
			totalUnits -= units;
		}

		if (totalUnits == 0) {
			this.aggPrice = 0.0;
		} else {
			this.aggPrice = totalPrice / totalUnits;
		}
		this.updatedDate = new Date();
		this.modified = true;
	}

	public boolean isModified() {
		return modified;
	}

	public void resetModified() {
		this.modified = false;
	}

	public Long getIconicInvestorId() {
		return iconicInvestorId;
	}

	public void setIconicInvestorId(Long iconicInvestorId) {
		this.iconicInvestorId = iconicInvestorId;
	}

	public Long getStockId() {
		return stockId;
	}

	public void setStockId(Long stockId) {
		this.stockId = stockId;
	}

	public Double getAggPrice() {
		return aggPrice;
	}

	public void setAggPrice(Double aggPrice) {
		this.aggPrice = aggPrice;
	}

	public Long getTotalUnits() {
		return totalUnits;
	}

	public void setTotalUnits(Long units) {
		this.totalUnits = units;
	}

	public Date getUpdatedDate() {
		return updatedDate;
	}

	public String getAggMonth() {
		return "";
	}

	@Override
	public Entity buildEntity() {

		Entity entity = createEntity(TABLE_NAME);

		buildEntity(entity);

		return entity;
	}

	protected void buildEntity(Entity entity) {
		entity.setProperty(AggregateInfoEnum.ICONIC_INVESTOR_ID.name(), iconicInvestorId);
		entity.setProperty(AggregateInfoEnum.STOCK_ID.name(), stockId);
		entity.setProperty(AggregateInfoEnum.AGGREATED_PRICE.name(), aggPrice);
		entity.setProperty(AggregateInfoEnum.TOTAL_UNITS.name(), totalUnits);
		entity.setProperty(AggregateInfoEnum.UPDATED_DATE.name(), new Date());

	}

	@Override
	public String toString() {
		StringBuilder sb = converToString();
		sb.append("]");

		return sb.toString();
	}

	protected StringBuilder converToString() {

		StringBuilder sb = new StringBuilder("[");

		sb.append("iconicInvestorId  : ");
		sb.append(iconicInvestorId);
		sb.append(", stockId : ");
		sb.append(stockId);
		sb.append(", aggPrice : ");
		sb.append(aggPrice);
		sb.append(", totalUnits : ");
		sb.append(totalUnits);
		sb.append(", updatedDate : ");
		sb.append(updatedDate);

		return sb;
	}

	@Override
	public int hashCode() {
		return Objects.hash(this.iconicInvestorId.longValue(), this.stockId.longValue());
	}

	@Override
	public boolean equals(Object obj) {

		boolean isEqual = true;

		if (!(obj instanceof MasterAggregate)) {
			isEqual = false;
		} else {
			MasterAggregate other = (MasterAggregate) obj;
			if ((this.iconicInvestorId.longValue() != other.getIconicInvestorId().longValue())
					|| (this.stockId.longValue() != other.getStockId().longValue())) {
				isEqual = false;
			}
		}

		return isEqual;
	}
}
