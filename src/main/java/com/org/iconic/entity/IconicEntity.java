package com.org.iconic.entity;

import com.google.appengine.api.datastore.Entity;

public interface IconicEntity {

	Entity buildEntity();

}
