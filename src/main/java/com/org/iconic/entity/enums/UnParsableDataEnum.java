package com.org.iconic.entity.enums;

public enum UnParsableDataEnum {

	ID,
	DATA,
	SOURCE_URL;
}
