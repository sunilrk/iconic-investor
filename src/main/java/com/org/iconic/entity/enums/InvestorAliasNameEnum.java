package com.org.iconic.entity.enums;

public enum InvestorAliasNameEnum {

	ID,
	ICONIC_INVESTOR_ID,
	NAME_FROM_SERVICE, 
	REST_SERVICE
}
