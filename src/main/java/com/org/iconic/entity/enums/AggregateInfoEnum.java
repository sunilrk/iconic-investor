package com.org.iconic.entity.enums;

public enum AggregateInfoEnum {

	ID,
	ICONIC_INVESTOR_ID,
	STOCK_ID,
	AGGREATED_PRICE,
	MONTH_YEAR,
	TOTAL_UNITS,
	UPDATED_DATE;
}
