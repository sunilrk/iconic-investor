package com.org.iconic.entity.enums;

public enum TransactionEnum {
	ID,
	STOCK_ID,
	ICONIC_INVESTOR_ID,
	TYPE,
	UNITS,
	DATE,
	INVESTOR_ALIAS_NAME_ID,
	ORIGIN,
	PRICE,
	SRC_STOCK_CODE;
}
