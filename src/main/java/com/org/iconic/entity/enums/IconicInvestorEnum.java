package com.org.iconic.entity.enums;

public enum IconicInvestorEnum {

	ID,
	DISPLAY_NAME,
	ACTIVE,
	LOGO_URL,
	SOCIAL_ACCOUNTS,
	EMAIL_ID,
	ABOUT_INVESTOR;
}
