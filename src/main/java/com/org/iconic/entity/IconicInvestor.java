package com.org.iconic.entity;

import com.google.appengine.api.datastore.Entity;
import com.org.iconic.entity.enums.IconicInvestorEnum;

/**
 * 
 * Entity object for table ICONIC_INVESTOR.
 *
 */
public class IconicInvestor extends AbstractIconicEntity {

	public static final String TABLE_NAME = "IC_ICONIC_INVESTOR";

	private String displayName;
	private boolean active = true;
	private String logImageUrl;
	private String socialAccounts;
	private String emailId;
	private String aboutInvestor;

	public IconicInvestor()
	{
		super(null);
	}
			
	public IconicInvestor(Entity entity) {
		super(entity);
		displayName = (String) entity.getProperty(IconicInvestorEnum.DISPLAY_NAME.name());
		active = (boolean) entity.getProperty(IconicInvestorEnum.ACTIVE.name());
		logImageUrl = (String) entity.getProperty(IconicInvestorEnum.LOGO_URL.name());
		socialAccounts = (String) entity.getProperty(IconicInvestorEnum.SOCIAL_ACCOUNTS.name());
		emailId = (String) entity.getProperty(IconicInvestorEnum.EMAIL_ID.name());
		aboutInvestor = (String) entity.getProperty(IconicInvestorEnum.ABOUT_INVESTOR.name());
	}
	
	public void update(IconicInvestor investor)
	{
		this.displayName = getActualValue(this.displayName, investor.getDisplayName());
		this.logImageUrl = getActualValue(this.logImageUrl, investor.getLogoImageUrl());
		this.socialAccounts = getActualValue(this.socialAccounts, investor.getSocialAccounts());
		this.emailId = getActualValue(this.emailId, investor.getEmailId());
		this.aboutInvestor = getActualValue(this.aboutInvestor, investor.getAboutInvestor());		
	}

	public String getDisplayName() {
		return displayName;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public String getLogoImageUrl() {
		return logImageUrl;
	}

	public void setLogoImageUrl(String logImageUrl) {
		this.logImageUrl = logImageUrl;
	}

	public String getSocialAccounts() {
		return socialAccounts;
	}

	public void setSocialAccounts(String socialAccounts) {
		this.socialAccounts = socialAccounts;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public String getAboutInvestor() {
		return aboutInvestor;
	}

	public void setAboutInvestor(String aboutInvestor) {
		this.aboutInvestor = aboutInvestor;
	}

	@Override
	public Entity buildEntity() {

		Entity entity = createEntity(TABLE_NAME);
		
		entity.setProperty(IconicInvestorEnum.DISPLAY_NAME.name(), getDisplayName());
		entity.setProperty(IconicInvestorEnum.ACTIVE.name(), isActive());
		entity.setProperty(IconicInvestorEnum.LOGO_URL.name(), getLogoImageUrl());
		entity.setProperty(IconicInvestorEnum.SOCIAL_ACCOUNTS.name(), getSocialAccounts());
		entity.setProperty(IconicInvestorEnum.EMAIL_ID.name(), getEmailId());
		entity.setProperty(IconicInvestorEnum.ABOUT_INVESTOR.name(), getAboutInvestor());

		return entity;
	}

	@Override
	public String toString() {
		
		StringBuilder sb = new StringBuilder("[");

		sb.append(" displayName : ");
		sb.append(displayName);
		sb.append(", active : ");
		sb.append(active);
		sb.append(", logImageUrl : ");
		sb.append(logImageUrl);
		sb.append(", socialAccounts : ");
		sb.append(socialAccounts);
		sb.append(", emailId : ");
		sb.append(emailId);
		sb.append(", aboutInvestor : ");
		sb.append(aboutInvestor);
		sb.append("]");

		return sb.toString();
	}

	
}
