package com.org.iconic.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.springframework.util.StringUtils;

public class DefaultConstants {
	
	private static final Logger LOGGER = Logger.getLogger(DefaultConstants.class.getName());

	private static final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

	public static final String BSE_INDIA_SERVICE = "BSE";	
	public static final String NSE_INDIA_SERVICE = "NSE";	
	public static final int DEFAULT_AGGREGATED_NO_OF_MONTHS = 120;
	public static final Long DUMMY_ID = -1L;
	
	public static final String DEFAULT_PROFILE_VIEW = "master";
	public static final String MONTH_PROFILE_VIEW = "month";

	
	public static String getStringDate(Date date)
	{
		return DefaultConstants.sdf.format(date);
	}
	
	public static Date convertToDate(String strDate) {
		
		if(StringUtils.isEmpty(strDate))
		{
			return null;
		}
		
		try {
			return sdf.parse(strDate);
		} catch (ParseException e) {
			LOGGER.log(Level.SEVERE, "Unable to parse date : " + strDate, e);
		}

		return null;
	}
	
	public static String replaceTabsAndMultpleSapces(String sText)
    {
		 sText = sText.toLowerCase().trim();
    	 sText = sText.replaceAll("\\t", " ");		
    	 return sText.replaceAll("\\s+", " ");		
    }
}
