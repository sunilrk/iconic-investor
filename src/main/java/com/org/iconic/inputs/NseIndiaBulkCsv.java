package com.org.iconic.inputs;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;


@PropertySource("classpath:application.properties")
@Component("nseIndiaBulkCsv")
public class NseIndiaBulkCsv extends AbstractNseIndiaCsvHeaders {

	@Value("${nse_india_bulk_url}")
	private String baseurl;

	@Value("${nse_bulk_no_of_columns}")
	private int lineLength;
	
	@Value("${nse_bulk_price_col_num}")
	private String priceColumn;
	
	@Override
	public String getURL() {
		return baseurl;
	}
	
	@Override
	public int getLineLength() {
		return lineLength;
	}

	@Override
	public String getPriceColumn() {
		return priceColumn;
	}
}
