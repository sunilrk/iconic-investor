package com.org.iconic.inputs;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;


@PropertySource("classpath:application.properties")
@Component("nseIndiaBlockCsv")
public class NseIndiaBlockCsv extends AbstractNseIndiaCsvHeaders {

	@Value("${nse_india_block_url}")
	private String baseurl;

	@Value("${nse_block_no_of_columns}")
	private int lineLength;
	
	@Value("${nse_block_price_col_num}")
	private String priceColumn;
	
	@Override
	public String getURL() {
		return baseurl;
	}
	
	@Override
	public int getLineLength() {
		return lineLength;
	}
	
	@Override
	public String getPriceColumn() {
		return priceColumn;
	}

}
