package com.org.iconic.inputs;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;


@PropertySource("classpath:application.properties")
@Component("bseIndiaBulkCsv")
public class BseIndiaBulkCsv extends AbstractBseIndiaCsvHeaders {

	@Value("${bse_india_bulk_url}")
	private String baseurl;

	
	@Override
	public String getURL() {
		return getURL(baseurl);
	}

}
