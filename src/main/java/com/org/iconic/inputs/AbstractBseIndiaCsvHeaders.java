package com.org.iconic.inputs;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;

import com.org.iconic.utils.DefaultConstants;

@PropertySource("classpath:application.properties")
public abstract class AbstractBseIndiaCsvHeaders implements CSVHeader {

	@Value("${bse_url_date_format}")
	private String dateFormat;

	@Value("${bse_date_col_num}")
	private String dateColumn;

	@Value("${bse_security_code_col_num}")
	private String securityColumn;

	@Value("${bse_company_col_num}")
	private String companyColumn;

	@Value("${bse_client_name_col_num}")
	private String clientNameColumn;

	@Value("${bse_deal_type_col_num}")
	private String dealTypeColumn;

	@Value("${bse_quantity_col_num}")
	private String quantityColumn;

	@Value("${bse_price_col_num}")
	private String priceColumn;

	@Value("${bse_expected_no_of_columns}")
	private int lineLength;

	@Value("${bse_date_format}")
	private String biDateFormat;

	protected Date customeDate;
	
	protected String getURL(String baseurl) {
		SimpleDateFormat sdf = new SimpleDateFormat(dateFormat);
		String date = sdf.format(getDate());
		return baseurl.replace("<DATE>", date);
	}

	@Override
	public String getServiceNumber() {
		return DefaultConstants.BSE_INDIA_SERVICE;
	}
	

	@Override
	public String getDateFormat() {
		return biDateFormat;
	}

	@Override
	public int getLineLength() {
		return lineLength;
	}

	@Override
	public String getDateColumn() {
		return dateColumn;
	}

	@Override
	public String getSecurityColumn() {
		return securityColumn;
	}

	@Override
	public String getCompanyColumn() {
		return companyColumn;
	}

	@Override
	public String getClientNameColumn() {
		return clientNameColumn;
	}

	@Override
	public String getDealTypeColumn() {
		return dealTypeColumn;
	}

	@Override
	public String getQuantityColumn() {
		return quantityColumn;
	}
	
	@Override
	public String getPriceColumn() {
		return priceColumn;
	}

	public void setCustomDate(Date customeDate)
	{
		this.customeDate = customeDate;
	}
	
	public Date getCustomDate()
	{
		return customeDate;			
	}
}
