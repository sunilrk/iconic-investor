package com.org.iconic.inputs;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;


@PropertySource("classpath:application.properties")
@Component("bseIndiaBlockCsv")
public class BseIndiaBlockCsv extends AbstractBseIndiaCsvHeaders {

	@Value("${bse_india_block_url}")
	private String baseurl;

	
	@Override
	public String getURL() {
		return getURL(baseurl);
	}

}
