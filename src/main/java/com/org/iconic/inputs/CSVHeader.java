package com.org.iconic.inputs;

import java.util.Calendar;
import java.util.Date;

public interface CSVHeader {

	String getServiceNumber();

	String getURL();

	String getDateFormat();

	int getLineLength();

	String getDateColumn();

	String getSecurityColumn();

	String getCompanyColumn();

	String getClientNameColumn();

	String getDealTypeColumn();

	String getQuantityColumn();

	String getPriceColumn();

	Date getCustomDate();

	void setCustomDate(Date customeDate);

	default Date getDate() {
		Date date = null;
		// Check is custom date fixed, if yes use custom date
		if (getCustomDate() != null) {
			date = getCustomDate();
		} else {
			// If not custom date use todays date.
			Calendar cal = Calendar.getInstance();
			cal.setTime(new Date());
			int hours = cal.get(Calendar.HOUR_OF_DAY);
			// if hour is not 6pm then change date one day less
			if (hours < 18 && cal.getTime().toString().contains("IST")) {
				cal.add(Calendar.DAY_OF_MONTH, -1);
			}
			date = cal.getTime();
		}

		System.out.println("Date : " + date);

		return date;
	}
}
