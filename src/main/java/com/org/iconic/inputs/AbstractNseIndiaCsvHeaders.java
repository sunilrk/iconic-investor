package com.org.iconic.inputs;

import java.util.Date;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;

import com.org.iconic.utils.DefaultConstants;

@PropertySource("classpath:application.properties")
public abstract class AbstractNseIndiaCsvHeaders implements CSVHeader {
	
	protected Date customeDate;

	@Value("${nse_url_date_format}")
	private String dateFormat;

	@Value("${nse_date_col_num}")
	private String dateColumn;

	@Value("${nse_security_code_col_num}")
	private String securityColumn;

	@Value("${nse_company_col_num}")
	private String companyColumn;

	@Value("${nse_client_name_col_num}")
	private String clientNameColumn;

	@Value("${nse_deal_type_col_num}")
	private String dealTypeColumn;

	@Value("${nse_quantity_col_num}")
	private String quantityColumn;



	@Value("${nse_date_format}")
	private String biDateFormat;

	@Override
	public String getServiceNumber() {
		return DefaultConstants.NSE_INDIA_SERVICE;
	}
	
	@Override
	public String getDateFormat() {
		return biDateFormat;
	}

	@Override
	public String getDateColumn() {
		return dateColumn;
	}

	@Override
	public String getSecurityColumn() {
		return securityColumn;
	}

	@Override
	public String getCompanyColumn() {
		return companyColumn;
	}

	@Override
	public String getClientNameColumn() {
		return clientNameColumn;
	}

	@Override
	public String getDealTypeColumn() {
		return dealTypeColumn;
	}

	@Override
	public String getQuantityColumn() {
		return quantityColumn;
	}
		
	public Date getCustomDate()
	{
		return customeDate;			
	}
	
	public void setCustomDate(Date customeDate)
	{
		this.customeDate = customeDate;
	}
	
}
