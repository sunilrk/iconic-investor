package com.org.iconic.rest.client.model;

import java.util.List;

public class Entities {

	private List<Object> hashtags = null;
	private List<Object> symbols = null;
	private List<UserMention> userMentions = null;
	private List<Object> urls = null;

	public List<Object> getHashtags() {
		return hashtags;
	}

	public void setHashtags(List<Object> hashtags) {
		this.hashtags = hashtags;
	}

	public List<Object> getSymbols() {
		return symbols;
	}

	public void setSymbols(List<Object> symbols) {
		this.symbols = symbols;
	}

	public List<UserMention> getUserMentions() {
		return userMentions;
	}

	public void setUserMentions(List<UserMention> userMentions) {
		this.userMentions = userMentions;
	}

	public List<Object> getUrls() {
		return urls;
	}

	public void setUrls(List<Object> urls) {
		this.urls = urls;
	}

}