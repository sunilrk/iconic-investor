package com.org.iconic.rest.client;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.org.iconic.dao.UnParsableDataDao;
import com.org.iconic.entity.Transaction;
import com.org.iconic.inputs.BseIndiaBlockCsv;
import com.org.iconic.inputs.BseIndiaBulkCsv;
import com.org.iconic.inputs.CSVHeader;
import com.org.iconic.inputs.NseIndiaBlockCsv;
import com.org.iconic.inputs.NseIndiaBulkCsv;
import com.org.iconic.logic.CsvReaderAsExcel;
import com.org.iconic.logic.InputTransactionsWriter;
import com.org.iconic.rest.client.model.RestClinetTransaction;
import com.org.iconic.rest.services.helper.RestResponse;
import com.org.iconic.rest.services.helper.RestResponseBuilder;
import com.org.iconic.utils.DefaultConstants;

/**
 * Rest Apis to fetch transaction - through cron jobs - for a custom date - to
 * upload back dated transactions using csv file
 *
 */
@PropertySource("classpath:application.properties")
@RestController
public class InvestorRestClient {

	private final Logger LOGGER = Logger.getLogger(InvestorRestClient.class.getName());

	@Autowired
	CsvReaderAsExcel csvReaderAsExcel;

	@Autowired
	BseIndiaBulkCsv bseIndiaBulkCsv;

	@Autowired
	BseIndiaBlockCsv bseIndiaBlockCsv;

	@Autowired
	NseIndiaBulkCsv nseIndiaBulkCsv;

	@Autowired
	NseIndiaBlockCsv nseIndiaBlockCsv;

	@Autowired
	InputTransactionsWriter transactionsWriter;

	@Autowired
	UnParsableDataDao unParsableDataDao;

	@Value("${run_corn_job}")
	private boolean doRun;

	private String toDay = null;

	/**
	 * Rest api to fetch transactions for a custom date
	 * 
	 * /customtransactions?data=2018-03-20
	 * 
	 * Take input parameter date in format yyyy-MM-dd
	 * 
	 * @param strDate
	 */
	@RequestMapping("/customtransactions")
	public String schdulerJob(@RequestParam("date") String strDate) {

		Date date = DefaultConstants.convertToDate(strDate);

		if (date == null) {
			return "Date is missing or Date is not expected format (yyyy-MM-dd) ex 2018-03-20";
		}

		LOGGER.log(Level.WARNING, "Date from request " + date);

		// Set custom date for all
		bseIndiaBulkCsv.setCustomDate(date);
		bseIndiaBlockCsv.setCustomDate(date);
		nseIndiaBulkCsv.setCustomDate(date);
		nseIndiaBlockCsv.setCustomDate(date);
		toDay = null;

		// call to get the data
		schdulerJob();

		// Set custom date to null
		bseIndiaBulkCsv.setCustomDate(null);
		bseIndiaBlockCsv.setCustomDate(null);
		nseIndiaBulkCsv.setCustomDate(null);
		nseIndiaBlockCsv.setCustomDate(null);

		return "Fetching data for the date " + strDate + " completed";
	}

	/**
	 * Rest API which will call from cron job
	 * 
	 * refer cron.xml
	 */
	@RequestMapping("/cron/pulltransactions")
	public void schdulerJob() {
		System.out.println("Started to get transaction");

		if (doRun) {

			// Make sure this should run only once per day
			if (toDay == null || !toDay.equalsIgnoreCase(DefaultConstants.getStringDate(bseIndiaBulkCsv.getDate()))) {

				LOGGER.log(Level.INFO, "Started to get transaction from BSE INDIA Bulk");
				process(bseIndiaBulkCsv);
				LOGGER.log(Level.INFO, "Completed to get transaction from BSE INDIA Bulk");

				LOGGER.log(Level.INFO, "Started to get transaction from BSE INDIA Block");
				process(bseIndiaBlockCsv);
				LOGGER.log(Level.INFO, "Completed to get transaction from BSE india Block");

				LOGGER.log(Level.INFO, "Started to get transaction from NSE INDIA Bulk");
				process(nseIndiaBulkCsv);
				LOGGER.log(Level.INFO, "Completed to get transaction from NSE INDIA Bulk");

				LOGGER.log(Level.INFO, "Started to get transaction from NSE INDIA Block");
				process(nseIndiaBlockCsv);
				LOGGER.log(Level.INFO, "Completed to get transaction from NSE INDIA Block");

				// update today
				toDay = DefaultConstants.getStringDate(bseIndiaBulkCsv.getDate());

			} else {
				LOGGER.log(Level.WARNING, "Transaction grabbed already for the day : " + toDay);
			}
		}
		LOGGER.log(Level.WARNING, "Completed to get transaction");
		System.out.println("End to get transaction");
	}

	private void process(CSVHeader csvHeader) {
		ArrayList<RestClinetTransaction> transactions = null;
		try {
			transactions = csvReaderAsExcel.readCsvFile(csvHeader);
		} catch (Exception e) {
			LOGGER.log(Level.SEVERE, "No transactions saved for " + csvHeader.getURL());
			return;
		}
		transactionsWriter.write(csvHeader.getServiceNumber(), transactions);

		// Write unparsable data if any
		if (!csvReaderAsExcel.getUnParsableList().isEmpty()) {
			unParsableDataDao.save(csvReaderAsExcel.getUnParsableList());
		}

	}

	/**
	 * Upload transactions by using csv file following are post request
	 * parameters
	 * 
	 * @param source
	 *            - NSE/BSE
	 * @param type
	 *            - Bulk/Block
	 * @param csvfile
	 *            - a csv file
	 * @return
	 */
	@RequestMapping(value = "/upload_transactions", method = RequestMethod.POST)
	public @ResponseBody RestResponse uploadTransactions(@RequestParam("source") String source,
			@RequestParam("type") String type, @RequestParam("csvfile") MultipartFile csvfile) {

		List<Transaction> transactions = null;
		RestResponse response = RestResponseBuilder.success("Data Uploaded successfully");

		if (csvfile != null && !csvfile.isEmpty()) {
			InputStream stream;
			try {
				stream = csvfile.getInputStream();
			} catch (IOException e) {
				return RestResponseBuilder.badRequest("Unable read csv file as stream");
			}

			try {
				if (source.equalsIgnoreCase(DefaultConstants.NSE_INDIA_SERVICE)) {

					if (type.equalsIgnoreCase("Block")) {
						transactions = processCsvFile(nseIndiaBlockCsv, stream);
					} else if (type.equalsIgnoreCase("Bulk")) {
						transactions = processCsvFile(nseIndiaBulkCsv, stream);

					} else {
						response = RestResponseBuilder.badRequest("Unrecognized Type, Expected : Block or Bulk");
					}
				} else if (source.equalsIgnoreCase(DefaultConstants.BSE_INDIA_SERVICE)) {
					if (type.equalsIgnoreCase("Block")) {
						transactions = processCsvFile(bseIndiaBlockCsv, stream);
					} else if (type.equalsIgnoreCase("Bulk")) {
						transactions = processCsvFile(bseIndiaBulkCsv, stream);
					} else {
						response = RestResponseBuilder.badRequest("Unrecognized Type, Expected : Block or Bulk");
					}
				} else {
					response = RestResponseBuilder.badRequest("Unrecognized source, Expected : "
							+ DefaultConstants.NSE_INDIA_SERVICE + " or " + DefaultConstants.BSE_INDIA_SERVICE);
				}

			} catch (Exception e) {
				return RestResponseBuilder.badRequest("Error got : " + e.getMessage());
			}

		} else {
			response = RestResponseBuilder.badRequest("Missing Required csv file");
		}

		if (transactions == null || transactions.isEmpty()) {
			response = RestResponseBuilder.exceptionFailed("Unable to save transaction, some error got");
		}

		return response;
	}

	private List<Transaction> processCsvFile(CSVHeader csvHeader, InputStream stream) throws Exception {
		ArrayList<RestClinetTransaction> transactions = null;
		try {
			transactions = csvReaderAsExcel.prepareTransactioins(csvHeader, stream, false);
		} catch (Exception e) {
			LOGGER.log(Level.SEVERE, "No transactions saved for " + csvHeader.getURL());
			throw e;
		}
		List<Transaction> dbtrans = transactionsWriter.write(csvHeader.getServiceNumber(), transactions);

		// Write unparsable data if any
		if (!csvReaderAsExcel.getUnParsableList().isEmpty()) {
			unParsableDataDao.save(csvReaderAsExcel.getUnParsableList());
		}
		return dbtrans;
	}

}
