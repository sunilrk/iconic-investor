package com.org.iconic.rest.client.model;

public class CurrentStockPriceWrapper extends CurrentStockPrice {
	private Long scriptCodeId;
	private String scriptCode;
	private String companyName;

	public CurrentStockPriceWrapper(CurrentStockPrice curStockPrice, Long stockId, String stockName, String companyName) {
		this.setChange(curStockPrice.getChange());
		this.setChangeper(curStockPrice.getChangeper());
		this.setDate(curStockPrice.getDate());
		this.setHigh(curStockPrice.getHigh());
		this.setLast(curStockPrice.getLast());
		this.setLow(curStockPrice.getLow());
		this.setOpen(curStockPrice.getOpen());
		this.setPrev(curStockPrice.getPrev());
		this.setScriptCode(stockName);
		this.setScriptCodeId(stockId);
		this.setVol(curStockPrice.getVol());
		this.setCompanyName(companyName);
	}

	public CurrentStockPriceWrapper(Long stockId, String stockName, String companyName) {
		this.setScriptCode(stockName);
		this.setScriptCodeId(stockId);
		this.setCompanyName(companyName);
	}

	/**
	 * @return the scriptCodeId
	 */
	public Long getScriptCodeId() {
		return scriptCodeId;
	}

	/**
	 * @param stockId
	 *            the scriptCodeId to set
	 */
	public void setScriptCodeId(Long stockId) {
		this.scriptCodeId = stockId;
	}

	/**
	 * @return the scriptCode
	 */
	public String getScriptCode() {
		return scriptCode;
	}

	/**
	 * @param scriptCode
	 *            the scriptCode to set
	 */
	public void setScriptCode(String scriptCode) {
		this.scriptCode = scriptCode;
	}

	/**
	 * @return the companyName
	 */
	public String getCompanyName() {
		return companyName;
	}

	/**
	 * @param companyName the companyName to set
	 */
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

}
