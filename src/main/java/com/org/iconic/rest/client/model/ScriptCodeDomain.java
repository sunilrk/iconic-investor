package com.org.iconic.rest.client.model;

import java.util.Date;

/**
 * @author sunil.r
 *
 */
public class ScriptCodeDomain {

	private Long scriptCodeId;

	private String nseCode;

	private String bseCode;

	private String companyName;

	private String group;

	private String faceValue;

	private String iSIN;

	private String industy;

	private String firstListingDate;

	private String status;

	private Date created;

	private Date updatedAt;

	/**
	 * 
	 */
	public ScriptCodeDomain() {
		super();
	}

	/**
	 * @param scriptCodeId
	 * @param nseCode
	 * @param bseCode
	 * @param companyName
	 * @param group
	 * @param faceValue
	 * @param iSIN
	 * @param industy
	 * @param firstListingDate
	 * @param status
	 * @param created
	 * @param updatedAt
	 */
	public ScriptCodeDomain(Long scriptCodeId, String nseCode, String bseCode, String companyName, String group,
			String faceValue, String iSIN, String industy, String firstListingDate, String status, Date created,
			Date updatedAt) {
		super();
		this.scriptCodeId = scriptCodeId;
		this.nseCode = nseCode == null ? "" :  nseCode;
		this.bseCode = bseCode == null ? "" :  bseCode;
		this.companyName = companyName;
		this.group = group;
		this.faceValue = faceValue;
		this.iSIN = iSIN;
		this.industy = industy;
		this.firstListingDate = firstListingDate;
		this.status = status;
		this.created = created;
		this.updatedAt = updatedAt;
	}

	/**
	 * @return the nseCode
	 */
	public String getNseCode() {
		return nseCode;
	}

	/**
	 * @param nseCode
	 *            the nseCode to set
	 */
	public void setNseCode(String nseCode) {
		this.nseCode = nseCode;
	}

	/**
	 * @return the bseCode
	 */
	public String getBseCode() {
		return bseCode;
	}

	/**
	 * @param bseCode
	 *            the bseCode to set
	 */
	public void setBseCode(String bseCode) {
		this.bseCode = bseCode;
	}

	/**
	 * @return the companyName
	 */
	public String getCompanyName() {
		return companyName;
	}

	/**
	 * @param companyName
	 *            the companyName to set
	 */
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	/**
	 * @return the group
	 */
	public String getGroup() {
		return group;
	}

	/**
	 * @param group
	 *            the group to set
	 */
	public void setGroup(String group) {
		this.group = group;
	}

	/**
	 * @return the faceValue
	 */
	public String getFaceValue() {
		return faceValue;
	}

	/**
	 * @param faceValue
	 *            the faceValue to set
	 */
	public void setFaceValue(String faceValue) {
		this.faceValue = faceValue;
	}

	/**
	 * @return the iSIN
	 */
	public String getiSIN() {
		return iSIN;
	}

	/**
	 * @param iSIN
	 *            the iSIN to set
	 */
	public void setiSIN(String iSIN) {
		this.iSIN = iSIN;
	}

	/**
	 * @return the industy
	 */
	public String getIndusty() {
		return industy;
	}

	/**
	 * @param industy
	 *            the industy to set
	 */
	public void setIndusty(String industy) {
		this.industy = industy;
	}

	/**
	 * @return the firstListingDate
	 */
	public String getFirstListingDate() {
		return firstListingDate;
	}

	/**
	 * @param firstListingDate
	 *            the firstListingDate to set
	 */
	public void setFirstListingDate(String firstListingDate) {
		this.firstListingDate = firstListingDate;
	}

	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * @param status
	 *            the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * @return the scriptCodeId
	 */
	public Long getScriptCodeId() {
		return scriptCodeId;
	}

	/**
	 * @return the created
	 */
	public Date getCreated() {
		return created;
	}

	/**
	 * @param created
	 *            the created to set
	 */
	public void setCreated(Date created) {
		this.created = created;
	}

	/**
	 * @return the updatedAt
	 */
	public Date getUpdatedAt() {
		return updatedAt;
	}

	/**
	 * @param updatedAt
	 *            the updatedAt to set
	 */
	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}

}
