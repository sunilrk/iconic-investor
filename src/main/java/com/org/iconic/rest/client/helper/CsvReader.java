package com.org.iconic.rest.client.helper;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.springframework.stereotype.Component;

import com.org.iconic.rest.client.model.RestClinetTransaction;

@Component
public class CsvReader {

	private static final Logger LOGGER = Logger.getLogger(CsvReader.class.getName());
	private int dateCol, securityCol, companyCol, clientNameCol, dealTypeCol, quantityCol, priceCol, lineLength;
	private String dateFormt;

	private Pattern ptn = Pattern.compile(",\\s*,");
	
	public CsvReader() {

	}

	public void setDateFormt(String dateFormt) {
		this.dateFormt = dateFormt;
	}

	public void setDateCol(int dateCol) {
		this.dateCol = dateCol;
	}

	public void setSecurityCol(int securityCol) {
		this.securityCol = securityCol;
	}

	public void setCompanyCol(int companyCol) {
		this.companyCol = companyCol;
	}

	public void setClientNameCol(int clientNameCol) {
		this.clientNameCol = clientNameCol;
	}

	public void setDealTypeCol(int dealTypeCol) {
		this.dealTypeCol = dealTypeCol;
	}

	public void setQuantityCol(int quantityCol) {
		this.quantityCol = quantityCol;
	}

	public void setPriceCol(int priceCol) {
		this.priceCol = priceCol;
	}

	public void setLineLength(int lineLength) {
		this.lineLength = lineLength;
	}

	public ArrayList<RestClinetTransaction> readCsvFile(InputStream inputStream) throws Exception {
		ArrayList<RestClinetTransaction> transactions = new ArrayList<>();
		BufferedReader br = null;
		String line = "";
		String formatedLine = "";
		String cvsSplitBy = ",";
		RestClinetTransaction transaction;
		SimpleDateFormat format = new SimpleDateFormat(dateFormt);
		String[] data = null;
		try {

			br = new BufferedReader(new InputStreamReader(inputStream));
			br.readLine();
			while ((line = br.readLine()) != null) {

				// replace more than one comma side by side with single comma
				formatedLine = ptn.matcher(line).replaceAll(",");
				// use comma as separator
				data = formatedLine.split(cvsSplitBy);
				if (data.length == lineLength) {
					transaction = new RestClinetTransaction();
					transaction.setTransDate(convertToDate(data[dateCol], format));
					transaction.setSecuriyCode(data[securityCol]);
					transaction.setCompany(data[companyCol]);
					transaction.setClientName(data[clientNameCol]);
					transaction.setTransType(data[dealTypeCol]);
					transaction.setQuantity(convertTolong(data[quantityCol]));
					transaction.setPrice(convertToDouble(data[priceCol]));
					transactions.add(transaction);
				} else {
					LOGGER.log(Level.SEVERE, "Missing or un expected information in Transaction Details : "+ line);
				}
			}

		} catch (Exception e) {
			LOGGER.log(Level.SEVERE, "Missing information in Transaction Details : "+ line);
			throw e;
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}

		return transactions;
	}

	private double convertToDouble(String strValue) throws Exception {
		double value = 0.0;
		try {
			value = Double.parseDouble(strValue);
		} catch (NumberFormatException e) {
			LOGGER.log(Level.SEVERE, "Unable to convert string "+strValue+" to double");
			throw e;
		}

		return value;
	}

	private long convertTolong(String strValue) throws Exception {
		long value = 0;
		try {
			value = Long.parseLong(strValue);
		} catch (NumberFormatException e) {
			LOGGER.log(Level.SEVERE, "Unable to convert string "+strValue+" to long");
			throw e;
		}

		return value;
	}

	private Date convertToDate(String strDate, SimpleDateFormat format) throws Exception {
		Date date = null;

		try {
			date = format.parse(strDate);
		} catch (ParseException e) {
			LOGGER.log(Level.SEVERE, "Unable to convert string "+strDate+" to date");
			throw e;
		}

		return date;
	}
}
