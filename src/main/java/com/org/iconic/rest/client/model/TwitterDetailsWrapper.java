package com.org.iconic.rest.client.model;

public class TwitterDetailsWrapper {
	TwitterUserDetails[] array ;

	/**
	 * @return the array
	 */
	public TwitterUserDetails[] getArray() {
		return array;
	}

	/**
	 * @param array the array to set
	 */
	public void setArray(TwitterUserDetails[] array) {
		this.array = array;
	}

	

}
