package com.org.iconic.rest.client.model;

import java.math.BigInteger;

public class TwitterUserDetails {

	private String createdAt;
	private BigInteger id;
	private String idStr;
	private String text;
	private Boolean truncated;
	private Entities entities;
	private String source;
	private Integer inReplyToStatusId;
	private String inReplyToStatusIdStr;
	private Integer inReplyToUserId;
	private String inReplyToUserIdStr;
	private String inReplyToScreenName;
	private User user;
	private Object geo;
	private Object coordinates;
	private Object place;
	private Object contributors;
	private Boolean isQuoteStatus;
	private Integer retweetCount;
	private Integer favoriteCount;
	private Boolean favorited;
	private Boolean retweeted;
	private String lang;

	public String getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(String createdAt) {
		this.createdAt = createdAt;
	}

	public BigInteger getId() {
		return id;
	}

	public void setId(BigInteger id) {
		this.id = id;
	}

	public String getIdStr() {
		return idStr;
	}

	public void setIdStr(String idStr) {
		this.idStr = idStr;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public Boolean getTruncated() {
		return truncated;
	}

	public void setTruncated(Boolean truncated) {
		this.truncated = truncated;
	}

	public Entities getEntities() {
		return entities;
	}

	public void setEntities(Entities entities) {
		this.entities = entities;
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public Integer getInReplyToStatusId() {
		return inReplyToStatusId;
	}

	public void setInReplyToStatusId(Integer inReplyToStatusId) {
		this.inReplyToStatusId = inReplyToStatusId;
	}

	public String getInReplyToStatusIdStr() {
		return inReplyToStatusIdStr;
	}

	public void setInReplyToStatusIdStr(String inReplyToStatusIdStr) {
		this.inReplyToStatusIdStr = inReplyToStatusIdStr;
	}

	public Integer getInReplyToUserId() {
		return inReplyToUserId;
	}

	public void setInReplyToUserId(Integer inReplyToUserId) {
		this.inReplyToUserId = inReplyToUserId;
	}

	public String getInReplyToUserIdStr() {
		return inReplyToUserIdStr;
	}

	public void setInReplyToUserIdStr(String inReplyToUserIdStr) {
		this.inReplyToUserIdStr = inReplyToUserIdStr;
	}

	public String getInReplyToScreenName() {
		return inReplyToScreenName;
	}

	public void setInReplyToScreenName(String inReplyToScreenName) {
		this.inReplyToScreenName = inReplyToScreenName;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Object getGeo() {
		return geo;
	}

	public void setGeo(Object geo) {
		this.geo = geo;
	}

	public Object getCoordinates() {
		return coordinates;
	}

	public void setCoordinates(Object coordinates) {
		this.coordinates = coordinates;
	}

	public Object getPlace() {
		return place;
	}

	public void setPlace(Object place) {
		this.place = place;
	}

	public Object getContributors() {
		return contributors;
	}

	public void setContributors(Object contributors) {
		this.contributors = contributors;
	}

	public Boolean getIsQuoteStatus() {
		return isQuoteStatus;
	}

	public void setIsQuoteStatus(Boolean isQuoteStatus) {
		this.isQuoteStatus = isQuoteStatus;
	}

	public Integer getRetweetCount() {
		return retweetCount;
	}

	public void setRetweetCount(Integer retweetCount) {
		this.retweetCount = retweetCount;
	}

	public Integer getFavoriteCount() {
		return favoriteCount;
	}

	public void setFavoriteCount(Integer favoriteCount) {
		this.favoriteCount = favoriteCount;
	}

	public Boolean getFavorited() {
		return favorited;
	}

	public void setFavorited(Boolean favorited) {
		this.favorited = favorited;
	}

	public Boolean getRetweeted() {
		return retweeted;
	}

	public void setRetweeted(Boolean retweeted) {
		this.retweeted = retweeted;
	}

	public String getLang() {
		return lang;
	}

	public void setLang(String lang) {
		this.lang = lang;
	}

}