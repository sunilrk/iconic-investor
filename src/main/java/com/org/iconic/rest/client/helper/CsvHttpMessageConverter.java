package com.org.iconic.rest.client.helper;

import java.io.IOException;
import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpInputMessage;
import org.springframework.http.HttpOutputMessage;
import org.springframework.http.MediaType;
import org.springframework.http.converter.AbstractHttpMessageConverter;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.http.converter.HttpMessageNotWritableException;
import org.springframework.stereotype.Component;

@Component
public class CsvHttpMessageConverter extends AbstractHttpMessageConverter<ArrayList> {
	@Autowired
	private CsvReader csvReader;

	public CsvHttpMessageConverter() {
		super(new MediaType("text", "palin"));
	}

	@Override
	public boolean canRead(Class<?> clazz, MediaType mediaType) {
		if (clazz.getName().equals("java.util.ArrayList"))
			return true;

		return false;
	}

	@Override
	protected boolean supports(Class<?> clazz) {
		return ArrayList.class.isAssignableFrom(clazz);
	}

	@Override
	protected ArrayList readInternal(Class<? extends ArrayList> clazz, HttpInputMessage inputMessage)
			throws IOException, HttpMessageNotReadableException {
		try {
			return csvReader.readCsvFile(inputMessage.getBody());
		} catch (Exception e) {
			throw new HttpMessageNotReadableException("Unable to read data ", e);
		}
	}

	@Override
	protected void writeInternal(ArrayList t, HttpOutputMessage outputMessage)
			throws IOException, HttpMessageNotWritableException {
	}

}