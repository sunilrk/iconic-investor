package com.org.iconic.rest.client.model;

import java.util.Date;

public class RestClinetTransaction {

	private Date transDate;
	private String securiyCode;
	private String company;
	private String clientName;
	private String transType;
	private long quantity;
	private double price;

	public Date getTransDate() {
		return transDate;
	}

	public void setTransDate(Date transDate) {
		this.transDate = transDate;
	}

	public String getSecuriyCode() {
		return securiyCode;
	}

	public void setSecuriyCode(String securiyCode) {
		this.securiyCode = securiyCode;
	}

	public String getCompany() {
		return company;
	}

	public void setCompany(String company) {
		this.company = company;
	}

	public String getClientName() {
		return clientName;
	}

	public void setClientName(String clientName) {
		if (clientName != null) {
			this.clientName = clientName.toLowerCase().trim();
		}else
		{
			this.clientName = "";
		}
	}

	public String getTransType() {
		return transType;
	}

	public void setTransType(String transType) {
		this.transType = transType;
	}

	public long getQuantity() {
		return quantity;
	}

	public void setQuantity(long quantity) {
		this.quantity = quantity;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

}
