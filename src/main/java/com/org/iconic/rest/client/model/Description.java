package com.org.iconic.rest.client.model;

import java.util.List;

public class Description {

	private List<Object> urls = null;

	public List<Object> getUrls() {
		return urls;
	}

	public void setUrls(List<Object> urls) {
		this.urls = urls;
	}

}