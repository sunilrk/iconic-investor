package com.org.iconic.rest.services.helper;

import org.apache.commons.lang3.StringUtils;

import com.org.iconic.entity.IconicInvestor;
import com.org.iconic.utils.DefaultConstants;

public class RestInput {

	IconicInvestor investor;
	String profileType;

	public IconicInvestor getInvestor() {
		return investor;
	}

	public void setInvestor(IconicInvestor investor) {
		this.investor = investor;
	}

	public String getProfileType() {
		return StringUtils.isEmpty(profileType) ? DefaultConstants.DEFAULT_PROFILE_VIEW : profileType;
	}

	public void setProfileType(String profileType) {
		this.profileType = profileType;
	}
	
}
