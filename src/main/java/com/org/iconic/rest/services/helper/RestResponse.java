package com.org.iconic.rest.services.helper;

import javax.ws.rs.core.Response.Status;

public class RestResponse {

	private Status status;
	private String message;
	private Object data;

	public int getStatus() {
		return status.getStatusCode();
	}

	public void setStatus(Status status) {
		this.status = status;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Object getData() {
		return data;
	}

	public void setData(Object data) {
		this.data = data;
	}

}
