package com.org.iconic.rest.services.helper;

import java.util.Date;
import java.util.logging.Logger;

import org.springframework.context.annotation.PropertySource;

import com.org.iconic.utils.DefaultConstants;

@PropertySource("classpath:application.properties")
public class RestInputForTransaction {


	private Long investorId;
	private Long stockId;
	private String fromDate;
	private String toDate;
	private int lastNTransaction;

	public Long getInvestorId() {
		return investorId;
	}

	public void setInvestorId(Long investorId) {
		this.investorId = investorId;
	}

	public String getFromDate() {
		return fromDate;
	}

	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}

	public String getToDate() {
		return toDate;
	}

	public void setToDate(String toDate) {
		this.toDate = toDate;
	}

	public Date getFormatedFromDate() {
		return DefaultConstants.convertToDate(fromDate);
	}

	public Date getFormatedToDate() {
		return DefaultConstants.convertToDate(toDate);
	}
	
	public int getLastNTransaction() {
		return lastNTransaction;
	}

	public void setLastNTransaction(int lastNTransaction) {
		this.lastNTransaction = lastNTransaction;
	}
	
	public Long getStockId() {
		return stockId;
	}

	public void setStockId(Long stockId) {
		this.stockId = stockId;
	}



}
