package com.org.iconic.rest.services.helper;

import javax.ws.rs.core.Response.Status;

public class RestResponseBuilder {

	public static RestResponse success(Object data)
	{
		RestResponse restResponse = new RestResponse();
		restResponse.setStatus(Status.OK);
		restResponse.setMessage("success");
		restResponse.setData(data);
		
		return restResponse;
	}
	
	public static RestResponse badRequest(String message)
	{
		RestResponse restResponse = new RestResponse();
		restResponse.setStatus(Status.BAD_REQUEST);
		restResponse.setMessage(message);
	
		return restResponse;
	}
	
	public static RestResponse exceptionFailed(String message)
	{
		RestResponse restResponse = new RestResponse();
		restResponse.setStatus(Status.EXPECTATION_FAILED);
		restResponse.setMessage(message);
	
		return restResponse;
	}
	
	public static RestResponse notFound(String message)
	{
		RestResponse restResponse = new RestResponse();
		restResponse.setStatus(Status.NOT_FOUND);
		restResponse.setMessage(message);
	
		return restResponse;
	}
	
	public static RestResponse partialContent(Object data, String message)
	{
		RestResponse restResponse = new RestResponse();
		restResponse.setStatus(Status.PARTIAL_CONTENT);
		restResponse.setMessage(message);
		restResponse.setData(data);

		return restResponse;
	}
}
