package com.org.iconic.rest.services.helper;

import java.util.List;

public class AliasNameInputs {

	private Long investorId;
	private List<String> aliasNames;

	public Long getInvestorId() {
		return investorId;
	}

	public void setInvestorId(Long investorId) {
		this.investorId = investorId;
	}

	public List<String> getAliasNames() {
		return aliasNames;
	}

	public void setAliasNames(List<String> aliasNames) {
		this.aliasNames = aliasNames;
	}

}
