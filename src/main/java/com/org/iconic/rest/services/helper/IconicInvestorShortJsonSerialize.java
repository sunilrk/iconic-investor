package com.org.iconic.rest.services.helper;

import java.lang.reflect.Type;

import org.springframework.stereotype.Component;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import com.org.iconic.entity.IconicInvestor;

@Component("shotJsonserializer")
public class IconicInvestorShortJsonSerialize implements JsonSerializer<IconicInvestor> {

	@Override
	public JsonElement serialize(IconicInvestor src, Type typeOfSrc, JsonSerializationContext context) {
		JsonObject jsonInvestor = new JsonObject();

		jsonInvestor.addProperty("Id", src.getId());
		jsonInvestor.addProperty("name", src.getDisplayName());
		jsonInvestor.addProperty("logoUrl", src.getLogoImageUrl());
		jsonInvestor.addProperty("emailId", src.getEmailId());
		jsonInvestor.addProperty("status", src.isActive());
		
		return jsonInvestor;
	}

}
