package com.org.iconic.rest.services.helper;

import java.util.List;

import com.org.iconic.ui.model.UiTransaction;

public class RestOutputForTransaction {

	private List<UiTransaction> transaction;

	private String status;

	public List<UiTransaction> getTransaction() {
		return transaction;
	}

	public void setTransaction(List<UiTransaction> transaction) {
		this.transaction = transaction;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
	
	
	
}
