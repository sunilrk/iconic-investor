package com.org.iconic.rest.services.helper;

import java.util.List;

import com.org.iconic.entity.IconicInvestor;
import com.org.iconic.ui.model.UiAggregates;
import com.org.iconic.ui.model.UiIconicInvestor;

public class ProfileOutput {

	private UiIconicInvestor investor;

	private List<? extends UiAggregates> aggregates;

	public ProfileOutput() {
	}

	public UiIconicInvestor getInvestor() {
		return investor;
	}

	public void setInvestor(UiIconicInvestor investor) {
		this.investor = investor;
	}
	

	public List<? extends UiAggregates> getAggregates() {
		return aggregates;
	}

	public void setAggregates(List<? extends UiAggregates> aggregates) {
		this.aggregates = aggregates;
	}


}
