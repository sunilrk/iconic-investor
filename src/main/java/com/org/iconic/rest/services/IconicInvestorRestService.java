package com.org.iconic.rest.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.google.appengine.api.datastore.Key;
import com.org.iconic.entity.IconicInvestor;
import com.org.iconic.entity.InvestorAliasName;
import com.org.iconic.logic.AliasNamesAndTransactionsMapper;
import com.org.iconic.logic.GcpImageUploader;
import com.org.iconic.rest.client.model.CurrentStockPriceWrapper;
import com.org.iconic.rest.client.model.TwitterUserDetails;
import com.org.iconic.rest.services.helper.AliasNameInputs;
import com.org.iconic.rest.services.helper.IconicInvestorShortJsonSerialize;
import com.org.iconic.rest.services.helper.ProfileOutput;
import com.org.iconic.rest.services.helper.RestInput;
import com.org.iconic.rest.services.helper.RestInputForTransaction;
import com.org.iconic.rest.services.helper.RestResponse;
import com.org.iconic.rest.services.helper.RestResponseBuilder;
import com.org.iconic.service.IconicInvestorService;
import com.org.iconic.service.InvestorsAggregateService;
import com.org.iconic.service.StockService;
import com.org.iconic.service.TransactionService;
import com.org.iconic.service.TwitterService;
import com.org.iconic.ui.model.UiIconicInvestor;
import com.org.iconic.utils.DefaultConstants;

/**
 * Rest service CURD operation to perform on investors information
 * 
 */
@RestController
@RequestMapping("/investors")
public class IconicInvestorRestService {

	@Autowired
	IconicInvestorService iconicInvestorService;

	@Autowired
	InvestorsAggregateService investorsAggregateService;

	@Autowired
	TransactionService transactionService;

	@Autowired
	AliasNamesAndTransactionsMapper transactionsMapper;

	@Autowired
	IconicInvestorShortJsonSerialize shotJsonserializer;

	@Autowired
	GcpImageUploader gcpImageUploader;
	
	@Autowired
	StockService stockService;
	
	@Autowired
	TwitterService twitterService;

	/**
	 * To get all active investors profile with basic information
	 * 
	 * /investors/list
	 * 
	 * @return
	 */
	@RequestMapping(value = "/list")
	public @ResponseBody RestResponse activeInvestorsList() {

		// GsonBuilder gsonBuilder = new GsonBuilder();
		// gsonBuilder.registerTypeAdapter(IconicInvestor.class,
		// shotJsonserializer);
		// Gson customGson = gsonBuilder.create();
		// String json =
		// customGson.toJson(iconicInvestorService.getAllActiveInvestors());

		return RestResponseBuilder.success(iconicInvestorService.getInvestorsList(true));
	}

	/**
	 * To get an investor profile
	 * 
	 * /investors/profile
	 * 
	 * Input Json Ex:
	 * 
	 * { "investor":{ "id":"4785074604081152" } profileType : "month/master
	 * default is master"}
	 * 
	 * Output Json Ex : { "investor": { "id": 4785074604081152, "displayName":
	 * "TestName4", "active": true, "socialAccounts": "@test", "emailId":
	 * "test@test.com", "aboutInvestor": "bla bla bla", "logoImageUrl":
	 * "http://test/test.img" }, "status": "OK" }
	 * 
	 * 
	 * 
	 * @param restInput
	 * @return
	 */
	@RequestMapping(value = "/profile", method = RequestMethod.POST)
	public @ResponseBody RestResponse investorProfile(@RequestBody RestInput restInput) {

		RestResponse restResponse = null;

		ProfileOutput profileOutput = new ProfileOutput();

		if (restInput == null || restInput.getInvestor() == null || restInput.getInvestor().getId() == null) {
			return RestResponseBuilder.badRequest("Investor id is required");
		}

		// Check investor exists or not for given id
		UiIconicInvestor investor = iconicInvestorService.getUiIconicInvestor(restInput.getInvestor().getId());
		if (investor == null || !investor.isActive()) {
			return RestResponseBuilder.notFound("No investor found for given id : " + restInput.getInvestor().getId());
		}

		profileOutput.setInvestor(investor);
		// Check profile view here
		String profile = restInput.getProfileType().toLowerCase().trim();

		if (profile.equals(DefaultConstants.MONTH_PROFILE_VIEW)
				|| profile.equals(DefaultConstants.DEFAULT_PROFILE_VIEW)) {

			profileOutput.setAggregates(investorsAggregateService.getAggregatedDetails(investor.getId(),
					DefaultConstants.DEFAULT_AGGREGATED_NO_OF_MONTHS, profile));
			restResponse = RestResponseBuilder.success(profileOutput);

		} else {
			String status = "Un recognized profile view : " + profile + ", supported profile views  are ("
					+ DefaultConstants.MONTH_PROFILE_VIEW + ", " + DefaultConstants.DEFAULT_PROFILE_VIEW + ")";
			restResponse = RestResponseBuilder.partialContent(profileOutput, status);
		}

		return restResponse;
	}

	/**
	 * 
	 * To activate or de activate an investor profile
	 * 
	 * /investors/profile/activate
	 * 
	 * 
	 * Input Json Ex : { "investor": { "id": 4785074604081152, "active":
	 * "true"/"false" } }
	 * 
	 * Output Json Ex :
	 * 
	 * { "investor": null , "status": "OK"}
	 * 
	 * @param restInput
	 * @return
	 */
	@RequestMapping(value = "/profile/activate", method = RequestMethod.POST)
	public @ResponseBody RestResponse deleteProfile(@RequestBody RestInput restInput) {

		if (restInput == null || restInput.getInvestor() == null) {
			return RestResponseBuilder.badRequest("Investor id is required");
		}

		Long id = restInput.getInvestor().getId();
		boolean status = restInput.getInvestor().isActive();

		IconicInvestor investor = iconicInvestorService.getInvestorProfile(id);

		if (investor == null) {
			return RestResponseBuilder.notFound("No investor found for given id : " + restInput.getInvestor().getId());
		}

		status = iconicInvestorService.changeProfileStatus(id, status);

		if (status) {
			return RestResponseBuilder.success(iconicInvestorService.getInvestorProfile(id));
		} else {
			return RestResponseBuilder.exceptionFailed("Unable to change the status for profile id : " + id);
		}

	}

	/**
	 * To update an investor profile
	 * 
	 * provide the fields which need to update along with id and keep remaining
	 * fields as null or empty
	 * 
	 * @param imgfile
	 *            - optional
	 * @param id
	 *            - required
	 * @param displayName
	 *            - optional
	 * @param socialAccounts
	 *            - optional
	 * @param emailId
	 *            - optional
	 * @param aboutInvestor
	 *            - optional
	 * @return
	 */
	@RequestMapping(value = "/profile/update", method = RequestMethod.POST)
	public @ResponseBody RestResponse updateInvestor(
			@RequestParam(value = "imgfile", required = false) MultipartFile imgfile, @RequestParam("id") long id,
			@RequestParam(value = "displayName", required = false) String displayName,
			@RequestParam(value = "socialAccounts", required = false) String socialAccounts,
			@RequestParam(value = "emailId", required = false) String emailId,
			@RequestParam(value = "aboutInvestor", required = false) String aboutInvestor) {

		if (id <= 0) {
			return RestResponseBuilder.badRequest("Investor id is required");
		}

		IconicInvestor investor = iconicInvestorService.getInvestorProfile(id);

		if (investor == null) {
			return RestResponseBuilder.notFound("No investor found for given id : " + id);
		}

		IconicInvestor inInvestor = new IconicInvestor();
		inInvestor.setId(id);
		inInvestor.setDisplayName(displayName);
		inInvestor.setSocialAccounts(socialAccounts);
		inInvestor.setEmailId(emailId);
		inInvestor.setAboutInvestor(aboutInvestor);

		if (imgfile != null && !imgfile.isEmpty()) {
			String imageLink = gcpImageUploader.uploadImageToGcp(inInvestor.getDisplayName(), imgfile);
			if (!StringUtils.isEmpty(imageLink)) {
				inInvestor.setLogoImageUrl(imageLink);
			}
		}

		investor.update(inInvestor);

		Key key = iconicInvestorService.saveIconicInvestor(investor);

		if (key != null && key.isComplete()) {
			return RestResponseBuilder.success(iconicInvestorService.getInvestorProfile(id));
		} else {
			return RestResponseBuilder.exceptionFailed("Unable to update for profile id : " + inInvestor.getId());
		}
	}

	/**
	 * To create an investor profile
	 * 
	 * @RequestMapping(value = "/investors/profile/create", method =
	 *                       RequestMethod.POST)
	 * @param imgfile
	 * @param displayName
	 * @param active
	 * @param socialAccounts
	 * @param emailId
	 * @param aboutInvestor
	 * @return
	 */
	@RequestMapping(value = "/profile/create", method = RequestMethod.POST)
	public @ResponseBody RestResponse createInvestor(@RequestParam("imgfile") MultipartFile imgfile,
			@RequestParam("displayName") String displayName, @RequestParam("active") boolean active,
			@RequestParam("socialAccounts") String socialAccounts, @RequestParam("emailId") String emailId,
			@RequestParam("aboutInvestor") String aboutInvestor) {

		RestResponse restOutput = null;

		if (StringUtils.isEmpty(displayName)) {
			return RestResponseBuilder.badRequest("Investor id is required");
		}

		IconicInvestor inInvestor = new IconicInvestor();
		inInvestor.setDisplayName(displayName);
		inInvestor.setActive(active);
		inInvestor.setSocialAccounts(socialAccounts);
		inInvestor.setEmailId(emailId);
		inInvestor.setAboutInvestor(aboutInvestor);

		IconicInvestor investor = iconicInvestorService.getInvestorProfileByName(inInvestor.getDisplayName());

		if (investor == null) {

			// upload image to GCP storage
			if (imgfile != null) {
				String imageLink = gcpImageUploader.uploadImageToGcp(inInvestor.getDisplayName(), imgfile);
				if (!StringUtils.isEmpty(imageLink)) {
					inInvestor.setLogoImageUrl(imageLink);
				}
			}
			// save investor data now
			Key key = iconicInvestorService.saveIconicInvestor(inInvestor);
			if (key != null && key.isComplete()) {
				restOutput = RestResponseBuilder.success(iconicInvestorService.getInvestorProfile(key.getId()));
			} else {
				restOutput = RestResponseBuilder
						.exceptionFailed("Unable to save profile id : " + inInvestor.getDisplayName());
			}
		} else {
			restOutput = RestResponseBuilder
					.badRequest("There is an Investor profile exsit with given name " + inInvestor.getDisplayName());
		}

		return restOutput;
	}

	/**
	 * Rest Service get Transactions
	 * 
	 * For a investor for given date range.
	 * 
	 * (OR)
	 * 
	 * last n number of latest transactions for given investor, stock id
	 * 
	 * 
	 * Input Json Ex : { "investorId": "1", "fromDate": "07/11/2017 00:00:00",
	 * "toDate": "08/11/2017 00:00:00" }}
	 * 
	 * @param restInput
	 * @return
	 */
	@RequestMapping(value = "/transactions", method = RequestMethod.POST)
	public @ResponseBody RestResponse getTransactions(@RequestBody RestInputForTransaction restInput) {

		RestResponse restResponse = null;
		if (restInput == null || restInput.getInvestorId() == null) {
			restResponse = RestResponseBuilder.badRequest(
					"Investor id is required and stock id, from date, to date and last no.of transaction optional");

		} else {
			restResponse = RestResponseBuilder.success(transactionService.getTransaction(restInput));
		}

		return restResponse;
	}

	/**
	 * Rest Service to update investor id for given alias names which are came
	 * for transactions. and it update investor id to the related transactions
	 * as well
	 * 
	 * Input Json Ex : { "investorId": "1", "aliasNames": ["name 1", "name 2",
	 * ...]}
	 * 
	 * @param restInput
	 * @return
	 */
	@RequestMapping(value = "/aliasnames/update", method = RequestMethod.POST)
	public @ResponseBody RestResponse mapAliasnamesToInvestorId(@RequestBody AliasNameInputs restInput) {

		RestResponse restResponse = null;

		if (restInput == null || restInput.getInvestorId() == null || restInput.getAliasNames() == null) {
			restResponse = RestResponseBuilder.badRequest("Required input data is missing ");

		} else {
			IconicInvestor investor = iconicInvestorService.getInvestorProfile(restInput.getInvestorId());

			if (investor == null) {
				restResponse = RestResponseBuilder
						.notFound("No investor found for given investor id  " + restInput.getInvestorId());
			} else {
				String message = transactionsMapper.process(restInput.getInvestorId(), restInput.getAliasNames());
				if (message.isEmpty()) {
					restResponse = RestResponseBuilder.success("");
				} else {
					restResponse = RestResponseBuilder.partialContent(message.split("<>"), "Request is completed partially");
				}
			}
		}

		return restResponse;
	}

	/**
	 * Rest Service to get list of alianNames for given investor id
	 * 
	 * Input Json Ex : { "investorId": "1"}
	 * 
	 * @param RestResponse
	 * @return
	 */
	@RequestMapping(value = "/aliasnames/list", method = RequestMethod.GET)
	public @ResponseBody RestResponse aliasNamesList(@RequestParam("investorId") Long investorId) {

		RestResponse restResponse = null;

		if (investorId == null || investorId <= 0) {
			restResponse = RestResponseBuilder.badRequest("Investor id is missing");

		} else {
			IconicInvestor investor = iconicInvestorService.getInvestorProfile(investorId);

			if (investor == null) {
				restResponse = RestResponseBuilder
						.notFound("No investor found for given investor id  " + investorId);
			} else {
				List<InvestorAliasName> list = transactionsMapper.getList(investorId);
				if (!list.isEmpty()) {
					restResponse = RestResponseBuilder.success(list);
				} else {
					restResponse = RestResponseBuilder
							.notFound("No Data found for given investor id " + investorId);
				}
			}
		}

		return restResponse;
	}
	
	/**
	 * Rest Service to get list of alianNames for given investor id
	 * 
	 * 
	 * @param RestResponse
	 * @return
	 */
	@RequestMapping(value = "/aliasnames/all", method = RequestMethod.GET)
	public @ResponseBody RestResponse getAllAliasnmaes() {

		List<String> names =  transactionsMapper.getAliasNames();

		RestResponse restResponse = RestResponseBuilder.success(names);
		
		return restResponse;
	}
	
	@RequestMapping(value = "/quotes", method = RequestMethod.GET)
	public @ResponseBody RestResponse getCurrentPrice(@RequestParam(name="scriptCodeId", required=true) Long scriptCodeId) {

		CurrentStockPriceWrapper currentStockPrice = stockService.getCurrentStockPrice(scriptCodeId);

		RestResponse restResponse = RestResponseBuilder.success(currentStockPrice);
		
		return restResponse;
	}
	
	@RequestMapping(value = "/tweets", method = RequestMethod.GET)
	public @ResponseBody RestResponse getTwitterDetails(@RequestParam(name="name", required=true) String name) {

		TwitterUserDetails[] details =  twitterService.getTwitterDetails(name);

		RestResponse restResponse = RestResponseBuilder.success(details);
		
		return restResponse;
	}
}
