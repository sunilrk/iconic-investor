package com.org.iconic.helper;

import com.googlecode.objectify.ObjectifyService;
import com.org.iconic.entity.IconicInvestor;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

/**
 * OfyHelper, a ServletContextListener, is setup in web.xml 
 **/
public class ObjectifyHelper implements ServletContextListener {
  public void contextInitialized(ServletContextEvent event) {

    //ObjectifyService.register(IconicInvestor.class);

  }

  public void contextDestroyed(ServletContextEvent event) {
    // App Engine does not currently invoke this method.
  }
}