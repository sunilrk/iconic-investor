package com.stockboo.app;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import com.rometools.rome.io.FeedException;

public class Test {

	public static void main(String[] args) throws IllegalArgumentException, FeedException, IOException { 
		URL obj = new URL("http://www.livemint.com/rss/money");
    HttpURLConnection con = (HttpURLConnection) obj.openConnection();
    // optional default is GET
    con.setRequestMethod("GET");
    //add request header
    int responseCode = con.getResponseCode();
    System.out.println("\nSending 'GET' request to URL : " + "http://www.livemint.com/rss/money");
    System.out.println("Response Code : " + responseCode);
    BufferedReader in = new BufferedReader(
            new InputStreamReader(con.getInputStream()));
    String inputLine;
    StringBuffer response = new StringBuffer();
    while ((inputLine = in.readLine()) != null) {
       response.append(inputLine);
    }
    in.close();
    //print in String
    System.out.println( response.toString());
	}
}
