package com.stockboo.app.enums;

/**
 * Contains 2018 holidays list
 * 
 * @author sunil.r
 *
 */
public class Holidays {

	public static final String[] MARKET_HOLIDAYS = {"2018-01-26","2018-02-13","2018-03-02","2018-03-29","2018-03-30","2018-05-01","2018-08-15","2018-08-22","2018-09-13",
			"2018-09-20","2018-10-02","2018-10-18","2018-11-07","2018-11-08","2018-11-23","2018-12-25"}; 
}
