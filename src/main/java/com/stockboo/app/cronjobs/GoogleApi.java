
package com.stockboo.app.cronjobs;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GoogleApi {

@SerializedName("id")
@Expose
private String id;
@SerializedName("t")
@Expose
private String t;
@SerializedName("e")
@Expose
private String e;
@SerializedName("l")
@Expose
private String l;
@SerializedName("l_fix")
@Expose
private String lFix;
@SerializedName("l_cur")
@Expose
private String lCur;
@SerializedName("s")
@Expose
private String s;
@SerializedName("ltt")
@Expose
private String ltt;
@SerializedName("lt")
@Expose
private String lt;
@SerializedName("lt_dts")
@Expose
private String ltDts;
@SerializedName("c")
@Expose
private String c;
@SerializedName("c_fix")
@Expose
private String cFix;
@SerializedName("cp")
@Expose
private String cp;
@SerializedName("cp_fix")
@Expose
private String cpFix;
@SerializedName("ccol")
@Expose
private String ccol;
@SerializedName("pcls_fix")
@Expose
private String pclsFix;

public String getId() {
return id;
}

public void setId(String id) {
this.id = id;
}

public String getT() {
return t;
}

public void setT(String t) {
this.t = t;
}

public String getE() {
return e;
}

public void setE(String e) {
this.e = e;
}

public String getL() {
return l;
}

public void setL(String l) {
this.l = l;
}

public String getLFix() {
return lFix;
}

public void setLFix(String lFix) {
this.lFix = lFix;
}

public String getLCur() {
return lCur;
}

public void setLCur(String lCur) {
this.lCur = lCur;
}

public String getS() {
return s;
}

public void setS(String s) {
this.s = s;
}

public String getLtt() {
return ltt;
}

public void setLtt(String ltt) {
this.ltt = ltt;
}

public String getLt() {
return lt;
}

public void setLt(String lt) {
this.lt = lt;
}

public String getLtDts() {
return ltDts;
}

public void setLtDts(String ltDts) {
this.ltDts = ltDts;
}

public String getC() {
return c;
}

public void setC(String c) {
this.c = c;
}

public String getCFix() {
return cFix;
}

public void setCFix(String cFix) {
this.cFix = cFix;
}

public String getCp() {
return cp;
}

public void setCp(String cp) {
this.cp = cp;
}

public String getCpFix() {
return cpFix;
}

public void setCpFix(String cpFix) {
this.cpFix = cpFix;
}

public String getCcol() {
return ccol;
}

public void setCcol(String ccol) {
this.ccol = ccol;
}

public String getPclsFix() {
return pclsFix;
}

public void setPclsFix(String pclsFix) {
this.pclsFix = pclsFix;
}

}