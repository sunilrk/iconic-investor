package com.stockboo.app.cronjobs;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.EntityNotFoundException;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.memcache.ErrorHandlers;
import com.google.appengine.api.memcache.MemcacheService;
import com.google.appengine.api.memcache.MemcacheServiceFactory;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.stockboo.app.cronjobs.domain.NewGoogleApi;
import com.stockboo.app.dao.HomeScreenDao;
import com.stockboo.app.domain.utility.DateUtil;
import com.stockboo.app.entity.Nifty;
import com.stockboo.app.entity.Sensex;
import com.stockboo.app.entity.SensexAndNiftyPrices;
import com.stockboo.app.service.HolidayService;

@RestController
@SuppressWarnings("serial")
@RequestMapping("/cron")
public class NewSensexAndNiftyCronJob extends HttpServlet {
	private static final Logger log = Logger.getLogger(SensexAndNiftyCronJob.class.getName());
	public static final String JOB_PARENT = "notification_job_parent";

	@RequestMapping(value=	"/newsensexnifty9.17", method = RequestMethod.GET)
	public void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		
		System.out.println("In sensex and nifty cron job");
		
		if(HolidayService.isSaturdayOrSundayOrHoliday()){
			return;
		}
		
		URL url = new URL("https://finance.google.com/finance?output=json&q=Nse:nifty");
		System.out.println("URL - " + url.toString());
		BufferedReader reader = new BufferedReader(new InputStreamReader(url.openStream()));
		StringBuffer json = new StringBuffer();
		String line;
		System.out.println("Response -" + json.toString());
		boolean firstLine = true;
		while ((line = reader.readLine()) != null) {
			if (line.contains("//") && firstLine) {
//				json.append(line.substring(3));
				firstLine =false;
			} else if (line.contains("reuters_url")) {
				json.append(",");
			}else if (line.contains("f_type")) {
//				json.append("dummy:abc");
			}else if(line.contains("f_ttm_date")){
				
			}else if (line.contains("http://www.google.com/finance")) {
				json.append(line.substring(1));
			}else if (line.equals("}]")) {
				json.append("}");
			}else
				json.append(line);
//			System.out.println("line-" + line);
		}
		reader.close();
		System.out.println("Final Json - " + json.toString());
		Gson gson = new Gson();

		NewGoogleApi niftyData = gson.fromJson(json.toString(),NewGoogleApi.class);
		
			
			System.out.println(niftyData.getL() + "   " + niftyData.getL());
	
		

		URL url1 = new URL("https://finance.google.com/finance?output=json&q=INDEXBOM:SENSEX");
		System.out.println("URL - " + url1.toString());
		BufferedReader reader1 = new BufferedReader(new InputStreamReader(url1.openStream()));
		StringBuffer json1 = new StringBuffer();
		String line1;
		System.out.println("Response -" + json1.toString());

		boolean firstLine1 = true;
		while ((line1 = reader1.readLine()) != null) {
			if (line1.contains("//") && firstLine1) {
//				json.append(line.substring(3));
				firstLine1 =false;
			} else if (line1.contains("reuters_url")) {
				json1.append(",");
			}else if (line1.contains("f_type")) {
//				json1.append("dummy:abc");
			}else if(line1.contains("f_ttm_date")){
				
			}else if (line1.contains("http://www.google.com/finance")) {
				json1.append(line1.substring(1));
			}else if (line1.equals("}]")) {
				json1.append("}");
			}else
				json1.append(line1);
//			System.out.println("line-" + line1);
		}
		reader.close();
		System.out.println("Final Json - " + json1.toString());
		NewGoogleApi sensexData = gson.fromJson(json1.toString(), NewGoogleApi.class);
		System.out.println(sensexData.getL() + "   " + sensexData.getL());
		
		
		Nifty niftyEntity = null;
		Sensex sensexEntity = null;
		try {
			niftyEntity = new Nifty(getEntityById("NIFTY", Nifty.TABLE_NAME));
			sensexEntity = new Sensex(getEntityById("SENSEX", Sensex.TABLE_NAME));
		} catch (Exception e) {
			e.printStackTrace();
		}
		if (niftyEntity == null) {
			niftyEntity = new Nifty();
			niftyEntity.setCreatedAt(new Date());
		} else {
			niftyEntity.setUpdatedAt(new Date());
		}
		if (sensexEntity == null) {
			sensexEntity = new Sensex();
			sensexEntity.setCreatedAt(new Date());
		} else {
			sensexEntity.setUpdatedAt(new Date());
		}
		niftyEntity = getNifty(niftyData, niftyEntity);
		sensexEntity = getSensex(sensexData, sensexEntity);
		SensexAndNiftyPrices sensexAndNiftyPricesEntity = new SensexAndNiftyPrices(niftyData.getL(), sensexData.getL(), new Date());
		
		MemcacheService memcache = MemcacheServiceFactory.getMemcacheService();
		memcache.setErrorHandler(ErrorHandlers.getConsistentLogAndContinue(Level.INFO));
		memcache.delete("nifty");
		memcache.put("nifty", niftyEntity);
		
		memcache.delete("sensex");
		memcache.put("sensex", sensexEntity);
		
		List<SensexAndNiftyPrices> sensexAndNiftyPrices = null;
		
		if(null == memcache.get("sensexAndNiftyPrices")){
			System.out.println("DEcare list*******************************");
			HomeScreenDao hsd = new HomeScreenDao();
			Date weekDay = DateUtil.getWeekDay();
			sensexAndNiftyPrices = new ArrayList<>();
			List<SensexAndNiftyPrices> sensexAndNiftyListFromDB = hsd.getSensexAndNiftyListFromDB(weekDay);
			for (SensexAndNiftyPrices sensexAndNiftyPrices2 : sensexAndNiftyListFromDB) {
				sensexAndNiftyPrices.add(sensexAndNiftyPrices2);
			}
		}else { 
			System.out.println("From cache*******************************");
		sensexAndNiftyPrices = (List<SensexAndNiftyPrices>) memcache.get("sensexAndNiftyPrices");
		}
		sensexAndNiftyPrices.add(sensexAndNiftyPricesEntity);
		
		memcache.delete("sensexAndNiftyPrices");
		memcache.put("sensexAndNiftyPrices", sensexAndNiftyPrices);
		
		try {
			DatastoreServiceFactory.getDatastoreService().put(niftyEntity.buildEntity());
			DatastoreServiceFactory.getDatastoreService().put(sensexEntity.buildEntity());
			DatastoreServiceFactory.getDatastoreService().put(sensexAndNiftyPricesEntity.buildEntity());
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}

	private Nifty getNifty(NewGoogleApi googleApi, Nifty nifty) {
		if (nifty == null) {
			nifty = new Nifty();
		}
		nifty.setChangePrice(googleApi.getC());
		nifty.setChr(googleApi.getCcol());
		//Since C_fix is not available, using 'c' only
		nifty.setChangePriceFinal(googleApi.getC());
		nifty.setChangePercentage(googleApi.getCp());
		//Since co_fix is not available, using 'cp' only
		nifty.setChangePercentageFinal(googleApi.getCp());
		nifty.setExchange(googleApi.getE());
//		nifty.setId(googleApi.getId());
		nifty.setLtp(googleApi.getL());
		//Since l_cur, l_fix, pcls_fix is not available using l 
		nifty.setLCur(googleApi.getL());
		nifty.setLtpFinal(googleApi.getL());
		
		
	     
	     
	     
		nifty.setLastTradeTimeFormatted(my_time_in("GMT+5:30", "MMMM dd,HH:mm a z"));
		
		 Date lastTradedDateTime=new Date();
	     SimpleDateFormat sdf1=new SimpleDateFormat("yyyy-MM-dd hh:mm:ssz");
	     String lastTradedDateTimeString = sdf1.format(lastTradedDateTime);
	     System.out.println(lastTradedDateTimeString);
		nifty.setLastTradedDateTime(lastTradedDateTimeString);
		
		 Date d=new Date();
	     SimpleDateFormat sdf=new SimpleDateFormat("hh:mm a");
	     String currentDateTimeString = sdf.format(d);
		nifty.setLastTradedTime(currentDateTimeString+" "+ "GMT+5:30");
		
		nifty.setPreviousClose(googleApi.getL());
		nifty.setS("0");
		nifty.setSymbol(googleApi.getT());
		return nifty;

	}

	private Sensex getSensex(NewGoogleApi googleApi, Sensex sensex) {
		if (sensex == null) {
			sensex = new Sensex();
		}
		sensex.setChange(googleApi.getC());
		sensex.setChr(googleApi.getCcol());
		sensex.setChangePercentageFinal(googleApi.getC());
		sensex.setChangePercentage(googleApi.getCp());
		sensex.setCpFix(googleApi.getCp());
		sensex.seExchangeE(googleApi.getE());
//		sensex.setId(googleApi.getId());
		sensex.setLtp(googleApi.getL());
		sensex.setLCur(googleApi.getL());
		sensex.setLtpFinal(googleApi.getL());
		
	     
		sensex.setLt(my_time_in("GMT+5:30", "MMMM dd,HH:mm a z"));
		
		 Date lastTradedDateTime=new Date();
	     SimpleDateFormat sdf1=new SimpleDateFormat("yyyy-MM-dd hh:mm:ssz");
	     String lastTradedDateTimeString = sdf1.format(lastTradedDateTime);
	     System.out.println(lastTradedDateTimeString);
	     sensex.setLastTradedDateTime(lastTradedDateTimeString);
		
		 Date d=new Date();
	     SimpleDateFormat sdf=new SimpleDateFormat("hh:mm a");
	     String currentDateTimeString = sdf.format(d);
	     sensex.setLastTradedTime(currentDateTimeString+" "+ "GMT+5:30");
		
		sensex.setpreviousClose(googleApi.getL());
		sensex.setS("0");
		sensex.setSymbol(googleApi.getT());
		return sensex;

	}
	
	 public static String my_time_in(String target_time_zone, String format){
	        TimeZone tz = TimeZone.getTimeZone(target_time_zone);
	        Date date = Calendar.getInstance().getTime();
	        SimpleDateFormat date_format_gmt = new SimpleDateFormat(format);
	        date_format_gmt.setTimeZone(tz);
	        return date_format_gmt.format(date);
	        

	    } 
	 
	 private Entity getEntityById(String id, String tableName) throws Exception {
			Key key = KeyFactory.createKey(tableName, id);
			System.out.println("Fetching entity for the " + key);
			try {
				return DatastoreServiceFactory.getDatastoreService().get(key);
			} catch (EntityNotFoundException e) {
				e.printStackTrace();
				throw new Exception("Record not found");
			}
		}

}