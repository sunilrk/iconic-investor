package com.stockboo.app.cronjobs;

import java.io.IOException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.EntityNotFoundException;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.memcache.ErrorHandlers;
import com.google.appengine.api.memcache.MemcacheService;
import com.google.appengine.api.memcache.MemcacheServiceFactory;
import com.google.appengine.api.urlfetch.FetchOptions;
import com.google.appengine.api.urlfetch.HTTPHeader;
import com.google.appengine.api.urlfetch.HTTPMethod;
import com.google.appengine.api.urlfetch.HTTPRequest;
import com.google.appengine.api.urlfetch.HTTPResponse;
import com.google.appengine.api.urlfetch.URLFetchService;
import com.google.appengine.api.urlfetch.URLFetchServiceFactory;
import com.google.gson.Gson;
import com.stockboo.app.configuration.Constants;
import com.stockboo.app.cronjobs.domain.MoneyControlSensexAndNifty;
import com.stockboo.app.dao.HomeScreenDao;
import com.stockboo.app.domain.utility.DateUtil;
import com.stockboo.app.entity.Nifty;
import com.stockboo.app.entity.Sensex;
import com.stockboo.app.entity.SensexAndNiftyPrices;
import com.stockboo.app.service.HolidayService;

@SuppressWarnings("serial")
@RestController
@RequestMapping("/cron")
public class MoneyControensexAndNiftyCronJob {
	private static final Logger log = Logger.getLogger(SensexAndNiftyCronJob.class.getName());
	public static final String JOB_PARENT = "notification_job_parent";

	
	@RequestMapping(value=	"/newsensexnifty", method = RequestMethod.GET)
	public void jobProcess(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		
		System.out.println("In sensex and nifty cron job");
		
		if(HolidayService.isSaturdayOrSundayOrHoliday()){
			return;
		}
		
		
		URLFetchService fetcher = URLFetchServiceFactory.getURLFetchService();
		FetchOptions fetchOptions = FetchOptions.Builder.validateCertificate();
		
		HTTPRequest request = new HTTPRequest(new URL("http://appfeeds.moneycontrol.com/jsonapi/market/indices&ind_id=9"), HTTPMethod.GET, fetchOptions);
		request.setHeader(new HTTPHeader(Constants.HEADER_USER_AGENT, Constants.HEADER_USER_AGENT_VALUE));
		HTTPResponse response = fetcher.fetch(request);
		System.out.println("Response code "+response.getResponseCode());
		String json = httpResponseToJsonString(response);
		
		System.out.println("Final Json - " + json.toString());
		Gson gson = new Gson();

		MoneyControlSensexAndNifty niftyData = gson.fromJson(json.toString(),MoneyControlSensexAndNifty.class);
		System.out.println(niftyData.getIndices().getLastprice());
			
		HTTPRequest request1 = new HTTPRequest(new URL("http://appfeeds.moneycontrol.com/jsonapi/market/indices&ind_id=4"), HTTPMethod.GET, fetchOptions);
		request.setHeader(new HTTPHeader(Constants.HEADER_USER_AGENT, Constants.HEADER_USER_AGENT_VALUE));
		HTTPResponse response1 = fetcher.fetch(request1);
		System.out.println("Response code "+response1.getResponseCode());
		String json1 = httpResponseToJsonString(response1);
		
		
		System.out.println("Final Json1111 - " + json1.toString());
		MoneyControlSensexAndNifty sensexData = gson.fromJson(json1.toString(), MoneyControlSensexAndNifty.class);
		System.out.println(sensexData.getIndices().getLastprice());
		
		
		Nifty niftyEntity = null;
		Sensex sensexEntity = null;
		try {
			niftyEntity = new Nifty(getEntityById("NIFTY", Nifty.TABLE_NAME));
			sensexEntity = new Sensex(getEntityById("SENSEX", Sensex.TABLE_NAME));
		} catch (Exception e) {
			e.printStackTrace();
		}
		if (niftyEntity == null) {
			niftyEntity = new Nifty();
			niftyEntity.setCreatedAt(new Date());
		} else {
			niftyEntity.setUpdatedAt(new Date());
		}
		if (sensexEntity == null) {
			sensexEntity = new Sensex();
			sensexEntity.setCreatedAt(new Date());
		} else {
			sensexEntity.setUpdatedAt(new Date());
		}
		niftyEntity = getNifty(niftyData, niftyEntity);
		sensexEntity = getSensex(sensexData, sensexEntity);
		SensexAndNiftyPrices sensexAndNiftyPricesEntity = new SensexAndNiftyPrices(niftyData.getIndices().getLastprice(), sensexData.getIndices().getLastprice(), new Date());
		
		MemcacheService memcache = MemcacheServiceFactory.getMemcacheService();
		memcache.setErrorHandler(ErrorHandlers.getConsistentLogAndContinue(Level.INFO));
		memcache.delete("nifty");
		memcache.put("nifty", niftyEntity);
		
		memcache.delete("sensex");
		memcache.put("sensex", sensexEntity);
		
		List<SensexAndNiftyPrices> sensexAndNiftyPrices = null;
		
		if(null == memcache.get("sensexAndNiftyPrices")){
			System.out.println("DEcare list*******************************");
			HomeScreenDao hsd = new HomeScreenDao();
			Date weekDay = DateUtil.getWeekDay();
			sensexAndNiftyPrices = new ArrayList<>();
			List<SensexAndNiftyPrices> sensexAndNiftyListFromDB = hsd.getSensexAndNiftyListFromDB(weekDay);
			for (SensexAndNiftyPrices sensexAndNiftyPrices2 : sensexAndNiftyListFromDB) {
				sensexAndNiftyPrices.add(sensexAndNiftyPrices2);
			}
		}else { 
			System.out.println("From cache*******************************");
		sensexAndNiftyPrices = (List<SensexAndNiftyPrices>) memcache.get("sensexAndNiftyPrices");
		}
		sensexAndNiftyPrices.add(sensexAndNiftyPricesEntity);
		
		memcache.delete("sensexAndNiftyPrices");
		memcache.put("sensexAndNiftyPrices", sensexAndNiftyPrices);
		
		try {
			DatastoreServiceFactory.getDatastoreService().put(niftyEntity.buildEntity());
			DatastoreServiceFactory.getDatastoreService().put(sensexEntity.buildEntity());
			DatastoreServiceFactory.getDatastoreService().put(sensexAndNiftyPricesEntity.buildEntity());
			System.out.println(niftyEntity.toString());
			System.out.println(sensexEntity.toString());
			System.out.println(sensexAndNiftyPricesEntity.toString());
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}

	private Nifty getNifty(MoneyControlSensexAndNifty mcNifty, Nifty nifty) {
		if (nifty == null) {
			nifty = new Nifty();
		}
		nifty.setChangePrice(mcNifty.getIndices().getChange());
		nifty.setChr("chr");
		//Since C_fix is not available, using 'c' only
		nifty.setChangePriceFinal(mcNifty.getIndices().getChange());
		nifty.setChangePercentage(mcNifty.getIndices().getPercentchange());
		//Since co_fix is not available, using 'cp' only
		nifty.setChangePercentageFinal(mcNifty.getIndices().getPercentchange());
		nifty.setExchange("NSE");
		//No key from money control
		nifty.setId(null);
		nifty.setLtp(mcNifty.getIndices().getLastprice());
		//Since l_cur, l_fix, pcls_fix is not available using l 
		nifty.setLCur(mcNifty.getIndices().getLastprice());
		nifty.setLtpFinal(mcNifty.getIndices().getLastprice());
	     
		nifty.setLastTradeTimeFormatted(my_time_in("GMT+5:30", "MMMM dd,HH:mm a z"));
		
		 Date lastTradedDateTime=new Date();
	     SimpleDateFormat sdf1=new SimpleDateFormat("yyyy-MM-dd hh:mm:ssz");
	     String lastTradedDateTimeString = sdf1.format(lastTradedDateTime);
	     System.out.println(lastTradedDateTimeString);
		nifty.setLastTradedDateTime(lastTradedDateTimeString);
		
		 Date d=new Date();
	     SimpleDateFormat sdf=new SimpleDateFormat("hh:mm a");
	     String currentDateTimeString = sdf.format(d);
		nifty.setLastTradedTime(currentDateTimeString+" "+ "GMT+5:30");
		nifty.setPreviousClose(mcNifty.getIndices().getPrevclose());
		nifty.setS("0");
		nifty.setSymbol("NIFTY");
		return nifty;

	}

	private Sensex getSensex(MoneyControlSensexAndNifty mcSensex, Sensex sensex) {
		if (sensex == null) {
			sensex = new Sensex();
		}
		sensex.setChange(mcSensex.getIndices().getChange());
		sensex.setChr("chr");
		//Since C_fix is not available, using 'c' only
//		sensex.setChangePriceFinal(mcSensex.getIndices().getChange());
		sensex.setChangePercentage(mcSensex.getIndices().getPercentchange());
		//Since co_fix is not available, using 'cp' only
		sensex.setChangePercentageFinal(mcSensex.getIndices().getPercentchange());
		sensex.seExchangeE("INDEXBOM");
		//No key from money control
		sensex.setId(null);
		sensex.setLtp(mcSensex.getIndices().getLastprice());
		//Since l_cur, l_fix, pcls_fix is not available using l 
		sensex.setLCur(mcSensex.getIndices().getLastprice());
		sensex.setLtpFinal(mcSensex.getIndices().getLastprice());
		
	     
		sensex.setLt(my_time_in("GMT+5:30", "MMMM dd,HH:mm a z"));
		
		 Date lastTradedDateTime=new Date();
	     SimpleDateFormat sdf1=new SimpleDateFormat("yyyy-MM-dd hh:mm:ssz");
	     String lastTradedDateTimeString = sdf1.format(lastTradedDateTime);
	     System.out.println(lastTradedDateTimeString);
	     sensex.setLastTradedDateTime(lastTradedDateTimeString);
		
		 Date d=new Date();
	     SimpleDateFormat sdf=new SimpleDateFormat("hh:mm a");
	     String currentDateTimeString = sdf.format(d);
	     sensex.setLastTradedTime(currentDateTimeString+" "+ "GMT+5:30");
		
		sensex.setpreviousClose(mcSensex.getIndices().getPrevclose());
		sensex.setS("0");
		sensex.setSymbol("SENSEX");
		return sensex;

	}
	
	 public static String my_time_in(String target_time_zone, String format){
	        TimeZone tz = TimeZone.getTimeZone(target_time_zone);
	        Date date = Calendar.getInstance().getTime();
	        SimpleDateFormat date_format_gmt = new SimpleDateFormat(format);
	        date_format_gmt.setTimeZone(tz);
	        return date_format_gmt.format(date);
	    } 
	 
	 private String  httpResponseToJsonString(HTTPResponse response) {
			String urlFetchedHtml = null;
			if (response != null) {
		            StringBuilder stringBuilder = new StringBuilder();
		            for (byte b : response.getContent()) {
		                stringBuilder.append((char) b);
		            }
		            urlFetchedHtml = stringBuilder.toString();
		            System.out.println("*************************"+urlFetchedHtml);}
			
			return urlFetchedHtml;
		}
	 
	 private Entity getEntityById(String id, String tableName) throws Exception {
			Key key = KeyFactory.createKey(tableName, id);
			System.out.println("Fetching entity for the " + key);
			try {
				return DatastoreServiceFactory.getDatastoreService().get(key);
			} catch (EntityNotFoundException e) {
				e.printStackTrace();
				throw new Exception("Record not found");
			}
		}

}