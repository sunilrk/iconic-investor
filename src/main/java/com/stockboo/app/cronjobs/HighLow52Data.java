package com.stockboo.app.cronjobs;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class HighLow52Data {

	@SerializedName("symbol")
	@Expose
	private String symbol;
	@SerializedName("symbolDesc")
	@Expose
	private String symbolDesc;
	@SerializedName("value")
	@Expose
	private String value;
	@SerializedName("year")
	@Expose
	private String year;
	@SerializedName("ltp")
	@Expose
	private String ltp;
	@SerializedName("value_old")
	@Expose
	private String valueOld;
	@SerializedName("dt")
	@Expose
	private String dt;
	@SerializedName("prev")
	@Expose
	private String prev;
	@SerializedName("change")
	@Expose
	private String change;
	@SerializedName("pChange")
	@Expose
	private String pChange;

	public String getSymbol() {
		return symbol;
	}

	public void setSymbol(String symbol) {
		this.symbol = symbol;
	}

	public String getSymbolDesc() {
		return symbolDesc;
	}

	public void setSymbolDesc(String symbolDesc) {
		this.symbolDesc = symbolDesc;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getYear() {
		return year;
	}

	public void setYear(String year) {
		this.year = year;
	}

	public String getLtp() {
		return ltp;
	}

	public void setLtp(String ltp) {
		this.ltp = ltp;
	}

	public String getValueOld() {
		return valueOld;
	}

	public void setValueOld(String valueOld) {
		this.valueOld = valueOld;
	}

	public String getDt() {
		return dt;
	}

	public void setDt(String dt) {
		this.dt = dt;
	}

	public String getPrev() {
		return prev;
	}

	public void setPrev(String prev) {
		this.prev = prev;
	}

	public String getChange() {
		return change;
	}

	public void setChange(String change) {
		this.change = change;
	}

	public String getPChange() {
		return pChange;
	}

	public void setPChange(String pChange) {
		this.pChange = pChange;
	}

}