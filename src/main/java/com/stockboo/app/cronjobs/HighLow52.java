package com.stockboo.app.cronjobs;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class HighLow52 {

	@SerializedName("time")
	@Expose
	private String time;
	@SerializedName("data")
	@Expose
	private List<HighLow52Data> data = null;

	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}

	public List<HighLow52Data> getData() {
		return data;
	}

	public void setData(List<HighLow52Data> data) {
		this.data = data;
	}

}