package com.stockboo.app.cronjobs;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.stockboo.app.configuration.Constants;
import com.stockboo.app.dao.ScriptDao;
import com.stockboo.app.domain.NSE;

@RestController
@SuppressWarnings("serial")
@RequestMapping("/cron")
public class StockScriptUpdateCronJob extends HttpServlet {
	private static final Logger log = Logger.getLogger(StockScriptUpdateCronJob.class.getName());

	@RequestMapping(value=	"/stockscriptupdate", method = RequestMethod.GET)
	public void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {

		try {
			System.out.println("Inside cron job");
			URL url12 = new URL("https://www.nseindia.com/corporates/datafiles/LDE_EQUITIES_LAST_1_MONTH.csv");
			URLConnection uc = url12.openConnection();
			uc.setRequestProperty(Constants.HEADER_USER_AGENT, Constants.HEADER_USER_AGENT_VALUE);
			String userpass = "username" + ":" + "password";
			String basicAuth = "Basic " + javax.xml.bind.DatatypeConverter.printBase64Binary(userpass.getBytes());

			uc.setRequestProperty("Authorization", basicAuth);

			InputStreamReader inStream = new InputStreamReader(uc.getInputStream());
			BufferedReader buff = new BufferedReader(inStream);

			String content2 = null;
			// "fIELD1": "SYMBOL",
			// "fIELD2": "NAME OF COMPANY",
			// "fIELD3": " SERIES",
			// "fIELD4": " DATE OF LISTING",
			// "fIELD5": " PAID UP VALUE",
			// "fIELD6": " MARKET LOT",
			// "fIELD7": " ISIN NUMBER",
			// "fIELD8": " FACE VALUE"
			
			//Skipping first line
			buff.readLine();
			List<NSE> nseList = new ArrayList<>();
			while ((content2 = buff.readLine()) != null) {
				System.out.println(content2);
				// StringDataFormat: "Symbol","ISIN","Company","First Listing
				// Date","Face Value","Paid Up Value","Market Lot","Industry"
				// industry in script update and series in the full script
				// equity url are not matching
				// only one above difference is there, rest all are matching
				String[] nseScriptArray = content2.split(",");
				if (nseScriptArray != null) {
					NSE nse = new NSE();
					nse.setfIELD1(nseScriptArray[0].replaceAll("^\"|\"$", ""));
					nse.setfIELD2(nseScriptArray[2].replaceAll("^\"|\"$", ""));
					nse.setfIELD3(null);
					nse.setfIELD4(nseScriptArray[3].replaceAll("^\"|\"$", ""));
					nse.setfIELD5(nseScriptArray[5].replaceAll("^\"|\"$", ""));
					nse.setfIELD6(nseScriptArray[6].replaceAll("^\"|\"$", ""));
					nse.setfIELD7(nseScriptArray[1].replaceAll("^\"|\"$", ""));
					nse.setfIELD8(nseScriptArray[4].replaceAll("^\"|\"$", ""));
					nseList.add(nse);
				}
			}
			ScriptDao sd = new ScriptDao();
			sd.updateNseScript(nseList);
		} catch (Exception e) {
			System.out.print("Exception");
		}
		System.out.println("done");

	}

}