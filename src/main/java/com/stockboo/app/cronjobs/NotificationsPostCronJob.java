package com.stockboo.app.cronjobs;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.PreparedQuery;
import com.google.appengine.api.datastore.Query;
import com.stockboo.app.dao.HomeScreenDao;
import com.stockboo.app.domain.GainersLoosersDataDomain;
import com.stockboo.app.domain.pushnotification.SendNotification;
import com.stockboo.app.domain.utility.DateUtil;
import com.stockboo.app.entity.Nifty;
import com.stockboo.app.entity.Sensex;
import com.stockboo.app.firebase.PushNotification;
import com.stockboo.app.service.HolidayService;

@RestController
@SuppressWarnings("serial")
@RequestMapping("/cron")
public class NotificationsPostCronJob {
	private static final Logger log = Logger.getLogger(NotificationsPostCronJob.class.getName());

	@RequestMapping(value=	"/notificationspostfirst", method = RequestMethod.GET)
	public void firstNotification(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		sendNotification();

	}
	
	@RequestMapping(value=	"/notificationspostsecond", method = RequestMethod.GET)
	public void secondNotification(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		sendNotification();

	}

	private void sendNotification() {
		System.out.println("In Push notifications CronJob");
		if (HolidayService.isSaturdayOrSundayOrHoliday()) {
			return;
		}

//		List<Nifty> niftyEntityList = null;
//		Query niftyQuery = pm.newQuery(Nifty.class);
//		// niftyQuery.setFilter("this.createdAt < createdAt");
//		// niftyQuery.declareParameters("java.util.Date createdAt");
//		niftyEntityList = (List<Nifty>) niftyQuery.execute();
		
		List<Nifty> niftyEntityList = new ArrayList<>();
		Query niftyQuery = new Query(Nifty.TABLE_NAME);
		PreparedQuery niftyResults = DatastoreServiceFactory.getDatastoreService().prepare(niftyQuery);
		for (Entity entity : niftyResults.asIterable()) {
			niftyEntityList.add(new Nifty(entity));
		}
		
		Nifty niftyEntity = niftyEntityList.get(0);
		float niftyPoints = 0;
		int niftyDisplayPoints = 0;
		if (!niftyEntityList.isEmpty()) {
			if (niftyEntity.getChangePrice() != "" || !niftyEntity.getChangePrice().isEmpty()) {
				System.out.println(niftyEntity.getChangePrice());
				niftyPoints = Float.parseFloat(niftyEntity.getChangePrice().replaceAll("^\"|\"$", ""));
				System.out.println(niftyEntity.getLtp());
				niftyDisplayPoints = getRoundValue(niftyEntity.getLtp());
				niftyDisplayPoints = roundOffTo50(niftyDisplayPoints);
			}
		}

//		List<Sensex> sensexEntityList = null;
//		Query sensexQuery = pm.newQuery(Sensex.class);
//		// sensexQuery.setFilter("this.createdAt < createdAt");
//		// sensexQuery.declareParameters("java.util.Date createdAt");
//		sensexEntityList = (List<Sensex>) sensexQuery.execute();
//		Sensex sensexEntity = sensexEntityList.get(0);
		
		List<Sensex> sensexEntityList = new ArrayList<>();
		Query sensexQuery = new Query(Sensex.TABLE_NAME);
		PreparedQuery sensexQueryResults = DatastoreServiceFactory.getDatastoreService().prepare(sensexQuery);
		for (Entity entity : sensexQueryResults.asIterable()) {
			sensexEntityList.add(new Sensex(entity));
		}
		Sensex sensexEntity = sensexEntityList.get(0);
		
		int sensexPoints = 0;
		if (!sensexEntityList.isEmpty()) {
			if (sensexEntity.getChange() != "" || !sensexEntity.getChange().isEmpty()) {
				System.out.println(sensexEntity.getChange());
				sensexPoints = getRoundValue(sensexEntity.getChange().replaceAll("^\"|\"$", ""));
			}
		}
		String messageString = "";
		messageString = midDayMessage(niftyPoints, niftyDisplayPoints, sensexPoints);
		
		System.out.println("************************************");
		System.out.println("Sensenx points ---"+sensexPoints+ "  Nifty points---"+ niftyPoints);
		System.out.println(messageString);
		SendNotification sendNotofication = new SendNotification("Market update", messageString, "market_alerts", true);
		
		try {
			SimpleDateFormat parser = new SimpleDateFormat("HH:mm");
			parser.setTimeZone(TimeZone.getTimeZone("IST"));
			Date userDate = parser.parse(DateUtil.getIST_CurrentTime());
			if (userDate.after(parser.parse("09:15")) && userDate.before(parser.parse("09:35"))) {
				sendNotofication.setTitle("Opening Bell");
				sendNotofication.setBody(openingBellMessage(niftyPoints, niftyDisplayPoints, sensexPoints));
			} else if (userDate.after(parser.parse("15:35")) && userDate.before(parser.parse("15:50"))) {
				sendNotofication.setTitle("Closing Bell");
				sendNotofication.setBody(closingBellMessage(niftyPoints, niftyDisplayPoints, sensexPoints));
			}
			System.out.println(sendNotofication.getTitle());
			System.out.println("**************************************");
			PushNotification.sendNotificationToTopic(sendNotofication);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private String openingBellMessage(float niftyPoints, int niftyDisplayPoints, int sensexPoints) {
		String messageString = "";
		if (sensexPoints > 30 && niftyPoints > 10) {
			messageString = messageString + "Sensex rises " + sensexPoints
					+ " points, Nifty above " + niftyDisplayPoints + " points; "+ getGainers();

		} else if (sensexPoints < -30 && niftyPoints < -10) {
			messageString = messageString + "Sensex sheds " + sensexPoints
					+ " points, Nifty below " + niftyDisplayPoints + " points; " + getLoosers();
		} else {
			messageString = messageString + "Markets start on cautious note Sensex rises " + sensexPoints + " points"
					+ ", Nifty near " + niftyDisplayPoints + " points; " + getFlatModeMessage();
		}
		return messageString;
	}
	
	private String midDayMessage(float niftyPoints, int niftyDisplayPoints, int sensexPoints) {
		String messageString = "";
		if (sensexPoints > 30 && niftyPoints > 10) {
			messageString = messageString + "Market is in positive mode, Sensex is up by " + sensexPoints
					+ " points and Nifty is above " + niftyDisplayPoints + " points.";

		} else if (sensexPoints < -30 && niftyPoints < -10) {
			messageString = messageString + "Market is in Negative mode, Sensex is down by " + sensexPoints
					+ " points and Nifty is below " + niftyDisplayPoints + " points.";
		} else {
			messageString = messageString + "Market is flat";

			if (sensexPoints > 0) {
				messageString = messageString + ", Sensex is up by " + sensexPoints + " points and ";

			} else {
				messageString = messageString + ", Sensex is down by " + sensexPoints + " points and ";
			}

			if (niftyPoints > 0) {
				messageString = messageString + "Nifty is near " + niftyDisplayPoints + " points.";

			} else {
				messageString = messageString + "Nifty is near " + niftyDisplayPoints + " points.";

			}
		}
		return messageString;
	}
	
	private String closingBellMessage(float niftyPoints, int niftyDisplayPoints, int sensexPoints) {
		String messageString = "";
		if (sensexPoints > 30 && niftyPoints > 10) {
			messageString = messageString + "Sensex ends " + sensexPoints
					+ " points higher, Nifty tops " + niftyDisplayPoints + " points; " + getGainers();

		} else if (sensexPoints < -30 && niftyPoints < -10) {
			messageString = messageString + "Sensex ends " + sensexPoints
					+ " points lower, Nifty below " + niftyDisplayPoints + " points; " + getLoosers();
		} else {
			messageString = messageString + "Sensex ends in flat note";

			if (sensexPoints > 0) {
				messageString = messageString + "up by " + sensexPoints + " points, ";

			} else {
				messageString = messageString + "down by " + sensexPoints + " points, ";
			}

			if (niftyPoints > 0) {
				messageString = messageString + "Nifty settles above " + niftyDisplayPoints + " points; " + getFlatModeMessage();

			} else {
				messageString = messageString + "Nifty settles below " + niftyDisplayPoints + " points; " +getFlatModeMessage();

			}
		}
		return messageString;
	}
	
	private String getGainers(){
		String gainers = "" ;
		HomeScreenDao hsd = new HomeScreenDao();
		List<GainersLoosersDataDomain> topThreeGainers = hsd.topThreeGainers();
		if(getRoundValue( topThreeGainers.get(0).getPChange() )==getRoundValue( topThreeGainers.get(1).getPChange() )){
			gainers = topThreeGainers.get(0).getSymbol() +  " and "
					+ topThreeGainers.get(1).getSymbol() + " up by " + getRoundValue(topThreeGainers.get(1).getPChange()) + "%";
		}else{
			gainers = topThreeGainers.get(0).getSymbol() + " up by " + getRoundValue(topThreeGainers.get(0).getPChange()) + "%" + ", "
					+ topThreeGainers.get(1).getSymbol() + " up by " + getRoundValue(topThreeGainers.get(1).getPChange()) + "%";
		}
		
		return gainers;
	}
	
	private String getLoosers(){
		String loosers = "";
		HomeScreenDao hsd = new HomeScreenDao();
		List<GainersLoosersDataDomain> topThreeLoosers = hsd.topThreeGainers();
		if(getRoundValue( topThreeLoosers.get(0).getPChange() )==getRoundValue( topThreeLoosers.get(1).getPChange() )){
			loosers = topThreeLoosers.get(0).getSymbol() +  " and "
					+ topThreeLoosers.get(1).getSymbol() + " slips by " + getRoundValue(topThreeLoosers.get(1).getPChange()) + "%";
		}else{
			loosers = topThreeLoosers.get(0).getSymbol() + " slips by " + getRoundValue(topThreeLoosers.get(0).getPChange()) + "%" + ", "
					+ topThreeLoosers.get(1).getSymbol() + " slips by " + getRoundValue(topThreeLoosers.get(1).getPChange()) + "%";
		}
		return loosers;
	}
	
	private String getFlatModeMessage() {
		String gainersAndLoosers = "";
		HomeScreenDao hsd = new HomeScreenDao();
		List<GainersLoosersDataDomain> topThreeGainers = hsd.topThreeGainers();
		List<GainersLoosersDataDomain> topThreeLoosers = hsd.topThreeLoosers();

		if (getRoundValue(topThreeGainers.get(0).getPChange()) == getRoundValue(topThreeLoosers.get(0).getPChange())) {
			gainersAndLoosers = topThreeGainers.get(0).getSymbol() + " up and " + topThreeLoosers.get(0).getSymbol()
					+ " slips by " + getRoundValue(topThreeLoosers.get(0).getPChange()) + "%";
		} else {
			gainersAndLoosers = topThreeGainers.get(0).getSymbol() + " up by " + getRoundValue(topThreeGainers.get(0).getPChange()) + "%"
					+ ",  " + topThreeLoosers.get(0).getSymbol() + " slips by " + getRoundValue(topThreeLoosers.get(0).getPChange()) + "%";
		}
		return gainersAndLoosers;
	}
	
	private int getRoundValue(String value){
		if(value.contains(",")){
			String[] value1 = value.split(",");
			value = value1[0]+ value1[1];
		}
		float floatVal = Float.parseFloat(value.replaceAll("^\"|\"$", ""));
		return (int)Math.round(floatVal);
	}
	
	private int  roundOffTo50(int num){
		int d = num/50;
		int rem = num % 50;
		return num = rem>=35 ? ((d*50)+50) : (d*50);
	}
	
}