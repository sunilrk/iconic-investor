package com.stockboo.app.cronjobs;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.EntityNotFoundException;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.Text;
import com.google.appengine.api.memcache.ErrorHandlers;
import com.google.appengine.api.memcache.MemcacheService;
import com.google.appengine.api.memcache.MemcacheServiceFactory;
import com.google.appengine.api.urlfetch.FetchOptions;
import com.google.appengine.api.urlfetch.HTTPHeader;
import com.google.appengine.api.urlfetch.HTTPMethod;
import com.google.appengine.api.urlfetch.HTTPRequest;
import com.google.appengine.api.urlfetch.HTTPResponse;
import com.google.appengine.api.urlfetch.URLFetchService;
import com.google.appengine.api.urlfetch.URLFetchServiceFactory;
import com.google.gson.Gson;
import com.stockboo.app.configuration.Constants;
import com.stockboo.app.entity.HighLow52Entity;
import com.stockboo.app.service.HolidayService;

@RestController
@SuppressWarnings("serial")
@RequestMapping("/cron")
public class HighLow52CronJob {
	private static final Logger log = Logger.getLogger(HighLow52CronJob.class.getName());
	public static final String JOB_PARENT = "notification_job_parent";

	@RequestMapping(value=	"/highlow", method = RequestMethod.GET)
	public void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		System.out.println("In HighLow52 CronJob");
		
		if(HolidayService.isSaturdayOrSundayOrHoliday()){
			return;
		}
		
		URLFetchService fetcher = URLFetchServiceFactory.getURLFetchService();
		FetchOptions fetchOptions = FetchOptions.Builder.validateCertificate();
		
		HTTPRequest requestForHigh = new HTTPRequest(new URL(Constants.HIGH52_URL), HTTPMethod.GET, fetchOptions);
		requestForHigh.setHeader(new HTTPHeader(Constants.HEADER_USER_AGENT, Constants.HEADER_USER_AGENT_VALUE));
		HTTPResponse responseHigh = fetcher.fetch(requestForHigh);
		System.out.println("High52 Response code "+responseHigh.getResponseCode());
		String jsonStringHigh = httpResponseToJsonString(responseHigh);
		Gson gson = new Gson();
		HighLow52 high52 = gson.fromJson(jsonStringHigh, HighLow52.class);
		List<HighLow52Data> topHighThreeList = new ArrayList<>();
		topHighThreeList.add(high52.getData().get(0));
		topHighThreeList.add(high52.getData().get(1));
//		topHighThreeList.add(high52.getData().get(2));
		HighLow52 highDomain = new HighLow52();
		highDomain.setData(topHighThreeList);
		
		String topHighThreeString = gson.toJson(topHighThreeList);
		
		MemcacheService memcache = MemcacheServiceFactory.getMemcacheService();
		memcache.setErrorHandler(ErrorHandlers.getConsistentLogAndContinue(Level.INFO));
		memcache.delete("high52");
		memcache.put("high52", jsonStringHigh);
		memcache.delete("topThreeHigh");
		memcache.put("topThreeHigh", topHighThreeString);
		
		HTTPRequest requestForLow = new HTTPRequest(new URL(Constants.LOW52_URL), HTTPMethod.GET, fetchOptions);
		requestForLow.setHeader(new HTTPHeader(Constants.HEADER_USER_AGENT, Constants.HEADER_USER_AGENT_VALUE));
		HTTPResponse responseLow = fetcher.fetch(requestForLow);
		System.out.println("Low52 Response code "+responseLow.getResponseCode());
		String jsonStringLow = httpResponseToJsonString(responseLow);
		HighLow52 low52 = gson.fromJson(jsonStringLow, HighLow52.class);
		List<HighLow52Data> topLowThreeList = new ArrayList<>();
		topLowThreeList.add(low52.getData().get(0));
		topLowThreeList.add(low52.getData().get(1));
//		topLowThreeList.add(low52.getData().get(2));
		HighLow52 lowDomain = new HighLow52();
		lowDomain.setData(topLowThreeList);
		
		String topLowThreeString = gson.toJson(topLowThreeList);
		
		memcache.delete("low52");
		memcache.put("low52", jsonStringLow);
		memcache.delete("topThreeLow");
		memcache.put("topThreeLow", topLowThreeString);
		
		HighLow52Entity high52Entity = null;
		HighLow52Entity low52Entity = null;
		
		try {
			high52Entity = new HighLow52Entity(getEntityById("52NewHigh", HighLow52Entity.TABLE_NAME));
			low52Entity = new HighLow52Entity(getEntityById("52NewLow", HighLow52Entity.TABLE_NAME));
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		if (high52Entity == null) {
			high52Entity = new HighLow52Entity();
			high52Entity.setCreatedAt(new Date());
		} else {
			high52Entity.setUpdatedAt(new Date());
		}
		high52Entity.setType("52NewHigh");
		high52Entity.setFullDetails(new Text(jsonStringHigh));
		high52Entity.setTopThree(new Text(topHighThreeString));
		
		if (low52Entity == null) {
			low52Entity = new HighLow52Entity();
			low52Entity.setCreatedAt(new Date());
		} else {
			low52Entity.setUpdatedAt(new Date());
		}
		low52Entity.setType("52NewLow");
		low52Entity.setFullDetails(new Text(jsonStringLow));
		low52Entity.setTopThree(new Text(topLowThreeString));
		
		try {
			DatastoreServiceFactory.getDatastoreService().put(high52Entity.buildEntity());
			DatastoreServiceFactory.getDatastoreService().put(low52Entity.buildEntity());
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}

	private String  httpResponseToJsonString(HTTPResponse response) {
		String urlFetchedHtml = null;
		if (response != null) {
	            StringBuilder stringBuilder = new StringBuilder();
	            for (byte b : response.getContent()) {
	                stringBuilder.append((char) b);
	            }
	            urlFetchedHtml = stringBuilder.toString();
	            System.out.println("*************************"+urlFetchedHtml);}
		
		return urlFetchedHtml;
	}

	private Entity getEntityById(String id, String tableName) throws Exception {
		Key key = KeyFactory.createKey(tableName, id);
		System.out.println("Fetching entity for the " + key);
		try {
			return DatastoreServiceFactory.getDatastoreService().get(key);
		} catch (EntityNotFoundException e) {
			e.printStackTrace();
			throw new Exception("Record not found");
		}
	}
	
}