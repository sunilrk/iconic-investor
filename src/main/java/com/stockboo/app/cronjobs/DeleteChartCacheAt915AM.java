package com.stockboo.app.cronjobs;

import java.io.IOException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.google.appengine.api.memcache.ErrorHandlers;
import com.google.appengine.api.memcache.MemcacheService;
import com.google.appengine.api.memcache.MemcacheServiceFactory;
import com.stockboo.app.entity.SensexAndNiftyPrices;
import com.stockboo.app.service.HolidayService;

/**
 * This class deletes the cache at 9.15AM ISD
 * 
 * @author sunil.r
 *
 */
@RestController
@SuppressWarnings("serial")
@RequestMapping("/cron")
public class DeleteChartCacheAt915AM  {
	private static final Logger log = Logger.getLogger(SensexAndNiftyCronJob.class.getName());
	public static final String JOB_PARENT = "notification_job_parent";

	@RequestMapping(value=	"/deletechartcache", method = RequestMethod.GET)
	public void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {

		System.out.println("Delete cache at 9.15 AM");

		if (HolidayService.isSaturdayOrSundayOrHoliday()) {
			return;
		}

		MemcacheService memcache = MemcacheServiceFactory.getMemcacheService();
		memcache.setErrorHandler(ErrorHandlers.getConsistentLogAndContinue(Level.INFO));
		List<SensexAndNiftyPrices> sensexAndNiftyPrices = (List<SensexAndNiftyPrices>) memcache
				.get("sensexAndNiftyPrices");
		if (null != sensexAndNiftyPrices || !sensexAndNiftyPrices.isEmpty()) {
			sensexAndNiftyPrices.clear();
			memcache.put("sensexAndNiftyPrices", sensexAndNiftyPrices);
			System.out.println("Chart cache cleared" + "cache value:" + memcache.get("sensexAndNiftyPrices"));
		}
	}
}