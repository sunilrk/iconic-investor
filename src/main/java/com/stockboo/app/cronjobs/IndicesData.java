package com.stockboo.app.cronjobs;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class IndicesData {

@SerializedName("name")
@Expose
private String name;
@SerializedName("lastPrice")
@Expose
private String lastPrice;
@SerializedName("change")
@Expose
private String change;
@SerializedName("pChange")
@Expose
private String pChange;
@SerializedName("imgFileName")
@Expose
private String imgFileName;

public String getName() {
return name;
}

public void setName(String name) {
this.name = name;
}

public String getLastPrice() {
return lastPrice;
}

public void setLastPrice(String lastPrice) {
this.lastPrice = lastPrice;
}

public String getChange() {
return change;
}

public void setChange(String change) {
this.change = change;
}

public String getPChange() {
return pChange;
}

public void setPChange(String pChange) {
this.pChange = pChange;
}

public String getImgFileName() {
return imgFileName;
}

public void setImgFileName(String imgFileName) {
this.imgFileName = imgFileName;
}

}