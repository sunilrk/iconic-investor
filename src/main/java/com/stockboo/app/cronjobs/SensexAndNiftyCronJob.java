package com.stockboo.app.cronjobs;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

//import javax.jdo.PersistenceManager;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;	

import com.google.appengine.api.memcache.ErrorHandlers;
import com.google.appengine.api.memcache.MemcacheService;
import com.google.appengine.api.memcache.MemcacheServiceFactory;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.stockboo.app.dao.HomeScreenDao;
//import com.stockboo.app.dao.PMF;
import com.stockboo.app.domain.utility.DateUtil;
import com.stockboo.app.entity.Nifty;
import com.stockboo.app.entity.Sensex;
import com.stockboo.app.entity.SensexAndNiftyPrices;
import com.stockboo.app.service.HolidayService;

@Deprecated
@SuppressWarnings("serial")
public class SensexAndNiftyCronJob extends HttpServlet {
	private static final Logger log = Logger.getLogger(SensexAndNiftyCronJob.class.getName());
	public static final String JOB_PARENT = "notification_job_parent";

	public void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		
		System.out.println("In sensex and nifty cron job");
		
		if(HolidayService.isSaturdayOrSundayOrHoliday()){
			return;
		}
		
		URL url = new URL("http://finance.google.com/finance/info?client=ig&q=INDEXBOM:SENSEX,NSE:NIFTY");
		System.out.println("URL - " + url.toString());
		BufferedReader reader = new BufferedReader(new InputStreamReader(url.openStream()));
		StringBuffer json = new StringBuffer();
		String line;
//		System.out.println("Response -" + json.toString());

		while ((line = reader.readLine()) != null) {
			if (line.contains("//")) {
				json.append(line.substring(3));
			} else
				json.append(line);
//			System.out.println("line-" + line);
		}
		reader.close();
		System.out.println("Final Json - " + json.toString());
		Gson gson = new Gson();

		List<GoogleApi> api = gson.fromJson(json.toString(), new TypeToken<List<GoogleApi>>() {
		}.getType());
		System.out.println(api.get(0).getL() + "   " + api.get(1).getL());
		GoogleApi niftyData = api.get(1);
		GoogleApi sensexData = api.get(0);
//		PersistenceManager pm = PMF.get().getPersistenceManager();
		Nifty niftyEntity = null;
		Sensex sensexEntity = null;
		try {
//			niftyEntity = pm.getObjectById(Nifty.class, api.get(0).getT());
//			sensexEntity = pm.getObjectById(Sensex.class, api.get(1).getT());
		} /*catch (javax.jdo.JDOObjectNotFoundException e) {

		}*/catch(Exception e){
			
		}
		if (niftyEntity == null) {
			niftyEntity = new Nifty();
			niftyEntity.setCreatedAt(new Date());
		} else {
			niftyEntity.setUpdatedAt(new Date());
		}
		if (sensexEntity == null) {
			sensexEntity = new Sensex();
			sensexEntity.setCreatedAt(new Date());
		} else {
			sensexEntity.setUpdatedAt(new Date());
		}
		niftyEntity = getNifty(niftyData, niftyEntity);
		sensexEntity = getSensex(sensexData, sensexEntity);
		SensexAndNiftyPrices sensexAndNiftyPricesEntity = new SensexAndNiftyPrices(niftyData.getL(), sensexData.getL(), new Date());
		
		MemcacheService memcache = MemcacheServiceFactory.getMemcacheService();
		memcache.setErrorHandler(ErrorHandlers.getConsistentLogAndContinue(Level.INFO));
		memcache.delete("nifty");
		memcache.put("nifty", niftyEntity);
		
		memcache.delete("sensex");
		memcache.put("sensex", sensexEntity);
		
		List<SensexAndNiftyPrices> sensexAndNiftyPrices = null;
		
		if(null == memcache.get("sensexAndNiftyPrices")){
			System.out.println("DEcare list*******************************");
			HomeScreenDao hsd = new HomeScreenDao();
			Date weekDay = DateUtil.getWeekDay();
			sensexAndNiftyPrices = new ArrayList<>();
//			List<SensexAndNiftyPrices> sensexAndNiftyListFromDB = hsd.getSensexAndNiftyListFromDB(weekDay, pm);
			/*for (SensexAndNiftyPrices sensexAndNiftyPrices2 : sensexAndNiftyListFromDB) {
				sensexAndNiftyPrices.add(sensexAndNiftyPrices2);
			}*/
		}else { 
			System.out.println("From cache*******************************");
		sensexAndNiftyPrices = (List<SensexAndNiftyPrices>) memcache.get("sensexAndNiftyPrices");
		}
		sensexAndNiftyPrices.add(sensexAndNiftyPricesEntity);
		
		memcache.delete("sensexAndNiftyPrices");
		memcache.put("sensexAndNiftyPrices", sensexAndNiftyPrices);
		
		try {
//			pm.makePersistent(niftyEntity);
//			pm.makePersistent(sensexEntity);
//			pm.makePersistent(sensexAndNiftyPricesEntity);
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}

	private Nifty getNifty(GoogleApi googleApi, Nifty nifty) {
		if (nifty == null) {
			nifty = new Nifty();
		}
		nifty.setChangePrice(googleApi.getC());
		nifty.setChr(googleApi.getCcol());
		nifty.setChangePriceFinal(googleApi.getCFix());
		nifty.setChangePercentage(googleApi.getCp());
		nifty.setChangePercentageFinal(googleApi.getCpFix());
		nifty.setExchange(googleApi.getE());
//		nifty.setId(googleApi.getId());
		nifty.setLtp(googleApi.getL());
		nifty.setLCur(googleApi.getLCur());
		nifty.setLtpFinal(googleApi.getLFix());
		nifty.setLastTradeTimeFormatted(googleApi.getLt());
		nifty.setLastTradedDateTime(googleApi.getLtDts());
		nifty.setLastTradedTime(googleApi.getLtt());
		nifty.setPreviousClose(googleApi.getPclsFix());
		nifty.setS(googleApi.getS());
		nifty.setSymbol(googleApi.getT());
		return nifty;

	}

	private Sensex getSensex(GoogleApi googleApi, Sensex sensex) {
		if (sensex == null) {
			sensex = new Sensex();
		}
		sensex.setChange(googleApi.getC());
		sensex.setChr(googleApi.getCcol());
		sensex.setChangePercentageFinal(googleApi.getCFix());
		sensex.setChangePercentage(googleApi.getCp());
		sensex.setCpFix(googleApi.getCpFix());
		sensex.seExchangeE(googleApi.getE());
//		sensex.setId(googleApi.getId());
		sensex.setLtp(googleApi.getL());
		sensex.setLCur(googleApi.getLCur());
		sensex.setLtpFinal(googleApi.getLFix());
		sensex.setLt(googleApi.getLt());
		sensex.setLastTradedDateTime(googleApi.getLtDts());
		sensex.setLastTradedTime(googleApi.getLtt());
		sensex.setpreviousClose(googleApi.getPclsFix());
		sensex.setS(googleApi.getS());
		sensex.setSymbol(googleApi.getT());
		return sensex;

	}
}