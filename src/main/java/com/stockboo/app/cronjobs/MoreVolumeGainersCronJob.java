package com.stockboo.app.cronjobs;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.bind.annotation.RestController;

import com.google.appengine.api.datastore.Text;
import com.google.appengine.api.memcache.ErrorHandlers;
import com.google.appengine.api.memcache.MemcacheService;
import com.google.appengine.api.memcache.MemcacheServiceFactory;
import com.google.appengine.api.urlfetch.FetchOptions;
import com.google.appengine.api.urlfetch.HTTPHeader;
import com.google.appengine.api.urlfetch.HTTPMethod;
import com.google.appengine.api.urlfetch.HTTPRequest;
import com.google.appengine.api.urlfetch.HTTPResponse;
import com.google.appengine.api.urlfetch.URLFetchService;
import com.google.appengine.api.urlfetch.URLFetchServiceFactory;
import com.google.gson.Gson;
import com.stockboo.app.configuration.Constants;
import com.stockboo.app.entity.GainersLoosersEntity;
import com.stockboo.app.service.HolidayService;

@RestController
@SuppressWarnings("serial")
public class MoreVolumeGainersCronJob extends HttpServlet {
	private static final Logger log = Logger.getLogger(MoreVolumeGainersCronJob.class.getName());

	public void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		System.out.println("In MoreVolumeGainers CronJob");
		
		if(HolidayService.isSaturdayOrSundayOrHoliday()){
			return;
		}
		
		URLFetchService fetcher = URLFetchServiceFactory.getURLFetchService();
		FetchOptions fetchOptions = FetchOptions.Builder.validateCertificate();
		
		HTTPRequest request = new HTTPRequest(new URL(Constants.MOREVOLUME_GAINERS_URL), HTTPMethod.GET, fetchOptions);
		request.setHeader(new HTTPHeader(Constants.HEADER_USER_AGENT, Constants.HEADER_USER_AGENT_VALUE));
		HTTPResponse response = fetcher.fetch(request);
		System.out.println("Response code "+response.getResponseCode());
		String jsonString = httpResponseToJsonString(response);
		
		Gson gson = new Gson();
		GainersLoosers moreVolumeGainers = gson.fromJson(jsonString, GainersLoosers.class);
		List<GainersLoosersData> topGainersThreeList = new ArrayList<>();
		topGainersThreeList.add(moreVolumeGainers.getData().get(0));
		topGainersThreeList.add(moreVolumeGainers.getData().get(1));
		topGainersThreeList.add(moreVolumeGainers.getData().get(2));
		
		String topThreeString = gson.toJson(topGainersThreeList);
		
		MemcacheService memcache = MemcacheServiceFactory.getMemcacheService();
		memcache.setErrorHandler(ErrorHandlers.getConsistentLogAndContinue(Level.INFO));
		memcache.delete("MoreVolumeGainers");
		memcache.put("MoreVolumeGainers", jsonString);
		memcache.delete("topThreeMoreVoulmeGainers");
		memcache.put("topThreeMoreVoulmeGainers", topThreeString);
		
//		PersistenceManager pm = PMF.get().getPersistenceManager();
		GainersLoosersEntity moreVolumeGainersEntity = null;
		/*try {
			moreVolumeGainersEntity = pm.getObjectById(GainersLoosersEntity.class, "MoreVolumeGainers");
		} catch (javax.jdo.JDOObjectNotFoundException e) {
*/
//		}
		
//		if (moreVolumeGainersEntity == null) {
//			moreVolumeGainersEntity = new GainersLoosersEntity();
//			moreVolumeGainersEntity.setCreatedAt(new Date());
//		} else {
//			moreVolumeGainersEntity.setUpdatedAt(new Date());
//		}
//		moreVolumeGainersEntity.setType("MoreVolumeGainers");
//		moreVolumeGainersEntity.setFullDetails(new Text(jsonString));
//		moreVolumeGainersEntity.setTopThree(new Text(topThreeString));
		
		try {
//			pm.makePersistent(moreVolumeGainersEntity);
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
//		pm.close();
	}

	private String  httpResponseToJsonString(HTTPResponse response) {
		String urlFetchedHtml = null;
		if (response != null) {
	            StringBuilder stringBuilder = new StringBuilder();
	            for (byte b : response.getContent()) {
	                stringBuilder.append((char) b);
	            }
	            urlFetchedHtml = stringBuilder.toString();
	            System.out.println("*************************"+urlFetchedHtml);}
		
		return urlFetchedHtml;
	}

	
}