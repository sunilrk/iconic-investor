package com.stockboo.app.cronjobs.domain;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class NewGoogleApi {

@SerializedName("symbol")
@Expose
private String symbol;
@SerializedName("exchange")
@Expose
private String exchange;
@SerializedName("id")
@Expose
private String id;
@SerializedName("t")
@Expose
private String t;
@SerializedName("e")
@Expose
private String e;
@SerializedName("name")
@Expose
private String name;
@SerializedName("f_recent_quarter_date")
@Expose
private String fRecentQuarterDate;
@SerializedName("f_annual_date")
@Expose
private String fAnnualDate;
@SerializedName("f_ttm_date")
@Expose
private String fTtmDate;
@SerializedName("financials")
@Expose
private List<Financial> financials = null;
@SerializedName("kr_recent_quarter_date")
@Expose
private String krRecentQuarterDate;
@SerializedName("kr_annual_date")
@Expose
private String krAnnualDate;
@SerializedName("kr_ttm_date")
@Expose
private String krTtmDate;
@SerializedName("keyratios")
@Expose
private List<Object> keyratios = null;
@SerializedName("c")
@Expose
private String c;
@SerializedName("l")
@Expose
private String l;
@SerializedName("cp")
@Expose
private String cp;
@SerializedName("ccol")
@Expose
private String ccol;
@SerializedName("op")
@Expose
private String op;
@SerializedName("hi")
@Expose
private String hi;
@SerializedName("lo")
@Expose
private String lo;
@SerializedName("vo")
@Expose
private String vo;
@SerializedName("avvo")
@Expose
private String avvo;
@SerializedName("hi52")
@Expose
private String hi52;
@SerializedName("lo52")
@Expose
private String lo52;
@SerializedName("mc")
@Expose
private String mc;
@SerializedName("pe")
@Expose
private String pe;
@SerializedName("fwpe")
@Expose
private String fwpe;
@SerializedName("beta")
@Expose
private String beta;
@SerializedName("eps")
@Expose
private String eps;
@SerializedName("dy")
@Expose
private String dy;
@SerializedName("ldiv")
@Expose
private String ldiv;
@SerializedName("shares")
@Expose
private String shares;
@SerializedName("instown")
@Expose
private String instown;
@SerializedName("eo")
@Expose
private String eo;

public String getSymbol() {
return symbol;
}

public void setSymbol(String symbol) {
this.symbol = symbol;
}

public String getExchange() {
return exchange;
}

public void setExchange(String exchange) {
this.exchange = exchange;
}

public String getId() {
return id;
}

public void setId(String id) {
this.id = id;
}

public String getT() {
return t;
}

public void setT(String t) {
this.t = t;
}

public String getE() {
return e;
}

public void setE(String e) {
this.e = e;
}

public String getName() {
return name;
}

public void setName(String name) {
this.name = name;
}

public String getFRecentQuarterDate() {
return fRecentQuarterDate;
}

public void setFRecentQuarterDate(String fRecentQuarterDate) {
this.fRecentQuarterDate = fRecentQuarterDate;
}

public String getFAnnualDate() {
return fAnnualDate;
}

public void setFAnnualDate(String fAnnualDate) {
this.fAnnualDate = fAnnualDate;
}

public String getFTtmDate() {
return fTtmDate;
}

public void setFTtmDate(String fTtmDate) {
this.fTtmDate = fTtmDate;
}

public List<Financial> getFinancials() {
return financials;
}

public void setFinancials(List<Financial> financials) {
this.financials = financials;
}

public String getKrRecentQuarterDate() {
return krRecentQuarterDate;
}

public void setKrRecentQuarterDate(String krRecentQuarterDate) {
this.krRecentQuarterDate = krRecentQuarterDate;
}

public String getKrAnnualDate() {
return krAnnualDate;
}

public void setKrAnnualDate(String krAnnualDate) {
this.krAnnualDate = krAnnualDate;
}

public String getKrTtmDate() {
return krTtmDate;
}

public void setKrTtmDate(String krTtmDate) {
this.krTtmDate = krTtmDate;
}

public List<Object> getKeyratios() {
return keyratios;
}

public void setKeyratios(List<Object> keyratios) {
this.keyratios = keyratios;
}

public String getC() {
return c;
}

public void setC(String c) {
this.c = c;
}

public String getL() {
return l;
}

public void setL(String l) {
this.l = l;
}

public String getCp() {
return cp;
}

public void setCp(String cp) {
this.cp = cp;
}

public String getCcol() {
return ccol;
}

public void setCcol(String ccol) {
this.ccol = ccol;
}

public String getOp() {
return op;
}

public void setOp(String op) {
this.op = op;
}

public String getHi() {
return hi;
}

public void setHi(String hi) {
this.hi = hi;
}

public String getLo() {
return lo;
}

public void setLo(String lo) {
this.lo = lo;
}

public String getVo() {
return vo;
}

public void setVo(String vo) {
this.vo = vo;
}

public String getAvvo() {
return avvo;
}

public void setAvvo(String avvo) {
this.avvo = avvo;
}

public String getHi52() {
return hi52;
}

public void setHi52(String hi52) {
this.hi52 = hi52;
}

public String getLo52() {
return lo52;
}

public void setLo52(String lo52) {
this.lo52 = lo52;
}

public String getMc() {
return mc;
}

public void setMc(String mc) {
this.mc = mc;
}

public String getPe() {
return pe;
}

public void setPe(String pe) {
this.pe = pe;
}

public String getFwpe() {
return fwpe;
}

public void setFwpe(String fwpe) {
this.fwpe = fwpe;
}

public String getBeta() {
return beta;
}

public void setBeta(String beta) {
this.beta = beta;
}

public String getEps() {
return eps;
}

public void setEps(String eps) {
this.eps = eps;
}

public String getDy() {
return dy;
}

public void setDy(String dy) {
this.dy = dy;
}

public String getLdiv() {
return ldiv;
}

public void setLdiv(String ldiv) {
this.ldiv = ldiv;
}

public String getShares() {
return shares;
}

public void setShares(String shares) {
this.shares = shares;
}

public String getInstown() {
return instown;
}

public void setInstown(String instown) {
this.instown = instown;
}

public String getEo() {
return eo;
}

public void setEo(String eo) {
this.eo = eo;
}

}