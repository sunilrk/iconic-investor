package com.stockboo.app.cronjobs.domain;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MoneyControlSensexAndNifty {

@SerializedName("refresh_details")
@Expose
private RefreshDetails refreshDetails;
@SerializedName("indices")
@Expose
private Indices indices;
@SerializedName("tabs")
@Expose
private Tabs tabs;
@SerializedName("graph_tab")
@Expose
private GraphTab graphTab;

public RefreshDetails getRefreshDetails() {
return refreshDetails;
}

public void setRefreshDetails(RefreshDetails refreshDetails) {
this.refreshDetails = refreshDetails;
}

public Indices getIndices() {
return indices;
}

public void setIndices(Indices indices) {
this.indices = indices;
}

public Tabs getTabs() {
return tabs;
}

public void setTabs(Tabs tabs) {
this.tabs = tabs;
}

public GraphTab getGraphTab() {
return graphTab;
}

public void setGraphTab(GraphTab graphTab) {
this.graphTab = graphTab;
}

}