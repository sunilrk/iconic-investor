package com.stockboo.app.cronjobs.domain;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Financial {

@SerializedName("url")
@Expose
private String url;
@SerializedName("f_figures")
@Expose
private List<Object> fFigures = null;

public String getUrl() {
return url;
}

public void setUrl(String url) {
this.url = url;
}

public List<Object> getFFigures() {
return fFigures;
}

public void setFFigures(List<Object> fFigures) {
this.fFigures = fFigures;
}
}