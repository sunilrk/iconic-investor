package com.stockboo.app.cronjobs.domain;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class RefreshDetails {

@SerializedName("flag")
@Expose
private String flag;
@SerializedName("rate")
@Expose
private String rate;

public String getFlag() {
return flag;
}

public void setFlag(String flag) {
this.flag = flag;
}

public String getRate() {
return rate;
}

public void setRate(String rate) {
this.rate = rate;
}

}