package com.stockboo.app.cronjobs;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Text;
import com.google.appengine.api.memcache.ErrorHandlers;
import com.google.appengine.api.memcache.MemcacheService;
import com.google.appengine.api.memcache.MemcacheServiceFactory;
import com.google.appengine.api.urlfetch.FetchOptions;
import com.google.appengine.api.urlfetch.HTTPMethod;
import com.google.appengine.api.urlfetch.HTTPRequest;
import com.google.appengine.api.urlfetch.HTTPResponse;
import com.google.appengine.api.urlfetch.URLFetchService;
import com.google.appengine.api.urlfetch.URLFetchServiceFactory;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.stockboo.app.configuration.Constants;
import com.stockboo.app.entity.CryptoCurrencyEntity;

@RestController
@RequestMapping("/cron")
@SuppressWarnings("serial")
public class CryptoCurrencyUpdateCronJob  {
	private static final Logger log = Logger.getLogger(CryptoCurrencyUpdateCronJob.class.getName());

	@RequestMapping(value=	"/cryptocurrency", method = RequestMethod.GET)
	public void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
			System.out.println("In CryptoCurrency udpate CronJob");
			
			URLFetchService fetcher = URLFetchServiceFactory.getURLFetchService();
			FetchOptions fetchOptions = FetchOptions.Builder.validateCertificate();
			
			HTTPRequest request = new HTTPRequest(new URL(Constants.CRYPTO_CURRENCY_URL), HTTPMethod.GET, fetchOptions);
			HTTPResponse response = fetcher.fetch(request);
			System.out.println("Response code "+response.getResponseCode());
			String jsonString = httpResponseToJsonString(response);
			System.out.println("Response in Json");
			System.out.println(jsonString);
			Gson gson = new Gson();
			List<CryptoCurrencyRaw> moreVolumeGainers = gson.fromJson(jsonString, new TypeToken<List<CryptoCurrencyRaw>>(){}.getType());
			List<CryptoCurrencyRaw> topGainersThreeList = new ArrayList<>();
			topGainersThreeList.add(moreVolumeGainers.get(0));
			System.out.println(moreVolumeGainers.get(0).getName()+" "+moreVolumeGainers.get(1).getName()+" "+moreVolumeGainers.get(2).getName());
			topGainersThreeList.add(moreVolumeGainers.get(1));
			topGainersThreeList.add(moreVolumeGainers.get(2));	
			
			String topThreeString = gson.toJson(topGainersThreeList);
			
			MemcacheService memcache = MemcacheServiceFactory.getMemcacheService();
			memcache.setErrorHandler(ErrorHandlers.getConsistentLogAndContinue(Level.INFO));
			memcache.delete("cryptocurrency");
			memcache.put("cryptocurrency", jsonString);
			memcache.delete("topThreecryptocurrency");
			memcache.put("topThreecryptocurrency", topThreeString);
			
			CryptoCurrencyEntity moreVolumeGainersEntity = null;
			try {
				moreVolumeGainersEntity = new CryptoCurrencyEntity();
				moreVolumeGainersEntity.setCreatedAt(new Date());
				moreVolumeGainersEntity.setFullDetails(new Text(jsonString));
				moreVolumeGainersEntity.setTopThree(new Text(topThreeString));
				DatastoreServiceFactory.getDatastoreService().put(moreVolumeGainersEntity.buildEntity());
			} catch (Exception e) {
				System.out.println(e.getMessage());
			}
		}

		private String  httpResponseToJsonString(HTTPResponse response) {
			String urlFetchedHtml = null;
			if (response != null) {
		            StringBuilder stringBuilder = new StringBuilder();
		            for (byte b : response.getContent()) {
		                stringBuilder.append((char) b);
		            }
		            urlFetchedHtml = stringBuilder.toString();
		            System.out.println("*************************"+urlFetchedHtml);}
			
			return urlFetchedHtml;
		}


}