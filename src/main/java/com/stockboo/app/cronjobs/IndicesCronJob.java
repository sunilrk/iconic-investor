package com.stockboo.app.cronjobs;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.EntityNotFoundException;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.Text;
import com.google.appengine.api.memcache.ErrorHandlers;
import com.google.appengine.api.memcache.MemcacheService;
import com.google.appengine.api.memcache.MemcacheServiceFactory;
import com.google.appengine.api.urlfetch.FetchOptions;
import com.google.appengine.api.urlfetch.HTTPHeader;
import com.google.appengine.api.urlfetch.HTTPMethod;
import com.google.appengine.api.urlfetch.HTTPRequest;
import com.google.appengine.api.urlfetch.HTTPResponse;
import com.google.appengine.api.urlfetch.URLFetchService;
import com.google.appengine.api.urlfetch.URLFetchServiceFactory;
import com.google.gson.Gson;
import com.stockboo.app.configuration.Constants;
import com.stockboo.app.entity.IndicesEntity;
import com.stockboo.app.service.HolidayService;

@RestController
@SuppressWarnings("serial")
@RequestMapping("/cron")
public class IndicesCronJob extends HttpServlet {
	private static final Logger log = Logger.getLogger(IndicesCronJob.class.getName());

	@RequestMapping(value=	"/indices", method = RequestMethod.GET)
	public void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		System.out.println("In Indices CronJob");
		
		if(HolidayService.isSaturdayOrSundayOrHoliday()){
			return;
		}
		
		URLFetchService fetcher = URLFetchServiceFactory.getURLFetchService();
		FetchOptions fetchOptions = FetchOptions.Builder.validateCertificate();
		
		HTTPRequest requestForIndices = new HTTPRequest(new URL(Constants.INDICES_URL), HTTPMethod.GET, fetchOptions);
		requestForIndices.setHeader(new HTTPHeader(Constants.HEADER_USER_AGENT, Constants.HEADER_USER_AGENT_VALUE));
		HTTPResponse response = fetcher.fetch(requestForIndices);
		System.out.println("Indices Response code "+response.getResponseCode());
		String jsonString = httpResponseToJsonString(response);
		
		Gson gson = new Gson();
		Indices indices = gson.fromJson(jsonString, Indices.class);
		List<IndicesData> indicesDataList = new ArrayList<>();
		for(IndicesData indicesData: indices.getData()){
			String indiceName = indicesData.getName();
			if(indiceName.equalsIgnoreCase("NIFTY BANK") || indiceName.equalsIgnoreCase("INDIA VIX") || indiceName.equalsIgnoreCase("NIFTY IT")){
				indicesDataList.add(indicesData);
			}
		}
		
		String topThreeString = gson.toJson(indicesDataList);
		
		MemcacheService memcache = MemcacheServiceFactory.getMemcacheService();
		memcache.setErrorHandler(ErrorHandlers.getConsistentLogAndContinue(Level.INFO));
		memcache.delete("indices");
		memcache.put("indices", jsonString);
		memcache.delete("topThreeIndices");
		memcache.put("topThreeIndices", topThreeString);
				
		IndicesEntity indicesEntity = null;
		try {
			indicesEntity = new IndicesEntity(getEntityById("Indices", IndicesEntity.TABLE_NAME));
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		if (indicesEntity == null) {
			indicesEntity = new IndicesEntity();
			indicesEntity.setCreatedAt(new Date());
		} else {
			indicesEntity.setUpdatedAt(new Date());
		}
		indicesEntity.setType("Indices");
		indicesEntity.setFullDetails(new Text(jsonString));
		indicesEntity.setTopThree(new Text(topThreeString));
		
		try {
			DatastoreServiceFactory.getDatastoreService().put(indicesEntity.buildEntity());
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}

	private String  httpResponseToJsonString(HTTPResponse response) {
		String urlFetchedHtml = null;
		if (response != null) {
	            StringBuilder stringBuilder = new StringBuilder();
	            for (byte b : response.getContent()) {
	                stringBuilder.append((char) b);
	            }
	            urlFetchedHtml = stringBuilder.toString();
	            System.out.println("*************************"+urlFetchedHtml);}
		
		return urlFetchedHtml;
	}
	
	private Entity getEntityById(String id, String tableName) throws Exception {
		Key key = KeyFactory.createKey(tableName, id);
		System.out.println("Fetching entity for the " + key);
		try {
			return DatastoreServiceFactory.getDatastoreService().get(key);
		} catch (EntityNotFoundException e) {
			e.printStackTrace();
			throw new Exception("Record not found");
		}
	}

	
}