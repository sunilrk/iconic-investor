package com.stockboo.app.cronjobs;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Indices {

@SerializedName("preOpen")
@Expose
private Integer preOpen;
@SerializedName("time")
@Expose
private String time;
@SerializedName("corrOpen")
@Expose
private Integer corrOpen;
@SerializedName("status")
@Expose
private String status;
@SerializedName("haltedStatus")
@Expose
private String haltedStatus;
@SerializedName("mktOpen")
@Expose
private Integer mktOpen;
@SerializedName("data")
@Expose
private List<IndicesData> data = null;
@SerializedName("code")
@Expose
private Integer code;
@SerializedName("corrClose")
@Expose
private Integer corrClose;
@SerializedName("preClose")
@Expose
private Integer preClose;
@SerializedName("mktClose")
@Expose
private Integer mktClose;

public Integer getPreOpen() {
return preOpen;
}

public void setPreOpen(Integer preOpen) {
this.preOpen = preOpen;
}

public String getTime() {
return time;
}

public void setTime(String time) {
this.time = time;
}

public Integer getCorrOpen() {
return corrOpen;
}

public void setCorrOpen(Integer corrOpen) {
this.corrOpen = corrOpen;
}

public String getStatus() {
return status;
}

public void setStatus(String status) {
this.status = status;
}

public String getHaltedStatus() {
return haltedStatus;
}

public void setHaltedStatus(String haltedStatus) {
this.haltedStatus = haltedStatus;
}

public Integer getMktOpen() {
return mktOpen;
}

public void setMktOpen(Integer mktOpen) {
this.mktOpen = mktOpen;
}

public List<IndicesData> getData() {
return data;
}

public void setData(List<IndicesData> data) {
this.data = data;
}

public Integer getCode() {
return code;
}

public void setCode(Integer code) {
this.code = code;
}

public Integer getCorrClose() {
return corrClose;
}

public void setCorrClose(Integer corrClose) {
this.corrClose = corrClose;
}

public Integer getPreClose() {
return preClose;
}

public void setPreClose(Integer preClose) {
this.preClose = preClose;
}

public Integer getMktClose() {
return mktClose;
}

public void setMktClose(Integer mktClose) {
this.mktClose = mktClose;
}

}