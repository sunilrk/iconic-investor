package com.stockboo.app.cronjobs;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.EntityNotFoundException;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.Text;
import com.google.appengine.api.memcache.ErrorHandlers;
import com.google.appengine.api.memcache.MemcacheService;
import com.google.appengine.api.memcache.MemcacheServiceFactory;
import com.google.appengine.api.urlfetch.FetchOptions;
import com.google.appengine.api.urlfetch.HTTPHeader;
import com.google.appengine.api.urlfetch.HTTPMethod;
import com.google.appengine.api.urlfetch.HTTPRequest;
import com.google.appengine.api.urlfetch.HTTPResponse;
import com.google.appengine.api.urlfetch.URLFetchService;
import com.google.appengine.api.urlfetch.URLFetchServiceFactory;
import com.google.gson.Gson;
import com.stockboo.app.configuration.Constants;
import com.stockboo.app.entity.GainersLoosersEntity;
import com.stockboo.app.service.HolidayService;

@RestController
@SuppressWarnings("serial")
@RequestMapping("/cron")
public class GainersLoosersCronJob {
	private static final Logger log = Logger.getLogger(GainersLoosersCronJob.class.getName());

	@RequestMapping(value=	"/gainerlooser", method = RequestMethod.GET)
	public void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		System.out.println("In GainersLoosers CronJob");
		
		if(HolidayService.isSaturdayOrSundayOrHoliday()){
			return;
		}
		
		URLFetchService fetcher = URLFetchServiceFactory.getURLFetchService();
		FetchOptions fetchOptions = FetchOptions.Builder.validateCertificate();
		
		HTTPRequest requestForGainer1 = new HTTPRequest(new URL(Constants.GAINERS1_URL), HTTPMethod.GET, fetchOptions);
		requestForGainer1.setHeader(new HTTPHeader(Constants.HEADER_USER_AGENT, Constants.HEADER_USER_AGENT_VALUE));
		HTTPResponse responseGainer1 = fetcher.fetch(requestForGainer1);
		System.out.println("Gainers-1 Response code "+responseGainer1.getResponseCode());
		String jsonStringGainer1 = httpResponseToJsonString(responseGainer1);
		Gson gson = new Gson();
		GainersLoosers gainer1 = gson.fromJson(jsonStringGainer1, GainersLoosers.class);
		
		HTTPRequest requestForGainer2 = new HTTPRequest(new URL(Constants.GAINERS2_URL), HTTPMethod.GET, fetchOptions);
		requestForGainer2.setHeader(new HTTPHeader(Constants.HEADER_USER_AGENT, Constants.HEADER_USER_AGENT_VALUE));
		HTTPResponse responseGainer2 = fetcher.fetch(requestForGainer2);
		System.out.println("Gainers-2 Response code "+responseGainer2.getResponseCode());
		String jsonStringGainer2 = httpResponseToJsonString(responseGainer2);
		
		GainersLoosers gainer2 = gson.fromJson(jsonStringGainer2, GainersLoosers.class);
		
		if (gainer2.getData() != null) {
			gainer1.addToList(gainer2.getData());
		}
		
		String gainerJson = gson.toJson(gainer1);
		List<GainersLoosersData> topGainersThreeList = new ArrayList<>();
		
		//To Sort on net price
				if (gainer1.getData().size() > 0) {
					Collections.sort(gainer1.getData(), new Comparator<GainersLoosersData>() {
						@Override
						public int compare(final GainersLoosersData object1, final GainersLoosersData object2) {
							return object1.getNetPrice().compareTo(object2.getNetPrice());
						}
					});
				}
				Collections.reverse(gainer1.getData());
		topGainersThreeList.add(gainer1.getData().get(0));
		topGainersThreeList.add(gainer1.getData().get(1));
		topGainersThreeList.add(gainer1.getData().get(2));
		
		String topGainersThreeString = gson.toJson(topGainersThreeList);
		
		MemcacheService memcache = MemcacheServiceFactory.getMemcacheService();
		memcache.setErrorHandler(ErrorHandlers.getConsistentLogAndContinue(Level.INFO));
		memcache.delete("gainers");
		memcache.put("gainers", gainerJson);
		memcache.delete("topThreeGainers");
		memcache.put("topThreeGainers", topGainersThreeString);
		
		HTTPRequest requestForLooser1 = new HTTPRequest(new URL(Constants.LOOSERS1_URL), HTTPMethod.GET, fetchOptions);
		requestForLooser1.setHeader(new HTTPHeader(Constants.HEADER_USER_AGENT, Constants.HEADER_USER_AGENT_VALUE));
		HTTPResponse responseLooser1 = fetcher.fetch(requestForLooser1);
		System.out.println("Looser-1 Response code "+responseLooser1.getResponseCode());
		String jsonStringLooser1 = httpResponseToJsonString(responseLooser1);
		GainersLoosers looser1 = gson.fromJson(jsonStringLooser1, GainersLoosers.class);
		
		HTTPRequest requestForLooser2 = new HTTPRequest(new URL(Constants.LOOSERS2_URL), HTTPMethod.GET, fetchOptions);
		requestForLooser2.setHeader(new HTTPHeader(Constants.HEADER_USER_AGENT, Constants.HEADER_USER_AGENT_VALUE));
		HTTPResponse responseLooser2 = fetcher.fetch(requestForLooser2);
		System.out.println("Looser-2 Response code "+responseLooser2.getResponseCode());
		String jsonStringLooser2 = httpResponseToJsonString(responseLooser2);
		GainersLoosers looser2 = gson.fromJson(jsonStringLooser2, GainersLoosers.class);
		
		if (looser2.getData() != null) {
			looser1.addToList(looser2.getData());
		}
		
		String looserJson = gson.toJson(looser1);
		List<GainersLoosersData> topLoosersThreeList = new ArrayList<>();
		
		
		//To Sort on net price
		if (looser1.getData().size() > 0) {
			Collections.sort(looser1.getData(), new Comparator<GainersLoosersData>() {
				@Override
				public int compare(final GainersLoosersData object1, final GainersLoosersData object2) {
					return object1.getNetPrice().compareTo(object2.getNetPrice());
				}
			});
		}
		Collections.reverse(looser1.getData());
		topLoosersThreeList.add(looser1.getData().get(0));
		topLoosersThreeList.add(looser1.getData().get(1));
		topLoosersThreeList.add(looser1.getData().get(2));
		
		String topLoosersThreeString = gson.toJson(topLoosersThreeList);
		
		memcache.delete("loosers");
		memcache.put("loosers", looserJson);
		memcache.delete("topThreeLoosers");
		memcache.put("topThreeLoosers", topLoosersThreeString);
		
		GainersLoosersEntity gainersEntity = null;
		GainersLoosersEntity loosersEntity = null;
		try {
				gainersEntity = new GainersLoosersEntity(getEntityById("Gainers", GainersLoosersEntity.TABLE_NAME));
				loosersEntity = new GainersLoosersEntity(getEntityById("Loosers", GainersLoosersEntity.TABLE_NAME));
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		if (gainersEntity == null) {
			gainersEntity = new GainersLoosersEntity();
			gainersEntity.setCreatedAt(new Date());
		} else {
			gainersEntity.setUpdatedAt(new Date());
		}
		gainersEntity.setType("Gainers");
		gainersEntity.setFullDetails(new Text(gainerJson));
		gainersEntity.setTopThree(new Text(topGainersThreeString));
		
		if (loosersEntity == null) {
			loosersEntity = new GainersLoosersEntity();
			loosersEntity.setCreatedAt(new Date());
		} else {
			loosersEntity.setUpdatedAt(new Date());
		}
		loosersEntity.setType("Loosers");
		loosersEntity.setFullDetails(new Text(looserJson));
		loosersEntity.setTopThree(new Text(topLoosersThreeString));
		
		try {
			DatastoreServiceFactory.getDatastoreService().put(gainersEntity.buildEntity());
			DatastoreServiceFactory.getDatastoreService().put(loosersEntity.buildEntity());
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}

	private String  httpResponseToJsonString(HTTPResponse response) {
		String urlFetchedHtml = null;
		if (response != null) {
	            StringBuilder stringBuilder = new StringBuilder();
	            for (byte b : response.getContent()) {
	                stringBuilder.append((char) b);
	            }
	            urlFetchedHtml = stringBuilder.toString();
	            System.out.println("*************************"+urlFetchedHtml);}
		
		return urlFetchedHtml;
	}
	
	private Entity getEntityById(String id, String tableName) throws Exception {
		Key key = KeyFactory.createKey(tableName, id);
		System.out.println("Fetching entity for the " + key);
		try {
			return DatastoreServiceFactory.getDatastoreService().get(key);
		} catch (EntityNotFoundException e) {
			e.printStackTrace();
			throw new Exception("Record not found");
		}
	}

	
}