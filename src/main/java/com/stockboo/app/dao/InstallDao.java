package com.stockboo.app.dao;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;


import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.PreparedQuery;
import com.google.appengine.api.datastore.Query;
import com.google.appengine.api.datastore.Query.Filter;
import com.google.appengine.api.datastore.Query.FilterOperator;
import com.google.appengine.api.datastore.Query.FilterPredicate;
import com.org.iconic.entity.IconicInvestor;
import com.org.iconic.entity.enums.IconicInvestorEnum;
import com.stockboo.app.domain.Install;
import com.stockboo.app.domain.InstallCreate;
import com.stockboo.app.domain.InstallUpdate;
import com.stockboo.app.entity.AppAuthentication;
import com.stockboo.app.entity.FcmtokenEntity;
import com.stockboo.app.entity.InstallEntity;
import com.stockboo.app.firebase.PushNotification;

/**
 * DTO for Installation JSON
 * 
 * @author sunil.r
 *
 */
public class InstallDao extends AbstractStockbooDao{

	/**
	 * Reads the installations based in installed id
	 * 
	 * @param id
	 * @return
	 * @throws Exception 
	 */
	public Install read(Long id) throws Exception {
			InstallEntity entity= new InstallEntity(getEntityById(id, InstallEntity.TABLE_NAME));
			return prepareDomain(entity);
	}

	/**
	 * Create the Installation details in database
	 * 
	 * @param i
	 * @return
	 * @throws Exception 
	 */
	public Install create(InstallCreate i) throws Exception {
		AppAuthenticationDao auth = new AppAuthenticationDao();
		InstallEntity dataFound = null;
		try {
			dataFound = getEntitytOnDeviceToken(i.getDeviceToken());
		} catch (Exception e) {
			//do nothing
		}
//		try {
			if (null != dataFound) {
				System.out.println("Updating " +dataFound.getId());
				dataFound.setAppVersion(i.getAppVersion());
				dataFound.setAppVersion(i.getDeviceToken());
				dataFound.setUpdatedAt(new Date());
				dataFound.setTimeZone(getTimezone(i.getTimeZone()));
				Key key = save(dataFound.buildEntity());
				System.out.println("Presisted");
				Install install = prepareDomain(dataFound);
				AppAuthentication readAuth = auth.read(dataFound.getId());
				if(null == readAuth){
					install.setToken(auth.create(dataFound.getId()));
				}else{
				install.setToken(readAuth.getToken());
				}
				return install;
			} else {
				InstallEntity entity = prepareCreateEntity(i);
				entity.setCreatedAt(new Date());
				Key key = save(entity.buildEntity());
				Install install = prepareDomain(entity);
				install.setToken(auth.create(entity.getId()));
				install.setInstallationId(key.getId());
				return install;
			}
		/*} catch (Exception e) {
			System.out.println(e);
			throw new Exception("System error" +e.getMessage());
		}*/

	}

	/**
	 * Lists all the installations
	 * 
	 * @return
	 */
	public List<Install> list() {
				
		List<Install> advisorList = new ArrayList<>();
		Query query = new Query(InstallEntity.TABLE_NAME);
		PreparedQuery results = getDataStoreService().prepare(query);
		System.out.println(results.toString());
		for (Entity entity : results.asIterable()) {
			System.out.println(entity.getProperty("appIdentifier"));
			advisorList.add(prepareDomain(new InstallEntity(entity)));
		}
		System.out.println(advisorList.size());
		return advisorList;
		
	}

	/**
	 * Update the version of installation
	 * 
	 * @param i
	 * @return
	 * @throws Exception 
	 */
	public Long update(InstallUpdate i) throws Exception {
		if(null==i.getInstallationId())
			throw new Exception("Installation Id is required");
		InstallEntity readData = null;
		readData = new InstallEntity(getEntityById(i.getInstallationId(), InstallEntity.TABLE_NAME));

		readData.setAppVersion(i.getAppVersion());
		readData.setAppVersion(i.getDeviceToken());
		readData.setUpdatedAt(new Date());
		try {
			save(readData.buildEntity());
		} catch (Exception e) {
			System.out.println(e.getMessage());
			throw new Exception("System error");
		}

		return readData.getId();
	}
	
	/**
	 * Deleting Installation record
	 * 
	 * @param id
	 * @throws Exception
	 */
	public void delete(Long id) throws Exception {
		deleteEntityById(id, InstallEntity.TABLE_NAME);
	}

	/*
	 * Converts db entity to rest object
	 */
	private Install prepareDomain(InstallEntity i) {
		return new Install(i.getId(), i.getAppIdentifier(), i.getAppName(), i.getAppVersion(), i.getDeviceType(), i.getDeviceToken(),
				i.getTimeZone().getID(), i.getCreatedAt(), i.getUpdatedAt());
	}
	
	/*
	 * Prepares entity for create
	 */
	private InstallEntity prepareCreateEntity(InstallCreate i) throws Exception {
		return new InstallEntity(i.getAppIdentifier(), i.getAppName(), i.getAppVersion(), i.getDeviceType(), i.getDeviceToken(),
				getTimezone(i.getTimeZone()), new Date());
	}
	
	private TimeZone getTimezone(String timezone) throws Exception{
		if(null == timezone){throw new Exception("Invalid Timezone");}
		TimeZone t = TimeZone.getTimeZone(timezone);
		return t;
	}
	
	private InstallEntity getEntitytOnDeviceToken(String deviceToken) throws Exception {
		AppAuthenticationDao authDao = new AppAuthenticationDao();
		try {
			List<InstallEntity> list = getInstallEntityOnDeviceToken(deviceToken);
			if (null == list || list.isEmpty()) {
				return null;
			} else {
				int listSize =  list.size();
				if(listSize>=2){
					Collections.sort(list, new Comparator<InstallEntity>() {
						  public int compare(InstallEntity o1, InstallEntity o2) {
						      if (o1.getCreatedAt() == null || o2.getCreatedAt() == null)
						        return 0;
						      return o1.getCreatedAt().compareTo(o2.getCreatedAt());
						  }
						});
					System.out.println("List size- "+listSize);
					for (int i=0;i<=listSize-2;i++) {
						System.out.println("Installtion id-"+list.get(i).getId());
						try{
							
						FcmtokenEntity entity = new FcmtokenEntity(getEntityById(list.get(i).getId(), FcmtokenEntity.TABLE_NAME));
						if(null != entity){
								System.out.println("FCM entity found " +entity.getInstallationId());
								boolean success = PushNotification.unSubscribeToTopic(entity.getFcmToken(), list.get(0).getDeviceType());
								System.out.println("FCM unsubcription for prevoius fcm id " +entity.getFcmToken()+ "is" +success);
								PushNotification pn= new PushNotification();
								pn.deleteFcmRecord(entity.getInstallationId());
						}
						} catch (Exception e) {
							System.out.println("Error in reterieving FCM id "+e.getMessage());
						}
						delete(list.get(i).getId());
						authDao.delete(list.get(i).getId());
					}
					return list.get(listSize-1);}
				return list.get(0);
			}
		} catch (Exception e) {
			System.out.println("FCM token not found "+deviceToken+" " +e.getMessage());
		} 
		
		return null;
	}
	
	private List<InstallEntity> getInstallEntityOnDeviceToken(String deviceToken){
		List<InstallEntity> installationList = new ArrayList<>();

		Filter propertyFilter = new FilterPredicate("deviceToken", FilterOperator.EQUAL, deviceToken);
		Query query = new Query(InstallEntity.TABLE_NAME).setFilter(propertyFilter);
		PreparedQuery results = getDataStoreService().prepare(query);

		for (Entity entity : results.asIterable()) {
			installationList.add(new InstallEntity(entity));
		}

		return installationList;
	}

	}
