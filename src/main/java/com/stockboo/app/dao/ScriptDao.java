/**
 * 
 */
package com.stockboo.app.dao;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.FetchOptions;
import com.google.appengine.api.datastore.PreparedQuery;
import com.stockboo.app.configuration.Constants;
import com.stockboo.app.domain.BSE;
import com.stockboo.app.domain.NSE;
import com.stockboo.app.domain.ScriptCodeDomain;
import com.stockboo.app.domain.ScriptListOutput;
import com.stockboo.app.entity.ScriptAuditDetailsEntity;
import com.stockboo.app.entity.ScriptCodeEntity;

/**
 * @author sunil.r
 *
 */

@SuppressWarnings("all")
public class ScriptDao extends AbstractStockbooDao{

	/**
	 * Method to create the NSE data
	 * 
	 * @param advisor
	 * @return
	 * @throws Exception
	 */
	public boolean createNse(List<NSE> nse) throws Exception {
		System.out.println("In create method");
		// System.out.println(nse);
		for (NSE nse2 : nse) {
			ScriptCodeEntity scriptCode = new ScriptCodeEntity();
			scriptCode.setNseCode(nse2.getfIELD1());
			//// scriptCode.setIndusty(industy);
			scriptCode.setFirstListingDate(nse2.getfIELD4());
			scriptCode.setFaceValue(nse2.getfIELD8());
			////// scriptCode.setStatus(status);
			scriptCode.setiSIN(nse2.getfIELD7());
			scriptCode.setCompanyName(nse2.getfIELD2());
			scriptCode.setCreated(new Date());
			//// sc.add(scriptCode);
			save(scriptCode.buildEntity());

		}

		ScriptAuditDetailsEntity scriptAudit = new ScriptAuditDetailsEntity();
		scriptAudit.setNseCreated(new Date());
		scriptAudit.setNseUpdatedAt(new Date());
		scriptAudit.setLastCreated(new Date());
		scriptAudit.setLastUpdated(new Date());
		save(scriptAudit.buildEntity());
		return true;

	}

	/**
	 * Method lists all the Script data
	 * 
	 * @param recordsFrom
	 * @param offset
	 * @param limit
	 * @return
	 */
	public ScriptListOutput list(Date recordsFrom, String offset, String limit) {
		int pageIndex =0;
		int pageSize =0;
		int countEntities = 0;
		int totalntities = 0;
		ScriptListOutput scriptListOutput = new ScriptListOutput();
		if(null != offset && "" != offset){
			pageIndex = Integer.parseInt(offset);
		}
		if(null != limit && "" != limit){
			pageSize = Integer.parseInt(limit);
		}else{
			pageSize = Constants.SCRIPTS_PAGING;
		}
		
		List<ScriptCodeEntity> scriptCodeList = null;
		if (null == recordsFrom && null != offset) {
//			System.out.println("***************offset");
			scriptCodeList = new ArrayList<>();
			 DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
			   com.google.appengine.api.datastore.Query queryForEntities = new com.google.appengine.api.datastore.Query("ScriptCodeEntity");
			PreparedQuery preppedEntityQuery = datastore.prepare(queryForEntities);
//			System.out.println("********************************************");
			totalntities = preppedEntityQuery.countEntities();
			countEntities = preppedEntityQuery.countEntities(FetchOptions.Builder.withOffset(0));
//			System.out.println(countEntities);
//			System.out.println("********************************************");
			List<Entity> randomEntities = preppedEntityQuery.asList(FetchOptions.Builder.withOffset(pageIndex).limit(pageSize));
//			System.out.println(randomEntities.size());
			for (Entity e : randomEntities) {
				ScriptCodeEntity scl = new ScriptCodeEntity();
//				System.out.println(e.getProperties().values());
//				System.out.println();
				if(0!=e.getKey().getId())
				scl.setScriptCodeId(e.getKey().getId());
				if(null!=e.getProperties().get("bseCode"))
				scl.setBseCode(e.getProperties().get("bseCode").toString());
				if(null!=e.getProperties().get("companyName"))
				scl.setCompanyName(e.getProperties().get("companyName").toString());
				if(null!=e.getProperties().get("created"))
				scl.setCreated((Date) e.getProperties().get("created"));
				if(null!=e.getProperties().get("faceValue"))
				scl.setFaceValue(e.getProperties().get("faceValue").toString());
				if(null!=e.getProperties().get("firstListingDate"))
				scl.setFirstListingDate(e.getProperties().get("firstListingDate").toString());
				if(null!=e.getProperties().get("group"))
				scl.setGroup(e.getProperties().get("group").toString());
				if(null!=e.getProperties().get("industy"))
				scl.setIndusty(e.getProperties().get("industy").toString());
				if(null!=e.getProperties().get("iSIN"))
				scl.setiSIN(e.getProperties().get("iSIN").toString());
				if(null!=e.getProperties().get("nseCode"))
				scl.setNseCode(e.getProperties().get("nseCode").toString());
				if(null!=e.getProperties().get("status"))
				scl.setStatus(e.getProperties().get("status").toString());
				if(null!=e.getProperties().get("updatedAt"))
				scl.setUpdatedAt((Date)e.getProperties().get("updatedAt"));
				scriptCodeList.add(scl);
			}
			int newpageIndex = pageIndex+scriptCodeList.size();
			System.out.println("pageIndex "+ newpageIndex);
			System.out.println("totalntities "+ totalntities);
			System.out.println("countEntities "+ countEntities);
			System.out.println("newpageIndex "+ newpageIndex);
			if(countEntities == newpageIndex || randomEntities.size() == 0){
				scriptListOutput.setNextOffset("-1");
			}else{
				countEntities = countEntities +1;
			scriptListOutput.setNextOffset(String.valueOf(newpageIndex));
			}
			scriptListOutput.setTotalNumOfRecords(Integer.toString(countEntities));
		} else {

//			System.out.println("***************From date");
//			try {
				/*if (recordsFrom == null) {
					Query query = pm.newQuery(ScriptCodeEntity.class);
					scriptCodeList = (List<ScriptCodeEntity>) query.execute();
				} else {
					Query q = pm.newQuery(ScriptCodeEntity.class);
					q.setFilter("created >= createdAt");
					q.declareParameters("java.util.Date createdAt");
					scriptCodeList = (List<ScriptCodeEntity>) q.execute(recordsFrom);
				}*/
			/*} catch (javax.jdo.JDOObjectNotFoundException e) {

			}*/
		}
		List<ScriptCodeDomain> scriptListDomainList = new ArrayList<>();
		for(ScriptCodeEntity entity:scriptCodeList){
			ScriptCodeDomain scriptCodeDomain = entityToDomain(entity);
			scriptListDomainList.add(scriptCodeDomain);
		}
		scriptListOutput.setScriptCodeList(scriptListDomainList);
//		scriptListOutput.setLastUpdatedDate(getScriptUpdatedDate());
		return scriptListOutput;
	}

	/**
	 * @return the firstListingDate
	 * @throws ParseException 
	 */
	public String getFirstListingDate(String date) {
		if (null != date && "" != date) {
			DateFormat parser = new SimpleDateFormat("dd-MMM-yyyy");
			SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
			try {
				return formatter.format(parser.parse(date));
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} else
			return null;
		return date;
	}
	
	/**
	 * Creates BSE data for existing NSE data
	 * 
	 * @param list
	 * @return
	 * @throws Exception
	 */
	public boolean updateNseScript(List<NSE> list) throws Exception {
		if (null == list)
			throw new Exception("NSE list not found");
		ScriptCodeEntity sc = null;
		List<ScriptCodeEntity> scriptCodeList = null;
		for (NSE nse : list) {
//			try {
//				Query q = pm.newQuery(ScriptCodeEntity.class);
//				q.setFilter("iSIN == iSINParam");
//				q.declareParameters("String iSINParam");
//				scriptCodeList = (List<ScriptCodeEntity>) q.execute(nse.getfIELD7());
//			} catch (javax.jdo.JDOObjectNotFoundException e) {

//			}
			if ((scriptCodeList == null) || (scriptCodeList.isEmpty())) {
				sc = new ScriptCodeEntity();
				sc.setNseCode(nse.getfIELD1());
				//// scriptCode.setIndusty(industy);
				sc.setFirstListingDate(nse.getfIELD4());
				sc.setFaceValue(nse.getfIELD8());
				////// scriptCode.setStatus(status);
				sc.setiSIN(nse.getfIELD7());
				sc.setCompanyName(nse.getfIELD2());
				sc.setCreated(new Date());
			} else {
				if (scriptCodeList.get(0) == null) {
					sc = new ScriptCodeEntity();
				} else {
					sc = scriptCodeList.get(0);
					sc.setNseCode(nse.getfIELD1());
					//// scriptCode.setIndusty(industy);
					sc.setFirstListingDate(nse.getfIELD4());
//					sc.setFaceValue(nse.getfIELD8());
					////// scriptCode.setStatus(status);
//					sc.setiSIN(nse.getfIELD7());
//					sc.setCompanyName(nse.getfIELD2());
					sc.setUpdatedAt(new Date());
				}
			}
			
			try {
//				Query q = pm.newQuery(ScriptAuditDetailsEntity.class);
//				List<ScriptAuditDetailsEntity> auditList = (List<ScriptAuditDetailsEntity>) q.execute();
//				ScriptAuditDetailsEntity scriptAudit = auditList.get(0);
//				scriptAudit.setNseUpdatedAt(new Date());
//				scriptAudit.setLastUpdated(new Date());
//				save(scriptAudit.buildEntity());
//				save(sc.buildEntity());
			} catch (Exception e) {
				// TODO: handle exception
			} 
		}
		return true;
	}
	
	/*private Date getScriptUpdatedDate(){
		Query q = pm.newQuery(ScriptAuditDetailsEntity.class);
		List<ScriptAuditDetailsEntity> scriptCodeList = (List<ScriptAuditDetailsEntity>) q.execute();
		return scriptCodeList.get(0).getLastUpdated() ==null ? scriptCodeList.get(0).getLastCreated():scriptCodeList.get(0).getLastUpdated();
	}
	*/
	
	/**
	 * Method to create the NSE data
	 * 
	 * @param advisor
	 * @return
	 * @throws Exception
	 */
	public boolean createBse(List<BSE> bseList) throws Exception {
		System.out.println("In create method");
		// System.out.println(nse);
		for (BSE bse : bseList) {
			ScriptCodeEntity sc = new ScriptCodeEntity();
			sc.setCompanyName(bse.getfIELD3());
			sc.setFaceValue(bse.getfIELD6());
			sc.setCreated(new Date());
			sc.setIndusty(bse.getfIELD8());
			sc.setStatus(bse.getfIELD4());
			sc.setGroup(bse.getfIELD5());
			sc.setBseCode(bse.getfIELD1());
			sc.setiSIN(bse.getfIELD7());
//			pm.makePersistent(sc);

		}

		ScriptAuditDetailsEntity scriptAudit = new ScriptAuditDetailsEntity();
		scriptAudit.setBseCreatedAt(new Date());
		scriptAudit.setBseUpdatedAt(new Date());
		scriptAudit.setLastCreated(new Date());
		scriptAudit.setLastUpdated(new Date());
		save(scriptAudit.buildEntity());
		return true;

	}
	
	public ScriptCodeDomain readScriptCode(String stockCode, Long scriptCodeId){
		System.out.println("securityCode " + stockCode);
		if(null != scriptCodeId){
			ScriptCodeEntity scriptCodeEntity = null;
			try {
//				scriptCodeEntity = pm.getObjectById(ScriptCodeEntity.class, scriptCodeId);
			} catch (Exception e) {
				return null;
			}
			return (null == scriptCodeEntity) ? null : entityToDomain(scriptCodeEntity);
		}
//		Query q = pm.newQuery(ScriptCodeEntity.class);
		List<ScriptCodeEntity> scriptCodeList = null;
		ScriptCodeEntity entity= null;
		if(""!=stockCode && null != stockCode){
			System.out.println("In nse block");
//			q.setFilter("nseCode == stockCode");
//			q.declareParameters("String stockCode");
//			scriptCodeList = (List<ScriptCodeEntity>) q.execute(stockCode.toUpperCase());
			System.out.println(scriptCodeList.toString());
		}
		if ((scriptCodeList == null || scriptCodeList.isEmpty()) && null != stockCode) {
			System.out.println("In bse block");
//			q.setFilter("bseCode == stockCode");
//			q.declareParameters("String stockCode");
//			scriptCodeList = (List<ScriptCodeEntity>) q.execute(stockCode);
			System.out.println(scriptCodeList.toString());
		}
		if ((scriptCodeList != null) && (!scriptCodeList.isEmpty())) {
			entity=  scriptCodeList.get(0)==null ? null:scriptCodeList.get(0);
			System.out.println("Entity----"+entity.toString());
		}
		if(entity != null){
			System.out.println("Data found");
			return entityToDomain(entity);
		}
		
		return null;
		
	}

	private ScriptCodeDomain entityToDomain(ScriptCodeEntity entity) {
		return new ScriptCodeDomain(entity.getScriptCodeId(), entity.getNseCode(), entity.getBseCode(),
				entity.getCompanyName(), entity.getGroup(), entity.getFaceValue(), entity.getiSIN(), entity.getIndusty(),
				getFirstListingDate(entity.getFirstListingDate()), entity.getStatus(), entity.getCreated(), entity.getUpdatedAt());
	}
}
