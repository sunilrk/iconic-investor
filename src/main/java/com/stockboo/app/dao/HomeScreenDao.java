/**
 * 
 */
package com.stockboo.app.dao;

import java.net.URL;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;

import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.PreparedQuery;
import com.google.appengine.api.datastore.Query;
import com.google.appengine.api.datastore.Query.Filter;
import com.google.appengine.api.datastore.Query.FilterOperator;
import com.google.appengine.api.datastore.Query.FilterPredicate;
import com.google.appengine.api.memcache.ErrorHandlers;
import com.google.appengine.api.memcache.MemcacheService;
import com.google.appengine.api.memcache.MemcacheServiceFactory;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.org.iconic.entity.IconicInvestor;
import com.rometools.rome.feed.synd.SyndEntry;
import com.rometools.rome.feed.synd.SyndFeed;
import com.rometools.rome.io.SyndFeedInput;
import com.rometools.rome.io.XmlReader;
import com.stockboo.app.configuration.Constants;
import com.stockboo.app.cronjobs.CryptoCurrencyRaw;
import com.stockboo.app.cronjobs.GainersLoosers;
import com.stockboo.app.cronjobs.GainersLoosersData;
import com.stockboo.app.cronjobs.HighLow52;
import com.stockboo.app.cronjobs.HighLow52Data;
import com.stockboo.app.cronjobs.Indices;
import com.stockboo.app.cronjobs.IndicesData;
import com.stockboo.app.domain.CryptoCurrencyDataDomain;
import com.stockboo.app.domain.GainersLoosersDataDomain;
import com.stockboo.app.domain.GainersLoosersDomain;
import com.stockboo.app.domain.IndicesDataDomain;
import com.stockboo.app.domain.IndicesDomain;
import com.stockboo.app.domain.RssFeedItems;
import com.stockboo.app.domain.utility.DateUtil;
import com.stockboo.app.entity.ColourCodingEntity;
import com.stockboo.app.entity.CryptoCurrencyEntity;
import com.stockboo.app.entity.GainersLoosersEntity;
import com.stockboo.app.entity.HighLow52Entity;
import com.stockboo.app.entity.IndicesEntity;
import com.stockboo.app.entity.InstallEntity;
import com.stockboo.app.entity.Nifty;
import com.stockboo.app.entity.Sensex;
import com.stockboo.app.entity.SensexAndNiftyPrices;
import com.stockboo.app.homescreen.Data;
import com.stockboo.app.homescreen.HomePage;
import com.stockboo.app.homescreen.HomeScreenAPI;
import com.stockboo.app.homescreen.MarketData;
import com.stockboo.app.homescreen.NiftySensexPrices;
import com.stockboo.app.homescreen.SensexAndNifty;

/**
 * @author sunil.r
 *
 */

@SuppressWarnings("all")
public class HomeScreenDao extends AbstractStockbooDao {
	
	private static DecimalFormat df2 = new DecimalFormat(".##");

	/**
	 * List data from below URL
	 * http://finance.google.com/finance/info?client=ig&q=INDEXBOM:SENSEX,NSE:NIFTY
	 * 
	 * @return
	 * @throws Exception 
	 */
	private HomePage getSensexAndNifty() throws Exception {
		Date weekDay = DateUtil.getWeekDay();
		System.out.println(weekDay);

		MemcacheService memcache = MemcacheServiceFactory.getMemcacheService();
		memcache.setErrorHandler(ErrorHandlers.getConsistentLogAndContinue(Level.INFO));

		Nifty niftyEntity = null;

		if (null != memcache.get("nifty")) {
			niftyEntity = (Nifty) memcache.get("nifty");
		} else {
			List<Nifty> niftyEntityList = new ArrayList<>();
			
			Query query = new Query(Nifty.TABLE_NAME);
			PreparedQuery results = getDataStoreService().prepare(query);

			for (Entity entity : results.asIterable()) {
				niftyEntityList.add(new Nifty(entity));
			}

			Query niftyQuery = new Query(Nifty.TABLE_NAME);
//			niftyQuery.setFilter("this.createdAt < createdAt");
//			niftyQuery.declareParameters("java.util.Date createdAt");
			niftyEntity = niftyEntityList.get(0);
			memcache.put("nifty", niftyEntity);
		}

		Sensex sensexEntity = null;

		if (null != memcache.get("sensex")) {
			sensexEntity = (Sensex) memcache.get("sensex");
		} else {
			List<Sensex> sensexEntityList = null;
			
			Query query = new Query(Sensex.TABLE_NAME);
			PreparedQuery results = getDataStoreService().prepare(query);

			for (Entity entity : results.asIterable()) {
				sensexEntityList.add(new Sensex(entity));
			}
//			sensexQuery.setFilter("this.createdAt < createdAt");
//			sensexQuery.declareParameters("java.util.Date createdAt");
			sensexEntity = sensexEntityList.get(0);
			memcache.put("sensex", sensexEntity);
		}

		List<SensexAndNiftyPrices> sensexAndNiftyPricesEntityList = null;

		if (null != memcache.get("sensexAndNiftyPrices")) {
			sensexAndNiftyPricesEntityList = (List<SensexAndNiftyPrices>) memcache.get("sensexAndNiftyPrices");
		} else {
			sensexAndNiftyPricesEntityList = getSensexAndNiftyListFromDB(weekDay);
//			memcache.put("sensexAndNiftyPrices", sensexAndNiftyPricesEntityList);
			Date tempdate = null;
			while(null == sensexAndNiftyPricesEntityList || sensexAndNiftyPricesEntityList.isEmpty()){
				if(tempdate == null){
				tempdate =DateUtil.subtractDay(weekDay);
				} else{
					tempdate = DateUtil.subtractDay(tempdate);
				}
				System.out.println("********************");
				System.out.println("This is a fix");
				System.out.println(weekDay+ "Doing previous day");
				sensexAndNiftyPricesEntityList = getSensexAndNiftyListFromDB(tempdate);
			}
		}
		List<NiftySensexPrices> sensexPricesDomainList = new ArrayList<>();
		List<NiftySensexPrices> niftyPricesDomainList = new ArrayList<>();
		for (SensexAndNiftyPrices sensexAndNiftyPrices : sensexAndNiftyPricesEntityList) {
			NiftySensexPrices sensexPrices = new NiftySensexPrices(null,
					sensexAndNiftyPrices.getSensexPrice(), sensexAndNiftyPrices.getCreatedAt());
			NiftySensexPrices niftyPrices = new NiftySensexPrices(null,
					sensexAndNiftyPrices.getNiftyPrice(), sensexAndNiftyPrices.getCreatedAt());
			sensexPricesDomainList.add(sensexPrices);
			niftyPricesDomainList.add(niftyPrices);
		}
		MarketData niftyMarketData = new MarketData(null != niftyEntity.getId() ?niftyEntity.getId().toString(): "", niftyEntity.getSymbol(), niftyEntity.getExchange(),
				niftyEntity.getLtp(), niftyEntity.getLtpFinal(), niftyEntity.getLCur(), niftyEntity.getS(),
				niftyEntity.getLastTradedTime(), niftyEntity.getLastTradeTimeFormatted(), niftyEntity.getLastTradedDateTime(), niftyEntity.getChangePrice(),
				niftyEntity.getChangePriceFinal(), niftyEntity.getChangePercentage(), niftyEntity.getChangePriceFinal(), niftyEntity.getChr(),
				niftyEntity.getPreviousClose(), niftyEntity.getCreatedAt(), niftyEntity.getUpdatedAt());
		MarketData sensexMarketData = new MarketData(null != sensexEntity.getId() ?sensexEntity.getId().toString(): "", sensexEntity.getSymbol(),
				sensexEntity.getExchange(), sensexEntity.getL(), sensexEntity.getLtpFinal(), sensexEntity.getLCur(),
				sensexEntity.getS(), sensexEntity.getLastTradedTime(), sensexEntity.getLastTradeTimeFormatted(), sensexEntity.getLtDts(),
				sensexEntity.getChange(), sensexEntity.getChange(), sensexEntity.getChangePercentage(), sensexEntity.getChangePercentageFinal(),
				sensexEntity.getChr(), sensexEntity.getPreviousClose(), sensexEntity.getCreatedAt(),
				sensexEntity.getUpdatedAt());

		
		SensexAndNifty sensexDomain =  new SensexAndNifty(sensexMarketData, sensexPricesDomainList);
		SensexAndNifty niftyDomain =  new SensexAndNifty(niftyMarketData, niftyPricesDomainList);
		List<Object> sensexAndNifties = new ArrayList<>();
		sensexAndNifties.add(sensexDomain);
		sensexAndNifties.add(niftyDomain);
		
		HomePage homePage = new HomePage();
		homePage.setTitle("NSE BSE");
		homePage.setViewType("v_grid_graph");
		homePage.setCount(sensexAndNifties.size());
		homePage.setData(sensexAndNifties);
		homePage.setColour(getColurCode("NSEBSE"));
		
		return homePage ;
	}

	public List<SensexAndNiftyPrices> getSensexAndNiftyListFromDB(Date weekDay) {
		List<SensexAndNiftyPrices> sensexAndNiftyPricesEntityList = new ArrayList<>();
//		String queryString = "";
//		String declaredParams = "";
//		queryString = queryString + " this.createdAt > createdAt";
//		declaredParams = declaredParams + "java.util.Date createdAt";
//		Map args = new HashMap();
//		args.put("createdAt", weekDay);
		
		
		Filter propertyFilter = new FilterPredicate("deviceToken", FilterOperator.GREATER_THAN, weekDay);
		Query query = new Query(SensexAndNiftyPrices.TABLE_NAME).setFilter(propertyFilter);
		PreparedQuery results = getDataStoreService().prepare(query);

		for (Entity entity : results.asIterable()) {
			sensexAndNiftyPricesEntityList.add(new SensexAndNiftyPrices(entity));
		}
		
		return sensexAndNiftyPricesEntityList;
	}
	
	public HomePage getTopThreeGainers() throws Exception {

		List<GainersLoosersDataDomain> gainersDomainList = topThreeGainers();
		
		//Doing sorting in cron job only, hence sorting is not required
		//To Sort on net price
		/*if (gainersDomainList.size() > 0) {
			Collections.sort(gainersDomainList, new Comparator<GainersLoosersDataDomain>() {
				@Override
				public int compare(final GainersLoosersDataDomain object1, final GainersLoosersDataDomain object2) {
					return object1.getPChange().compareTo(object2.getPChange());
				}
			});
		}*/
//		Collections.reverse(gainersDomainList);
		List<Object> objectList = new ArrayList<Object>(gainersDomainList);
		HomePage homePage = new HomePage();
		homePage.setTitle("Top Gainers");
		homePage.setViewType("v_list");
		homePage.setColour(getColurCode("TopGainers"));
		homePage.setUrlType("todayshigh_list");
		homePage.setCount(objectList.size());
		homePage.setData(objectList);
		homePage.setUrl("homescreen/market/todayshigh");

		return homePage;
	}

	public List<GainersLoosersDataDomain> topThreeGainers() {
		MemcacheService memcache = MemcacheServiceFactory.getMemcacheService();
		memcache.setErrorHandler(ErrorHandlers.getConsistentLogAndContinue(Level.INFO));

		String topThreeGainers = null;

		if (null != memcache.get("topThreeGainers")) {
			topThreeGainers = (String) memcache.get("topThreeGainers");
		} else {
			GainersLoosersEntity entity = null;
			try {
				entity = new GainersLoosersEntity(getEntityById("Gainers", GainersLoosersEntity.TABLE_NAME));
			} catch (Exception e) {
				// TODO: handle exception
			}
			
			if(null != entity){
				topThreeGainers = entity.getTopThree().getValue();
				memcache.put("topThreeGainers", topThreeGainers);
			}
		}
		Gson gson = new Gson();
		List<GainersLoosersData> gainersList = gson.fromJson(topThreeGainers, new TypeToken<List<GainersLoosersData>>() {
		}.getType());
		
		List<GainersLoosersDataDomain> gainersDomainList = new ArrayList<>();
		for (GainersLoosersData g : gainersList) {
			GainersLoosersDataDomain domain = new GainersLoosersDataDomain(g.getSymbol(), g.getSeries(),
					g.getOpenPrice(), g.getHighPrice(), g.getLowPrice(), g.getLtp(), g.getPreviousPrice(),
					g.getNetPrice(), g.getTradedQuantity(), g.getTurnoverInLakhs(), g.getLastCorpAnnouncementDate(), g.getLastCorpAnnouncement());
			gainersDomainList.add(domain);
		}
		return gainersDomainList;
	}
	
	public HomePage getTopThreeLoosers() throws Exception {

		List<GainersLoosersDataDomain> loosersDomainList = topThreeLoosers();
		
		//Doing sorting in cron job only, hence sorting is not required
		// To Sort on net price
	/*	if (loosersDomainList.size() > 0) {
			Collections.sort(loosersDomainList, new Comparator<GainersLoosersDataDomain>() {
				@Override
				public int compare(final GainersLoosersDataDomain object1, final GainersLoosersDataDomain object2) {
					return object1.getPChange().compareTo(object2.getPChange());
				}
			});
		}*/
//		Collections.reverse(loosersDomainList);
		List<Object> objectList = new ArrayList<Object>(loosersDomainList);

		HomePage homePage = new HomePage();
		homePage.setTitle("Top Losers");
		homePage.setViewType("v_list");
		homePage.setColour(getColurCode("TopLosers"));
		homePage.setUrlType("todayslow_list");
		homePage.setCount(objectList.size());
		homePage.setData(objectList);
		homePage.setUrl("homescreen/market/todayslow");

		return homePage;
	}

	public List<GainersLoosersDataDomain> topThreeLoosers() {

		MemcacheService memcache = MemcacheServiceFactory.getMemcacheService();
		memcache.setErrorHandler(ErrorHandlers.getConsistentLogAndContinue(Level.INFO));

		String topThreeLoosers = null;

		if (null != memcache.get("topThreeLoosers")) {
			topThreeLoosers = (String) memcache.get("topThreeLoosers");
		} else {
			GainersLoosersEntity entity = null;
			
			try {
				entity = new GainersLoosersEntity(getEntityById("Loosers", GainersLoosersEntity.TABLE_NAME));
			} catch (Exception e) {
				// TODO: handle exception
			}
			
			
			if(null != entity){
				topThreeLoosers = entity.getTopThree().getValue();
				memcache.put("topThreeLoosers", topThreeLoosers);
			}
		}
		Gson gson = new Gson();
		List<GainersLoosersData> loosersList = gson.fromJson(topThreeLoosers, new TypeToken<List<GainersLoosersData>>() {
		}.getType());
		
		List<GainersLoosersDataDomain> loosersDomainList = new ArrayList<>();
		for (GainersLoosersData g : loosersList) {
			GainersLoosersDataDomain domain = new GainersLoosersDataDomain(g.getSymbol(), g.getSeries(),
					g.getOpenPrice(), g.getHighPrice(), g.getLowPrice(), g.getLtp(), g.getPreviousPrice(),
					g.getNetPrice(), g.getTradedQuantity(), g.getTurnoverInLakhs(), g.getLastCorpAnnouncementDate(), g.getLastCorpAnnouncement());
			loosersDomainList.add(domain);
		}
		return loosersDomainList;
	}
	
	private HomePage getTopThree52WeekHigh() throws Exception {

		MemcacheService memcache = MemcacheServiceFactory.getMemcacheService();
		memcache.setErrorHandler(ErrorHandlers.getConsistentLogAndContinue(Level.INFO));

		String topThreeHigh = null;
		Integer totalCount = null;
		Gson gson = new Gson();
		if ((null != memcache.get("topThreeHigh")) && (null != memcache.get("high52"))) {
			topThreeHigh = (String) memcache.get("topThreeHigh");
			HighLow52 high52weekList = gson.fromJson((String)memcache.get("high52"), HighLow52.class);
			totalCount = high52weekList.getData().size();
		} else {
			HighLow52Entity entity = null;
			try {
				entity = new HighLow52Entity(getEntityById("52NewHigh", HighLow52Entity.TABLE_NAME));
			} catch (Exception e) {
				// TODO: handle exception
			}
			if(null != entity){
				topThreeHigh = entity.getTopThree().getValue();
				totalCount = gson.fromJson(entity.getFullDetails().getValue(), HighLow52.class).getData().size();
				memcache.put("topThreeHigh", topThreeHigh);
				memcache.put("high52", entity.getFullDetails().getValue());
			}
		}
		List<Object> high52List = gson.fromJson(topThreeHigh, new TypeToken<List<HighLow52Data>>() {
		}.getType());

		HomePage homePage = new HomePage();
		homePage.setTitle("52W High");
		homePage.setDesc("desc");
		homePage.setViewType("grid");
		homePage.setUrlType("52weekhigh_list");
		homePage.setColour(getColurCode("52WHigh"));
		homePage.setCount(high52List.size());
		homePage.setTotalCount(totalCount);
		homePage.setData(high52List);
		homePage.setUrl("homescreen/market/52weekhigh");

		return homePage;
	}
	
	private HomePage getTopThree52WeekLow() throws Exception {

		MemcacheService memcache = MemcacheServiceFactory.getMemcacheService();
		memcache.setErrorHandler(ErrorHandlers.getConsistentLogAndContinue(Level.INFO));

		String topThreeLow = null;
		Integer totalCount = null;
		Gson gson = new Gson();
		if ((null != memcache.get("topThreeLow")) && (null != memcache.get("low52"))) {
			topThreeLow = (String) memcache.get("topThreeLow");
			HighLow52 high52weekList = gson.fromJson((String)memcache.get("low52"), HighLow52.class);
			totalCount = high52weekList.getData().size();

		} else {
			HighLow52Entity entity = null;
			try {
				entity = new HighLow52Entity(getEntityById("52NewLow", HighLow52Entity.TABLE_NAME));
			} catch (Exception e) {
				// TODO: handle exception
			}
			if(null != entity){
				topThreeLow = entity.getTopThree().getValue();
				totalCount = gson.fromJson(entity.getFullDetails().getValue(), HighLow52.class).getData().size();
				memcache.put("topThreeLow", topThreeLow);
				memcache.put("low52", entity.getFullDetails().getValue());
			}
		}
		List<Object> high52List = gson.fromJson(topThreeLow, new TypeToken<List<HighLow52Data>>() {
		}.getType());

		HomePage homePage = new HomePage();
		homePage.setTitle("52W Low");
		homePage.setDesc("desc");
		homePage.setViewType("grid");
		homePage.setColour(getColurCode("52WLow"));
		homePage.setUrlType("52weeklow_list");
		homePage.setCount(high52List.size());
		homePage.setTotalCount(totalCount);
		homePage.setData(high52List);
		homePage.setUrl("homescreen/market/52weeklow");

		return homePage;
	}
	
	private HomePage getIndices() throws Exception {

		MemcacheService memcache = MemcacheServiceFactory.getMemcacheService();
		memcache.setErrorHandler(ErrorHandlers.getConsistentLogAndContinue(Level.INFO));

		String indicesJson = null;

		if (null != memcache.get("topThreeIndices")) {
			indicesJson = (String) memcache.get("topThreeIndices");
		} else {
			IndicesEntity entity  = null;
			try {
				entity = new IndicesEntity(getEntityById("Indices", IndicesEntity.TABLE_NAME));
			} catch (Exception e) {
				// TODO: handle exception
			}
			if(null != entity){
				indicesJson = entity.getTopThree().getValue();
				memcache.put("topThreeIndices", indicesJson);
			}
		}
		Gson gson = new Gson();
		List<IndicesData> indicesDataList = gson.fromJson(indicesJson,  new TypeToken<List<IndicesData>>() {
		}.getType());
		
		List<IndicesDataDomain> indicesDataDomainList = new ArrayList<>();
		for (IndicesData indicesData : indicesDataList) {
			IndicesDataDomain indicesDataDomain = new IndicesDataDomain(indicesData.getName(),
					indicesData.getLastPrice(), indicesData.getChange(), indicesData.getPChange(),
					indicesData.getImgFileName());
			indicesDataDomainList.add(indicesDataDomain);
		}
		
		List<Object> objectList = new ArrayList<Object>(indicesDataDomainList);
		
		HomePage homePage = new HomePage();
		homePage.setTitle("Indices");
		homePage.setViewType("v_list");
		homePage.setColour(getColurCode("Indices"));
		homePage.setUrlType("indices_list");
		homePage.setCount(objectList.size());
		homePage.setData(objectList);
		homePage.setUrl("homescreen/market/indices");

		return homePage;
	}
	
	private HomePage getMoreVolumeGainers() throws Exception {

		MemcacheService memcache = MemcacheServiceFactory.getMemcacheService();
		memcache.setErrorHandler(ErrorHandlers.getConsistentLogAndContinue(Level.INFO));

		String volumeGainersJson = null;

		if (null != memcache.get("topThreeMoreVoulmeGainers")) {
			volumeGainersJson = (String) memcache.get("topThreeMoreVoulmeGainers");
		} else {
			
			GainersLoosersEntity entity = null;
			try {
				entity = new GainersLoosersEntity(getEntityById("MoreVolumeGainers", GainersLoosersEntity.TABLE_NAME));
			} catch (Exception e) {
				// TODO: handle exception
			}
			
			if(null != entity){
				volumeGainersJson = entity.getTopThree().getValue();
				memcache.put("topThreeMoreVoulmeGainers", volumeGainersJson);
			}
		}
		Gson gson = new Gson();
		List<GainersLoosersData> volumeGainersDataList = gson.fromJson(volumeGainersJson,  new TypeToken<List<GainersLoosersData>>() {
		}.getType());
		
		List<GainersLoosersDataDomain> volumeGainersDomainList = new ArrayList<>();
		for (GainersLoosersData g : volumeGainersDataList) {
			GainersLoosersDataDomain domain = new GainersLoosersDataDomain(g.getSymbol(), g.getSeries(),
					g.getOpenPrice(), g.getHighPrice(), g.getLowPrice(), g.getLtp(), g.getPreviousPrice(),
					g.getNetPrice(), g.getTradedQuantity(), g.getTurnoverInLakhs(), g.getLastCorpAnnouncementDate(), g.getLastCorpAnnouncement());
			volumeGainersDomainList.add(domain);
		}
		
		List<Object> objectList = new ArrayList<Object>(volumeGainersDomainList);
		
		HomePage homePage = new HomePage();
		homePage.setTitle("Volume Gainers");
		homePage.setViewType("v_list");
		homePage.setColour(getColurCode("VolumeGainers"));
		homePage.setUrlType("morevolumegainers_list");
		homePage.setCount(objectList.size());
		homePage.setData(objectList);
		homePage.setUrl("homescreen/market/morevolumegainers");

		return homePage;
	}
	
	
	public HomeScreenAPI homeScreenAPI() throws Exception{
		HomeScreenAPI homeScreenAPI =  new HomeScreenAPI();
		Data data = new Data();
		List<Object> homePage = new ArrayList<>();
		homePage.add(getSensexAndNifty());
		homePage.add(getIndices());
		homePage.add(getTopThreeGainers());
		homePage.add(getTopThreeLoosers());
		homePage.add(get52WeekHighLow());
		homePage.add(getMoreVolumeGainers());
		homePage.add(getThreeCryptoCurrency());
		data.setHome_page(homePage);
		homeScreenAPI.setData(data);
		homeScreenAPI.setStatus(200);
		homeScreenAPI.setMessage("success");
		return homeScreenAPI;
		
	}
	
	private HomePage get52WeekHighLow() throws Exception {
		List<Object> highLow52List = new ArrayList<>();
		highLow52List.add(getTopThree52WeekHigh());
		highLow52List.add(getTopThree52WeekLow());
		HomePage homePage = new HomePage();
		homePage.setTitle("52 week high-low in same row");
		homePage.setViewType("grid");
		homePage.setCount(highLow52List.size());
		homePage.setData(highLow52List);
		return homePage;
	}

	public HighLow52 high52WeekCompleteList() {
		HighLow52Entity highEntity = null;
		try {
			highEntity = new HighLow52Entity(getEntityById("52NewHigh", HighLow52Entity.TABLE_NAME));
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		if(null != highEntity){
			String fullDetails = highEntity.getFullDetails().getValue();
			Gson gson = new Gson();
			HighLow52 high52 = gson.fromJson(fullDetails, HighLow52.class);
			return high52;
		}
		return null;
		
	}
	
	public HighLow52 low52WeekCompleteList() {
		HighLow52Entity lowEntity = null;
		try {
			lowEntity = new HighLow52Entity(getEntityById("52NewLow", HighLow52Entity.TABLE_NAME));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if (null != lowEntity) {
			String fullDetails = lowEntity.getFullDetails().getValue();
			Gson gson = new Gson();
			HighLow52 high52 = gson.fromJson(fullDetails, HighLow52.class);
			return high52;
		}
		return null;
	}
	
	public GainersLoosersDomain todaysGainersCompleteList() {
		
		GainersLoosersEntity entity = null;
		try {
			entity = new GainersLoosersEntity(getEntityById("Gainers", GainersLoosersEntity.TABLE_NAME));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		String fullDetails = entity.getFullDetails().getValue();
		Gson gson = new Gson();
		GainersLoosers gainers = gson.fromJson(fullDetails, GainersLoosers.class);
		
		List<GainersLoosersDataDomain> gainersDataDomainList = new ArrayList<>();
		for (GainersLoosersData g : gainers.getData()) {
			GainersLoosersDataDomain domain = new GainersLoosersDataDomain(g.getSymbol(), g.getSeries(),
					g.getOpenPrice(), g.getHighPrice(), g.getLowPrice(), g.getLtp(), g.getPreviousPrice(),
					g.getNetPrice(), g.getTradedQuantity(), g.getTurnoverInLakhs(), g.getLastCorpAnnouncementDate(), g.getLastCorpAnnouncement());
			domain.getChange();
			gainersDataDomainList.add(domain);
		}
		
		// To Sort on p change
			if (gainersDataDomainList.size() > 0) {
				Collections.sort(gainersDataDomainList, new Comparator<GainersLoosersDataDomain>() {
					@Override
					public int compare(final GainersLoosersDataDomain object1, final GainersLoosersDataDomain object2) {
						return Double.compare(Double.parseDouble(object1.getPChange()), Double.parseDouble(object2.getPChange()));
					}
				});
			}
		Collections.reverse(gainersDataDomainList);
		GainersLoosersDomain gainersDomain = new GainersLoosersDomain(gainers.getTime(),
				gainersDataDomainList);
		
		return gainersDomain;
	}

	public GainersLoosersDomain todaysLoosersCompleteList() {
		GainersLoosersEntity entity = null;
		try {
			entity = new GainersLoosersEntity(getEntityById("Loosers", GainersLoosersEntity.TABLE_NAME));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		String fullDetails = entity.getFullDetails().getValue();
		Gson gson = new Gson();
		GainersLoosers loosers = gson.fromJson(fullDetails, GainersLoosers.class);
		
		List<GainersLoosersDataDomain> loosersDataDomainList = new ArrayList<>();
		for (GainersLoosersData g : loosers.getData()) {
			GainersLoosersDataDomain domain = new GainersLoosersDataDomain(g.getSymbol(), g.getSeries(),
					g.getOpenPrice(), g.getHighPrice(), g.getLowPrice(), g.getLtp(), g.getPreviousPrice(),
					g.getNetPrice(), g.getTradedQuantity(), g.getTurnoverInLakhs(), g.getLastCorpAnnouncementDate(), g.getLastCorpAnnouncement());
			domain.getChange();
			loosersDataDomainList.add(domain);
		}
		
		// To Sort on p change
			if (loosersDataDomainList.size() > 0) {
				Collections.sort(loosersDataDomainList, new Comparator<GainersLoosersDataDomain>() {
					@Override
					public int compare(final GainersLoosersDataDomain object1, final GainersLoosersDataDomain object2) {
						return Double.compare(Double.parseDouble(object1.getPChange()), Double.parseDouble(object2.getPChange()));
					}
				});
			}
		GainersLoosersDomain loosersDomain = new GainersLoosersDomain(loosers.getTime(),
				loosersDataDomainList);
		
		return loosersDomain;
	}
	
	public GainersLoosersDomain moreVolumeGainersCompleteList() {
		GainersLoosersEntity entity = null;
		try {
			entity = new GainersLoosersEntity(getEntityById("MoreVolumeGainers", GainersLoosersEntity.TABLE_NAME));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		String fullDetails = entity.getFullDetails().getValue();
		Gson gson = new Gson();
		GainersLoosers moreVolumeGainers = gson.fromJson(fullDetails, GainersLoosers.class);
		
		List<GainersLoosersDataDomain> volumeGainersDataDomainList = new ArrayList<>();
		for (GainersLoosersData g : moreVolumeGainers.getData()) {
			GainersLoosersDataDomain domain = new GainersLoosersDataDomain(g.getSymbol(), g.getSeries(),
					g.getOpenPrice(), g.getHighPrice(), g.getLowPrice(), g.getLtp(), g.getPreviousPrice(),
					g.getNetPrice(), g.getTradedQuantity(), g.getTurnoverInLakhs(), g.getLastCorpAnnouncementDate(), g.getLastCorpAnnouncement());
			volumeGainersDataDomainList.add(domain);
		}
		
		GainersLoosersDomain moreVolumeGainersDomain = new GainersLoosersDomain(moreVolumeGainers.getTime(),
				volumeGainersDataDomainList);
		return moreVolumeGainersDomain;
	}

	public IndicesDomain indicesCompleteList() {
		IndicesEntity entity = null;
		try {
			entity = new IndicesEntity(getEntityById("Indices", IndicesEntity.TABLE_NAME));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		String fullDetails = entity.getFullDetails().getValue();
		Gson gson = new Gson();
		Indices indices = gson.fromJson(fullDetails, Indices.class);
		
		List<IndicesDataDomain> indicesDataDomainList = new ArrayList<>();
		for (IndicesData indicesData : indices.getData()) {
			IndicesDataDomain indicesDataDomain = new IndicesDataDomain(indicesData.getName(),
					indicesData.getLastPrice(), indicesData.getChange(), indicesData.getPChange(),
					indicesData.getImgFileName());
			indicesDataDomainList.add(indicesDataDomain);
		}
		
		IndicesDomain indicesDomain = new IndicesDomain(indices.getPreOpen(), indices.getTime(), indices.getCorrOpen(),
				indices.getStatus(), indices.getHaltedStatus(), indices.getMktOpen(), indicesDataDomainList, indices.getCode(),
				indices.getCorrClose(), indices.getPreClose(), indices.getMktClose());
		
		return indicesDomain;
	}
	
	public List<RssFeedItems> newsApi() {
		try {
			List<URL> urlList = new ArrayList<>();
			urlList.add(new URL("http://economictimes.indiatimes.com/rssfeedsdefault.cms"));
			urlList.add(new URL("http://www.livemint.com/rss/money"));
//			List<RssFeed> list = new ArrayList<>();
			List<RssFeedItems> items = new ArrayList<>();
			for (URL url : urlList) {
				
				try {

					SyndFeedInput input = new SyndFeedInput();
					System.out.println(url);
					SyndFeed feed = input.build(new XmlReader(url));
					feed.getEntries();
					int numberOfRecords = 1 ;
					for (SyndEntry syndEntry : feed.getEntries()) {
						RssFeedItems item = new RssFeedItems(syndEntry.getTitle(), syndEntry.getLink(),
								syndEntry.getDescription().getValue(), syndEntry.getPublishedDate(), syndEntry.getAuthor());
						System.out.println(syndEntry.getDescription().getValue());
						items.add(item);
						if(numberOfRecords == Constants.NUM_OF_REC_FOR_NEWS_FEEDS){
							break;
						}
						numberOfRecords++;
					}
				
				} catch (Exception e) {
					e.printStackTrace();
					System.out.println("ERROR: " + e.getMessage());
				}
			}
			if (items.size() > 0) {
			Collections.sort(items, new Comparator<RssFeedItems>() {
				@Override
				public int compare(final RssFeedItems object1, final RssFeedItems object2) {
					return object1.getPubDate().compareTo(object2.getPubDate());
				}
			});
		}
			Collections.reverse(items);
			return items;
		} catch (Exception ex) {
			ex.printStackTrace();
			System.out.println("ERROR: " + ex.getMessage());
		}
		return null;
	}
	
/*	NSEBSE #DAE6F4
	TopGainers #c2e187
	TopLosers #fed7b2
	52WHigh #c2e187
	52WLow #fed7b2
	Indices #9adada
	VolumeGainers #77a4dc*/
	public void colorcreate(){
		
		try {
			ColourCodingEntity cc1 = new ColourCodingEntity();
			cc1.setColourcode("#DAE6F4");
			cc1.setScreen("NSEBSE");
			save(cc1.buildEntity());
			
			ColourCodingEntity cc2 = new ColourCodingEntity();
			cc2.setColourcode("#c2e187");
			cc2.setScreen("TopGainers");
			save(cc2.buildEntity());
			
			ColourCodingEntity cc3 = new ColourCodingEntity();
			cc3.setColourcode("#fed7b2");
			cc3.setScreen("TopLosers");
			save(cc3.buildEntity());
			
			
			ColourCodingEntity cc4 = new ColourCodingEntity();
			cc4.setColourcode("#c2e187");
			cc4.setScreen("52WHigh");
			save(cc4.buildEntity());
			
			ColourCodingEntity cc5 = new ColourCodingEntity();
			cc5.setColourcode("#fed7b2");
			cc5.setScreen("52WLow");
			save(cc5.buildEntity());
			
			ColourCodingEntity cc6 = new ColourCodingEntity();
			cc6.setColourcode("#9adada");
			cc6.setScreen("Indices");
			save(cc6.buildEntity());
			
			ColourCodingEntity cc7 = new ColourCodingEntity();
			cc7.setColourcode("#77a4dc");
			cc7.setScreen("VolumeGainers");
			save(cc7.buildEntity());
			
		} catch (Exception e) {
		} 
	}
	
	private String getColurCode(String screen) throws Exception{
		ColourCodingEntity entity = new ColourCodingEntity(getEntityById(screen, ColourCodingEntity.TABLE_NAME));
		System.out.println("color code-"+entity.getColourcode());
		return entity.getColourcode();
		
	}

	public void colorupdate(String color, String screen) throws Exception {
		MemcacheService memcache = MemcacheServiceFactory.getMemcacheService();
		memcache.clearAll();
		ColourCodingEntity entity = new ColourCodingEntity(getEntityById(screen, ColourCodingEntity.TABLE_NAME));
		entity.setColourcode(color);
		save(entity.buildEntity());
		System.out.println("color code-"+entity.getColourcode() +"---------"+"screen-"+entity.getScreen());
		
	}
	
	private HomePage getThreeCryptoCurrency() throws Exception {

		MemcacheService memcache = MemcacheServiceFactory.getMemcacheService();
		memcache.setErrorHandler(ErrorHandlers.getConsistentLogAndContinue(Level.INFO));

		String topThreeCrypto = null;

		if (null != memcache.get("topThreecryptocurrency")) {
			topThreeCrypto = (String) memcache.get("topThreecryptocurrency");
		} else {
			List<CryptoCurrencyEntity> cryptoEntityList = new ArrayList<>();
			Query cryptoQuery = new Query(CryptoCurrencyEntity.TABLE_NAME);
			
			PreparedQuery results = getDataStoreService().prepare(cryptoQuery);

			for (Entity entity : results.asIterable()) {
				cryptoEntityList.add(new CryptoCurrencyEntity(entity));
			}
			CryptoCurrencyEntity cryptoEntity = cryptoEntityList.get(0);
			if(null != cryptoEntity){
				topThreeCrypto = cryptoEntity.getTopThree().getValue();
				memcache.put("topThreecryptocurrency", topThreeCrypto);
			}
		}
		Gson gson = new Gson();
		List<CryptoCurrencyRaw> cryptoList = gson.fromJson(topThreeCrypto, new TypeToken<List<CryptoCurrencyRaw>>() {
		}.getType());
		
		List<CryptoCurrencyDataDomain> cryptoDomainList = new ArrayList<>();
		for (CryptoCurrencyRaw crypto : cryptoList) {
			CryptoCurrencyDataDomain domain = new CryptoCurrencyDataDomain();
			domain.setSymbol(crypto.getName());
			Double priceInr = Double.parseDouble(crypto.getPriceInr());
			domain.setLtp(df2.format(priceInr));
			domain.setPChange(crypto.getPercentChange1h());
			Double previousPrice = (100 * Double.parseDouble(crypto.getPriceInr()))
					/ (Double.parseDouble(crypto.getPercentChange1h()) + 100);
			domain.setPreviousPrice(df2.format(previousPrice));
			Double change = Double.parseDouble(crypto.getPriceInr()) - previousPrice;
			domain.setChange(df2.format(change));
			cryptoDomainList.add(domain);
		}
		List<Object> objectList = new ArrayList<Object>(cryptoDomainList);
		
		HomePage homePage = new HomePage();
		homePage.setTitle("Crypto Currency");
		homePage.setViewType("v_list");
		homePage.setColour(getColurCode("CryptoCurrency"));
		homePage.setUrlType("cryptocurrency_list");
		homePage.setCount(objectList.size());
		homePage.setData(objectList);
		homePage.setUrl("homescreen/market/cryptocurrency");

		return homePage;
	}
}
