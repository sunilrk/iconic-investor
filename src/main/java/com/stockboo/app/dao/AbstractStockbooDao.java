package com.stockboo.app.dao;

import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.EntityNotFoundException;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.org.iconic.dao.AbstractDao;

public abstract class AbstractStockbooDao extends AbstractDao {

	public Entity getEntityById(Long id, String tableName) throws Exception {
		Key key = KeyFactory.createKey(tableName, id);
		System.out.println("Fetching entity for the " + key);
		try {
			return getDataStoreService().get(key);
		} catch (EntityNotFoundException e) {
			e.printStackTrace();
			throw new Exception("Record not found");
		}
	}
	
	public Entity getEntityById(String id, String tableName) throws Exception {
		Key key = KeyFactory.createKey(tableName, id);
		System.out.println("Fetching entity for the " + key);
		try {
			return getDataStoreService().get(key);
		} catch (EntityNotFoundException e) {
			e.printStackTrace();
			throw new Exception("Record not found");
		}
	}

	public boolean deleteEntityById(Long id, String tableName) {
		Key key = KeyFactory.createKey(tableName, id);
		System.out.println("Deleting entity for the " + key);
		getDataStoreService().delete(key);
		return true;
	}

}
