/**
 * 
 */
package com.stockboo.app.dao;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;


import org.apache.commons.lang3.StringUtils;

import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.EntityNotFoundException;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.PreparedQuery;
import com.google.appengine.api.datastore.Query;
import com.google.appengine.api.datastore.Query.CompositeFilterOperator;
import com.google.appengine.api.datastore.Query.Filter;
import com.google.appengine.api.datastore.Query.FilterOperator;
import com.google.appengine.api.datastore.Query.FilterPredicate;
import com.org.iconic.dao.AbstractDao;
import com.org.iconic.entity.IconicInvestor;
import com.org.iconic.entity.Transaction;
import com.org.iconic.entity.enums.TransactionEnum;
import com.stockboo.app.domain.Advisor;
import com.stockboo.app.domain.AdvisorCreate;
import com.stockboo.app.domain.AdvisorUpdate;
import com.stockboo.app.entity.AdvisorEntity;

import jdk.nashorn.internal.runtime.ECMAException;

/**
 * @author jayavardhan
 *
 */

@SuppressWarnings("all")
public class AdvisorDao extends AbstractStockbooDao {

	/**
	 * Method to create the Advisor
	 * 
	 * @param advisor
	 * @return
	 * @throws Exception
	 */
	public Advisor create(AdvisorCreate advisor) throws Exception {
		AdvisorEntity advisorEntity = prepareAdvisorEntity(advisor);
		// Default values
		advisorEntity.setAdvisorBlocked(false);
		advisorEntity.setSuperAdmin(false);
		advisorEntity.setCreatedAt(new Date());
		advisorEntity.setUpdatedAt(new Date());
		advisorEntity.setLastLogin(new Date());

		try {
			Key key = save(advisorEntity.buildEntity());
			if (key != null && key.isComplete()) {
				advisorEntity.setId(key.getId());
				advisorEntity.setAdvisorId(key.getId());
			}
			return prepareAdvisorDomain(advisorEntity);
		} catch (Exception e) {
			throw new Exception("System error");
		}

	}

	/**
	 * Method creates the Entity model from domain model
	 * 
	 * @param advisor
	 * @return
	 * @throws Exception
	 */
	public AdvisorEntity prepareAdvisorEntity(AdvisorCreate advisor) throws Exception {
		return new AdvisorEntity(advisor.getName(), advisor.getInfo(), false, false, advisor.getEmail(),
				validateMobileNumber(advisor.getMobileNumber()), advisor.getPassword(), new Date(), new Date());
	}

	public AdvisorEntity prepareAdvisorEntity(Advisor advisor) throws Exception {
		return new AdvisorEntity(advisor.getName(), advisor.getInfo(), false, false, advisor.getEmail(),
				validateMobileNumber(advisor.getMobileNumber()), advisor.getPassword(), new Date(), new Date());
	}

	/**
	 * Method lists all the advisors
	 * 
	 * @return
	 */
	public List<Advisor> list() {
		List<Advisor> advisorList = new ArrayList<>();
		Query query = new Query(AdvisorEntity.TABLE_NAME);
		PreparedQuery results = getDataStoreService().prepare(query);
		System.out.println(results.toString());
		for (Entity entity : results.asIterable()) {
			System.out.println(entity.getProperty("name"));
			advisorList.add(prepareAdvisorDomain(new AdvisorEntity(entity)));
		}
		System.out.println(advisorList.size());
		return advisorList;
	}

	/**
	 * Method converts the Advisor Entity to Domain mod
	 * 
	 * @param advisorEntity
	 * @return
	 */
	public Advisor prepareAdvisorDomain(AdvisorEntity advisorEntity) {
		return new Advisor(advisorEntity.getAdvisorId(), advisorEntity.getName(), advisorEntity.getInfo(),
				advisorEntity.isSuperAdmin(), advisorEntity.isAdvisorBlocked(), advisorEntity.getEmail(),
				advisorEntity.getMobileNumber().toString(), advisorEntity.getPassword(), advisorEntity.getCreatedAt(),
				advisorEntity.getUpdatedAt(), advisorEntity.getLastLogin());
	}

	/**
	 * Method to read the Advisor by Id
	 * 
	 * @param advisorId
	 * @return
	 * @throws Exception
	 */
	public Advisor read(Long advisorId) throws Exception {

			AdvisorEntity advisorEntity = new AdvisorEntity(getEntityById(advisorId, AdvisorEntity.TABLE_NAME));
			if (advisorEntity != null)
				return new Advisor(advisorEntity.getAdvisorId(), advisorEntity.getName(), advisorEntity.getInfo(),
						advisorEntity.isSuperAdmin(), advisorEntity.isAdvisorBlocked(), advisorEntity.getEmail(),
						advisorEntity.getMobileNumber().toString(), advisorEntity.getPassword(),
						advisorEntity.getCreatedAt(), advisorEntity.getUpdatedAt(), advisorEntity.getLastLogin());
		return null;

	}

	/**
	 * Method to update the advisor
	 * 
	 * @param advisor
	 * @return
	 * @throws Exception
	 */
	public Advisor update(AdvisorUpdate advisor) throws Exception {

		if (null == advisor.getId())
			throw new Exception("Advisor Id is required");
		AdvisorEntity readData = null;
			Entity entity = getEntityById(advisor.getId(), AdvisorEntity.TABLE_NAME);
			readData = new AdvisorEntity(entity);
			System.out.println(readData.getName());

		if (readData != null) {
			// advisorEntity.setName(advisor.getName());
			// advisorEntity.setInfo(advisor.getInfo());
			if (null != advisor.getEmail())
				readData.setEmail(advisor.getEmail());
			if (null != advisor.getInfo())
				readData.setInfo(advisor.getInfo());
			if (null != advisor.getMobileNumber())
				readData.setMobileNumber(validateMobileNumber(advisor.getMobileNumber()));
			if (null != advisor.getName())
				readData.setName(advisor.getName());
			if (null != advisor.getPassword())
				readData.setPassword(advisor.getPassword());

			try {
				// advisorEntity = prepareAdvisorEntity(readData);
				// advisorEntity.setAdvisorId(advisor.getId());
				readData.setUpdatedAt(new Date());
				save(readData.buildEntity());
			} catch (Exception e) {
				throw e;
				// throw new Exception("System error");
			}

		} else
			throw new Exception("Advisor not found");
		return new Advisor(readData.getAdvisorId(), readData.getName(), readData.getInfo(), readData.isSuperAdmin(),
				readData.isAdvisorBlocked(), readData.getEmail(), readData.getMobileNumber().toString(),
				readData.getPassword(), readData.getCreatedAt(), readData.getUpdatedAt(), readData.getLastLogin());
	}

	/**
	 * Method blocks the Advisor
	 * 
	 * @param advisorId
	 * @return
	 * @throws Exception
	 */
	public Advisor block(Long advisorId) throws Exception {

		AdvisorEntity advisorEntity = null;
			advisorEntity = new AdvisorEntity(getEntityById(advisorId, AdvisorEntity.TABLE_NAME));

		if (advisorEntity != null) {
			if (advisorEntity.isSuperAdmin()) {
				throw new Exception("Super Admin cannot be blocked");
			}
			advisorEntity.setAdvisorBlocked(true);
			advisorEntity.setUpdatedAt(new Date());
			try {
				save(advisorEntity.buildEntity());
			} catch (Exception e) {
				throw new Exception("System error");
			}

		} else
			throw new Exception("Advisor not found");
		return new Advisor(advisorEntity.getAdvisorId(), advisorEntity.getName(), advisorEntity.getInfo(),
				advisorEntity.isSuperAdmin(), advisorEntity.isAdvisorBlocked(), advisorEntity.getEmail(),
				advisorEntity.getMobileNumber().toString(), advisorEntity.getPassword(), advisorEntity.getCreatedAt(),
				advisorEntity.getUpdatedAt(), advisorEntity.getLastLogin());
	}

	/**
	 * Deleting advisor
	 * 
	 * @param advisorId
	 * @throws Exception
	 */
	public void delete(Long advisorId) throws Exception {
			Advisor advisor = read(advisorId);
			if (advisor.isSuperAdmin()) {
				throw new Exception("Super Admin can not be deleted");
			}
			deleteEntityById(advisorId, AdvisorEntity.TABLE_NAME);
	}

	private String validateMobileNumber(String mobileNumber) throws Exception {
		Long i = null;
		if (!StringUtils.isNumeric(mobileNumber)) {
			throw new Exception("Not a valid Phone Number");
		}

		if (!(10 == mobileNumber.length())) {
			throw new Exception("Not a valid Phone Number");
		}
		return mobileNumber;
	}
}
