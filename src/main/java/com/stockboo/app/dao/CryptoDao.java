package com.stockboo.app.dao;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;

import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.PreparedQuery;
import com.google.appengine.api.datastore.Query;
import com.google.appengine.api.memcache.ErrorHandlers;
import com.google.appengine.api.memcache.MemcacheService;
import com.google.appengine.api.memcache.MemcacheServiceFactory;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.stockboo.app.cronjobs.CryptoCurrencyRaw;
import com.stockboo.app.domain.CryptoCurrencyDataDomain;
import com.stockboo.app.domain.CryptoCurrencyDomain;
import com.stockboo.app.entity.CryptoCurrencyEntity;

public class CryptoDao extends AbstractStockbooDao{
	private static DecimalFormat df2 = new DecimalFormat(".##");

	public CryptoCurrencyDomain getCryptoCurrencyList() {

		MemcacheService memcache = MemcacheServiceFactory.getMemcacheService();
		memcache.setErrorHandler(ErrorHandlers.getConsistentLogAndContinue(Level.INFO));

		String topThreeCrypto = null;

		if (null != memcache.get("cryptocurrency")) {
			topThreeCrypto = (String) memcache.get("cryptocurrency");
		} else {
			List<CryptoCurrencyEntity> cryptoEntityList = new ArrayList<>();
			Query cryptoQuery = new Query(CryptoCurrencyEntity.TABLE_NAME);
			
			PreparedQuery results = getDataStoreService().prepare(cryptoQuery);

			for (Entity entity : results.asIterable()) {
				cryptoEntityList.add(new CryptoCurrencyEntity(entity));
			}
			CryptoCurrencyEntity cryptoEntity = cryptoEntityList.get(0);
			if (null != cryptoEntity) {
				topThreeCrypto = cryptoEntity.getFullDetails().getValue();
				memcache.put("topThreecryptocurrency", topThreeCrypto);
			}
		}
		Gson gson = new Gson();
		List<CryptoCurrencyRaw> cryptoList = gson.fromJson(topThreeCrypto, new TypeToken<List<CryptoCurrencyRaw>>() {
		}.getType());

		List<CryptoCurrencyDataDomain> cryptoDomainList = new ArrayList<>();
		for (CryptoCurrencyRaw crypto : cryptoList) {
			CryptoCurrencyDataDomain domain = new CryptoCurrencyDataDomain();
			domain.setSymbol(crypto.getName());
			Double priceInr = Double.parseDouble(crypto.getPriceInr());
			domain.setLtp(df2.format(priceInr));
			domain.setPChange(crypto.getPercentChange1h());
			Double previousPrice = (100 * Double.parseDouble(crypto.getPriceInr()))
					/ (Double.parseDouble(crypto.getPercentChange1h()) + 100);
			domain.setPreviousPrice(df2.format(previousPrice));
			Double change = Double.parseDouble(crypto.getPriceInr()) - previousPrice;
			domain.setChange(df2.format(change));
			cryptoDomainList.add(domain);
		}

		CryptoCurrencyDomain cryptoCompleteList = new CryptoCurrencyDomain("",
				cryptoDomainList);
		
		return cryptoCompleteList;
	}

}
