/**
 * 
 */
package com.stockboo.app.domain;

/**
 * @author jayavardhan
 *
 */
public class BSE {
	 /*{
		   "FIELD1": "Security Code",
		   "FIELD2": "Security Id",
		   "FIELD3": "Security Name",
		   "FIELD4": "Status",
		   "FIELD5": "Group",
		   "FIELD6": "Face Value",
		   "FIELD7": "ISIN No",
		   "FIELD8": "Industry",
		   "FIELD9": "Instrument"
		 }*/

	private String fIELD1;
	private String fIELD2;
	private String fIELD3;
	private String fIELD4;
	private String fIELD5;
	private String fIELD6;
	private String fIELD7;
	private String fIELD8;
	private String fIELD9;
	public BSE() {

	}
	/**
	 * @return the fIELD1
	 */
	public String getfIELD1() {
		return fIELD1;
	}
	/**
	 * @param fIELD1 the fIELD1 to set
	 */
	public void setfIELD1(String fIELD1) {
		this.fIELD1 = fIELD1;
	}
	/**
	 * @return the fIELD2
	 */
	public String getfIELD2() {
		return fIELD2;
	}
	/**
	 * @param fIELD2 the fIELD2 to set
	 */
	public void setfIELD2(String fIELD2) {
		this.fIELD2 = fIELD2;
	}
	/**
	 * @return the fIELD3
	 */
	public String getfIELD3() {
		return fIELD3;
	}
	/**
	 * @param fIELD3 the fIELD3 to set
	 */
	public void setfIELD3(String fIELD3) {
		this.fIELD3 = fIELD3;
	}
	/**
	 * @return the fIELD4
	 */
	public String getfIELD4() {
		return fIELD4;
	}
	/**
	 * @param fIELD4 the fIELD4 to set
	 */
	public void setfIELD4(String fIELD4) {
		this.fIELD4 = fIELD4;
	}
	/**
	 * @return the fIELD5
	 */
	public String getfIELD5() {
		return fIELD5;
	}
	/**
	 * @param fIELD5 the fIELD5 to set
	 */
	public void setfIELD5(String fIELD5) {
		this.fIELD5 = fIELD5;
	}
	/**
	 * @return the fIELD6
	 */
	public String getfIELD6() {
		return fIELD6;
	}
	/**
	 * @param fIELD6 the fIELD6 to set
	 */
	public void setfIELD6(String fIELD6) {
		this.fIELD6 = fIELD6;
	}
	/**
	 * @return the fIELD7
	 */
	public String getfIELD7() {
		return fIELD7;
	}
	/**
	 * @param fIELD7 the fIELD7 to set
	 */
	public void setfIELD7(String fIELD7) {
		this.fIELD7 = fIELD7;
	}
	/**
	 * @return the fIELD8
	 */
	public String getfIELD8() {
		return fIELD8;
	}
	/**
	 * @param fIELD8 the fIELD8 to set
	 */
	public void setfIELD8(String fIELD8) {
		this.fIELD8 = fIELD8;
	}
	/**
	 * @return the fIELD9
	 */
	public String getfIELD9() {
		return fIELD9;
	}
	/**
	 * @param fIELD9 the fIELD9 to set
	 */
	public void setfIELD9(String fIELD9) {
		this.fIELD9 = fIELD9;
	}
}	