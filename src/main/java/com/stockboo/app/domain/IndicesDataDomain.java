package com.stockboo.app.domain;

public class IndicesDataDomain {

	private String symbol;
	private String ltp;
	private String change;
	private String pChange;
	private String imgFileName;
	
	/**
	 * @param name
	 * @param lastPrice
	 * @param change
	 * @param pChange
	 * @param imgFileName
	 */
	public IndicesDataDomain(String name, String lastPrice, String change, String pChange, String imgFileName) {
		super();
		this.symbol = name;
		this.ltp = lastPrice;
		this.change = change;
		this.pChange = pChange;
		this.imgFileName = imgFileName;
	}

	/**
	 * 
	 */
	public IndicesDataDomain() {
		super();
		// TODO Auto-generated constructor stub
	}

	public String getSymbol() {
		return symbol;
	}

	public void setSymbol(String name) {
		this.symbol = name;
	}

	public String getLtp() {
		return ltp;
	}

	public void setLtp(String lastPrice) {
		this.ltp = lastPrice;
	}

	public String getChange() {
		return change;
	}

	public void setChange(String change) {
		this.change = change;
	}

	public String getPChange() {
		return pChange;
	}

	public void setPChange(String pChange) {
		this.pChange = pChange;
	}

	public String getImgFileName() {
		return imgFileName;
	}

	public void setImgFileName(String imgFileName) {
		this.imgFileName = imgFileName;
	}

}