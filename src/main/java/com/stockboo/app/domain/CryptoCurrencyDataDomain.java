package com.stockboo.app.domain;

import java.math.BigDecimal;

public class CryptoCurrencyDataDomain {

	private String symbol;
	private String series;
	private String openPrice;
	private String highPrice;
	private String lowPrice;
	private String ltp;
	private String previousPrice;
	private String pChange;
	private String tradedQuantity;
	private String turnoverInLakhs;
	private String lastCorpAnnouncementDate;
	private String lastCorpAnnouncement;
	private String change;

	/**
	 * 
	 */
	public CryptoCurrencyDataDomain() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param symbol
	 * @param series
	 * @param openPrice
	 * @param highPrice
	 * @param lowPrice
	 * @param ltp
	 * @param previousPrice
	 * @param netPrice
	 * @param tradedQuantity
	 * @param turnoverInLakhs
	 * @param lastCorpAnnouncementDate
	 * @param lastCorpAnnouncement
	 */
	public CryptoCurrencyDataDomain(String symbol, String series, String openPrice, String highPrice, String lowPrice,
			String ltp, String previousPrice, String netPrice, String tradedQuantity, String turnoverInLakhs,
			String lastCorpAnnouncementDate, String lastCorpAnnouncement) {
		super();
		this.symbol = symbol;
		this.series = series;
		this.openPrice = openPrice;
		this.highPrice = highPrice;
		this.lowPrice = lowPrice;
		this.ltp = ltp;
		this.previousPrice = previousPrice;
		this.pChange = netPrice;
		this.tradedQuantity = tradedQuantity;
		this.turnoverInLakhs = turnoverInLakhs;
		this.lastCorpAnnouncementDate = lastCorpAnnouncementDate;
		this.lastCorpAnnouncement = lastCorpAnnouncement;
	}

	public String getSymbol() {
		return symbol;
	}

	public void setSymbol(String symbol) {
		this.symbol = symbol;
	}

	public String getSeries() {
		return series;
	}

	public void setSeries(String series) {
		this.series = series;
	}

	public String getOpenPrice() {
		return openPrice;
	}

	public void setOpenPrice(String openPrice) {
		this.openPrice = openPrice;
	}

	public String getHighPrice() {
		return highPrice;
	}

	public void setHighPrice(String highPrice) {
		this.highPrice = highPrice;
	}

	public String getLowPrice() {
		return lowPrice;
	}

	public void setLowPrice(String lowPrice) {
		this.lowPrice = lowPrice;
	}

	public String getLtp() {
		return ltp;
	}

	public void setLtp(String ltp) {
		this.ltp = ltp;
	}

	public String getPreviousPrice() {
		return previousPrice;
	}

	public void setPreviousPrice(String previousPrice) {
		this.previousPrice = previousPrice;
	}

	public String getPChange() {
		return pChange;
	}

	public void setPChange(String netPrice) {
		this.pChange = netPrice;
	}

	public String getTradedQuantity() {
		return tradedQuantity;
	}

	public void setTradedQuantity(String tradedQuantity) {
		this.tradedQuantity = tradedQuantity;
	}

	public String getTurnoverInLakhs() {
		return turnoverInLakhs;
	}

	public void setTurnoverInLakhs(String turnoverInLakhs) {
		this.turnoverInLakhs = turnoverInLakhs;
	}

	public String getLastCorpAnnouncementDate() {
		return lastCorpAnnouncementDate;
	}

	public void setLastCorpAnnouncementDate(String lastCorpAnnouncementDate) {
		this.lastCorpAnnouncementDate = lastCorpAnnouncementDate;
	}

	public String getLastCorpAnnouncement() {
		return lastCorpAnnouncement;
	}

	public void setLastCorpAnnouncement(String lastCorpAnnouncement) {
		this.lastCorpAnnouncement = lastCorpAnnouncement;
	}

	public String getChange() {
		return change;
	}

	public void setChange(String change) {
		this.change = change;
	}
}