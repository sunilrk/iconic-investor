package com.stockboo.app.domain;

import java.util.Date;

public class RssFeedItems {

	private String title;

	private String link;

	// private String descriptionType;

	private String img;

	private String description;

	private Date publDate;

	private String author;

	/**
	 * 
	 */
	public RssFeedItems() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param title
	 * @param link
	 * @param descriptionType
	 * @param description
	 * @param pubDate
	 * @param author
	 */
	public RssFeedItems(String title, String link, String description, Date pubDate, String author) {
		super();
		this.title = title;
		this.link = link;
		// this.descriptionType = descriptionType;
		descriptionSeparation(description);
		this.publDate = pubDate;
		if (author == "") {
			this.author = null;
		} else {
			this.author = author;
		}

	}

	/**
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * @param title
	 *            the title to set
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * @return the link
	 */
	public String getLink() {
		return link;
	}

	/**
	 * @param link
	 *            the link to set
	 */
	public void setLink(String link) {
		this.link = link;
	}

	/**
	 * @return the descriptionValue
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description
	 *            the descriptionValue to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @return the pub date
	 */
	public Date getPubDate() {
		return publDate;
	}

	/**
	 * @param pubDate
	 *            the pub date to set
	 */
	public void setPubDate(Date pubDate) {
		this.publDate = pubDate;
	}

	/**
	 * @return the author
	 */
	public String getAuthor() {
		return author;
	}

	/**
	 * @param author
	 *            the author to set
	 */
	public void setAuthor(String author) {
		this.author = author;
	}

	public void descriptionSeparation(String description) {
		if (description.contains("src=")) {
			String[] splitOnArrow = description.split("\\>");
			this.description = splitOnArrow[1];
			if ((!splitOnArrow[0].isEmpty()) || (splitOnArrow[0] != null)) {
				String[] split = splitOnArrow[0].split("\"");
				if ((!split[1].isEmpty()) || (split[1] != null)) {
					this.img = split[1];
				}
			}
		} else
			this.description = description;

	}

	/**
	 * @return the img
	 */
	public String getImg() {
		return img;
	}

	/**
	 * @param img
	 *            the img to set
	 */
	public void setImg(String img) {
		this.img = img;
	}
}