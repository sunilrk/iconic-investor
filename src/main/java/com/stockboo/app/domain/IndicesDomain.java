package com.stockboo.app.domain;

import java.util.List;

public class IndicesDomain {

	private Integer preOpen;
	private String time;
	private Integer corrOpen;
	private String status;
	private String haltedStatus;
	private Integer mktOpen;
	private List<IndicesDataDomain> data = null;
	private Integer code;
	private Integer corrClose;
	private Integer preClose;
	private Integer mktClose;

	/**
	 * 
	 */
	public IndicesDomain() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param preOpen
	 * @param time
	 * @param corrOpen
	 * @param status
	 * @param haltedStatus
	 * @param mktOpen
	 * @param data
	 * @param code
	 * @param corrClose
	 * @param preClose
	 * @param mktClose
	 */
	public IndicesDomain(Integer preOpen, String time, Integer corrOpen, String status, String haltedStatus,
			Integer mktOpen, List<IndicesDataDomain> data, Integer code, Integer corrClose, Integer preClose,
			Integer mktClose) {
		super();
		this.preOpen = preOpen;
		this.time = time;
		this.corrOpen = corrOpen;
		this.status = status;
		this.haltedStatus = haltedStatus;
		this.mktOpen = mktOpen;
		this.data = data;
		this.code = code;
		this.corrClose = corrClose;
		this.preClose = preClose;
		this.mktClose = mktClose;
	}

	public Integer getPreOpen() {
		return preOpen;
	}

	public void setPreOpen(Integer preOpen) {
		this.preOpen = preOpen;
	}

	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}

	public Integer getCorrOpen() {
		return corrOpen;
	}

	public void setCorrOpen(Integer corrOpen) {
		this.corrOpen = corrOpen;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getHaltedStatus() {
		return haltedStatus;
	}

	public void setHaltedStatus(String haltedStatus) {
		this.haltedStatus = haltedStatus;
	}

	public Integer getMktOpen() {
		return mktOpen;
	}

	public void setMktOpen(Integer mktOpen) {
		this.mktOpen = mktOpen;
	}

	public List<IndicesDataDomain> getData() {
		return data;
	}

	public void setData(List<IndicesDataDomain> data) {
		this.data = data;
	}

	public Integer getCode() {
		return code;
	}

	public void setCode(Integer code) {
		this.code = code;
	}

	public Integer getCorrClose() {
		return corrClose;
	}

	public void setCorrClose(Integer corrClose) {
		this.corrClose = corrClose;
	}

	public Integer getPreClose() {
		return preClose;
	}

	public void setPreClose(Integer preClose) {
		this.preClose = preClose;
	}

	public Integer getMktClose() {
		return mktClose;
	}

	public void setMktClose(Integer mktClose) {
		this.mktClose = mktClose;
	}

}