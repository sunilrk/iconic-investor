/**
 * 
 */
package com.stockboo.app.domain;

import java.util.List;

/**
 * @author sunil.r
 *
 */
public class BSEData {

	private List<BSE> bse;

	private String data;

	/**
	 * @return the bse
	 */
	public List<BSE> getBse() {
		return bse;
	}

	/**
	 * @param bse
	 *            the bse to set
	 */
	public void setBse(List<BSE> bse) {
		this.bse = bse;
	}

	/**
	 * @return the data
	 */
	public String getData() {
		return data;
	}

	/**
	 * @param data
	 *            the data to set
	 */
	public void setData(String data) {
		this.data = data;
	}

}
