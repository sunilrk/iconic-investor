/**
 * 
 */
package com.stockboo.app.domain;

import java.util.List;

/**
 * @author sunil.r
 *
 */
public class NSEData {

	private List<NSE> nse;

	private String data;

	/**
	 * @return the nse
	 */
	public List<NSE> getNse() {
		return nse;
	}

	/**
	 * @param nse
	 *            the nse to set
	 */
	public void setNse(List<NSE> nse) {
		this.nse = nse;
	}

	/**
	 * @return the data
	 */
	public String getData() {
		return data;
	}

	/**
	 * @param data
	 *            the data to set
	 */
	public void setData(String data) {
		this.data = data;
	}

}
