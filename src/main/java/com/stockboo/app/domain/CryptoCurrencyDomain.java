package com.stockboo.app.domain;

import java.util.List;

public class CryptoCurrencyDomain {

	private String time;
	private List<CryptoCurrencyDataDomain> data = null;

	/**
	 * 
	 */
	public CryptoCurrencyDomain() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param time
	 * @param data
	 */
	public CryptoCurrencyDomain(String time, List<CryptoCurrencyDataDomain> data) {
		super();
		this.time = time;
		this.data = data;
	}

	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}

	public List<CryptoCurrencyDataDomain> getData() {
		return data;
	}

	public void setData(List<CryptoCurrencyDataDomain> data) {
		this.data = data;
	}

	public void addToList(List<CryptoCurrencyDataDomain> data) {
		this.data.addAll(data);
	}

}