package com.stockboo.app.domain.utility;

import java.util.Date;

/**
 * 
 * @author sunil.r
 *
 */
public class DateRequest {

	private Date fromDate;

	public Date getFromDate() {
		return fromDate;
	}

	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}

}
