package com.stockboo.app.domain.utility;

/**
 * 
 * @author sunil.r
 *
 */
public class StringUtil {

	private String response;
	
	

	/**
	 * @param response
	 */
	public StringUtil(String response) {
		super();
		this.response = response;
	}

	/**
	 * 
	 */
	public StringUtil() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @return the response
	 */
	public String getResponse() {
		return response;
	}

	/**
	 * @param response
	 *            the response to set
	 */
	public void setResponse(String response) {
		this.response = response;
	}

}
