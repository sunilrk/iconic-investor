package com.stockboo.app.domain.utility;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import org.joda.time.DateTime;

/**
 * 
 * @author sunil.r
 *
 */
public class DateUtil {

	public static Date getWeekDay() {
		Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("GMT"));
		cal.setTimeZone(TimeZone.getTimeZone("Asia/Calcutta"));
		cal.get(Calendar.HOUR_OF_DAY);
		Date date = null;
		if (cal.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY) {
			System.out.println("Sunday!");
			cal.add(Calendar.DATE, -2);
			date = cal.getTime();
		}

		else if (cal.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY) {
			System.out.println("Saturday!");
			cal.add(Calendar.DATE, -1);
			date = cal.getTime();
		} else {
			date = cal.getTime();
		}
		System.out.println(date);
		String expectedPattern = "MM/dd/yyyy";
		SimpleDateFormat formatter = new SimpleDateFormat(expectedPattern);
		String format = formatter.format(date);
		Date timeZeroDate = null;
		try {
			timeZeroDate = formatter.parse(format);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		System.out.println(timeZeroDate);
		return timeZeroDate;

	}

	public static Date subtractDay(Date date) {
	    Calendar cal = Calendar.getInstance();
	    cal.setTime(date);
	    cal.add(Calendar.DAY_OF_MONTH, -1);
	    return cal.getTime();
	}
	
	public static String getIST_CurrentDate() {
		SimpleDateFormat sd = new SimpleDateFormat("yyyy.MM.dd G 'at' HH:mm:ss z");
		Date date = new Date();
		// TODO: Avoid using the abbreviations when fetching time zones.
		// Use the full Olson zone ID instead.
		sd.setTimeZone(TimeZone.getTimeZone("IST"));
		return sd.format(date);
	}
	
	public static String getIST_CurrentTime() {
		SimpleDateFormat sd = new SimpleDateFormat("HH:mm");
		Date date = new Date();
		sd.setTimeZone(TimeZone.getTimeZone("IST"));
		String time = sd.format(date);
		System.out.println("ISD-TIme"+ time);
		return time;
	}
	
	public static Date convertLongToDate(Object object){
		Date date=new Date(1524981882094L);
        SimpleDateFormat df2 = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
        String dateText = df2.format(date);
        System.out.println(dateText);
		return date;
	}

}
