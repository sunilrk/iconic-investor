package com.stockboo.app.domain.utility;

import java.util.Map;

/**
 * 
 * @author sunil.r
 *
 */
public class MapUtil {

	Map<Object, Object> response;

	public MapUtil(Map<Object, Object> response) {
		super();
		this.response = response;
	}

	public Map<Object, Object> getResponse() {
		return response;
	}

	public void setResponse(Map<Object, Object> response) {
		this.response = response;
	}

}
