package com.stockboo.app.domain.utility;

import javax.servlet.http.HttpServletRequest;

import com.stockboo.app.configuration.Constants;

public class CommonHelper {

	public static String getInstallationIdFromHeader(HttpServletRequest req) throws Exception {
		String installationId = req.getHeader(Constants.INSTALLATION_ID);
		return (null==installationId)? "" :installationId;
	}
}
