package com.stockboo.app.domain.pushnotification;

public class SendNotification {

	private String title;

	private String body;

	private String topic;
	
	private String badge;
	
	private String sound;

	private boolean send;
	
	private String dataMessage;
	
	public SendNotification() {
		super();
		// TODO Auto-generated constructor stub
	}

	public SendNotification(String title, String body, String topic, boolean send) {
		super();
		this.title = title;
		this.body = body;
		this.topic = topic;
		this.send = send;
	}

	public String getDataMessage() {
		return dataMessage;
	}

	public void setDataMessage(String dataMessage) {
		this.dataMessage = dataMessage;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getBody() {
		return body;
	}

	public void setBody(String body) {
		this.body = body;
	}

	public String getTopic() {
		return topic;
	}

	public void setTopic(String topic) {
		this.topic = topic;
	}

	public boolean isSend() {
		return send;
	}

	public void setSend(boolean send) {
		this.send = send;
	}

	public String getBadge() {
		return badge;
	}

	public void setBadge(String badge) {
		this.badge = badge;
	}

	public String getSound() {
		return sound;
	}

	public void setSound(String sound) {
		this.sound = sound;
	}
	
	

}
