package com.stockboo.app.domain.pushnotification;

/**
 * 
 * @author sunil.r
 *
 */
public class SubscriptionTopic {

	private String title;
	private String id;
	private String description;
	
	public SubscriptionTopic(String title, String id, String description) {
		super();
		this.title = title;
		this.id = id;
		this.description = description;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

}
