package com.stockboo.app.domain.pushnotification;

/**
 * 
 * @author sunil.r
 *
 */
public enum SubscriptionTopics {

	MARKET_ALERTS("Market Alerts", "market_alerts", "Market alerts will bring you live market trends of Nifty and Sensex alerts, which includes  Opening trend, Closing position, and top performing stock. Max 5 Notifications per day."), 
	NEWS("News", "news", "You will get News Notifications"), 
	GENERAL("General", "general", "General alerts bring you some important happenings in the stock market. You will receive max 1 or 2 Notifications.");

	private String tittle;
	private String id;
	private String description;
	
	private SubscriptionTopics(String tittle, String id, String description) {
		this.tittle = tittle;
		this.id = id;
		this.description = description;
	}

	public String getTittle() {
		return tittle;
	}


	public String getId() {
		return id;
	}

	public String getDescription() {
		return description;
	}

	

}
