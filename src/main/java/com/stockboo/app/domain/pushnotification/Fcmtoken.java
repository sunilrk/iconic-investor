package com.stockboo.app.domain.pushnotification;

/**
 * 
 * @author sunil.r
 *
 */
public class Fcmtoken {
	
	private String installationId;
	
	private String fcmToken;

	public String getInstallationId() {
		return installationId;
	}

	public void setInstallationId(String installationId) {
		this.installationId = installationId;
	}

	public String getFcmToken() {
		return fcmToken;
	}

	public void setFcmToken(String fcmToken) {
		this.fcmToken = fcmToken;
	}
	
	

}
