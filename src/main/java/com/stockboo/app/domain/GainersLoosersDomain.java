package com.stockboo.app.domain;

import java.util.List;

public class GainersLoosersDomain {

	private String time;
	private List<GainersLoosersDataDomain> data = null;

	/**
	 * 
	 */
	public GainersLoosersDomain() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param time
	 * @param data
	 */
	public GainersLoosersDomain(String time, List<GainersLoosersDataDomain> data) {
		super();
		this.time = time;
		this.data = data;
	}

	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}

	public List<GainersLoosersDataDomain> getData() {
		return data;
	}

	public void setData(List<GainersLoosersDataDomain> data) {
		this.data = data;
	}

	public void addToList(List<GainersLoosersDataDomain> data) {
		this.data.addAll(data);
	}

}