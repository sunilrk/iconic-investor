package com.stockboo.app.domain;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

public class ScriptListOutput implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private List<ScriptCodeDomain> scriptCodeList;

	private String nextOffset;
	
	private String totalNumOfRecords;
	
	private Date lastUpdatedDate;

	/**
	 * @return the scriptCodeList
	 */
	public List<ScriptCodeDomain> getScriptCodeList() {
		return scriptCodeList;
	}

	/**
	 * @param scriptCodeList
	 *            the scriptCodeList to set
	 */
	public void setScriptCodeList(List<ScriptCodeDomain> scriptCodeList) {
		this.scriptCodeList = scriptCodeList;
	}

	/**
	 * @return the Offset
	 */
	public String getNextOffset() {
		return nextOffset;
	}

	/**
	 * @param Offset
	 */
	public void setNextOffset(String offset) {
		this.nextOffset = offset;
	}

	/**
	 * @return the totalNumOfRecords
	 */
	public String getTotalNumOfRecords() {
		return totalNumOfRecords;
	}

	/**
	 * @param totalNumOfRecords the totalNumOfRecords to set
	 */
	public void setTotalNumOfRecords(String totalNumOfRecords) {
		this.totalNumOfRecords = totalNumOfRecords;
	}

	/**
	 * @return the lastUpdatedDate
	 */
	public Date getLastUpdatedDate() {
		return lastUpdatedDate;
	}

	/**
	 * @param lastUpdatedDate the lastUpdatedDate to set
	 */
	public void setLastUpdatedDate(Date lastUpdatedDate) {
		this.lastUpdatedDate = lastUpdatedDate;
	}
	
}
