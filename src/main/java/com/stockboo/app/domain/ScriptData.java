/**
 * 
 */
package com.stockboo.app.domain;

import java.util.Date;
import java.util.List;

/**
 * @author sunil.r
 *
 */
public class ScriptData {
 
	private Date created;
	
	private Date updated;
	
	private List<ScriptCodes> scriptCodes;

	/**
	 * @return the created
	 */
	public Date getCreated() {
		return created;
	}

	/**
	 * @param created the created to set
	 */
	public void setCreated(Date created) {
		this.created = created;
	}

	/**
	 * @return the updated
	 */
	public Date getUpdated() {
		return updated;
	}

	/**
	 * @param updated the updated to set
	 */
	public void setUpdated(Date updated) {
		this.updated = updated;
	}

	/**
	 * @return the scriptCodes
	 */
	public List<ScriptCodes> getScriptCodes() {
		return scriptCodes;
	}

	/**
	 * @param scriptCodes the scriptCodes to set
	 */
	public void setScriptCodes(List<ScriptCodes> scriptCodes) {
		this.scriptCodes = scriptCodes;
	}
}
