/**
 * 
 */
package com.stockboo.app.domain;

/**
 * @author sunil.r
 *
 */
public class ScriptCodes {

	private String nseCode;
	
	private String bseCode;
	
	private String companyName;
	
	private String group;
	
	private String faceValue;
	
	private String iSIN;
	
	private String industy;
	
	private String firstListingDate;

	/**
	 * @return the nseCode
	 */
	public String getNseCode() {
		return nseCode;
	}

	/**
	 * @param nseCode the nseCode to set
	 */
	public void setNseCode(String nseCode) {
		this.nseCode = nseCode;
	}

	/**
	 * @return the bseCode
	 */
	public String getBseCode() {
		return bseCode;
	}

	/**
	 * @param bseCode the bseCode to set
	 */
	public void setBseCode(String bseCode) {
		this.bseCode = bseCode;
	}

	/**
	 * @return the companyName
	 */
	public String getCompanyName() {
		return companyName;
	}

	/**
	 * @param companyName the companyName to set
	 */
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	/**
	 * @return the group
	 */
	public String getGroup() {
		return group;
	}

	/**
	 * @param group the group to set
	 */
	public void setGroup(String group) {
		this.group = group;
	}

	/**
	 * @return the faceValue
	 */
	public String getFaceValue() {
		return faceValue;
	}

	/**
	 * @param faceValue the faceValue to set
	 */
	public void setFaceValue(String faceValue) {
		this.faceValue = faceValue;
	}

	/**
	 * @return the iSIN
	 */
	public String getiSIN() {
		return iSIN;
	}

	/**
	 * @param iSIN the iSIN to set
	 */
	public void setiSIN(String iSIN) {
		this.iSIN = iSIN;
	}

	/**
	 * @return the industy
	 */
	public String getIndusty() {
		return industy;
	}

	/**
	 * @param industy the industy to set
	 */
	public void setIndusty(String industy) {
		this.industy = industy;
	}

	/**
	 * @return the firstListingDate
	 */
	public String getFirstListingDate() {
		return firstListingDate;
	}

	/**
	 * @param firstListingDate the firstListingDate to set
	 */
	public void setFirstListingDate(String firstListingDate) {
		this.firstListingDate = firstListingDate;
	}
	
}
