package com.stockboo.app;

import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.annotation.Nullable;
import javax.servlet.http.HttpServletRequest;
import javax.validation.constraints.NotNull;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

//import com.google.api.server.spi.config.Api;
//import com.google.api.server.spi.config.ApiMethod;
//import com.google.api.server.spi.config.Named;
//import com.google.api.server.spi.config.Nullable;
import com.google.appengine.api.memcache.ErrorHandlers;
import com.google.appengine.api.memcache.MemcacheService;
import com.google.appengine.api.memcache.MemcacheServiceFactory;
import com.org.iconic.rest.services.helper.RestResponse;
import com.org.iconic.rest.services.helper.RestResponseBuilder;
import com.stockboo.app.configuration.Constants;
import com.stockboo.app.cronjobs.HighLow52;
import com.stockboo.app.dao.AdvisorDao;
import com.stockboo.app.dao.AppAuthenticationDao;
import com.stockboo.app.dao.CryptoDao;
import com.stockboo.app.dao.HomeScreenDao;
import com.stockboo.app.dao.InstallDao;
import com.stockboo.app.dao.MessageDao;
import com.stockboo.app.dao.ScriptDao;
import com.stockboo.app.dao.StockDao;
import com.stockboo.app.domain.Advisor;
import com.stockboo.app.domain.AdvisorCreate;
import com.stockboo.app.domain.AdvisorUpdate;
import com.stockboo.app.domain.BSEData;
import com.stockboo.app.domain.CryptoCurrencyDomain;
import com.stockboo.app.domain.GainersLoosersDomain;
import com.stockboo.app.domain.IndicesDomain;
import com.stockboo.app.domain.Install;
import com.stockboo.app.domain.InstallCreate;
import com.stockboo.app.domain.InstallUpdate;
import com.stockboo.app.domain.Message;
import com.stockboo.app.domain.MessageCreate;
import com.stockboo.app.domain.NSEData;
import com.stockboo.app.domain.NewToken;
import com.stockboo.app.domain.RefreshOutput;
import com.stockboo.app.domain.RssFeedItems;
import com.stockboo.app.domain.ScriptCodeDomain;
import com.stockboo.app.domain.ScriptListOutput;
import com.stockboo.app.domain.pushnotification.Fcmtoken;
import com.stockboo.app.domain.pushnotification.SendNotification;
import com.stockboo.app.domain.pushnotification.SubscriptionTopic;
import com.stockboo.app.domain.utility.CommonHelper;
import com.stockboo.app.domain.utility.DateRequest;
import com.stockboo.app.domain.utility.StringUtil;
import com.stockboo.app.entity.FcmVerification;
import com.stockboo.app.entity.NotificationEntity;
import com.stockboo.app.firebase.PushNotification;
import com.stockboo.app.homescreen.HomeScreenAPI;
import com.stockboo.app.service.ScriptCodeService;
import com.stockboo.app.stock.domain.Status;
import com.stockboo.app.stock.domain.Stock;
import com.stockboo.app.stock.domain.StockCreate;
import com.stockboo.app.stock.domain.StockUpdate;
import com.stockboo.app.stock.domain.SugesstionType;

/**
 * Add your first API methods in this class, or you may create another class. In
 * that case, please update your web.xml accordingly.
 **/
@RestController
@RequestMapping("/_ah/api/stockboo/v1")
public class YourFirstAPI {
	
	private static final Logger log = Logger.getLogger(YourFirstAPI.class.getName());

	/**
	 * Reads the installation based on installation id
	 * 
	 * @param installationsID
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "installations/read", method = RequestMethod.GET)
	public Install installationRead(HttpServletRequest req, @Nullable @RequestParam(value="installationsID") Long installationsID) throws Exception {
		authentication(req);
		InstallDao idao = new InstallDao();
		Install i = idao.read(installationsID);
		return i;
	}

	/**
	 * Creates the installation
	 * 
	 * @param i
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "installations/create", method = RequestMethod.POST)
	public Install installationCreate(@RequestBody InstallCreate install) throws Exception {
		InstallDao idao = new InstallDao();
		return idao.create(install);
	}

	/**
	 * Lists all the installations
	 * 
	 * @return List<Install>
	 * @throws Exception 
	 */
	@RequestMapping(value = "installations/list", method = RequestMethod.GET)
	public List<Install> installationList(HttpServletRequest req) throws Exception {
		authentication(req);
		InstallDao idao = new InstallDao();
		List<Install> installList = idao.list();
		if (null != installList) {
			return installList;
		} else
			return null;

	}

	/**
	 * Updates the installation details
	 * 
	 * @param i
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "installations/update", method = RequestMethod.POST)
	public Install installationUpdate(HttpServletRequest req, @RequestBody InstallUpdate i) throws Exception {
		authentication(req);
		System.out.println("Test executed successfully with " + i);
		InstallDao idao = new InstallDao();
		Long createdId = idao.update(i);
		if (null != createdId || 0 != createdId) {
			Install io = new Install();
			io.setInstallationId(createdId);
			return io;
		} else
			return null;

	}

	/**
	 * Method to create a new advisor from client
	 * 
	 * @param advisor
	 *            advisor
	 * @return Advisor
	 * @throws Exception
	 */
	@RequestMapping(value = "advisors/create", method = RequestMethod.POST)
	public RestResponse advisorCreate(@RequestBody AdvisorCreate advisor) throws Exception {
		System.out.println("Advisor successfully created with " + advisor);
		AdvisorDao advisorDao = new AdvisorDao();
		if (advisor != null) {
			return RestResponseBuilder.success(advisorDao.create(advisor));
		} else {
			return null;
		}
	}

	/**
	 * Lists all the advisors present in DB
	 * 
	 * @return
	 * @throws Exception 
	 */
	@RequestMapping(value = "advisors/list", method = RequestMethod.GET)
	public RestResponse advisorList() throws Exception {
		System.out.println("Advisors list successfully retrieved");
		AdvisorDao advisorDao = new AdvisorDao();
		 List<Advisor> list = advisorDao.list();
		 System.out.println(list.size());
		 System.out.println(list.get(0).getMobileNumber());
		 return RestResponseBuilder.success(list);
	}

	/**
	 * Method to read the Advisor from the advisor id
	 * 
	 * @param advisorId
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "advisors/read", method = RequestMethod.GET)
	public RestResponse advisorRead(@Nullable @RequestParam(value="advisorId") Long advisorId) throws Exception {
		System.out.println("Advisor red successfully with id " + advisorId);
		AdvisorDao advisorDao = new AdvisorDao();
		return RestResponseBuilder.success(advisorDao.read(advisorId));
	}

	/**
	 * Method updates the advisor
	 * 
	 * @param advisor
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "advisors/update", method = RequestMethod.POST)
	public RestResponse advisorUpdate(@RequestBody AdvisorUpdate advisor) throws Exception {
		System.out.println("Advisor successfully updated for " + advisor.getId());
		AdvisorDao advisorDao = new AdvisorDao();
		return RestResponseBuilder.success(advisorDao.update(advisor));
	}

	/**
	 * Method blocks the advisor by using the advisor id
	 * 
	 * @param advisorId
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "advisors/block", method = RequestMethod.PUT)
	public RestResponse advisorBlock( @Nullable @RequestParam(value="advisorId") Long advisorId) throws Exception {
		System.out.println("Advisor successfully blocked for " + advisorId);
		AdvisorDao advisorDao = new AdvisorDao();
		return RestResponseBuilder.success(advisorDao.block(advisorId));
	}

	/**
	 * Method deletes the advisor by using the advisor id
	 * 
	 * @param advisorId
	 * @return
	 * @throws Exception
	 */
	@DeleteMapping(value = "advisors/delete")
	public void advisorDelete( @Nullable @RequestParam(value="advisorId") Long advisorId) throws Exception {
		System.out.println("Advisor successfully deleted for " + advisorId);
		AdvisorDao advisorDao = new AdvisorDao();
		advisorDao.delete(advisorId);
	}

	/**
	 * Creates the Stock
	 * 
	 * @param i
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "stocks/create", method = RequestMethod.POST)
	public Stock stockCreate(HttpServletRequest req, @RequestBody StockCreate i) throws Exception {
		log.info(i.getAdvisorId().toString());
		log.info(i.getName());
		authentication(req);
		StockDao sdao = new StockDao();
		return sdao.create(i);
	}

	/**
	 * Updates the Stock
	 * 
	 * @param s
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "stocks/update", method = RequestMethod.POST)
	public Stock stockUpdate(HttpServletRequest req, @RequestBody StockUpdate s) throws Exception {
		authentication(req);
		StockDao sdao = new StockDao();
		Stock existingStock = sdao.read(s.getId());
		if (existingStock.getAdvisor().getId() != s.getAdvisorId())
			throw new Exception("You are not authorized to update the stock");
		return sdao.update(s);
	}

	/**
	 * Method deletes the Stock by using the stock id
	 * 
	 * @param advisorId
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "stocks/delete", method = RequestMethod.DELETE)
	public void stockDelete(HttpServletRequest req, @RequestParam(value="stockId") Long stockId) throws Exception {
		authentication(req);
		StockDao stockDao = new StockDao();
		stockDao.delete(stockId);
	}

	/**
	 * Lists all the stocks
	 * 
	 * @return List<Stock>
	 * @throws Exception 
	 */
	@RequestMapping(value = "stocks/list", method = RequestMethod.GET)
	public List<Stock> stockListWithFilters(HttpServletRequest req, @Nullable @RequestParam(value="sugesstionType") SugesstionType sugesstionType,
			@Nullable @RequestParam(value="status") Status status, @Nullable @RequestParam(value="fromDate") Date recordsFrom,
			@Nullable @RequestParam(value="stockId") Long stockId) throws Exception {
		authentication(req);
		StockDao stockDao = new StockDao();
		List<Stock> stockList = stockDao.listwithfilters(sugesstionType, status, recordsFrom, stockId);
		if (null != stockList) {
			return stockList;
		} else
			return null;

	}

	/**
	 * Lists all the stocks
	 * 
	 * @return List<Stock>
	 */
	/*	 * @ApiMethod(name = "stocks.list", path = "stocks/list", httpMethod =
		 * ApiMethod.HttpMethod.GET) public List<Stock> stockList() { StockDao
		 * stockDao = new StockDao(); List<Stock> stockList = stockDao.list();
		 * if (null != stockList) { return stockList; } else return null;
		 * 
		 * }*/
		 

	/**
	 * Creates the messages
	 * 
	 * @param message
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "messages/create", method = RequestMethod.POST)
	public Message messageCreate(HttpServletRequest req, @RequestBody MessageCreate message) throws Exception {
		authentication(req);
		MessageDao mdao = new MessageDao();
		return mdao.create(message);
	}

	/**
	 * Method deletes the Stock by using the stock id
	 * 
	 * @param advisorId
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "messages/delete", method = RequestMethod.DELETE)
	public void messageDelete(HttpServletRequest req, @RequestParam(value="messageId") Long messageId) throws Exception {
		authentication(req);
		MessageDao mdao = new MessageDao();
		mdao.delete(messageId);
	}

	/**
	 * Lists all the messages
	 * 
	 * @return List<Message>
	 * @throws Exception 
	 */
	@RequestMapping(value = "messages/list", method = RequestMethod.GET)
	public List<Message> messageList(HttpServletRequest req, @Nullable @RequestParam(value="stockId") Long stockId,
			@Nullable @RequestParam(value="count") Long messageSize) throws Exception {
		authentication(req);
		MessageDao mdao = new MessageDao();
		List<Message> messageList = mdao.list(stockId, messageSize);
		if (null != messageList) {
			return messageList;
		} else
			return null;

	}
	
	/**
	 * Method updates the auth token
	 * 
	 * @param installation ID
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "auth/tokenupdate", method = RequestMethod.GET)
	public NewToken tokenUpdate(@NotNull @RequestParam(value="installationId") Long installationId, @NotNull @RequestParam(value="token") String token) throws Exception {
		AppAuthenticationDao auth = new AppAuthenticationDao();
		NewToken newToken = new NewToken(auth.update(installationId, token));
		return newToken;
	}
	
	/**
	 * Method updates the refresh date
	 * 
	 * @param installation ID, token
	 * @return
	 * @throws Exception
	 * 
	 */
	@RequestMapping(value = "auth/refresh", method = RequestMethod.GET)
	public RefreshOutput refresh(@NotNull @RequestParam(value="installationId") Long installationId, @NotNull @RequestParam(value="token") String token) throws Exception {
		AppAuthenticationDao auth = new AppAuthenticationDao();
		RefreshOutput ro = new RefreshOutput();
		boolean flag;
		try{
			flag = auth.refresh(installationId, token);
		}catch (Exception e){
			flag = false;
		}
		ro.setRefreshed(flag);
		return ro;
		
		
	}
	
	private void authentication(HttpServletRequest req) throws Exception {
		String token = req.getHeader(Constants.AUTH_TOKEN);
		System.out.println(req.getHeader(Constants.AUTH_TOKEN));
		String installationId = req.getHeader(Constants.INSTALLATION_ID);
		System.out.println(req.getHeader(Constants.INSTALLATION_ID));
		AppAuthenticationDao auth = new AppAuthenticationDao();
		try {
			auth.checkRefreshValidity(Long.parseLong(installationId), token);
		} catch (NumberFormatException e) {
			e.printStackTrace();
			throw new Exception("Unable to parse the header");
		} 
	}
	
	/**
	 * Creates NSE data in DB
	 * 
	 * @param nseData
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "scripts/nsedata", method = RequestMethod.POST)
	public StringUtil CreateNSEData(@RequestBody  NSEData nseData) throws Exception {
		ScriptDao sd = new ScriptDao();
		sd.createNse(nseData.getNse());
		return new StringUtil("Success");
	}
	
	/**
	 * Creates BSE data in DB
	 * 
	 * @param bseData
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "scripts/bsedata", method = RequestMethod.POST)
	public StringUtil CreateBSEData(@RequestBody  BSEData bseData) throws Exception {
		ScriptDao sd = new ScriptDao();
		sd.createBse(bseData.getBse());
		return new StringUtil("Success");
	}
	
	/**
	 * List NSE data Based on date
	 * 
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "scripts/list", method = RequestMethod.GET)
	public ScriptListOutput listAllNSEData(HttpServletRequest req, @Nullable @RequestParam(value="lastUpdatedDate") Date recordsFrom,
			@Nullable @RequestParam(value="offset") String offset, @Nullable @RequestParam(value="limit") String limit) throws Exception {
		// System.out.println(index);
		String key = "sl" + offset + limit;
		ScriptListOutput list = null;
		ScriptDao sd = new ScriptDao();
		MemcacheService memcache = MemcacheServiceFactory.getMemcacheService();
		memcache.setErrorHandler(ErrorHandlers.getConsistentLogAndContinue(Level.INFO));
		if(recordsFrom ==null){
		if (memcache.contains(key)) {
			System.out.println("***********Retrieveing from cache");
			return (ScriptListOutput) memcache.get(key);
		} else {
			
			list = sd.list(recordsFrom, offset, limit);
			memcache.put(key, list);
			System.out.println("****************putting to cache");
			
		}
		}
		else
			list = sd.list(recordsFrom, offset, limit);
		return list;
	}
	
	/**
	 * List NSE data Based on date
	 * 
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "scripts/read", method = RequestMethod.GET)
	public ScriptCodeDomain scriptCodeRead(HttpServletRequest req, @Nullable @RequestParam(value="stockCode") String stockCode, @Nullable @RequestParam(value="scriptCodeId") Long scriptCodeId) throws Exception {
		ScriptCodeService scriptCodeService = new ScriptCodeService();
		return scriptCodeService.readScriptCode(stockCode, scriptCodeId);
	}
	
	/**
	 * Creates BSE data by updating Existing NSE records.
	 * 
	 * @param bseData
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "scripts/nseupdate", method = RequestMethod.POST)
	public StringUtil updateNSE(@RequestBody  NSEData nseData) throws Exception {
		ScriptDao sc = new ScriptDao();	
		sc.updateNseScript(nseData.getNse());
//		MemcacheService memcache = MemcacheServiceFactory.getMemcacheService();
//		memcache.setErrorHandler(ErrorHandlers.getConsistentLogAndContinue(Level.INFO));
//		memcache.clearAll();
		return new StringUtil("Success");
	}
	
	@RequestMapping(value = "homescreen/market", method = RequestMethod.GET)
	public HomeScreenAPI homescreenMarket(HttpServletRequest req) throws Exception {
		authentication(req);
		HomeScreenDao hmd = new HomeScreenDao();
		return hmd.homeScreenAPI();
	}
	
	@RequestMapping(value = "homescreen/market/52weekhigh", method = RequestMethod.GET)
	public HighLow52 high52WeekList(HttpServletRequest req) throws Exception {
		HomeScreenDao hmd = new HomeScreenDao();
		return hmd.high52WeekCompleteList();
	}
	
	@RequestMapping(value = "homescreen/market/52weeklow", method = RequestMethod.GET)
	public HighLow52 low52WeekList(HttpServletRequest req) throws Exception {
		authentication(req);
		HomeScreenDao hmd = new HomeScreenDao();
		return hmd.low52WeekCompleteList();
	}
	
	@RequestMapping(value = "homescreen/market/todayshigh", method = RequestMethod.GET)
	public GainersLoosersDomain todayGainersList(HttpServletRequest req) throws Exception {
		authentication(req);
		HomeScreenDao hmd = new HomeScreenDao();
		return hmd.todaysGainersCompleteList();
	}
	
	@RequestMapping(value = "homescreen/market/todayslow", method = RequestMethod.GET)
	public GainersLoosersDomain todayLoosersList(HttpServletRequest req) throws Exception {
		authentication(req);
		HomeScreenDao hmd = new HomeScreenDao();
		return hmd.todaysLoosersCompleteList();
	}
	
	@RequestMapping(value = "homescreen/market/indices", method = RequestMethod.GET)
	public IndicesDomain indicesList(HttpServletRequest req) throws Exception {
		authentication(req);
		HomeScreenDao hmd = new HomeScreenDao();
		return hmd.indicesCompleteList();
	}
	
	@RequestMapping(value = "homescreen/market/morevolumegainers", method = RequestMethod.GET)
	public GainersLoosersDomain moreVolumeGainersList(HttpServletRequest req) throws Exception {
		authentication(req);
		HomeScreenDao hmd = new HomeScreenDao();
		return hmd.moreVolumeGainersCompleteList();
	}
	
	@RequestMapping(value = "homescreen/market/news", method = RequestMethod.GET)
	public List<RssFeedItems> marketNews(HttpServletRequest req) throws Exception {
//		authentication(req);
		HomeScreenDao hmd = new HomeScreenDao();
		return hmd.newsApi();
	}
	
	@RequestMapping(value = "homescreen/market/colorcreate", method = RequestMethod.GET)
	public StringUtil colorCodeCreate() throws Exception {
		HomeScreenDao hmd = new HomeScreenDao();
		hmd.colorcreate();
		return new StringUtil("Success");
	}
	
	@RequestMapping(value = "homescreen/market/colorupdate", method = RequestMethod.GET)
	public StringUtil colorCodeUpdate(@NotNull @RequestParam(value="color") String color, @NotNull @RequestParam(value="screen") String screen ) throws Exception {
		HomeScreenDao hmd = new HomeScreenDao();
		hmd.colorupdate(color, screen);
		return new StringUtil("Success");
	}
	
	@RequestMapping(value = "notify/fcmtoken", method = RequestMethod.POST)
	public StringUtil fcmToken(@RequestBody Fcmtoken fcmtoken) throws Exception {
		PushNotification pn= new PushNotification();
		return new StringUtil(pn.storeFcmToken(fcmtoken));
	}
	
	@RequestMapping(value = "notify/sendnotificationstofcm", method = RequestMethod.POST)
	public StringUtil sendNotificationToFCM(@RequestBody SendNotification sendNotofication) throws Exception {
		PushNotification pn= new PushNotification();
		return new StringUtil(pn.sendNotification(sendNotofication));
	}
	
	@RequestMapping(value = "notify/topics", method = RequestMethod.GET)
	public List<SubscriptionTopic> getSubscriptionTopics() throws Exception {
		return PushNotification.getSubscriptionTopics();
	}
	
	@RequestMapping(value = "notify/sendnotificationstotopics", method = RequestMethod.POST)
	public StringUtil sendNotificationToTopics(@RequestBody SendNotification sendNotofication) throws Exception {
		return new StringUtil(PushNotification.sendNotificationToTopic(sendNotofication));
	}
	
	@RequestMapping(value = "notify/notificationslist", method = RequestMethod.POST)
	public List<NotificationEntity> notificationList(@RequestBody DateRequest fromDate) throws Exception {
		PushNotification pn= new PushNotification();
		return pn.getNotificationsList(fromDate.getFromDate());
	}
	
	@RequestMapping(value = "homescreen/market/cryptocurrency", method = RequestMethod.GET)
	public CryptoCurrencyDomain cryptoCurrencyList(HttpServletRequest req) throws Exception {
		authentication(req);
		CryptoDao cryptoDao = new CryptoDao();
		return cryptoDao.getCryptoCurrencyList();
	}
	
	@RequestMapping(value = "authenticate/verify", method = RequestMethod.POST)
	public StringUtil firebaseLoginAuthentication(HttpServletRequest request, @RequestBody Object object) throws Exception {
		authentication(request);
		String installationId = CommonHelper.getInstallationIdFromHeader(request);
		PushNotification pn= new PushNotification();
		return pn.storeFcmVerificationToken(installationId, object);	
	}
	
	@RequestMapping(value = "authenticate/list", method = RequestMethod.GET)
	public List<FcmVerification> firebaseLoginAuthenticationDetailsList(HttpServletRequest request) throws Exception {
		// idToken comes from the client app (shown above) 
		authentication(request);
		PushNotification pn= new PushNotification();
		return pn.FcmVerificationTokenList();
	}
}