package com.stockboo.app.homescreen;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * Get the data from below URL and this is used in CRON job
 * http://finance.google.com/finance/info?client=ig&q=INDEXBOM:SENSEX,NSE:NIFTY
 * 
 * This saves needed data from sensex and nifty
 * 
 * @author sunil.r
 *
 */

public class NiftySensexPrices implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Long id;

	private BigDecimal value;

	private Date createdAt;
	
	

	/**
	 * 
	 */
	public NiftySensexPrices() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param id
	 * @param sensexPrice
	 * @param createdAt
	 */
	public NiftySensexPrices(Long id, String sensexPrice, Date createdAt) {
		super();
		this.id = id;
		this.value = new BigDecimal(sensexPrice.replace(",", ""));
		this.createdAt = createdAt;
	}

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the sensexPrice
	 */
	public BigDecimal getValue() {
		return value;
	}

	/**
	 * @param sensexPrice
	 *            the sensexPrice to set
	 */
	public void setValue(String sensexPrice) {
		if (null == sensexPrice || "" == sensexPrice) {
			this.value = new BigDecimal(sensexPrice.replace(",", ""));
		} else {
			this.value = BigDecimal.ZERO;
		}

	}

	/**
	 * @return the createdAt
	 */
	public Date getCreatedAt() {
		return createdAt;
	}

	/**
	 * @param createdAt the createdAt to set
	 */
	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	

}
