/**
 * 
 */
package com.stockboo.app.homescreen;

import java.util.Date;

/**
 * @author sunil.r
 *
 */
public class MarketData {
	private String id;
	private String symbol;
	private String exchange;
	private String ltp;
	private String ltpFinal;
	private String lCur;
	private String s;
	private String lastTradedTime;
	private String lastTradeTimeFormatted;
	private String lastTradedDateTime;
	private String changePrice;
	private String changePriceFinal;
	private String changePercentage;
	private String changePercentageFinal;
	private String chr;
	private String previousClose;
	private Date createdAt;
	private Date updatedAt;

	/**
	 * @param id
	 * @param symbol
	 * @param exchange
	 * @param ltp
	 * @param ltpFinal
	 * @param lCur
	 * @param s
	 * @param lastTradedTime
	 * @param lastTradeTimeFormatted
	 * @param lastTradedDateTime
	 * @param changePrice
	 * @param changePriceFinal
	 * @param changePercentage
	 * @param changePercentageFinal
	 * @param chr
	 * @param previousClose
	 * @param createdAt
	 * @param updatedAt
	 */
	public MarketData(String id, String symbol, String exchange, String ltp, String ltpFinal, String lCur, String s,
			String lastTradedTime, String lastTradeTimeFormatted, String lastTradedDateTime, String changePrice,
			String changePriceFinal, String changePercentage, String changePercentageFinal, String chr,
			String previousClose, Date createdAt, Date updatedAt) {
		super();
		this.id = id;
		this.symbol = symbol;
		this.exchange = exchange;
		this.ltp = ltp;
		this.ltpFinal = ltpFinal;
		this.lCur = lCur;
		this.s = s;
		this.lastTradedTime = lastTradedTime;
		this.lastTradeTimeFormatted = lastTradeTimeFormatted;
		this.lastTradedDateTime = lastTradedDateTime;
		this.changePrice = changePrice;
		this.changePriceFinal = changePriceFinal;
		this.changePercentage = changePercentage;
		this.changePercentageFinal = changePercentageFinal;
		this.chr = chr;
		this.previousClose = previousClose;
		this.createdAt = createdAt;
		this.updatedAt = updatedAt;
	}

	/**
	 * 
	 */
	public MarketData() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * @return the symbol
	 */
	public String getSymbol() {
		return symbol;
	}

	/**
	 * @param symbol
	 *            the symbol to set
	 */
	public void setSymbol(String symbol) {
		this.symbol = symbol;
	}

	/**
	 * @return the exchange
	 */
	public String getExchange() {
		return exchange;
	}

	/**
	 * @param exchange
	 *            the exchange to set
	 */
	public void setExchange(String exchange) {
		this.exchange = exchange;
	}

	/**
	 * @return the ltp
	 */
	public String getLtp() {
		return ltp;
	}

	/**
	 * @param ltp
	 *            the ltp to set
	 */
	public void setLtp(String ltp) {
		this.ltp = ltp;
	}

	/**
	 * @return the ltpFinal
	 */
	public String getLtpFinal() {
		return ltpFinal;
	}

	/**
	 * @param ltpFinal
	 *            the ltpFinal to set
	 */
	public void setLtpFinal(String ltpFinal) {
		this.ltpFinal = ltpFinal;
	}

	/**
	 * @return the lCur
	 */
	public String getlCur() {
		return lCur;
	}

	/**
	 * @param lCur
	 *            the lCur to set
	 */
	public void setlCur(String lCur) {
		this.lCur = lCur;
	}

	/**
	 * @return the s
	 */
	public String getS() {
		return s;
	}

	/**
	 * @param s
	 *            the s to set
	 */
	public void setS(String s) {
		this.s = s;
	}

	/**
	 * @return the lastTradedTime
	 */
	public String getLastTradedTime() {
		return lastTradedTime;
	}

	/**
	 * @param lastTradedTime
	 *            the lastTradedTime to set
	 */
	public void setLastTradedTime(String lastTradedTime) {
		this.lastTradedTime = lastTradedTime;
	}

	/**
	 * @return the lastTradeTimeFormatted
	 */
	public String getLastTradeTimeFormatted() {
		return lastTradeTimeFormatted;
	}

	/**
	 * @param lastTradeTimeFormatted
	 *            the lastTradeTimeFormatted to set
	 */
	public void setLastTradeTimeFormatted(String lastTradeTimeFormatted) {
		this.lastTradeTimeFormatted = lastTradeTimeFormatted;
	}

	/**
	 * @return the lastTradedDateTime
	 */
	public String getLastTradedDateTime() {
		return lastTradedDateTime;
	}

	/**
	 * @param lastTradedDateTime
	 *            the lastTradedDateTime to set
	 */
	public void setLastTradedDateTime(String lastTradedDateTime) {
		this.lastTradedDateTime = lastTradedDateTime;
	}

	/**
	 * @return the changePrice
	 */
	public String getChangePrice() {
		return changePrice;
	}

	/**
	 * @param changePrice
	 *            the changePrice to set
	 */
	public void setChangePrice(String changePrice) {
		this.changePrice = changePrice;
	}

	/**
	 * @return the changePriceFinal
	 */
	public String getChangePriceFinal() {
		return changePriceFinal;
	}

	/**
	 * @param changePriceFinal
	 *            the changePriceFinal to set
	 */
	public void setChangePriceFinal(String changePriceFinal) {
		this.changePriceFinal = changePriceFinal;
	}

	/**
	 * @return the changePercentage
	 */
	public String getChangePercentage() {
		return changePercentage;
	}

	/**
	 * @param changePercentage
	 *            the changePercentage to set
	 */
	public void setChangePercentage(String changePercentage) {
		this.changePercentage = changePercentage;
	}

	/**
	 * @return the changePercentageFinal
	 */
	public String getChangePercentageFinal() {
		return changePercentageFinal;
	}

	/**
	 * @param changePercentageFinal
	 *            the changePercentageFinal to set
	 */
	public void setChangePercentageFinal(String changePercentageFinal) {
		this.changePercentageFinal = changePercentageFinal;
	}

	/**
	 * @return the chr
	 */
	public String getChr() {
		return chr;
	}

	/**
	 * @param chr
	 *            the chr to set
	 */
	public void setChr(String chr) {
		this.chr = chr;
	}

	/**
	 * @return the previousClose
	 */
	public String getPreviousClose() {
		return previousClose;
	}

	/**
	 * @param previousClose
	 *            the previousClose to set
	 */
	public void setPreviousClose(String previousClose) {
		this.previousClose = previousClose;
	}

	/**
	 * @return the createdAt
	 */
	public Date getCreatedAt() {
		return createdAt;
	}

	/**
	 * @param createdAt
	 *            the createdAt to set
	 */
	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	/**
	 * @return the updatedAt
	 */
	public Date getUpdatedAt() {
		return updatedAt;
	}

	/**
	 * @param updatedAt
	 *            the updatedAt to set
	 */
	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}

}
