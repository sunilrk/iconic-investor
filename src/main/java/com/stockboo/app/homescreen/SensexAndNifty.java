/**
 * 
 */
package com.stockboo.app.homescreen;

import java.util.List;

/**
 * @author sunil.r
 *
 */
public class SensexAndNifty {

	private MarketData marketData = null;

	private List<NiftySensexPrices> chartData = null;

	/**
	 * @param marketData
	 * @param chartData
	 */
	public SensexAndNifty(MarketData marketData, List<NiftySensexPrices> chartData) {
		super();
		this.marketData = marketData;
		this.chartData = chartData;
	}

	/**
	 * 
	 */
	public SensexAndNifty() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @return the marketData
	 */
	public MarketData getMarketData() {
		return marketData;
	}

	/**
	 * @param marketData the marketData to set
	 */
	public void setMarketData(MarketData marketData) {
		this.marketData = marketData;
	}

	/**
	 * @return the chartData
	 */
	public List<NiftySensexPrices> getChartData() {
		return chartData;
	}

	/**
	 * @param chartData the chartData to set
	 */
	public void setChartData(List<NiftySensexPrices> chartData) {
		this.chartData = chartData;
	}

	
}
