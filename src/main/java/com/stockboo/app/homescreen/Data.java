package com.stockboo.app.homescreen;

import java.util.List;

public class Data {

	private List<Object> home_page = null;

	/**
	 * @return the home_page
	 */
	public List<Object> getHome_page() {
		return home_page;
	}

	/**
	 * @param home_page
	 *            the home_page to set
	 */
	public void setHome_page(List<Object> home_page) {
		this.home_page = home_page;
	}

}