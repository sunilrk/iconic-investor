package com.stockboo.app.service;

import com.stockboo.app.dao.ScriptDao;
import com.stockboo.app.domain.ScriptCodeDomain;

public class ScriptCodeService {
	
	public ScriptCodeDomain readScriptCode(String stockCode, Long scriptCodeId){
		ScriptDao scriptDao = new ScriptDao();
		return scriptDao.readScriptCode(stockCode, scriptCodeId);
	}

}
