package com.stockboo.app.configuration;

/**
 * Contains the client IDs and scopes for allowed clients consuming your API.
 */
public class Constants {
  public static final String WEB_CLIENT_ID = "703914324096-drubk915sshkgmcarbikisbjg1ju7kds.apps.googleusercontent.com";
  public static final String ANDROID_CLIENT_ID = "replace this with your Android client ID";
  public static final String IOS_CLIENT_ID = "703914324096-clss0rbdjds95fqtuuuci219tmmmd5fm.apps.googleusercontent.com";
  public static final String ANDROID_AUDIENCE = WEB_CLIENT_ID;

  public static final String EMAIL_SCOPE = "https://www.googleapis.com/auth/userinfo.email";
  
  //Authorization
  public static final String REFRESH_VALIDITY = "30";
  public static final String AUTH_TOKEN_VALIDITY = "90";
  public static final String AUTH_TOKEN = "X-StockBoo-Token";
  public static final String INSTALLATION_ID = "X-StockBoo-InstallationId";
  public static final int SCRIPTS_PAGING = 2000;
  
  //Home screen API's
  public static final String HIGH52_URL = "http://www.nseindia.com/products/dynaContent/equities/equities/json/online52NewHigh.json";
  public static final String LOW52_URL = "http://www.nseindia.com/products/dynaContent/equities/equities/json/online52NewLow.json";
  
  public static final String GAINERS1_URL = "http://www.nseindia.com/live_market/dynaContent/live_analysis/gainers/niftyGainers1.json";
  public static final String GAINERS2_URL = "http://www.nseindia.com/live_market/dynaContent/live_analysis/gainers/jrNiftyGainers1.json";
  public static final String LOOSERS1_URL = "http://www.nseindia.com/live_market/dynaContent/live_analysis/losers/niftyLosers1.json";
  public static final String LOOSERS2_URL = "http://www.nseindia.com/live_market/dynaContent/live_analysis/losers/jrNiftyLosers1.json";
  
  public static final String MOREVOLUME_GAINERS_URL = "https://www.nseindia.com/live_market/dynaContent/live_analysis/most_active/allTopVolume1.json";
  
  public static final String INDICES_URL = "https://www.nseindia.com/homepage/Indices1.json";
  
  public static final String HEADER_USER_AGENT = "User-Agent";
  public static final String HEADER_USER_AGENT_VALUE = "Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2228.0 Safari/537.36";
  public static final String NEWSFEEDS1_URL = "http://www.livemint.com/rss/money";
  public static final String NEWSFEEDS2_URL = "http://news.google.co.in/news?pz=1&cf=all&ned=in&hl=en&topic=b&output=rss";
  
  public static final int NUM_OF_REC_FOR_NEWS_FEEDS = 10;
  
  public final static String AUTH_KEY_FCM = "AAAAttHbopY:APA91bHNBCJW3iU0Pm65AS0TVBozO0ereADPUMQbIHUsEreLzh9MR73NGV3FSuNveGgTyw7maqd5DnAYSjhY3_8cbDtJMBQ63lro8huOGUF7e5da2xiUvSBII3ohWc4WovF4tj80k6CO";
  public final static String API_URL_FCM = "https://fcm.googleapis.com/fcm/send";
  public final static String MSG_TO_TOPIC_URL = "https://gcm-http.googleapis.com/gcm/send";

  public static final String CRYPTO_CURRENCY_URL = "https://api.coinmarketcap.com/v1/ticker/?convert=INR&limit=10";
  
}
