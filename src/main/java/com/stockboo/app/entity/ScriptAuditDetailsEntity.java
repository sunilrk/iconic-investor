/**
 * 
 */
package com.stockboo.app.entity;

import java.io.Serializable;
import java.util.Date;







/**
 * @author sunil.r
 *
 */

//@PersistenceCapable(identityType = IdentityType.APPLICATION)
public class ScriptAuditDetailsEntity extends AbstractStockbooEntity implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	//@PrimaryKey
	//@Persistent(valueStrategy = IdGeneratorStrategy.IDENTITY)
	private Long scriptAuditId;

	//@Persistent
	private Date nseCreated;

	//@Persistent
	private Date nseUpdatedAt;
	
	//@Persistent
	private Date bseCreatedAt;

	//@Persistent
	private Date bseUpdatedAt;
	
	//@Persistent
	private Date lastCreated;

	//@Persistent
	private Date lastUpdated;
	
	public ScriptAuditDetailsEntity() {
		super(null);
	}
	
	public ScriptAuditDetailsEntity(com.google.appengine.api.datastore.Entity entity) {
		super(entity);
		this.scriptAuditId = ((Long) entity.getKey().getId());
		this.nseCreated = (Date) entity.getProperty("nseCreated");
		this.nseUpdatedAt = (Date) entity.getProperty("nseUpdatedAt");
		this.bseCreatedAt = (Date) entity.getProperty("bseCreatedAt");
		this.bseUpdatedAt = (Date) entity.getProperty("bseUpdatedAt");
		this.lastCreated = (Date) entity.getProperty("lastCreated");
		this.lastUpdated = (Date) entity.getProperty("lastUpdated");
	}
	
	

	/**
	 * @param nseCreated
	 * @param nseUpdatedAt
	 * @param bseCreatedAt
	 * @param bseUpdatedAt
	 */
	public ScriptAuditDetailsEntity(Date nseCreated, Date nseUpdatedAt, Date bseCreatedAt, Date bseUpdatedAt) {
		super(null);
		this.nseCreated = nseCreated;
		this.nseUpdatedAt = nseUpdatedAt;
		this.bseCreatedAt = bseCreatedAt;
		this.bseUpdatedAt = bseUpdatedAt;
	}


	/**
	 * @return the scriptAuditId
	 */
	public Long getScriptAuditId() {
		return scriptAuditId;
	}

	/**
	 * @param scriptAuditId the scriptAuditId to set
	 */
	public void setScriptAuditId(Long scriptAuditId) {
		this.scriptAuditId = scriptAuditId;
	}

	/**
	 * @return the nseCreated
	 */
	public Date getNseCreated() {
		return nseCreated;
	}

	/**
	 * @param nseCreated the nseCreated to set
	 */
	public void setNseCreated(Date nseCreated) {
		this.nseCreated = nseCreated;
	}

	/**
	 * @return the nseUpdatedAt
	 */
	public Date getNseUpdatedAt() {
		return nseUpdatedAt;
	}

	/**
	 * @param nseUpdatedAt the nseUpdatedAt to set
	 */
	public void setNseUpdatedAt(Date nseUpdatedAt) {
		this.nseUpdatedAt = nseUpdatedAt;
	}

	/**
	 * @return the bseCreatedAt
	 */
	public Date getBseCreatedAt() {
		return bseCreatedAt;
	}

	/**
	 * @param bseCreatedAt the bseCreatedAt to set
	 */
	public void setBseCreatedAt(Date bseCreatedAt) {
		this.bseCreatedAt = bseCreatedAt;
	}

	/**
	 * @return the bseUpdatedAt
	 */
	public Date getBseUpdatedAt() {
		return bseUpdatedAt;
	}

	/**
	 * @param bseUpdatedAt the bseUpdatedAt to set
	 */
	public void setBseUpdatedAt(Date bseUpdatedAt) {
		this.bseUpdatedAt = bseUpdatedAt;
	}

	/**
	 * @return the lastCreated
	 */
	public Date getLastCreated() {
		return lastCreated;
	}

	/**
	 * @param lastCreated the lastCreated to set
	 */
	public void setLastCreated(Date lastCreated) {
		this.lastCreated = lastCreated;
	}

	/**
	 * @return the lastUpdated
	 */
	public Date getLastUpdated() {
		return lastUpdated;
	}

	/**
	 * @param lastUpdated the lastUpdated to set
	 */
	public void setLastUpdated(Date lastUpdated) {
		this.lastUpdated = lastUpdated;
	}
	
	@Override
	public com.google.appengine.api.datastore.Entity buildEntity() {
		com.google.appengine.api.datastore.Entity entity = createEntity(Nifty.TABLE_NAME);

		entity.setProperty("nseCreated", getNseCreated());
		entity.setProperty("nseUpdatedAt", getNseUpdatedAt());
		entity.setProperty("bseCreatedAt", getBseCreatedAt());
		entity.setProperty("bseUpdatedAt", getBseUpdatedAt());
		entity.setProperty("lastCreated", getLastCreated());
		entity.setProperty("lastUpdated", getLastUpdated());
		
		System.out.println(entity.getKey().getId());
		System.out.println(entity.getKey());
		System.out.println(entity.getKey().getKind());
		return entity;
	}

}
