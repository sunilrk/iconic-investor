/**
 * 
 */
package com.stockboo.app.entity;

import java.util.Date;

import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Email;
import com.googlecode.objectify.ObjectifyService;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;

/**
 * @author jayavardhan
 *
 */
@Entity
public class AdvisorEntity extends AbstractStockbooEntity{

	public static final String TABLE_NAME = "AdvisorEntity";

	@Id
	private Long advisorId;

	private String name;

	private String info;

	private boolean isSuperAdmin;

	private boolean isAdvisorBlocked;

	private Email email;

	private String mobileNumber;

	private String password;

	private Date createdAt;

	private Date updatedAt;
	
	private Date lastLogin;

	public AdvisorEntity() {

		super(null);
	}

	public AdvisorEntity(String name, String info, boolean superAdminFlag, boolean advisorBlockedFlag, Email email, String mobileNumber, String password,
			Date advisorCreatedAt, Date lastLogin) {
		super(null);
		this.name = name;
		this.info = info;
		this.isSuperAdmin = superAdminFlag;
		this.isAdvisorBlocked = advisorBlockedFlag;
		this.email = email;
		this.mobileNumber = mobileNumber;
		this.password = password;
		this.createdAt = advisorCreatedAt;
		this.lastLogin = lastLogin;
	}
	
	public AdvisorEntity(com.google.appengine.api.datastore.Entity entity){
		super(entity);
		advisorId = (Long) entity.getKey().getId();
		name = (String) entity.getProperty("name");
		info = (String) entity.getProperty("info");
		isSuperAdmin = (boolean) entity.getProperty("isSuperAdmin");
		isAdvisorBlocked = (boolean) entity.getProperty("isAdvisorBlocked");
		email = (Email) entity.getProperty("email");
		mobileNumber = (String) entity.getProperty("mobileNumber");
		password = (String) entity.getProperty("password");
		System.out.println("datebjjjj:"+entity.getProperty("createdAt"));
		createdAt = (Date)entity.getProperty("createdAt");
		updatedAt = (Date) entity.getProperty("updatedAt");
		lastLogin = (Date) entity.getProperty("lastLogin");
	}

	/**
	 * @return the advisorId
	 */
	public Long getAdvisorId() {
		return advisorId;
	}

	/**
	 * @param advisorId
	 *            the advisorId to set
	 */
	public void setAdvisorId(Long advisorId) {
		this.advisorId = advisorId;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name
	 *            the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the isSuperAdmin
	 */
	public boolean isSuperAdmin() {
		return isSuperAdmin;
	}

	/**
	 * @param isSuperAdmin
	 *            the isSuperAdmin to set
	 */
	public void setSuperAdmin(boolean isSuperAdmin) {
		this.isSuperAdmin = isSuperAdmin;
	}

	/**
	 * @return the isAdvisorBlocked
	 */
	public boolean isAdvisorBlocked() {
		return isAdvisorBlocked;
	}

	/**
	 * @param isAdvisorBlocked
	 *            the isAdvisorBlocked to set
	 */
	public void setAdvisorBlocked(boolean isAdvisorBlocked) {
		this.isAdvisorBlocked = isAdvisorBlocked;
	}

	/**
	 * @return the info
	 */
	public String getInfo() {
		return info;
	}

	/**
	 * @param info
	 *            the info to set
	 */
	public void setInfo(String info) {
		this.info = info;
	}

	/**
	 * @return the createdAt
	 */
	public Date getCreatedAt() {	
		return createdAt;
	}

	/**
	 * @param createdAt
	 *            the createdAt to set
	 */
	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	/**
	 * @return the updatedAt
	 */
	public Date getUpdatedAt() {
		return updatedAt;
	}

	/**
	 * @param updatedAt
	 *            the updatedAt to set
	 */
	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}

	/**
	 * @return the email
	 */
	public Email getEmail() {
		return email;
	}

	/**
	 * @param email
	 *            the email to set
	 */
	public void setEmail(Email email) {
		this.email = email;
	}

	/**
	 * @return the mobileNumber
	 */
	public String getMobileNumber() {
		return mobileNumber;
	}

	/**
	 * @param mobileNumber
	 *            the mobileNumber to set
	 */
	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * @param password
	 *            the password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	/**
	 * @return the lastLogin
	 */
	public Date getLastLogin() {
		return lastLogin;
	}

	/**
	 * @param lastLogin the lastLogin to set
	 */
	public void setLastLogin(Date lastLogin) {
		this.lastLogin = lastLogin;
	}
	
	public com.google.appengine.api.datastore.Entity create() {
		ObjectifyService.register(AdvisorEntity.class);
		DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
		
		com.google.appengine.api.datastore.Entity advisorEntity = null;
		if(null != getAdvisorId()){
			advisorEntity = new com.google.appengine.api.datastore.Entity(
					"AdvisorEntity", getAdvisorId()); 
			System.out.println("update"+getAdvisorId());
		}else{
			advisorEntity = new com.google.appengine.api.datastore.Entity(
					"AdvisorEntity"); 
		}
		advisorEntity.setProperty("name", getName());
		advisorEntity.setProperty("info", getInfo());
		advisorEntity.setProperty("isSuperAdmin", isSuperAdmin());
		advisorEntity.setProperty("isAdvisorBlocked", isAdvisorBlocked());
		advisorEntity.setProperty("email", getEmail());
		advisorEntity.setProperty("mobileNumber", getMobileNumber());
		advisorEntity.setProperty("password", getPassword());
		advisorEntity.setProperty("createdAt", getCreatedAt());
		advisorEntity.setProperty("lastLogin", getLastLogin());
		advisorEntity.setProperty("updatedAt", getUpdatedAt());
		
		datastore.put(advisorEntity);
		setAdvisorId(advisorEntity.getKey().getId());
		System.out.println(advisorEntity.getKey().getId());
		System.out.println(advisorEntity.getKey());
		System.out.println(advisorEntity.getKey().getKind());
		return advisorEntity;
	}

	@Override
	public com.google.appengine.api.datastore.Entity buildEntity() {
		setId(advisorId);
		com.google.appengine.api.datastore.Entity advisorEntity = createEntity(TABLE_NAME);
		advisorEntity.setProperty("name", getName());
		advisorEntity.setProperty("info", getInfo());
		advisorEntity.setProperty("isSuperAdmin", isSuperAdmin());
		advisorEntity.setProperty("isAdvisorBlocked", isAdvisorBlocked());
		advisorEntity.setProperty("email", getEmail());
		advisorEntity.setProperty("mobileNumber", getMobileNumber());
		advisorEntity.setProperty("password", getPassword());
		advisorEntity.setProperty("createdAt", getCreatedAt());
		advisorEntity.setProperty("lastLogin", getLastLogin());
		advisorEntity.setProperty("updatedAt", getUpdatedAt());
		System.out.println(advisorEntity.getKey().getId());
		System.out.println(advisorEntity.getKey());
		System.out.println(advisorEntity.getKey().getKind());
		return advisorEntity;
	}


	public void update(AdvisorEntity advisor)
	{
		this.name = getActualValue(this.name, advisor.getName());
		this.info = getActualValue(this.info, advisor.getInfo());
		this.email = new Email(getActualValue(email.getEmail() != null ? this.email.getEmail().toString(): null, advisor.getEmail().getEmail().toString()));
		this.mobileNumber = getActualValue(this.mobileNumber, advisor.getMobileNumber());
		this.password = getActualValue(this.password, advisor.getPassword());
		this.updatedAt = new Date();
	}
	

}
