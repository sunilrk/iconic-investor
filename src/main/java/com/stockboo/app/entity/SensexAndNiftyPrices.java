package com.stockboo.app.entity;

import java.io.Serializable;
import java.util.Date;








/**
 * Get the data from below URL and this is used in CRON job
 * http://finance.google.com/finance/info?client=ig&q=INDEXBOM:SENSEX,NSE:NIFTY
 * 
 * This saves needed data from sensex and nifty
 * 
 * @author sunil.r
 *
 */

public class SensexAndNiftyPrices extends AbstractStockbooEntity implements Serializable {
	
	public static final String TABLE_NAME = "SensexAndNiftyPrices";

	private static final long serialVersionUID = 1L;

	private String niftyPrice;

	private String sensexPrice;

	private Date createdAt;

	/**
	 * @param niftyPrice
	 * @param sensexPrice
	 * @param createdAt
	 */
	public SensexAndNiftyPrices(String niftyPrice, String sensexPrice, Date createdAt) {
		super(null);
		this.niftyPrice = niftyPrice;
		this.sensexPrice = sensexPrice;
		this.createdAt = createdAt;
	}
	
	public SensexAndNiftyPrices(com.google.appengine.api.datastore.Entity entity) {
		super(entity);
		setId(entity.getKey().getId());
		this.niftyPrice = (String) entity.getProperty("niftyPrice");
		this.sensexPrice = (String) entity.getProperty("sensexPrice");
		this.createdAt = (Date) entity.getProperty("createdAt");
	}

	/**
	 * @return the niftyPrice
	 */
	public String getNiftyPrice() {
		return niftyPrice;
	}

	/**
	 * @param niftyPrice
	 *            the niftyPrice to set
	 */
	public void setNiftyPrice(String niftyPrice) {
		this.niftyPrice = niftyPrice;
	}

	/**
	 * @return the sensexPrice
	 */
	public String getSensexPrice() {
		return sensexPrice;
	}

	/**
	 * @param sensexPrice
	 *            the sensexPrice to set
	 */
	public void setSensexPrice(String sensexPrice) {
		this.sensexPrice = sensexPrice;
	}

	/**
	 * @return the createdAt
	 */
	public Date getCreatedAt() {
		return createdAt;
	}

	/**
	 * @param createdAt
	 *            the createdAt to set
	 */
	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}
	
	@Override
	public com.google.appengine.api.datastore.Entity buildEntity() {
		com.google.appengine.api.datastore.Entity entity = createEntity(SensexAndNiftyPrices.TABLE_NAME);
		
		entity.setProperty("niftyPrice", getNiftyPrice());
		entity.setProperty("sensexPrice", getSensexPrice());
		entity.setProperty("createdAt", getCreatedAt());
		
		System.out.println(entity.getKey().getId());
		System.out.println(entity.getKey());
		System.out.println(entity.getKey().getKind());
		return entity;
	}	

}
