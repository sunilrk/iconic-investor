package com.stockboo.app.entity;

import java.io.Serializable;
import java.util.Date;







//import javax.jdo.annotations.Unique;
import javax.validation.constraints.NotNull;

/**
 * @author sunil.r
 *
 */
//@PersistenceCapable(identityType = IdentityType.APPLICATION)
public class ScriptCodeEntity extends AbstractStockbooEntity implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	//@PrimaryKey
	//@Persistent(valueStrategy = IdGeneratorStrategy.IDENTITY)
	//@Index
	private Long scriptCodeId;
	
	//@Persistent
	private String nseCode;
	
	//@Persistent
	private String bseCode;
	
	//@Persistent
	private String companyName;
	
	//@Persistent
	private String group;

	//@Persistent
	private String faceValue;
	
//	@Unique
	@NotNull
	//@Persistent
	private String iSIN;
	
	//@Persistent
	private String industy;
	
	//@Persistent
	private String firstListingDate;
	
	//@Persistent
	private String status;
	
	//@Persistent
	private Date created;

	//@Persistent
	private Date updatedAt;
	
	public ScriptCodeEntity(com.google.appengine.api.datastore.Entity entity) {
		super(entity);
		this.scriptCodeId = ((Long) entity.getKey().getId());
		this.nseCode = (String) entity.getProperty("nseCode");
		this.bseCode = (String) entity.getProperty("bseCode");
		this.companyName = (String) entity.getProperty("companyName");
		this.group = (String) entity.getProperty("group");
		this.faceValue = (String) entity.getProperty("faceValue");
		this.iSIN = (String) entity.getProperty("iSIN");
		this.industy = (String) entity.getProperty("industy");
		this.firstListingDate = (String) entity.getProperty("firstListingDate");
		this.status = (String) entity.getProperty("status");
		this.created = (Date) entity.getProperty("created");
		this.updatedAt = (Date) entity.getProperty("updatedAt");
	}
	
	public ScriptCodeEntity() {
		super(null);
	}

	/**
	 * @return the nseCode
	 */
	public String getNseCode() {
		return nseCode;
	}

	/**
	 * @param nseCode the nseCode to set
	 */
	public void setNseCode(String nseCode) {
		this.nseCode = nseCode;
	}

	/**
	 * @return the bseCode
	 */
	public String getBseCode() {
		return bseCode;
	}

	/**
	 * @param bseCode the bseCode to set
	 */
	public void setBseCode(String bseCode) {
		this.bseCode = bseCode;
	}

	/**
	 * @return the companyName
	 */
	public String getCompanyName() {
		return companyName;
	}

	/**
	 * @param companyName the companyName to set
	 */
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	/**
	 * @return the group
	 */
	public String getGroup() {
		return group;
	}

	/**
	 * @param group the group to set
	 */
	public void setGroup(String group) {
		this.group = group;
	}

	/**
	 * @return the faceValue
	 */
	public String getFaceValue() {
		return faceValue;
	}

	/**
	 * @param faceValue the faceValue to set
	 */
	public void setFaceValue(String faceValue) {
		this.faceValue = faceValue;
	}

	/**
	 * @return the iSIN
	 */
	public String getiSIN() {
		return iSIN;
	}

	/**
	 * @param iSIN the iSIN to set
	 */
	public void setiSIN(String iSIN) {
		this.iSIN = iSIN;
	}

	/**
	 * @return the industy
	 */
	public String getIndusty() {
		return industy;
	}

	/**
	 * @param industy the industy to set
	 */
	public void setIndusty(String industy) {
		this.industy = industy;
	}

	/**
	 * @return the firstListingDate
	 */
	public String getFirstListingDate() {
		return firstListingDate;
	}

	/**
	 * @param firstListingDate the firstListingDate to set
	 */
	public void setFirstListingDate(String firstListingDate) {
		this.firstListingDate = firstListingDate;
	}

	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * @param status the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * @return the scriptCodeId
	 */
	public Long getScriptCodeId() {
		return scriptCodeId;
	}

	/**
	 * @return the created
	 */
	public Date getCreated() {
		return created;
	}

	/**
	 * @param scriptCodeId the scriptCodeId to set
	 */
	public void setScriptCodeId(Long scriptCodeId) {
		this.scriptCodeId = scriptCodeId;
	}

	/**
	 * @param created the created to set
	 */
	public void setCreated(Date created) {
		this.created = created;
	}

	/**
	 * @return the updatedAt
	 */
	public Date getUpdatedAt() {
		return updatedAt;
	}

	/**
	 * @param updatedAt the updatedAt to set
	 */
	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}
	
	@Override
	public com.google.appengine.api.datastore.Entity buildEntity() {
		com.google.appengine.api.datastore.Entity entity = createEntity(Nifty.TABLE_NAME);

		entity.setProperty("nseCode", getNseCode());
		entity.setProperty("bseCode", getBseCode());
		entity.setProperty("companyName", getCompanyName());
		entity.setProperty("group", getGroup());
		entity.setProperty("faceValue", getFaceValue());
		entity.setProperty("iSIN", getiSIN());
		entity.setProperty("industy", getIndusty());
		entity.setProperty("firstListingDate", getFirstListingDate());
		entity.setProperty("status", getStatus());
		entity.setProperty("created", getCreated());
		entity.setProperty("updatedAt", getUpdatedAt());
		
		System.out.println(entity.getKey().getId());
		System.out.println(entity.getKey());
		System.out.println(entity.getKey().getKind());
		return entity;
	}	
	
}
