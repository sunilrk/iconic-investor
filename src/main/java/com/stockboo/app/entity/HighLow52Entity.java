package com.stockboo.app.entity;

import java.util.Date;






import com.google.appengine.api.datastore.Text;

public class HighLow52Entity extends AbstractHomePageEntity {
	public static final String TABLE_NAME = "HighLow52Entity";

	private String type;
	private Text fullDetails;
	private Text topThree;
	private Date createdAt;
	private Date updatedAt;
	
	public HighLow52Entity(com.google.appengine.api.datastore.Entity entity) {
		super(entity);
		setTableId(entity.getKey().getName());
		this.type = entity.getKey().getName();
		this.fullDetails = (Text) entity.getProperty("symbol");
		this.topThree = (Text) entity.getProperty("exchange");
		this.createdAt = (Date) entity.getProperty("ltp");
		this.updatedAt = (Date) entity.getProperty("ltpFinal");
	}


	/**
	 * 
	 */
	public HighLow52Entity() {
		super(null);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param type
	 * @param fullDetails
	 * @param topThree
	 * @param createdAt
	 * @param updatedAt
	 */
	public HighLow52Entity(String type, Text fullDetails, Text topThree, Date createdAt, Date updatedAt) {
		super(null);
		this.type = type;
		this.fullDetails = fullDetails;
		this.topThree = topThree;
		this.createdAt = createdAt;
		this.updatedAt = updatedAt;
	}

	/**
	 * @return the type
	 */
	public String getType() {
		return type;
	}

	/**
	 * @param type
	 *            the type to set
	 */
	public void setType(String type) {
		this.type = type;
	}

	/**
	 * @return the fullDetails
	 */
	public Text getFullDetails() {
		return fullDetails;
	}

	/**
	 * @param fullDetails
	 *            the fullDetails to set
	 */
	public void setFullDetails(Text fullDetails) {
		this.fullDetails = fullDetails;
	}

	/**
	 * @return the topThree
	 */
	public Text getTopThree() {
		return topThree;
	}

	/**
	 * @param topThree
	 *            the topThree to set
	 */
	public void setTopThree(Text topThree) {
		this.topThree = topThree;
	}

	/**
	 * @return the createdAt
	 */
	public Date getCreatedAt() {
		return createdAt;
	}

	/**
	 * @param createdAt
	 *            the createdAt to set
	 */
	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	/**
	 * @return the updatedAt
	 */
	public Date getUpdatedAt() {
		return updatedAt;
	}

	/**
	 * @param updatedAt
	 *            the updatedAt to set
	 */
	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}
	
	@Override
	public com.google.appengine.api.datastore.Entity buildEntity() {
		setTableId(getType());
		com.google.appengine.api.datastore.Entity entity = createEntity(HighLow52Entity.TABLE_NAME);

		entity.setProperty("fullDetails", getFullDetails());
		entity.setProperty("topThree", getTopThree());
		entity.setProperty("createdAt", getCreatedAt());
		entity.setProperty("updatedAt", getUpdatedAt());
		
		System.out.println(entity.getKey().getId());
		System.out.println(entity.getKey());
		System.out.println(entity.getKey().getKind());
		return entity;
	}

}