package com.stockboo.app.entity;

import java.util.Date;







import com.google.appengine.api.datastore.Text;

//@PersistenceCapable(identityType = IdentityType.APPLICATION)
public class CryptoCurrencyEntity extends AbstractStockbooEntity{
	public static final String TABLE_NAME = "CryptoCurrencyEntity";

	//@PrimaryKey
	//@Persistent(valueStrategy = IdGeneratorStrategy.IDENTITY)
	private Long cryptoId;
	//@Persistent
	private Text fullDetails;
	//@Persistent
	private Text topThree;
	//@Persistent
	private Date createdAt;
	
	public CryptoCurrencyEntity(com.google.appengine.api.datastore.Entity entity) {
		super(entity);
		this.cryptoId = entity.getKey().getId();
		this.fullDetails= (Text) entity.getProperty("colourcode");
		this.topThree = (Text) entity.getProperty("colourcode");
		this.createdAt = (Date) entity.getProperty("colourcode");
	}
	
	public CryptoCurrencyEntity() {
		super(null);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @return the createdAt
	 */
	public Date getCreatedAt() {
		return createdAt;
	}

	/**
	 * @param createdAt
	 *            the createdAt to set
	 */
	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	/**
	 * @return the topThree
	 */
	public Text getTopThree() {
		return topThree;
	}

	/**
	 * @param topThree
	 *            the topThree to set
	 */
	public void setTopThree(Text topThree) {
		this.topThree = topThree;
	}

	/**
	 * @return the fullDetails
	 */
	public Text getFullDetails() {
		return fullDetails;
	}

	/**
	 * @param fullDetails
	 *            the fullDetails to set
	 */
	public void setFullDetails(Text fullDetails) {
		this.fullDetails = fullDetails;
	}

	public Long getCryptoId() {
		return cryptoId;
	}

	public void setCryptoId(Long cryptoId) {
		this.cryptoId = cryptoId;
	}
	
	@Override
	public com.google.appengine.api.datastore.Entity buildEntity() {
		setId(getCryptoId());
		com.google.appengine.api.datastore.Entity entity = createEntity(CryptoCurrencyEntity.TABLE_NAME);
		entity.setProperty("fullDetails", getFullDetails());
		entity.setProperty("topThree", getTopThree());
		entity.setProperty("createdAt", getCreatedAt());
		
		System.out.println(entity.getKey().getId());
		System.out.println(entity.getKey());
		System.out.println(entity.getKey().getKind());
		return entity;
	}
}