
package com.stockboo.app.entity;

import java.io.Serializable;
import java.util.Date;






/**
 * Get the data from below URL and this is used in CRON job
 * http://finance.google.com/finance/info?client=ig&q=INDEXBOM:SENSEX,NSE:NIFTY
 * 
 * @author sunil.r
 *
 */

public class Nifty extends AbstractHomePageEntity implements Serializable{
	
	public static final String TABLE_NAME = "Nifty";

	private static final long serialVersionUID = 1L;
	
	private String symbol;
	private String exchange;
	private String ltp;
	private String ltpFinal;
	private String lCur;
	private String s;
	private String lastTradedTime;
	private String lastTradeTimeFormatted;
	private String lastTradedDateTime;
	private String changePrice;
	private String changePriceFinal;
	private String changePercentage;
	private String changePercentageFinal;
	private String chr;
	private String previousClose;
	private Date createdAt;
	private Date updatedAt;

	/**
	 * 
	 */
	public Nifty() {
		super(null);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param id
	 * @param t
	 * @param e
	 * @param l
	 * @param lFix
	 * @param lCur
	 * @param s
	 * @param ltt
	 * @param lt
	 * @param ltDts
	 * @param c
	 * @param cFix
	 * @param cp
	 * @param cpFix
	 * @param ccol
	 * @param pclsFix
	 */
	public Nifty(String id, String t, String e, String l, String lFix, String lCur, String s, String ltt, String lt,
			String ltDts, String c, String cFix, String cp, String cpFix, String ccol, String pclsFix) {
		super(null);
		setId(Long.parseLong(id));
		this.symbol = t;
		this.exchange = e;
		this.ltp = l;
		this.ltpFinal = lFix;
		this.lCur = lCur;
		this.s = s;
		this.lastTradedTime = ltt;
		this.lastTradeTimeFormatted = lt;
		this.lastTradedDateTime = ltDts;
		this.changePrice = c;
		this.changePriceFinal = cFix;
		this.changePercentage = cp;
		this.changePercentageFinal = cpFix;
		this.chr = ccol;
		this.previousClose = pclsFix;
	}
	
	
	public Nifty(com.google.appengine.api.datastore.Entity entity) {
		super(entity);
		setId((Long) entity.getKey().getId());
		this.symbol = (String) entity.getProperty("symbol");
		this.exchange = (String) entity.getProperty("exchange");
		this.ltp = (String) entity.getProperty("ltp");
		this.ltpFinal = (String) entity.getProperty("ltpFinal");
		this.lCur = (String) entity.getProperty("lCur");
		this.s = (String) entity.getProperty("s");
		this.lastTradedTime = (String) entity.getProperty("lastTradedTime");
		this.lastTradeTimeFormatted = (String) entity.getProperty("lastTradeTimeFormatted");
		this.lastTradedDateTime = (String) entity.getProperty("lastTradedDateTime");
		this.changePrice = (String) entity.getProperty("changePrice");
		this.changePriceFinal = (String) entity.getProperty("changePriceFinal");
		this.changePercentage = (String) entity.getProperty("changePercentage");
		this.changePercentageFinal = (String) entity.getProperty("changePercentageFinal");
		this.chr = (String) entity.getProperty("chr");
		this.previousClose = (String) entity.getProperty("previousClose");
		this.createdAt = (Date) entity.getProperty("createdAt");
		this.updatedAt = (Date) entity.getProperty("updatedAt");
		
	}
	
	public String getSymbol() {
		return symbol;
	}

	public void setSymbol(String symbol) {
		this.symbol = symbol;
	}

	public String getExchange() {
		return exchange;
	}

	public void setExchange(String exchange) {
		this.exchange = exchange;
	}

	public String getLtp() {
		return ltp;
	}

	public void setLtp(String ltp) {
		this.ltp = ltp;
	}

	public String getLtpFinal() {
		return ltpFinal;
	}

	public void setLtpFinal(String ltpFinal) {
		this.ltpFinal = ltpFinal;
	}

	public String getLCur() {
		return lCur;
	}

	public void setLCur(String lCur) {
		this.lCur = lCur;
	}

	public String getS() {
		return s;
	}

	public void setS(String s) {
		this.s = s;
	}

	public String getLastTradedTime() {
		return lastTradedTime;
	}

	public void setLastTradedTime(String lastTradedTime) {
		this.lastTradedTime = lastTradedTime;
	}

	public String getLastTradeTimeFormatted() {
		return lastTradeTimeFormatted;
	}

	public void setLastTradeTimeFormatted(String lastTradeTimeFormatted) {
		this.lastTradeTimeFormatted = lastTradeTimeFormatted;
	}

	public String getLastTradedDateTime() {
		return lastTradedDateTime;
	}

	public void setLastTradedDateTime(String lastTradedDateTime) {
		this.lastTradedDateTime = lastTradedDateTime;
	}

	public String getChangePrice() {
		return changePrice;
	}

	public void setChangePrice(String changePrice) {
		this.changePrice = changePrice;
	}

	public String getChangePriceFinal() {
		return changePriceFinal;
	}

	public void setChangePriceFinal(String changePriceFinal) {
		this.changePriceFinal = changePriceFinal;
	}

	public String getChangePercentage() {
		return changePercentage;
	}

	public void setChangePercentage(String changePercentage) {
		this.changePercentage = changePercentage;
	}

	public String getChangePercentageFinal() {
		return changePercentageFinal;
	}

	public void setChangePercentageFinal(String changePercentageFinal) {
		this.changePercentageFinal = changePercentageFinal;
	}

	public String getChr() {
		return chr;
	}

	public void setChr(String chr) {
		this.chr = chr;
	}

	public String getPreviousClose() {
		return previousClose;
	}

	public void setPreviousClose(String previousClose) {
		this.previousClose = previousClose;
	}

	/**
	 * @return the createdAt
	 */
	public Date getCreatedAt() {
		return createdAt;
	}

	/**
	 * @param createdAt
	 *            the createdAt to set
	 */
	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	/**
	 * @return the updatedAt
	 */
	public Date getUpdatedAt() {
		return updatedAt;
	}

	/**
	 * @param updatedAt
	 *            the updatedAt to set
	 */
	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}

	@Override
	public com.google.appengine.api.datastore.Entity buildEntity() {
		setTableId("NIFTY");
		com.google.appengine.api.datastore.Entity entity = createEntity(Nifty.TABLE_NAME);

		entity.setProperty("id", null);
		entity.setProperty("exchange", getExchange());
		entity.setProperty("ltp", getLtp());
		entity.setProperty("ltpFinal", getLtpFinal());
		entity.setProperty("lCur", getLCur());
		entity.setProperty("s", getS());
		entity.setProperty("lastTradedTime",getLastTradedTime());
		entity.setProperty("lastTradeTimeFormatted", getLastTradeTimeFormatted());
		entity.setProperty("lastTradedDateTime", getLastTradedDateTime());
		entity.setProperty("changePrice", getChangePrice());
		entity.setProperty("changePriceFinal", getChangePriceFinal());
		entity.setProperty("changePercentage", getChangePercentage());
		entity.setProperty("changePercentageFinal", getChangePercentageFinal());
		entity.setProperty("chr", getChr());
		entity.setProperty("previousClose", getPreviousClose());
		entity.setProperty("createdAt", getCreatedAt());
		entity.setProperty("updatedAt", getUpdatedAt());
		
		System.out.println(entity.getKey().getId());
		System.out.println(entity.getKey());
		System.out.println(entity.getKey().getKind());
		return entity;
	}	
}