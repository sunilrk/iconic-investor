
package com.stockboo.app.entity;

/**
 * @author sunil.r
 *
 */

public class ColourCodingEntity extends AbstractHomePageEntity{
	public static final String TABLE_NAME = "ColourCodingEntity";

	private String screen;

	private String colourcode;
	
	

	public ColourCodingEntity(com.google.appengine.api.datastore.Entity entity) {
		super(entity);
		setTableId(entity.getKey().getName());
		this.screen = entity.getKey().getName();
		this.colourcode = (String) entity.getProperty("colourcode");
	}


	public ColourCodingEntity() {
		super(null);
		// TODO Auto-generated constructor stub
	}


	/**
	 * @return the screen
	 */
	public String getScreen() {
		return screen;
	}

	/**
	 * @param screen
	 *            the screen to set
	 */
	public void setScreen(String screen) {
		this.screen = screen;
	}

	/**
	 * @return the colourcode
	 */
	public String getColourcode() {
		return colourcode;
	}

	/**
	 * @param colourcode
	 *            the colourcode to set
	 */
	public void setColourcode(String colourcode) {
		this.colourcode = colourcode;
	}
	
	@Override
	public com.google.appengine.api.datastore.Entity buildEntity() {
		setTableId(getScreen());
		com.google.appengine.api.datastore.Entity entity = createEntity(ColourCodingEntity.TABLE_NAME);

		entity.setProperty("colourcode", getColourcode());
		
		System.out.println(entity.getKey().getId());
		System.out.println(entity.getKey());
		System.out.println(entity.getKey().getKind());
		return entity;
	}

}
