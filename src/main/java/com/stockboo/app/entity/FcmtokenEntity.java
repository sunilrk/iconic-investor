package com.stockboo.app.entity;

import java.util.Date;

import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;

/**
 * 
 * @author sunil.r
 *
 */
@Entity
public class FcmtokenEntity extends AbstractStockbooEntity{
	
	public static final String TABLE_NAME = "FcmtokenEntity";

	@Id
	Long installationId;

	String fcmToken;

	private Date created;
	
	public FcmtokenEntity(com.google.appengine.api.datastore.Entity entity) {
		super(entity);
		installationId = (Long) entity.getKey().getId();
		fcmToken = (String) entity.getProperty("fcmToken");
		created = (Date) entity.getProperty("created");
	}

	public FcmtokenEntity() {
		super(null);
	}

	public Long getInstallationId() {
		return installationId;
	}

	public void setInstallationId(Long installationId) {
		this.installationId = installationId;
	}

	public String getFcmToken() {
		return fcmToken;
	}

	public void setFcmToken(String fcmToken) {
		this.fcmToken = fcmToken;
	}

	public Date getCreated() {
		return created;
	}

	public void setCreated(Date created) {
		this.created = created;
	}
	
	/*public com.google.appengine.api.datastore.Entity create() {
		setId(getInstallationId());
		ObjectifyService.register(FcmtokenEntity.class);
		DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
		
		com.google.appengine.api.datastore.Entity fcmEntity = new com.google.appengine.api.datastore.Entity(
				"FcmtokenEntity", getInstallationId()); 
		fcmEntity.setProperty("fcmToken", getFcmToken());
		fcmEntity.setProperty("created", new Date());
		
		datastore.put(fcmEntity);
		return fcmEntity;
	}
	*/
	@Override
	public com.google.appengine.api.datastore.Entity buildEntity() {
		setId(getInstallationId());
		com.google.appengine.api.datastore.Entity installEntity = createEntity(TABLE_NAME);
		installEntity.setProperty("fcmToken", getFcmToken());
		installEntity.setProperty("created", getCreated());
		System.out.println(installEntity.getKey().getId());
		System.out.println(installEntity.getKey());
		System.out.println(installEntity.getKey().getKind());
		return installEntity;
	}

}
