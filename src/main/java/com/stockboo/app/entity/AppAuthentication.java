package com.stockboo.app.entity;

import java.util.Date;

import com.googlecode.objectify.annotation.Entity;


/**
 * DTO for creating UUID 
 * 
 * @author sunil.r
 *
 */
@Entity
public class AppAuthentication extends AbstractStockbooEntity{
	
	public static final String TABLE_NAME = "AppAuthentication";
	
	Long installationId;
	
	String token;
	
	Date tokenCreatedAt;
	
	Date tokenUpdatedAt;
	
	Date refreshedDate;

	/**
	 * 
	 */
	public AppAuthentication() {
		super(null);
		// TODO Auto-generated constructor stub
	}

	
	/**
	 * @param installationId
	 * @param token
	 * @param tokenCreatedAt
	 * @param tokenUpdatedAt
	 * @param refreshedDate
	 */
	public AppAuthentication(Long installationId, String token, Date tokenCreatedAt, Date tokenUpdatedAt,
			Date refreshedDate) {
		super(null);
		this.installationId = installationId;
		this.token = token;
		this.tokenCreatedAt = tokenCreatedAt;
		this.tokenUpdatedAt = tokenUpdatedAt;
		this.refreshedDate = refreshedDate;
	}
	
	public AppAuthentication(com.google.appengine.api.datastore.Entity entity) {
		super(entity);
		installationId = (Long) entity.getKey().getId();
		token = (String) entity.getProperty("token");
		tokenCreatedAt = (Date) entity.getProperty("tokenCreatedAt");
		tokenUpdatedAt = (Date) entity.getProperty("tokenUpdatedAt");
		refreshedDate = (Date) entity.getProperty("refreshedDate");
	}


	/**
	 * @return the installationId
	 */
	public Long getInstallationId() {
		return installationId;
	}

	/**
	 * @param installationId the installationId to set
	 */
	public void setInstallationId(Long installationId) {
		this.installationId = installationId;
	}

	/**
	 * @return the token
	 */
	public String getToken() {
		return token;
	}

	/**
	 * @param token the token to set
	 */
	public void setToken(String token) {
		this.token = token;
	}

	/**
	 * @return the tokenCreatedAt
	 */
	public Date getTokenCreatedAt() {
		return tokenCreatedAt;
	}

	/**
	 * @param tokenCreatedAt the tokenCreatedAt to set
	 */
	public void setTokenCreatedAt(Date tokenCreatedAt) {
		this.tokenCreatedAt = tokenCreatedAt;
	}

	/**
	 * @return the tokenUpdatedAt
	 */
	public Date getTokenUpdatedAt() {
		return tokenUpdatedAt;
	}

	/**
	 * @param tokenUpdatedAt the tokenUpdatedAt to set
	 */
	public void setTokenUpdatedAt(Date tokenUpdatedAt) {
		this.tokenUpdatedAt = tokenUpdatedAt;
	}

	/**
	 * @return the refreshedDate
	 */
	public Date getRefreshedDate() {
		return refreshedDate;
	}

	/**s
	 * @param refreshedDate the refreshedDate to set
	 */
	public void setRefreshedDate(Date refreshedDate) {
		this.refreshedDate = refreshedDate;
	}


	@Override
	public com.google.appengine.api.datastore.Entity buildEntity() {
		setId(getInstallationId());
		com.google.appengine.api.datastore.Entity installEntity = createEntity(TABLE_NAME);
		installEntity.setProperty("token", getToken());
		installEntity.setProperty("tokenCreatedAt", getTokenCreatedAt());
		installEntity.setProperty("tokenUpdatedAt", getTokenUpdatedAt());
		installEntity.setProperty("refreshedDate", getRefreshedDate());
		System.out.println(installEntity.getKey().getId());
		System.out.println(installEntity.getKey());
		System.out.println(installEntity.getKey().getKind());
		return installEntity;
	}	
	
	
}
