package com.stockboo.app.entity;

import java.util.Date;






/**
 * 
 * @author sunil.r
 *
 */
//@PersistenceCapable(identityType = IdentityType.APPLICATION)
public class FcmVerification extends AbstractStockbooEntity{
	
	public static final String TABLE_NAME = "FcmVerification";

	//@PrimaryKey
	//@Persistent
	Long installationId;

	//@Persistent
	String json;

	//@Persistent
	private Date created;
	
	public FcmVerification(Long installationId, String json, Date created) {
		super(null);
		this.installationId = installationId;
		this.json = json;
		this.created = created;
	}
	
	public FcmVerification(com.google.appengine.api.datastore.Entity entity) {
		super(entity);
		installationId = (Long) entity.getKey().getId();
		json = (String) entity.getProperty("json");
		created = (Date) entity.getProperty("created");
	}

	public Long getInstallationId() {
		return installationId;
	}

	public void setInstallationId(Long installationId) {
		this.installationId = installationId;
	}

	public Date getCreated() {
		return created;
	}

	public void setCreated(Date created) {
		this.created = created;
	}

	public String getJson() {
		return json;
	}

	public void setJson(String json) {
		this.json = json;
	}
	
	@Override
	public com.google.appengine.api.datastore.Entity buildEntity() {
		setId(getInstallationId());
		com.google.appengine.api.datastore.Entity installEntity = createEntity(TABLE_NAME);
		installEntity.setProperty("json", getJson());
		installEntity.setProperty("created", getCreated());
		System.out.println(installEntity.getKey().getId());
		System.out.println(installEntity.getKey());
		System.out.println(installEntity.getKey().getKind());
		return installEntity;
	}

}
