
package com.stockboo.app.entity;

import java.io.Serializable;
import java.util.Date;






/**
 * Get the data from below URL and this is used in CRON job
 * http://finance.google.com/finance/info?client=ig&q=INDEXBOM:SENSEX,NSE:NIFTY
 * 
 * @author sunil.r
 *
 */

public class Sensex extends AbstractHomePageEntity implements Serializable{
	
	public static final String TABLE_NAME = "Sensex";

	private static final long serialVersionUID = 1L;

	private String symbol;
	private String exchange;
	private String ltp;
	private String ltpFinal;
	private String lCur;
	private String s;
	private String lastTradedTime;
	private String lastTradeTimeFormatted;
	private String lastTradedDateTime;
	private String change;
	private String changePercentageFinal;
	private String changePercentage;
	private String cpFix;
	private String chr;
	private String previousClose;
	private Date createdAt;
	private Date updatedAt;

	public Sensex() {
		super(null);
		// TODO Auto-generated constructor stub
	}
	
	public Sensex(com.google.appengine.api.datastore.Entity entity) {
		super(entity);
		
		this.symbol = (String) entity.getProperty("symbol");
		this.exchange = (String) entity.getProperty("exchange");
		this.ltp = (String) entity.getProperty("ltp");
		this.ltpFinal = (String) entity.getProperty("ltpFinal");
		this.lCur = (String) entity.getProperty("lCur");
		this.s = (String) entity.getProperty("s");
		this.lastTradedTime = (String) entity.getProperty("lastTradedTime");
		this.lastTradeTimeFormatted = (String) entity.getProperty("lastTradeTimeFormatted");
		this.lastTradedDateTime = (String) entity.getProperty("lastTradedDateTime");
		this.change = (String) entity.getProperty("change");
		this.cpFix = (String) entity.getProperty("cpFix");
		this.changePercentage = (String) entity.getProperty("changePercentage");
		this.changePercentageFinal = (String) entity.getProperty("changePercentageFinal");
		this.chr = (String) entity.getProperty("chr");
		this.previousClose = (String) entity.getProperty("previousClose");
		this.createdAt = (Date) entity.getProperty("createdAt");
		this.updatedAt = (Date) entity.getProperty("updatedAt");
		
	}

	/**
	 * @param id
	 * @param t
	 * @param e
	 * @param l
	 * @param lFix
	 * @param lCur
	 * @param s
	 * @param ltt
	 * @param lt
	 * @param ltDts
	 * @param c
	 * @param cFix
	 * @param cp
	 * @param cpFix
	 * @param ccol
	 * @param pclsFix
	 */
	public Sensex(String id, String t, String e, String l, String lFix, String lCur, String s, String ltt, String lt,
			String ltDts, String c, String cFix, String cp, String cpFix, String ccol, String pclsFix) {
		super(null);
		this.symbol = t;
		this.exchange = e;
		this.ltp = l;
		this.ltpFinal = lFix;
		this.lCur = lCur;
		this.s = s;
		this.lastTradedTime = ltt;
		this.lastTradeTimeFormatted = lt;
		this.lastTradedDateTime = ltDts;
		this.change = c;
		this.changePercentageFinal = cFix;
		this.changePercentage = cp;
		this.cpFix = cpFix;
		this.chr = ccol;
		this.previousClose = pclsFix;
	}

	public String getSymbol() {
		return symbol;
	}

	public void setSymbol(String symbol) {
		this.symbol = symbol;
	}

	public String getExchange() {
		return exchange;
	}

	public void seExchangeE(String exchange) {
		this.exchange = exchange;
	}

	public String getL() {
		return ltp;
	}

	public void setLtp(String ltp) {
		this.ltp = ltp;
	}

	public String getLtpFinal() {
		return ltpFinal;
	}

	public void setLtpFinal(String ltpFinal) {
		this.ltpFinal = ltpFinal;
	}

	public String getLCur() {
		return lCur;
	}

	public void setLCur(String lCur) {
		this.lCur = lCur;
	}

	public String getS() {
		return s;
	}

	public void setS(String s) {
		this.s = s;
	}

	public String getLastTradedTime() {
		return lastTradedTime;
	}

	public void setLastTradedTime(String lastTradedTime) {
		this.lastTradedTime = lastTradedTime;
	}

	public String getLastTradeTimeFormatted() {
		return lastTradeTimeFormatted;
	}

	public void setLt(String lastTradeTimeFormatted) {
		this.lastTradeTimeFormatted = lastTradeTimeFormatted;
	}

	public String getLtDts() {
		return lastTradedDateTime;
	}

	public void setLastTradedDateTime(String lastTradedDateTime) {
		this.lastTradedDateTime = lastTradedDateTime;
	}

	public String getChange() {
		return change;
	}

	public void setChange(String change) {
		this.change = change;
	}

	public String getChangePercentageFinal() {
		return changePercentageFinal;
	}

	public void setChangePercentageFinal(String changePercentageFinal) {
		this.changePercentageFinal = changePercentageFinal;
	}

	public String getChangePercentage() {
		return changePercentage;
	}

	public void setChangePercentage(String changePercentage) {
		this.changePercentage = changePercentage;
	}

	public String getCpFix() {
		return cpFix;
	}

	public void setCpFix(String cpFix) {
		this.cpFix = cpFix;
	}

	public String getChr() {
		return chr;
	}

	public void setChr(String chr) {
		this.chr = chr;
	}

	public String getPreviousClose() {
		return previousClose;
	}

	public void setpreviousClose(String previousClose) {
		this.previousClose = previousClose;
	}

	/**
	 * @return the createdAt
	 */
	public Date getCreatedAt() {
		return createdAt;
	}

	/**
	 * @param createdAt
	 *            the createdAt to set
	 */
	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	/**
	 * @return the updatedAt
	 */
	public Date getUpdatedAt() {
		return updatedAt;
	}

	/**
	 * @param updatedAt
	 *            the updatedAt to set
	 */
	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}
	
	@Override
	public com.google.appengine.api.datastore.Entity buildEntity() {
		setTableId("SENSEX");
		com.google.appengine.api.datastore.Entity entity = createEntity(Sensex.TABLE_NAME);

		entity.setProperty("id", null);
		entity.setProperty("exchange", getExchange());
		entity.setProperty("ltp", getL());
		entity.setProperty("ltpFinal", getLtpFinal());
		entity.setProperty("lCur", getLCur());
		entity.setProperty("s", getS());
		entity.setProperty("lastTradedTime",getLastTradedTime());
		entity.setProperty("lastTradeTimeFormatted", getLastTradeTimeFormatted());
		entity.setProperty("lastTradedDateTime", getLtDts());
		entity.setProperty("change", getChange());
		entity.setProperty("cpFix", getCpFix());
		entity.setProperty("changePercentage", getChangePercentage());
		entity.setProperty("changePercentageFinal", getChangePercentageFinal());
		entity.setProperty("chr", getChr());
		entity.setProperty("previousClose", getPreviousClose());
		entity.setProperty("createdAt", getCreatedAt());
		entity.setProperty("updatedAt", getUpdatedAt());
		
		System.out.println(entity.getKey().getId());
		System.out.println(entity.getKey());
		System.out.println(entity.getKey().getKind());
		return entity;
	}	

}