package com.stockboo.app.entity;

import java.util.Date;
import java.util.TimeZone;

import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;

/**
 * DTO for Installation
 * This class will create, update, read, list operations of installation
 * 
 * @author sunil.r
 *
 */
@Entity
public class InstallEntity extends AbstractStockbooEntity {
	
	public static final String TABLE_NAME = "InstallEntity";
	
	String appIdentifier;
	
	String appName;
	
	String appVersion;
	
	String deviceType;
	
	String deviceToken;

	TimeZone timeZone;
	
	String installationId;
	
	Date createdAt;
	
	Date updatedAt;
	

	/**
	 * 
	 */
	public InstallEntity() {
		super(null);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param appIdentifier
	 * @param appName
	 * @param appVersion
	 * @param deviceType
	 * @param timeZone
	 * @param installationId
	 * @param createdAt
	 * @param updatedAt
	 */
	public InstallEntity(String appIdentifier, String appName, String appVersion, String deviceType, String deviceToken, TimeZone timeZone,
			Date createdAt) {
		super(null);
		this.appIdentifier = appIdentifier;
		this.appName = appName;
		this.appVersion = appVersion;
		this.deviceType = deviceType;
		this.timeZone = timeZone;
		this.createdAt = createdAt;
		this. deviceToken = deviceToken;
	}
	
	public InstallEntity(com.google.appengine.api.datastore.Entity entity){
		super(entity);
		setId((Long) entity.getKey().getId());
		appIdentifier = (String) entity.getProperty("appIdentifier");
		appVersion = (String) entity.getProperty("appVersion");
		appName = (String) entity.getProperty("appName");
		deviceType = (String) entity.getProperty("deviceType");
		deviceToken = (String) entity.getProperty("deviceToken");
		timeZone = (TimeZone) entity.getProperty("timeZone");
		installationId = (String) entity.getProperty("installationId");
		createdAt = (Date)entity.getProperty("createdAt");
		updatedAt = (Date) entity.getProperty("updatedAt");
	}

	/**
	 * @return the appIdentifier
	 */
	public String getAppIdentifier() {
		return appIdentifier;
	}

	/**
	 * @param appIdentifier the appIdentifier to set
	 */
	public void setAppIdentifier(String appIdentifier) {
		this.appIdentifier = appIdentifier;
	}

	/**
	 * @return the appName
	 */
	public String getAppName() {
		return appName;
	}

	/**
	 * @param appName the appName to set
	 */
	public void setAppName(String appName) {
		this.appName = appName;
	}

	/**
	 * @return the appVersion
	 */
	public String getAppVersion() {
		return appVersion;
	}

	/**
	 * @param appVersion the appVersion to set
	 */
	public void setAppVersion(String appVersion) {
		this.appVersion = appVersion;
	}

	/**
	 * @return the deviceType
	 */
	public String getDeviceType() {
		return deviceType;
	}

	/**
	 * @param deviceType the deviceType to set
	 */
	public void setDeviceType(String deviceType) {
		this.deviceType = deviceType;
	}

	/**
	 * @return the timeZone
	 */
	public TimeZone getTimeZone() {
		return timeZone;
	}

	/**
	 * @param timeZone the timeZone to set
	 */
	public void setTimeZone(TimeZone timeZone) {
		this.timeZone = timeZone;
	}

	/**
	 * @return the installationId
	 */
	public String getInstallationId() {
		return installationId;
	}

	/**
	 * @param installationId the installationId to set
	 */
	public void setInstallationId(String installationId) {
		this.installationId = installationId;
	}

	/**
	 * @return the createdAt
	 */
	public Date getCreatedAt() {
		return createdAt;
	}

	/**
	 * @param createdAt the createdAt to set
	 */
	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	/**
	 * @return the updatedAt
	 */
	public Date getUpdatedAt() {
		return updatedAt;
	}

	/**
	 * @param updatedAt the updatedAt to set
	 */
	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}

	/**
	 * @return the deviceToken
	 */
	public String getDeviceToken() {
		return deviceToken;
	}

	/**
	 * @param deviceToken the deviceToken to set
	 */
	public void setDeviceToken(String deviceToken) {
		this.deviceToken = deviceToken;
	}

	/*public com.google.appengine.api.datastore.Entity create() {
		ObjectifyService.register(InstallEntity.class);
		DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
		
		com.google.appengine.api.datastore.Entity installEntity = null;
		if(null != getId()){
			installEntity = new com.google.appengine.api.datastore.Entity(
					TABLE_NAME, getId()); 
			System.out.println("update"+getId());
		}else{
			installEntity = new com.google.appengine.api.datastore.Entity(
					TABLE_NAME); 
		}
		installEntity.setProperty("appIdentifier", getAppIdentifier());
		installEntity.setProperty("appName", getAppName());
		installEntity.setProperty("appVersion", getAppVersion());
		installEntity.setProperty("deviceType", getDeviceType());
		installEntity.setProperty("deviceToken", getDeviceToken());
		installEntity.setProperty("timeZone", getTimeZone());
		installEntity.setProperty("installationId", getInstallationId());
		installEntity.setProperty("createdAt", getCreatedAt());
		installEntity.setProperty("updatedAt", getUpdatedAt());
		
		datastore.put(installEntity);
		System.out.println(installEntity.getKey().getId());
		System.out.println(installEntity.getKey());
		System.out.println(installEntity.getKey().getKind());
		return installEntity;
	}*/

	@Override
	public com.google.appengine.api.datastore.Entity buildEntity() {
		com.google.appengine.api.datastore.Entity installEntity = createEntity(TABLE_NAME);
		installEntity.setProperty("appIdentifier", getAppIdentifier());
		installEntity.setProperty("appName", getAppName());
		installEntity.setProperty("appVersion", getAppVersion());
		installEntity.setProperty("deviceType", getDeviceType());
		installEntity.setProperty("deviceToken", getDeviceToken());
		installEntity.setProperty("timeZone", getTimeZone() != null ? getTimeZone().getID() : "");
		installEntity.setProperty("installationId", getInstallationId());
		installEntity.setProperty("createdAt", getCreatedAt());
		installEntity.setProperty("updatedAt", getUpdatedAt());
		System.out.println(installEntity.getKey().getId());
		System.out.println(installEntity.getKey());
		System.out.println(installEntity.getKey().getKind());
		return installEntity;
	}


	public void update(InstallEntity install)
	{
		this.appVersion = getActualValue(this.appVersion, install.getAppVersion());
		this.deviceToken = getActualValue(this.deviceToken, install.getDeviceToken());
		this.updatedAt= new Date();
	}
	
		
}
