package com.stockboo.app.entity;

/**
 * Messages
 * 
 * @author sunil.r
 *
 */
public class MessageEntity extends AbstractStockbooEntity {
	
	public static final String TABLE_NAME = "MessageEntity";

	private String content;

	private Long stockId;

	private String stockName;

	private long createdAt;

	public MessageEntity() {
		super(null);
	}

	/**
	 * @param messageId
	 * @param content
	 * @param stockId
	 * @param stockName
	 */
	public MessageEntity(String content, Long stockId, String stockName) {
		super(null);
		this.content = content;
		this.stockId = stockId;
		this.stockName = stockName;
	}
	
	public MessageEntity(com.google.appengine.api.datastore.Entity entity) {
		super(entity);
		this.content = (String) entity.getProperty("content");
		this.stockId = (Long) entity.getProperty("stockId");
		this.stockName = (String) entity.getProperty("stockName");
		this.createdAt = (long) entity.getProperty("createdAt");
	}

	/**
	 * @return the content
	 */
	public String getContent() {
		return content;
	}

	/**
	 * @param content
	 *            the content to set
	 */
	public void setContent(String content) {
		this.content = content;
	}

	/**
	 * @return the stockId
	 */
	public Long getStockId() {
		return stockId;
	}

	/**
	 * @param stockId
	 *            the stockId to set
	 */
	public void setStockId(Long stockId) {
		this.stockId = stockId;
	}

	/**
	 * @return the stockName
	 */
	public String getStockName() {
		return stockName;
	}

	/**
	 * @param stockName
	 *            the stockName to set
	 */
	public void setStockName(String stockName) {
		this.stockName = stockName;
	}
	
	/**
	 * @return the createdAt
	 */
	public long getCreatedAt() {
		return createdAt;
	}

	/**
	 * @param createdAt the createdAt to set
	 */
	public void setCreatedAt(long createdAt) {
		this.createdAt = createdAt;
	}

	@Override
	public com.google.appengine.api.datastore.Entity buildEntity() {
		com.google.appengine.api.datastore.Entity entity = createEntity(Nifty.TABLE_NAME);

		entity.setProperty("content", getContent());
		entity.setProperty("stockId", getStockId());
		entity.setProperty("stockName", getStockName());
		entity.setProperty("createdAt", getCreatedAt());
		
		System.out.println(entity.getKey().getId());
		System.out.println(entity.getKey());
		System.out.println(entity.getKey().getKind());
		return entity;
	}

}
