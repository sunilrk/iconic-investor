package com.stockboo.app.entity;

import java.util.Date;






import com.google.appengine.api.datastore.Text;

public class GainersLoosersEntity extends AbstractHomePageEntity{
	
	public static final String TABLE_NAME = "GainersLoosersEntity";

	private String type;
	private Text fullDetails;
	private Text topThree;
	private Date createdAt;
	private Date updatedAt;
	
	
	public GainersLoosersEntity(com.google.appengine.api.datastore.Entity entity) {
		super(entity);
		setTableId(entity.getKey().getName());
		this.type = entity.getKey().getName();
		this.fullDetails = (Text) entity.getProperty("symbol");
		this.topThree = (Text) entity.getProperty("exchange");
		this.createdAt = (Date) entity.getProperty("ltp");
		this.updatedAt = (Date) entity.getProperty("ltpFinal");
	}

	public GainersLoosersEntity() {
		super(null);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @return the type
	 */
	public String getType() {
		return type;
	}

	
	/**
	 * @return the createdAt
	 */
	public Date getCreatedAt() {
		return createdAt;
	}

	/**
	 * @param type the type to set
	 */
	public void setType(String type) {
		this.type = type;
	}

	/**
	 * @param createdAt
	 *            the createdAt to set
	 */
	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	/**
	 * @return the updatedAt
	 */
	public Date getUpdatedAt() {
		return updatedAt;
	}

	/**
	 * @param updatedAt
	 *            the updatedAt to set
	 */
	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}

	/**
	 * @return the topThree
	 */
	public Text getTopThree() {
		return topThree;
	}

	/**
	 * @param topThree
	 *            the topThree to set
	 */
	public void setTopThree(Text topThree) {
		this.topThree = topThree;
	}

	/**
	 * @return the fullDetails
	 */
	public Text getFullDetails() {
		return fullDetails;
	}

	/**
	 * @param fullDetails
	 *            the fullDetails to set
	 */
	public void setFullDetails(Text fullDetails) {
		this.fullDetails = fullDetails;
	}
	
	@Override
	public com.google.appengine.api.datastore.Entity buildEntity() {
		setTableId(getType());
		com.google.appengine.api.datastore.Entity entity = createEntity(GainersLoosersEntity.TABLE_NAME);

		entity.setProperty("fullDetails", getFullDetails());
		entity.setProperty("topThree", getTopThree());
		entity.setProperty("createdAt", getCreatedAt());
		entity.setProperty("updatedAt", getUpdatedAt());
		
		System.out.println(entity.getKey().getId());
		System.out.println(entity.getKey());
		System.out.println(entity.getKey().getKind());
		return entity;
	}	

}