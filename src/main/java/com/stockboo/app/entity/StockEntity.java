package com.stockboo.app.entity;

import java.util.Date;


/**
 * Database entity for stocks
 * 
 * @author sunil.r
 *
 */
public class StockEntity extends AbstractStockbooEntity {
	
	public static final String TABLE_NAME = "StockEntity";
	
	private Long advisorId;
	
	private String stockName;
	
	private String scriptCode;
	
	private Integer sugesstionType;// 0,1,------------------>Enums with
	
	private Double buyPrice;
	
	private Double stopLoss;
	
	private Double targetPrice;
	
	private Double bookingPrice;
	
	private String message;
	
	private Integer status; // 1,---------------------------->0(open) or 1
	
	private String result;
	
	private Date createdAt;
	
	private Date updatedAt;

	/**
	 * 
	 */
	public StockEntity() {
		super(null);
		// TODO Auto-generated constructor stub
	}

	
	/**
	 * @param stockName
	 * @param scriptCode
	 * @param sugesstionType
	 * @param buyPrice
	 * @param stopLoss
	 * @param targetPrice
	 * @param bookingPrice
	 * @param message
	 * @param status
	 * @param result
	 * @param createdAt
	 */
	public StockEntity(String stockName, String scriptCode, Integer sugesstionType, Double buyPrice, Double stopLoss,
			Double targetPrice, Double bookingPrice, String message, Integer status, String result,
			Date createdAt, Long advisorId ) {
		super(null);
		this.stockName = stockName;
		this.scriptCode = scriptCode;
		this.sugesstionType = sugesstionType;
		this.buyPrice = buyPrice;
		this.stopLoss = stopLoss;
		this.targetPrice = targetPrice;
		this.bookingPrice = bookingPrice;
		this.message = message;
		this.status = status;
		this.result = result;
		this.createdAt = createdAt;
		this.advisorId = advisorId;
	}
	
	public StockEntity(com.google.appengine.api.datastore.Entity entity) {
		super(entity);

		this.advisorId = (Long) entity.getProperty("advisorId");
		this.stockName = (String) entity.getProperty("stockName");
		this.scriptCode = (String) entity.getProperty("scriptCode");
		this.sugesstionType = (Integer) entity.getProperty("sugesstionType");
		this.buyPrice = (Double) entity.getProperty("buyPrice");
		this.stopLoss = (Double) entity.getProperty("stopLoss");
		this.targetPrice = (Double) entity.getProperty("targetPrice");
		this.bookingPrice = (Double) entity.getProperty("bookingPrice");
		this.message = (String) entity.getProperty("message");
		this.status = (Integer) entity.getProperty("status");
		this.result = (String) entity.getProperty("result");
		this.createdAt = (Date) entity.getProperty("createdAt");
		this.updatedAt = (Date) entity.getProperty("updatedAt");
	}

	/**
	 * @return the stockName
	 */
	public String getStockName() {
		return stockName;
	}

	/**
	 * @param stockName
	 *            the stockName to set
	 */
	public void setStockName(String stockName) {
		this.stockName = stockName;
	}

	/**
	 * @return the scriptCode
	 */
	public String getScriptCode() {
		return scriptCode;
	}

	/**
	 * @param scriptCode
	 *            the scriptCode to set
	 */
	public void setScriptCode(String scriptCode) {
		this.scriptCode = scriptCode;
	}

	/**
	 * @return the sugesstionType
	 */
	public Integer getSugesstionType() {
		return sugesstionType;
	}

	/**
	 * @param sugesstionType
	 *            the sugesstionType to set
	 */
	public void setSugesstionType(Integer sugesstionType) {
		this.sugesstionType = sugesstionType;
	}

	/**
	 * @return the buyPrice
	 */
	public Double getBuyPrice() {
		return buyPrice;
	}

	/**
	 * @param buyPrice
	 *            the buyPrice to set
	 */
	public void setBuyPrice(Double buyPrice) {
		this.buyPrice = buyPrice;
	}

	/**
	 * @return the stopLoss
	 */
	public Double getStopLoss() {
		return stopLoss;
	}

	/**
	 * @param stopLoss
	 *            the stopLoss to set
	 */
	public void setStopLoss(Double stopLoss) {
		this.stopLoss = stopLoss;
	}

	/**
	 * @return the targetPrice
	 */
	public Double getTargetPrice() {
		return targetPrice;
	}

	/**
	 * @param targetPrice
	 *            the targetPrice to set
	 */
	public void setTargetPrice(Double targetPrice) {
		this.targetPrice = targetPrice;
	}

	/**
	 * @return the bookingPrice
	 */
	public Double getBookingPrice() {
		return bookingPrice;
	}

	/**
	 * @param bookingPrice
	 *            the bookingPrice to set
	 */
	public void setBookingPrice(Double bookingPrice) {
		this.bookingPrice = bookingPrice;
	}

	/**
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}

	/**
	 * @param message
	 *            the message to set
	 */
	public void setMessage(String message) {
		this.message = message;
	}

	/**
	 * @return the status
	 */
	public Integer getStatus() {
		return status;
	}

	/**
	 * @param status
	 *            the status to set
	 */
	public void setStatus(Integer status) {
		this.status = status;
	}

	/**
	 * @return the result
	 */
	public String getResult() {
		return result;
	}

	/**
	 * @param result
	 *            the result to set
	 */
	public void setResult(String result) {
		this.result = result;
	}

	/**
	 * @return the createdAt
	 */
	public Date getCreatedAt() {
		return createdAt;
	}

	/**
	 * @param createdAt
	 *            the createdAt to set
	 */
	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	/**
	 * @return the updatedAt
	 */
	public Date getUpdatedAt() {
		return updatedAt;
	}

	/**
	 * @param updatedAt
	 *            the updatedAt to set
	 */
	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}

	/**
	 * @return the advisorId
	 */
	public Long getAdvisorId() {
		return advisorId;
	}


	/**
	 * @param advisorId the advisorId to set
	 */
	public void setAdvisorId(Long advisorId) {
		this.advisorId = advisorId;
	}
	
	@Override
	public com.google.appengine.api.datastore.Entity buildEntity() {
		com.google.appengine.api.datastore.Entity entity = createEntity(Nifty.TABLE_NAME);
		
		entity.setProperty("advisorId", getAdvisorId());
		entity.setProperty("stockName", getStockName());
		entity.setProperty("scriptCode", getScriptCode());
		entity.setProperty("sugesstionType", getSugesstionType());
		entity.setProperty("buyPrice", getBuyPrice());
		entity.setProperty("stopLoss", getStopLoss());
		entity.setProperty("targetPrice", getTargetPrice());
		entity.setProperty("bookingPrice", getBookingPrice());
		entity.setProperty("message", getMessage());
		entity.setProperty("status", getStatus());
		entity.setProperty("result", getResult());
		entity.setProperty("createdAt", getCreatedAt());
		entity.setProperty("updatedAt", getUpdatedAt());
		
		System.out.println(entity.getKey().getId());
		System.out.println(entity.getKey());
		System.out.println(entity.getKey().getKind());
		return entity;
	}


}
