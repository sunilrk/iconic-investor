package com.stockboo.app.entity;

import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;

public abstract class AbstractHomePageEntity extends AbstractStockbooEntity{
	
	private String tableId;

	public AbstractHomePageEntity(Entity entity) {
		super(entity);
		// TODO Auto-generated constructor stub
	}
	
	public Entity createEntity(String tableName) {
		Entity entity = null;

		if (tableId != null) {
			Key key = KeyFactory.createKey(tableName, tableId);
			System.out.println("***********************************************************************");
			System.out.println(key);
			System.out.println("***********************************************************************");
			entity = new Entity(key);
		} else {
			entity = new Entity(tableName);
			System.out.println("***********************************************************************");
			System.out.println("***********************************************************************");
		}

		return entity;
	}


	/**
	 * @param id the id to set
	 */
	public void setTableId(String id) {
		this.tableId = id;
	}

	
	
}
