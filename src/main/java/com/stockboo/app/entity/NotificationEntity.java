
package com.stockboo.app.entity;

import java.io.Serializable;
import java.util.Date;







/**
 * Stores the notificatons
 * 
 * @author sunil.r
 *
 */

//@PersistenceCapable(identityType = IdentityType.APPLICATION)
public class NotificationEntity extends AbstractStockbooEntity implements Serializable {
	public static final String TABLE_NAME = "NotificationEntity";

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	//@PrimaryKey
	//@Persistent(valueStrategy = IdGeneratorStrategy.IDENTITY)
	//@Persistent
	private String type;
	//@Persistent
	private String tittle;
	//@Persistent
	private String body;
	//@Persistent
	private String dataMessage;
	//@Persistent
	private String topic;
	//@Persistent
	private boolean sent;
	//@Persistent
	private Date createdAt;
	
	public NotificationEntity(com.google.appengine.api.datastore.Entity entity) {
		super(entity);
		this.type= (String) entity.getProperty("type");
		this.tittle= (String) entity.getProperty("tittle");
		this.body= (String) entity.getProperty("body");
		this.dataMessage= (String) entity.getProperty("dataMessage");
		this.sent= (boolean) entity.getProperty("sent");
		this.topic= (String) entity.getProperty("topic");
		this.createdAt= (Date) entity.getProperty("createdAt");
	}

	public NotificationEntity() {
		super(null);
		// TODO Auto-generated constructor stub
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getTittle() {
		return tittle;
	}

	public void setTittle(String tittle) {
		this.tittle = tittle;
	}

	public String getBody() {
		return body;
	}

	public void setBody(String body) {
		this.body = body;
	}

	public boolean isSent() {
		return sent;
	}

	public void setSent(boolean sent) {
		this.sent = sent;
	}

	public Date getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	public String getDataMessage() {
		return dataMessage;
	}

	public void setDataMessage(String dataMessage) {
		this.dataMessage = dataMessage;
	}

	public String getTopic() {
		return topic;
	}

	public void setTopic(String topic) {
		this.topic = topic;
	}
	
	@Override
	public com.google.appengine.api.datastore.Entity buildEntity() {
		com.google.appengine.api.datastore.Entity entity = createEntity(CryptoCurrencyEntity.TABLE_NAME);
		
		entity.setProperty("type", getType());
		entity.setProperty("tittle", getTittle());
		entity.setProperty("body", getBody());
		entity.setProperty("dataMessage", getDataMessage());
		entity.setProperty("topic", getTopic());
		entity.setProperty("sent", isSent());
		entity.setProperty("createdAt", getCreatedAt());
		
		
		System.out.println(entity.getKey().getId());
		System.out.println(entity.getKey());
		System.out.println(entity.getKey().getKind());
		return entity;
	}

}