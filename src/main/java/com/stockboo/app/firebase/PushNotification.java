package com.stockboo.app.firebase;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;

import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.PreparedQuery;
import com.google.appengine.api.datastore.Query;
import com.google.appengine.api.datastore.Query.Filter;
import com.google.appengine.api.datastore.Query.FilterOperator;
import com.google.appengine.api.datastore.Query.FilterPredicate;
import com.stockboo.app.dao.AbstractStockbooDao;
import com.stockboo.app.domain.pushnotification.Fcmtoken;
import com.stockboo.app.domain.pushnotification.SendNotification;
import com.stockboo.app.domain.pushnotification.SubscriptionTopic;
import com.stockboo.app.domain.pushnotification.SubscriptionTopics;
import com.stockboo.app.domain.utility.StringUtil;
import com.stockboo.app.entity.FcmVerification;
import com.stockboo.app.entity.FcmtokenEntity;
//import com.stockboo.app.entity.NotificationEntity;
import com.stockboo.app.entity.NotificationEntity;
import com.stockboo.app.entity.SensexAndNiftyPrices;

public class PushNotification extends AbstractStockbooDao {

	public final static String AUTH_KEY_FCM_IOS = "AAAAttHbopY:APA91bHNBCJW3iU0Pm65AS0TVBozO0ereADPUMQbIHUsEreLzh9MR73NGV3FSuNveGgTyw7maqd5DnAYSjhY3_8cbDtJMBQ63lro8huOGUF7e5da2xiUvSBII3ohWc4WovF4tj80k6CO";
	public final static String AUTH_KEY_FCM_ANDROID = "AAAApI6UpH8:APA91bFODBc9AG5pV0EVNuxyO-Bwx53JdMP7DKBtr05SowSF_qdeqBC1uO0fff_u4dHq6RqXsQNZljDPOk27QwfVUKCfrca2wfnfsv7jHKBtNyYvgyiGvUAZswMo5Z5S8Mft62PWrfBW";
	public final static String API_URL_FCM = "https://fcm.googleapis.com/fcm/send";
	public final static String MSG_TO_TOPIC_URL = "https://fcm.googleapis.com/fcm/send";
	public final static String FCM_UNSUBSCRIBE = "https://iid.googleapis.com/iid/v1:batchRemove";

	public static String pushNotification(String fcmToken, String tittle, String body, String topic)
			throws IOException, JSONException {
		URL url = new URL(API_URL_FCM);
		HttpURLConnection conn = (HttpURLConnection) url.openConnection();

		conn.setUseCaches(false);
		conn.setDoInput(true);
		conn.setDoOutput(true);

		conn.setRequestMethod("POST");
		conn.setRequestProperty("Authorization", "key=" + AUTH_KEY_FCM_IOS);
		conn.setRequestProperty("Content-Type", "application/json");

		JSONObject json = new JSONObject();

		json.put("to", fcmToken.trim());
		JSONObject info = new JSONObject();
		info.put("title", tittle); // Notification title
		info.put("body", body); // Notification
								// body
		json.put("notification", info);
		System.out.println(json.toString());
		String resultIOS = httpPostData(conn, json);
		System.out.println("GCM Notification status for IOS" + resultIOS);
		conn.setRequestProperty("Authorization", "key=" + AUTH_KEY_FCM_ANDROID);
		String resultAndroid = httpPostData(conn, json);
		System.out.println("GCM Notification status for Android" + resultAndroid);
		return "IOS-"+resultIOS+"   "+"ANDROID-"+resultAndroid;
	}

	private static String httpPostData(HttpURLConnection conn, JSONObject json) {
		String result;
		BufferedReader br = null;
		OutputStreamWriter wr = null;
		try {
			wr = new OutputStreamWriter(conn.getOutputStream());
			wr.write(json.toString());
			wr.flush();
			br = new BufferedReader(new InputStreamReader((conn.getInputStream())));
			String output;
			System.out.println("Response code- "+ conn.getResponseCode());
			System.out.println("Output from Server .... \n");
			while ((output = br.readLine()) != null) {
				System.out.println(output);
			}
			if (200 == conn.getResponseCode()) {
				result = "SUCCESS";
			} else {
				result = "FAILURE";
			}
		} catch (Exception e) {
			e.printStackTrace();
			result = "FAILURE";
		}
		finally{
			 if (br != null) {
			        try {
			        	br.close();
			        } catch (IOException e) {
			        }
			    }
			 
			 if (wr != null) {
			        try {
			        	wr.close();
			        } catch (IOException e) {
			        }
			    }
			 if (conn != null) {
			        conn.disconnect();
			    }
		}
		return result;
	}

	public String sendNotification(SendNotification sendNotofication) throws Exception {
		if (null == sendNotofication.getBody() || null == sendNotofication.getTitle()
				|| null == sendNotofication.getTopic()) {
			throw new Exception("Mandatory fields are missing");
		}
		if (sendNotofication.isSend()) {
			List<FcmtokenEntity> fcmTokenList = new ArrayList<>();
			Query query = new Query(FcmtokenEntity.TABLE_NAME);
			
			PreparedQuery results = getDataStoreService().prepare(query);

			for (Entity entity : results.asIterable()) {
				fcmTokenList.add(new FcmtokenEntity(entity));
			}
			
			for (FcmtokenEntity fcmtokenEntity : fcmTokenList) {
				pushNotification(fcmtokenEntity.getFcmToken(), sendNotofication.getTitle(), sendNotofication.getBody(),
						sendNotofication.getTopic());
			}
		}

		NotificationEntity entity = new NotificationEntity();
		entity.setBody(sendNotofication.getBody());
		entity.setTittle(sendNotofication.getTitle());
		entity.setType(sendNotofication.getTopic());
		entity.setSent(sendNotofication.isSend());
		entity.setCreatedAt(new Date());
		save(entity.buildEntity());

		return "Success";
	}

	public String storeFcmToken(Fcmtoken fcmtoken) throws Exception {

		if (null == fcmtoken.getFcmToken() || null == fcmtoken.getInstallationId()) {
			throw new Exception("Mandatory fields are missing");
		}
		
		FcmtokenEntity entity = new FcmtokenEntity();
		entity.setFcmToken(fcmtoken.getFcmToken());
		entity.setInstallationId(Long.parseLong(fcmtoken.getInstallationId()));
		entity.setCreated(new Date());
		save(entity.buildEntity());
		return "Success";
	}
	
	/**
	 * Deleting fcm record
	 * 
	 * @param id
	 * @throws Exception
	 */
	public void deleteFcmRecord(Long id) throws Exception {
		System.out.println("In FCM delete");
		try {
			deleteEntityById(id, FcmtokenEntity.TABLE_NAME);
		} /*catch (javax.jdo.JDOObjectNotFoundException e) {
		}*/ catch (Exception e) {
		} 
	}

	public static List<SubscriptionTopic> getSubscriptionTopics() {
		List<SubscriptionTopic> subscriptionTopicList = new ArrayList<>();
		for (SubscriptionTopics topic : SubscriptionTopics.values()) {
			SubscriptionTopic subscriptionTopic = new SubscriptionTopic(topic.getTittle(), topic.getId(), topic.getDescription());
			subscriptionTopicList.add(subscriptionTopic);
		}
		return subscriptionTopicList;
	}

	public static String sendNotificationToTopic(SendNotification sendNotofication) throws Exception {
		validateNotifications(sendNotofication);
		String result = "";
		URL url = new URL(MSG_TO_TOPIC_URL);
		HttpURLConnection conn = (HttpURLConnection) url.openConnection();

		conn.setUseCaches(false);
		conn.setDoInput(true);
		conn.setDoOutput(true);

		conn.setRequestMethod("POST");
		conn.setRequestProperty("Authorization", "key=" + AUTH_KEY_FCM_IOS);
		conn.setRequestProperty("Content-Type", "application/json");
		
		HttpURLConnection connAndroid = (HttpURLConnection) url.openConnection();

		connAndroid.setUseCaches(false);
		connAndroid.setDoInput(true);
		connAndroid.setDoOutput(true);

		connAndroid.setRequestMethod("POST");
		connAndroid.setRequestProperty("Authorization", "key=" + AUTH_KEY_FCM_ANDROID);
		connAndroid.setRequestProperty("Content-Type", "application/json");

		JSONObject json = new JSONObject();
		json.put("to", "/topics/" + sendNotofication.getTopic().trim());
		NotificationEntity ne = new NotificationEntity();

		if (sendNotofication.getBody() != "" && sendNotofication.getBody() != null && sendNotofication.getTitle() != ""
				&& sendNotofication.getTitle() != null) {
			JSONObject info = new JSONObject();
			info.put("title", sendNotofication.getTitle());
			info.put("body", sendNotofication.getBody());
			json.put("notification", info);
			json.put("badge", sendNotofication.getBadge());
			json.put("sound", sendNotofication.getSound());
			ne.setBody(sendNotofication.getBody());
			ne.setTittle(sendNotofication.getTitle());
			ne.setSent(true);
			ne.setTopic(sendNotofication.getTopic());
			//set type
		}

		if (sendNotofication.getDataMessage() != "" && sendNotofication.getDataMessage() != null) {
			JSONObject message = new JSONObject();
			message.put("message", sendNotofication.getDataMessage());

			JSONObject data = new JSONObject();
			data.put("data", message);
			json.put("notification", data);
			ne.setDataMessage(sendNotofication.getDataMessage());
		}
		ne.setCreatedAt(new Date());
		//Saving
		Key key = DatastoreServiceFactory.getDatastoreService().put(ne.buildEntity());
		
		System.out.println(json.toString());
		String resultIOS = httpPostData(conn, json);
		System.out.println("GCM Notification status for IOS " + resultIOS);
		conn.setRequestProperty("Authorization", "key=" + AUTH_KEY_FCM_ANDROID);
		String resultAndroid = httpPostData(connAndroid, json);
		System.out.println("GCM Notification status for Android " + resultAndroid);
		return "IOS-"+resultIOS+"   "+"ANDROID-"+resultAndroid;
	}

	private static void validateNotifications(SendNotification sendNotofication) throws Exception {
		if (sendNotofication.getBody() == "" && sendNotofication.getBody() == null && sendNotofication.getTitle() == ""
				&& sendNotofication.getTitle() == null && sendNotofication.getDataMessage() == null) {
			throw new Exception("Mandatory fields are missing");
		}
	}

	public List<NotificationEntity> getNotificationsList(Date date) {
		
		List<NotificationEntity> notifications = new ArrayList<>();
		Query cryptoQuery = null;
		PreparedQuery results = getDataStoreService().prepare(cryptoQuery);
		
		if(null != date){
			Filter propertyFilter = new FilterPredicate("createdAt", FilterOperator.GREATER_THAN, date);
			cryptoQuery = new Query(SensexAndNiftyPrices.TABLE_NAME).setFilter(propertyFilter);
		}else{
			cryptoQuery = new Query(NotificationEntity.TABLE_NAME);
		}
		
		for (Entity entity : results.asIterable()) {
			notifications.add(new NotificationEntity(entity));
		}
		
		return notifications;
	}
	
	public static boolean unSubscribeToTopic(String fcmToken, String deviceType) throws Exception {
		URL url = new URL(FCM_UNSUBSCRIBE);
		String result = "";
		ArrayList<String> fcmTokenList = new ArrayList<>();
		fcmTokenList.add(fcmToken);
		if (deviceType.equalsIgnoreCase("Andriod")) {
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();

			conn.setUseCaches(false);
			conn.setDoInput(true);
			conn.setDoOutput(true);

			conn.setRequestMethod("POST");
			conn.setRequestProperty("Authorization", "key=" + AUTH_KEY_FCM_IOS);
			conn.setRequestProperty("Content-Type", "application/json");

			JSONObject json = new JSONObject();

			json.put("to", "/topics/market_alerts");
			JSONObject info = new JSONObject();
			info.put("registration_tokens", fcmTokenList); // Notification title
			System.out.println(json.toString());
			result = httpPostData(conn, json);
			System.out.println("Andriod Unsubscribed for market_alerts for " + fcmToken + "-" + result);
			return true;
		}
		if (deviceType.equalsIgnoreCase("iOS")) {
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();

			conn.setUseCaches(false);
			conn.setDoInput(true);
			conn.setDoOutput(true);

			conn.setRequestMethod("POST");
			conn.setRequestProperty("Authorization", "key=" + AUTH_KEY_FCM_ANDROID);
			conn.setRequestProperty("Content-Type", "application/json");

			JSONObject json = new JSONObject();

			json.put("to", "/topics/market_alerts");
			JSONObject info = new JSONObject();
			info.put("registration_tokens", fcmTokenList); // Notification title
			System.out.println(json.toString());
			result = httpPostData(conn, json);
			System.out.println("iOS Unsubscribed for market_alerts for " + fcmToken + "-" + result);
			return true;
		}
		return false;

	}

	/**
	 * 
	 * Dummy method as of now
	 * 
	 * @param firebaseAuthToken
	 * @return
	 * @throws Exception 
	 */
	public StringUtil storeFcmVerificationToken(String installationId, Object object) throws Exception {
		System.out.println(object.toString());
		FcmVerification loginEntity = new FcmVerification(Long.parseLong(installationId), null==object ? "":object.toString(), new Date());
		try {
			save(loginEntity.buildEntity());
		} catch (Exception e) {
			System.out.println(e.getMessage());
			throw new Exception("System error");
		}
		
	    
	    return new StringUtil("Success");	}

	/**
	 * Dummy method to retrieve list
	 * 
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public List<FcmVerification> FcmVerificationTokenList() {
		List<FcmVerification> authList = new ArrayList<>();
		Query query = new Query(FcmVerification.TABLE_NAME);
		PreparedQuery results = getDataStoreService().prepare(query);
		for (Entity entity : results.asIterable()) {
			authList.add(new FcmVerification(entity));
		}
		return authList;
	}
		
}
