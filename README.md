Iconic investor application rest services using App Engine Standard (Java 8)
=================================================================================
-  cron.xml --> to configure the cron job to fetch data every day form NSE and BSE URLS
-  application.properties --> to configure some properties which are used in application like 
							- Excel Headers information which are come part of NSE and BSE rest urls
							- Data formats 

-  The investor logo images which are uploading as part of investor profile creation or update,
will be suppose to store in GCP storage bucket, so before uploading this code to app engine.
	- Need to create A GCP Storage bucket 
	- update bucket name in  application.properties for the property gcp_storage_bucket_name
	- Assign "App Engine default service account" as "Storage Object Admin" to the created GCP bucket
	- replace service_acc_auth.json with key of the  "App Engine default service account", 
	- key can be create in GCP Console --> Service Account --> you can fine option at right side to create key againest each service account.
	
